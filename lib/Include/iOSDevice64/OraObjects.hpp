﻿// CodeGear C++Builder
// Copyright (c) 1995, 2018 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'OraObjects.pas' rev: 33.00 (iOS)

#ifndef OraobjectsHPP
#define OraobjectsHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.Classes.hpp>
#include <System.SysUtils.hpp>
#include <Data.SqlTimSt.hpp>
#include <System.Types.hpp>
#include <System.Variants.hpp>
#include <OraCall.hpp>
#include <OraClasses.hpp>
#include <CLRClasses.hpp>
#include <CRTypes.hpp>
#include <MemUtils.hpp>
#include <MemData.hpp>
#include <CRAccess.hpp>

//-- user supplied -----------------------------------------------------------

namespace Oraobjects
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TOraType;
class DELPHICLASS TObjectTypes;
class DELPHICLASS TOraObject;
class DELPHICLASS TOraRef;
class DELPHICLASS TOraXML;
class DELPHICLASS TOraAnyData;
class DELPHICLASS TOraArray;
class DELPHICLASS TOraNestTable;
class DELPHICLASS TRefData;
class DELPHICLASS TTableData;
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TTypeState : unsigned char { tsUnknown, tsDisconnected, tsInit };

class PASCALIMPLEMENTATION TOraType : public Memdata::TObjectType
{
	typedef Memdata::TObjectType inherited;
	
private:
	Oracall::TOCISvcCtx* FOCISvcCtx;
	Oracall::TOCI8API* FOCI8;
	TTypeState FState;
	System::Word FDataSize;
	System::Word FIndicatorSize;
	int FTypeCode;
	bool FFinal;
	bool FAlignNeeded;
	void __fastcall SetOCISvcCtx(Oracall::TOCISvcCtx* Value);
	
protected:
	void *hTDO;
	HIDESBASE void __fastcall ClearAttributes();
	void __fastcall Describe(Oracall::TOCISvcCtx* OCISvcCtx, const System::UnicodeString Name);
	void __fastcall DescribeAttribute(Memdata::TAttribute* Attribute, int TypeCode, bool National, int Len, int Prec, int Scale, const System::UnicodeString FullTypeName, System::Word &IndicatorSize);
	void __fastcall DescribeHeader(Oracall::TOCISvcCtx* OCISvcCtx);
	void __fastcall Validate(Oracall::TOCISvcCtx* OCISvcCtx);
	__property Oracall::TOCISvcCtx* OCISvcCtx = {read=FOCISvcCtx};
	__property Oracall::TOCI8API* OCI8 = {read=FOCI8};
	
public:
	__fastcall TOraType(Oracall::TOCISvcCtx* OCISvcCtx, const System::UnicodeString Name);
	__fastcall virtual ~TOraType();
	void __fastcall Init(const System::UnicodeString Name);
	__property void * TDO = {read=hTDO};
	__property System::Word DataSize = {read=FDataSize, nodefault};
	__property System::Word IndicatorSize = {read=FIndicatorSize, nodefault};
	__property int TypeCode = {read=FTypeCode, nodefault};
	__property bool Final = {read=FFinal, nodefault};
};


class PASCALIMPLEMENTATION TObjectTypes : public System::Classes::TThreadList
{
	typedef System::Classes::TThreadList inherited;
	
public:
	System::UnicodeString __fastcall GetTypeName(Oracall::TOCISvcCtx* OCISvcCtx, void * TDO);
	TOraType* __fastcall FindType(Oracall::TOCISvcCtx* OCISvcCtx, const System::UnicodeString Name);
	void __fastcall ClearTypes(Oracall::TOCISvcCtx* OCISvcCtx, bool DisconnectMode);
public:
	/* TThreadList.Create */ inline __fastcall TObjectTypes() : System::Classes::TThreadList() { }
	/* TThreadList.Destroy */ inline __fastcall virtual ~TObjectTypes() { }
	
};


class PASCALIMPLEMENTATION TOraObject : public Memdata::TDBObject
{
	typedef Memdata::TDBObject inherited;
	
private:
	TOraType* FObjectType;
	Oracall::TOCISvcCtx* FOCISvcCtx;
	Oracall::TOCI8API* FOCI8;
	bool FNativeInstance;
	System::Classes::TList* FObjects;
	bool FAutoCreate;
	bool FTrimFixedChar;
	TOraObject* FOwner;
	int FOwnerIndex;
	void __fastcall SetOCISvcCtx(Oracall::TOCISvcCtx* Value);
	HIDESBASE void __fastcall SetObjectType(TOraType* Value);
	void __fastcall SetInstance(void * Value);
	void * __fastcall GetFInstance();
	void __fastcall SetFInstance(void * Value);
	void * __fastcall GetFIndicator();
	void __fastcall SetFIndicator(void * Value);
	void * __fastcall GetObjectData(TOraType* OwnerType, TOraType* InstanceType, void * Instance);
	Oracall::OCIDate __fastcall GetAsOCIDate(const System::UnicodeString Name);
	void __fastcall SetAsOCIDate(const System::UnicodeString Name, const Oracall::OCIDate &Value);
	Oracall::OCIDate __fastcall ReadAsOCIDate(Memdata::TAttribute* Attr, void * Data);
	void __fastcall WriteAsOCIDate(Memdata::TAttribute* Attr, void * Data, const Oracall::OCIDate &Value);
	Oracall::OCINumber __fastcall GetAsOCINumber(const System::UnicodeString Name);
	void __fastcall SetAsOCINumber(const System::UnicodeString Name, const Oracall::OCINumber &Value);
	Oracall::OCINumber __fastcall ReadAsOCINumber(Memdata::TAttribute* Attr, void * Data);
	void __fastcall WriteAsOCINumber(Memdata::TAttribute* Attr, void * Data, const Oracall::OCINumber &Value);
	void * __fastcall GetAsOCIString(const System::UnicodeString Name);
	void __fastcall SetAsOCIString(const System::UnicodeString Name, void * Value);
	void * __fastcall ReadAsOCIString(Memdata::TAttribute* Attr, void * Data);
	void __fastcall WriteAsOCIString(Memdata::TAttribute* Attr, void * Data, void * Value);
	System::TDateTime __fastcall GetAsDateTime(const System::UnicodeString Name);
	void __fastcall SetAsDateTime(const System::UnicodeString Name, System::TDateTime Value);
	System::TDateTime __fastcall ReadAsDateTime(Memdata::TAttribute* Attr, void * Data, short Ind);
	void __fastcall WriteAsDateTime(Memdata::TAttribute* Attr, void * Data, System::TDateTime Value);
	double __fastcall GetAsFloat(const System::UnicodeString Name);
	void __fastcall SetAsFloat(const System::UnicodeString Name, double Value);
	double __fastcall ReadAsFloat(Memdata::TAttribute* Attr, void * Data, short Ind);
	void __fastcall WriteAsFloat(Memdata::TAttribute* Attr, void * Data, double Value);
	int __fastcall GetAsInteger(const System::UnicodeString Name);
	void __fastcall SetAsInteger(const System::UnicodeString Name, int Value);
	int __fastcall ReadAsInteger(Memdata::TAttribute* Attr, void * Data, short Ind);
	void __fastcall WriteAsInteger(Memdata::TAttribute* Attr, void * Data, int Value);
	__int64 __fastcall GetAsLargeInt(const System::UnicodeString Name);
	void __fastcall SetAsLargeInt(const System::UnicodeString Name, __int64 Value);
	__int64 __fastcall ReadAsLargeInt(Memdata::TAttribute* Attr, void * Data, short Ind);
	void __fastcall WriteAsLargeInt(Memdata::TAttribute* Attr, void * Data, __int64 Value);
	System::UnicodeString __fastcall GetAsString(const System::UnicodeString Name);
	void __fastcall SetAsString(const System::UnicodeString Name, const System::UnicodeString Value);
	System::UnicodeString __fastcall ReadAsString(Memdata::TAttribute* Attr, void * Data, short Ind);
	void __fastcall WriteAsString(Memdata::TAttribute* Attr, void * Data, const System::UnicodeString Value);
	Crtypes::AnsiString __fastcall GetAsAnsiString(const System::UnicodeString Name);
	void __fastcall SetAsAnsiString(const System::UnicodeString Name, const Crtypes::AnsiString &Value);
	Crtypes::AnsiString __fastcall ReadAsAnsiString(Memdata::TAttribute* Attr, void * Data, short Ind);
	void __fastcall WriteAsAnsiString(Memdata::TAttribute* Attr, void * Data, const Crtypes::AnsiString &Value);
	System::UnicodeString __fastcall GetAsWideString(const System::UnicodeString Name);
	void __fastcall SetAsWideString(const System::UnicodeString Name, const System::UnicodeString Value);
	System::UnicodeString __fastcall ReadAsWideString(Memdata::TAttribute* Attr, void * Data, short Ind);
	void __fastcall WriteAsWideString(Memdata::TAttribute* Attr, void * Data, const System::UnicodeString Value);
	TOraObject* __fastcall GetAsObject(const System::UnicodeString Name);
	TOraObject* __fastcall ReadAsObject(Memdata::TAttributeChain* AttrChain);
	TOraRef* __fastcall GetAsRef(const System::UnicodeString Name);
	TOraRef* __fastcall ReadAsRef(Memdata::TAttributeChain* AttrChain);
	TOraArray* __fastcall GetAsArray(const System::UnicodeString Name);
	TOraArray* __fastcall ReadAsArray(Memdata::TAttributeChain* AttrChain);
	Oraclasses::TOraLob* __fastcall GetAsLob(const System::UnicodeString Name);
	Oraclasses::TOraLob* __fastcall ReadAsLob(Memdata::TAttributeChain* AttrChain);
	Oraclasses::TOraTimeStamp* __fastcall GetAsOraTimeStamp(const System::UnicodeString Name);
	Oraclasses::TOraTimeStamp* __fastcall ReadAsOraTimeStamp(Memdata::TAttributeChain* AttrChain);
	Oraclasses::TOraInterval* __fastcall GetAsOraInterval(const System::UnicodeString Name);
	Oraclasses::TOraInterval* __fastcall ReadAsOraInterval(Memdata::TAttributeChain* AttrChain);
	
protected:
	void *FInstancePtr;
	void *FIndicatorPtr;
	bool FCached;
	void __fastcall Check(int Status);
	virtual void __fastcall CheckType();
	void __fastcall CheckSession();
	void __fastcall CheckAlloc(bool RaiseException = true);
	void __fastcall CheckNotCached();
	void __fastcall AllocNewObject();
	void __fastcall FreeObjects();
	Memdata::TAttribute* __fastcall GetAttribute(const System::UnicodeString Name, /* out */ void * &Data, /* out */ short &Ind, /* out */ void * &Indicator, bool AutoAlloc = false, bool MakeNotNull = false)/* overload */;
	Memdata::TAttribute* __fastcall GetAttribute(Memdata::TAttributeChain* AttrChain, /* out */ void * &Data, /* out */ short &Ind, /* out */ void * &Indicator, bool AutoAlloc = false, bool MakeNotNull = false)/* overload */;
	void __fastcall GetAttribute(Memdata::TAttribute* Attr, int Index, /* out */ void * &Data, /* out */ short &Ind, /* out */ void * &Indicator, bool AutoAlloc = false, bool MakeNotNull = false)/* overload */;
	void __fastcall GetAttributeData(Memdata::TAttributeChain* AttrChain, Memdata::TAttribute* Attr, bool &IsNotNull, void * &Data, /* out */ short &Ind, void * &Indicator, bool AutoAlloc, bool MakeNotNull);
	void __fastcall GetAttributeItemData(Memdata::TAttribute* Attr, int Index, void * &Data, /* out */ short &Ind, void * &Indicator, bool AutoAlloc, bool MakeNotNull);
	void __fastcall SetAttributeData(Memdata::TAttribute* Attr, void * Data, short Ind);
	void __fastcall SetAttributeItemData(Memdata::TAttribute* Attr, int Index, void * Data, short Ind);
	virtual bool __fastcall GetAttrIsNull(const System::UnicodeString Name);
	void __fastcall SetAttrIsNull(const System::UnicodeString Name, bool Value);
	bool __fastcall ReadAttrIsNull(Memdata::TAttribute* Attr, short Ind);
	void __fastcall WriteAttrIsNull(Memdata::TAttribute* Attr, void * IndPtr, bool Value);
	virtual void __fastcall GetAttributeValue(const System::UnicodeString Name, /* out */ void * &AttrBuf, /* out */ bool &IsBlank, /* out */ bool &NativeBuffer)/* overload */;
	HIDESBASE void __fastcall GetAttributeValue(Memdata::TAttributeChain* AttrChain, /* out */ void * &AttrBuf, /* out */ bool &IsBlank, /* out */ bool &NativeBuffer)/* overload */;
	virtual void __fastcall SetAttributeValue(const System::UnicodeString Name, void * Source)/* overload */;
	HIDESBASE void __fastcall SetAttributeValue(Memdata::TAttributeChain* AttrChain, void * Source)/* overload */;
	Memdata::TSharedObject* __fastcall GetComplexAttribute(const System::UnicodeString Name)/* overload */;
	Memdata::TSharedObject* __fastcall GetComplexAttribute(Memdata::TAttributeChain* AttrChain)/* overload */;
	Memdata::TSharedObject* __fastcall FindComplexAttribute(Memdata::TAttributeChain* AttrChain);
	Memdata::TSharedObject* __fastcall GetAttributeObject(int Index);
	void __fastcall SetAttributeObject(int Index, Memdata::TSharedObject* Obj);
	virtual bool __fastcall GetIsNull();
	virtual void __fastcall SetIsNull(bool Value);
	short __fastcall ReadIndicator();
	void __fastcall WriteIndicator(short Value);
	virtual void * __fastcall GetIndicatorPtr();
	virtual void __fastcall AssignInstance(void * Value);
	Memdata::TSharedObject* __fastcall GetChildObject(Memdata::TAttribute* Attr, int ArrayIndex = 0xffffffff);
	Memdata::TSharedObject* __fastcall ReadChildObject(TOraObject* Parent, Memdata::TAttribute* Attr, int ArrayIndex, void * Value, short Ind, void * IndPtr);
	int __fastcall GetChildIndex(TOraObject* Child);
	int __fastcall GetOwnerIndex();
	virtual void __fastcall ChildAlloc(TOraObject* Child);
	virtual void * __fastcall GetOCIRef();
	__property void * FInstance = {read=GetFInstance, write=SetFInstance};
	__property void * FIndicator = {read=GetFIndicator, write=SetFIndicator};
	__property Oracall::TOCI8API* OCI8 = {read=FOCI8};
	
public:
	__fastcall TOraObject(TOraType* AObjectType);
	__fastcall virtual ~TOraObject();
	virtual void __fastcall AllocObject()/* overload */;
	void __fastcall AllocObject(Oracall::TOCISvcCtx* OCISvcCtx)/* overload */;
	virtual void __fastcall AllocObject(const System::UnicodeString TypeName)/* overload */;
	virtual void __fastcall AllocObject(Oracall::TOCISvcCtx* OCISvcCtx, const System::UnicodeString TypeName)/* overload */;
	virtual void __fastcall FreeObject(bool FreeChild = true);
	virtual void __fastcall Disconnect();
	void __fastcall CreateObject _DEPRECATED_ATTRIBUTE0 (Oracall::TOCISvcCtx* OCISvcCtx, const System::UnicodeString TypeName);
	void __fastcall CacheObject();
	void __fastcall Lock();
	void __fastcall Flush();
	void __fastcall Refresh();
	void __fastcall MarkDelete();
	void __fastcall MarkUpdate();
	void __fastcall Unmark();
	void __fastcall WriteLobs();
	void __fastcall ReadLobs();
	void __fastcall ResetInstance(void * NewInstance, void * NewIndicator);
	bool __fastcall Exists();
	bool __fastcall IsLocked();
	bool __fastcall IsDirty();
	virtual void __fastcall Assign(TOraObject* Source);
	void __fastcall ObjectAssign(void * DataSrc, void * DataDest, TOraType* OType);
	void __fastcall NestTableAssign(void * Source, void * Dest, TOraType* OType);
	__property bool AttrIsNull[const System::UnicodeString Name] = {read=GetAttrIsNull, write=SetAttrIsNull};
	__property Oracall::OCIDate AttrAsOCIDate[const System::UnicodeString Name] = {read=GetAsOCIDate, write=SetAsOCIDate};
	__property Oracall::OCINumber AttrAsOCINumber[const System::UnicodeString Name] = {read=GetAsOCINumber, write=SetAsOCINumber};
	__property void * AttrAsOCIString[const System::UnicodeString Name] = {read=GetAsOCIString, write=SetAsOCIString};
	__property System::TDateTime AttrAsDateTime[const System::UnicodeString Name] = {read=GetAsDateTime, write=SetAsDateTime};
	__property double AttrAsFloat[const System::UnicodeString Name] = {read=GetAsFloat, write=SetAsFloat};
	__property int AttrAsInteger[const System::UnicodeString Name] = {read=GetAsInteger, write=SetAsInteger};
	__property __int64 AttrAsLargeInt[const System::UnicodeString Name] = {read=GetAsLargeInt, write=SetAsLargeInt};
	__property System::UnicodeString AttrAsString[const System::UnicodeString Name] = {read=GetAsString, write=SetAsString};
	
protected:
	__property Crtypes::AnsiString AttrAsAnsiString[const System::UnicodeString Name] = {read=GetAsAnsiString, write=SetAsAnsiString};
	
public:
	__property System::UnicodeString AttrAsWideString[const System::UnicodeString Name] = {read=GetAsWideString, write=SetAsWideString};
	__property TOraObject* AttrAsObject[const System::UnicodeString Name] = {read=GetAsObject};
	__property TOraRef* AttrAsRef[const System::UnicodeString Name] = {read=GetAsRef};
	__property TOraArray* AttrAsArray[const System::UnicodeString Name] = {read=GetAsArray};
	__property Oraclasses::TOraLob* AttrAsLob[const System::UnicodeString Name] = {read=GetAsLob};
	__property Oraclasses::TOraTimeStamp* AttrAsTimeStamp[const System::UnicodeString Name] = {read=GetAsOraTimeStamp};
	__property Oraclasses::TOraInterval* AttrAsInterval[const System::UnicodeString Name] = {read=GetAsOraInterval};
	__property TOraType* ObjectType = {read=FObjectType, write=SetObjectType};
	__property Oracall::TOCISvcCtx* OCISvcCtx = {read=FOCISvcCtx, write=SetOCISvcCtx};
	__property void * Instance = {read=GetFInstance, write=SetInstance};
	__property void * Indicator = {read=GetFIndicator, write=SetFIndicator};
	__property void * InstancePtr = {read=FInstancePtr};
	__property void * IndicatorPtr = {read=GetIndicatorPtr};
	__property bool IsNull = {read=GetIsNull, write=SetIsNull, nodefault};
	__property bool AutoCreate = {read=FAutoCreate, write=FAutoCreate, nodefault};
	__property bool NativeInstance = {read=FNativeInstance, write=FNativeInstance, nodefault};
};


class PASCALIMPLEMENTATION TOraRef : public TOraObject
{
	typedef TOraObject inherited;
	
private:
	bool InvalidObject;
	void * __fastcall GethOCIRef();
	void __fastcall SethOCIRef(void * Value);
	void __fastcall SetOCIRef(void * Value);
	Oracall::ppOCIRef __fastcall GetOCIRefPtr();
	
protected:
	void * *FpOCIRef;
	virtual bool __fastcall GetIsNull();
	virtual void __fastcall SetIsNull(bool Value);
	System::UnicodeString __fastcall GetAsHex();
	__property void * FOCIRef = {read=GethOCIRef, write=SethOCIRef};
	
public:
	__fastcall TOraRef(TOraType* ObjectType);
	__fastcall virtual ~TOraRef();
	void __fastcall Pin();
	void __fastcall Unpin();
	bool __fastcall RefIsNull();
	void __fastcall Clear();
	virtual void __fastcall Assign(TOraObject* Source);
	__property void * OCIRef = {read=GethOCIRef, write=SetOCIRef};
	__property Oracall::ppOCIRef OCIRefPtr = {read=GetOCIRefPtr};
	__property System::UnicodeString AsHex = {read=GetAsHex};
};


class PASCALIMPLEMENTATION TOraXML : public TOraObject
{
	typedef TOraObject inherited;
	
private:
	void *phOCIDescriptor;
	System::TArray__1<System::Byte> CacheValue;
	void *LocalIndicator;
	void *LocalIndicatorPtr;
	void __fastcall CreateXMLStream();
	void __fastcall FreeXMLStream();
	HIDESBASE System::UnicodeString __fastcall GetAsString();
	HIDESBASE void __fastcall SetAsString(const System::UnicodeString Value);
	HIDESBASE System::UnicodeString __fastcall GetAsWideString();
	HIDESBASE void __fastcall SetAsWideString(const System::UnicodeString Value);
	bool __fastcall ObjectIsNull();
	void __fastcall StartRead();
	unsigned __fastcall Read(unsigned Count, void * Dest);
	bool __fastcall AccessBySQL();
	void __fastcall WriteXMLFromStringBySQL(void * Buf, int BufSize);
	void __fastcall WriteXMLFromClobBySQL(Oraclasses::TOraLob* OraLob);
	
protected:
	virtual void __fastcall CheckType();
	virtual bool __fastcall GetIsNull();
	virtual void __fastcall SetIsNull(bool Value);
	virtual void * __fastcall GetIndicatorPtr();
	virtual void __fastcall AssignInstance(void * Value);
	void __fastcall ReadLocalIndicator();
	
public:
	__fastcall TOraXML(Oracall::TOCISvcCtx* AOCISvcCtx, TOraType* AObjectType);
	__fastcall virtual ~TOraXML();
	virtual void __fastcall AllocObject()/* overload */;
	virtual void __fastcall AllocObject(const System::UnicodeString TypeName)/* overload */;
	virtual void __fastcall AllocObject(Oracall::TOCISvcCtx* AOCISvcCtx, const System::UnicodeString TypeName)/* overload */;
	HIDESBASE void __fastcall AllocObject(Oracall::TOCISvcCtx* AOCISvcCtx, Oraclasses::TOraLob* AOraLob)/* overload */;
	virtual void __fastcall FreeObject(bool FreeChild = true);
	virtual void __fastcall Assign(TOraObject* Source);
	void __fastcall ReadXML();
	void __fastcall LoadFromStream(System::Classes::TStream* Stream);
	void __fastcall SaveToStream(System::Classes::TStream* Stream);
	void __fastcall Extract(TOraXML* RetDoc, System::UnicodeString XPathExpr, System::UnicodeString NSmap = System::UnicodeString());
	HIDESBASE bool __fastcall Exists(const System::UnicodeString XPathExpr, const System::UnicodeString NSmap = System::UnicodeString());
	void __fastcall Transform(TOraXML* XSLDoc, TOraXML* RetDoc);
	void __fastcall GetSchema(TOraXML* SchemaDoc, System::UnicodeString &SchemaURL, System::UnicodeString &RootElem);
	bool __fastcall Validate(const System::UnicodeString SchemaURL);
	bool __fastcall IsSchemaBased();
	__property System::UnicodeString AsString = {read=GetAsString, write=SetAsString};
	__property System::UnicodeString AsWideString = {read=GetAsWideString, write=SetAsWideString};
	/* Hoisted overloads: */
	
public:
	inline void __fastcall  AllocObject(Oracall::TOCISvcCtx* OCISvcCtx){ TOraObject::AllocObject(OCISvcCtx); }
	
};


class PASCALIMPLEMENTATION TOraAnyData : public TOraObject
{
	typedef TOraObject inherited;
	
private:
	Memdata::TAttribute* FValueAttribute;
	System::Word FValueTypeCode;
	void *FValueData;
	Memdata::TSharedObject* FValueObject;
	void *FValueIndicator;
	void *FValueIndicatorPtr;
	void *hValueTDO;
	System::Word __fastcall GetValueType();
	void __fastcall SetValueType(System::Word Value);
	HIDESBASE System::TDateTime __fastcall GetAsDateTime();
	HIDESBASE void __fastcall SetAsDateTime(System::TDateTime Value);
	HIDESBASE double __fastcall GetAsFloat();
	HIDESBASE void __fastcall SetAsFloat(double Value);
	HIDESBASE int __fastcall GetAsInteger();
	HIDESBASE void __fastcall SetAsInteger(int Value);
	HIDESBASE __int64 __fastcall GetAsLargeInt();
	HIDESBASE void __fastcall SetAsLargeInt(__int64 Value);
	HIDESBASE System::UnicodeString __fastcall GetAsString();
	HIDESBASE void __fastcall SetAsString(const System::UnicodeString Value);
	HIDESBASE System::UnicodeString __fastcall GetAsWideString();
	HIDESBASE void __fastcall SetAsWideString(const System::UnicodeString Value);
	HIDESBASE TOraObject* __fastcall GetAsObject();
	HIDESBASE TOraRef* __fastcall GetAsRef();
	HIDESBASE TOraArray* __fastcall GetAsArray();
	HIDESBASE Oraclasses::TOraLob* __fastcall GetAsLob();
	HIDESBASE Oraclasses::TOraTimeStamp* __fastcall GetAsOraTimeStamp();
	HIDESBASE Oraclasses::TOraInterval* __fastcall GetAsOraInterval();
	System::Variant __fastcall GetAsVariant();
	void __fastcall SetAsVariant(const System::Variant &Value);
	
protected:
	virtual void __fastcall CheckType();
	virtual bool __fastcall GetIsNull();
	virtual void __fastcall SetIsNull(bool Value);
	virtual void * __fastcall GetIndicatorPtr();
	void __fastcall ClearValue();
	System::Word __fastcall GetTypeCode(System::Word DataType);
	void __fastcall ReadAttribute();
	void __fastcall AllocAttribute(System::Word DataType, /* out */ System::Word &TypeCode)/* overload */;
	void __fastcall AllocAttribute(System::Word DataType, int Len, /* out */ System::Word &TypeCode)/* overload */;
	void __fastcall AllocAttribute(System::Word DataType, int Prec, int Scale, /* out */ System::Word &TypeCode)/* overload */;
	void __fastcall AllocAttribute(System::Word DataType, const System::UnicodeString FullTypeName, /* out */ System::Word &TypeCode)/* overload */;
	void __fastcall AllocAttribute(System::Word DataType, int Len, int Prec, int Scale, const System::UnicodeString FullTypeName, /* out */ System::Word &TypeCode)/* overload */;
	void __fastcall WriteData(Memdata::TAttribute* Attribute, System::Word TypeCode, void * Data)/* overload */;
	void __fastcall WriteData(Memdata::TAttribute* Attribute, System::Word TypeCode, void * TDO, void * Data, void * IndPtr)/* overload */;
	
public:
	__fastcall TOraAnyData(Oracall::TOCISvcCtx* AOCISvcCtx, TOraType* AObjectType);
	__fastcall virtual ~TOraAnyData();
	virtual void __fastcall AllocObject()/* overload */;
	virtual void __fastcall Assign(TOraObject* Source);
	void __fastcall WriteAnyData();
	__property System::Word ValueType = {read=GetValueType, write=SetValueType, nodefault};
	__property System::TDateTime AsDateTime = {read=GetAsDateTime, write=SetAsDateTime};
	__property double AsFloat = {read=GetAsFloat, write=SetAsFloat};
	__property int AsInteger = {read=GetAsInteger, write=SetAsInteger, nodefault};
	__property __int64 AsLargeInt = {read=GetAsLargeInt, write=SetAsLargeInt};
	__property System::UnicodeString AsString = {read=GetAsString, write=SetAsString};
	__property System::UnicodeString AsWideString = {read=GetAsWideString, write=SetAsWideString};
	__property TOraObject* AsObject = {read=GetAsObject};
	__property TOraRef* AsRef = {read=GetAsRef};
	__property TOraArray* AsArray = {read=GetAsArray};
	__property Oraclasses::TOraLob* AsLob = {read=GetAsLob};
	__property Oraclasses::TOraTimeStamp* AsTimeStamp = {read=GetAsOraTimeStamp};
	__property Oraclasses::TOraInterval* AsInterval = {read=GetAsOraInterval};
	__property System::Variant AsVariant = {read=GetAsVariant, write=SetAsVariant};
	/* Hoisted overloads: */
	
public:
	inline void __fastcall  AllocObject(Oracall::TOCISvcCtx* OCISvcCtx){ TOraObject::AllocObject(OCISvcCtx); }
	inline void __fastcall  AllocObject(const System::UnicodeString TypeName){ TOraObject::AllocObject(TypeName); }
	inline void __fastcall  AllocObject(Oracall::TOCISvcCtx* OCISvcCtx, const System::UnicodeString TypeName){ TOraObject::AllocObject(OCISvcCtx, TypeName); }
	
};


class PASCALIMPLEMENTATION TOraArray : public TOraObject
{
	typedef TOraObject inherited;
	
private:
	void __fastcall SetSize(int Value);
	int __fastcall GetMaxSize();
	System::Word __fastcall GetItemType();
	System::Word __fastcall GetItemSubType();
	bool __fastcall GetItemExists(int Index);
	bool __fastcall GetItemIsNull(int Index);
	void __fastcall SetItemIsNull(int Index, bool Value);
	void * __fastcall GetItemAsOCIString(int Index);
	void __fastcall SetItemAsOCIString(int Index, void * Value);
	System::TDateTime __fastcall GetItemAsDateTime(int Index);
	void __fastcall SetItemAsDateTime(int Index, System::TDateTime Value);
	double __fastcall GetItemAsFloat(int Index);
	void __fastcall SetItemAsFloat(int Index, double Value);
	int __fastcall GetItemAsInteger(int Index);
	void __fastcall SetItemAsInteger(int Index, int Value);
	__int64 __fastcall GetItemAsLargeInt(int Index);
	void __fastcall SetItemAsLargeInt(int Index, __int64 Value);
	System::UnicodeString __fastcall GetItemAsString(int Index);
	void __fastcall SetItemAsString(int Index, const System::UnicodeString Value);
	Crtypes::AnsiString __fastcall GetItemAsAnsiString(int Index);
	void __fastcall SetItemAsAnsiString(int Index, const Crtypes::AnsiString &Value);
	System::UnicodeString __fastcall GetItemAsWideString(int Index);
	void __fastcall SetItemAsWideString(int Index, const System::UnicodeString Value);
	TOraObject* __fastcall GetItemAsObject(int Index);
	void __fastcall SetItemAsObject(int Index, TOraObject* Value);
	TOraRef* __fastcall GetItemAsRef(int Index);
	void __fastcall SetItemAsRef(int Index, TOraRef* Value);
	Oraclasses::TOraLob* __fastcall GetItemAsLob(int Index);
	void __fastcall SetItemAsLob(int Index, Oraclasses::TOraLob* Value);
	Oraclasses::TOraTimeStamp* __fastcall GetItemAsOraTimeStamp(int Index);
	void __fastcall SetItemAsOraTimeStamp(int Index, Oraclasses::TOraTimeStamp* Value);
	Oraclasses::TOraInterval* __fastcall GetItemAsOraInterval(int Index);
	void __fastcall SetItemAsOraInterval(int Index, Oraclasses::TOraInterval* Value);
	
protected:
	virtual void __fastcall CheckType();
	void __fastcall CheckIndex(int Index);
	virtual void __fastcall ChildAlloc(TOraObject* Child);
	virtual int __fastcall GetSize();
	
public:
	void __fastcall Clear();
	int __fastcall AppendItem();
	void __fastcall InsertItem(int Index);
	virtual void __fastcall Assign(TOraObject* Source);
	__property int Size = {read=GetSize, write=SetSize, nodefault};
	__property int MaxSize = {read=GetMaxSize, nodefault};
	__property System::Word ItemType = {read=GetItemType, nodefault};
	__property System::Word ItemSubType = {read=GetItemSubType, nodefault};
	__property bool ItemExists[int Index] = {read=GetItemExists};
	__property bool ItemIsNull[int Index] = {read=GetItemIsNull, write=SetItemIsNull};
	__property void * ItemAsOCIString[int Index] = {read=GetItemAsOCIString, write=SetItemAsOCIString};
	__property System::TDateTime ItemAsDateTime[int Index] = {read=GetItemAsDateTime, write=SetItemAsDateTime};
	__property double ItemAsFloat[int Index] = {read=GetItemAsFloat, write=SetItemAsFloat};
	__property int ItemAsInteger[int Index] = {read=GetItemAsInteger, write=SetItemAsInteger};
	__property __int64 ItemAsLargeInt[int Index] = {read=GetItemAsLargeInt, write=SetItemAsLargeInt};
	__property System::UnicodeString ItemAsString[int Index] = {read=GetItemAsString, write=SetItemAsString};
	__property Crtypes::AnsiString ItemAsAnsiString[int Index] = {read=GetItemAsAnsiString, write=SetItemAsAnsiString};
	__property System::UnicodeString ItemAsWideString[int Index] = {read=GetItemAsWideString, write=SetItemAsWideString};
	__property TOraObject* ItemAsObject[int Index] = {read=GetItemAsObject, write=SetItemAsObject};
	__property TOraRef* ItemAsRef[int Index] = {read=GetItemAsRef, write=SetItemAsRef};
	__property Oraclasses::TOraLob* ItemAsLob[int Index] = {read=GetItemAsLob, write=SetItemAsLob};
	__property Oraclasses::TOraTimeStamp* ItemAsTimeStamp[int Index] = {read=GetItemAsOraTimeStamp, write=SetItemAsOraTimeStamp};
	__property Oraclasses::TOraInterval* ItemAsInterval[int Index] = {read=GetItemAsOraInterval, write=SetItemAsOraInterval};
public:
	/* TOraObject.Create */ inline __fastcall TOraArray(TOraType* AObjectType) : TOraObject(AObjectType) { }
	/* TOraObject.Destroy */ inline __fastcall virtual ~TOraArray() { }
	
};


class PASCALIMPLEMENTATION TOraNestTable : public TOraArray
{
	typedef TOraArray inherited;
	
protected:
	virtual void __fastcall CheckType();
	virtual int __fastcall GetSize();
	
public:
	virtual void __fastcall Assign(TOraObject* Source);
	void __fastcall DeleteItem(int Index);
	__property int Size = {read=GetSize, nodefault};
public:
	/* TOraObject.Create */ inline __fastcall TOraNestTable(TOraType* AObjectType) : TOraArray(AObjectType) { }
	/* TOraObject.Destroy */ inline __fastcall virtual ~TOraNestTable() { }
	
};


class PASCALIMPLEMENTATION TRefData : public Memdata::TData
{
	typedef Memdata::TData inherited;
	
private:
	TOraRef* FRef;
	bool FIncludeObjectField;
	void __fastcall SetRef(TOraRef* Value);
	
protected:
	virtual void __fastcall InternalPrepare();
	virtual void __fastcall InternalOpen(bool DisableInitFields = false);
	virtual void __fastcall CreateFieldDescs();
	
public:
	__fastcall TRefData();
	__fastcall virtual ~TRefData();
	virtual void __fastcall Reopen();
	virtual void __fastcall GetRecord(void * RecBuf);
	virtual void __fastcall GetNextRecord(void * RecBuf);
	virtual void __fastcall GetPriorRecord(void * RecBuf);
	virtual void __fastcall PutRecord(void * RecBuf);
	virtual void __fastcall AppendRecord(void * RecBuf);
	virtual void __fastcall InsertRecord(void * RecBuf);
	virtual void __fastcall UpdateRecord(void * RecBuf);
	virtual void __fastcall DeleteRecord();
	virtual void __fastcall CreateComplexField(void * RecBuf, Memdata::TFieldDesc* Field);
	virtual void __fastcall FreeComplexField(void * RecBuf, Memdata::TFieldDesc* Field);
	virtual void __fastcall CopyComplexField(void * Source, void * Dest, Memdata::TFieldDesc* Field);
	virtual void __fastcall GetBookmark(Memdata::PRecBookmark Bookmark);
	virtual void __fastcall SetToBookmark(Memdata::PRecBookmark Bookmark);
	__property TOraRef* Ref = {read=FRef, write=SetRef};
	__property bool IncludeObjectField = {read=FIncludeObjectField, write=FIncludeObjectField, nodefault};
};


class PASCALIMPLEMENTATION TTableData : public Memdata::TMemData
{
	typedef Memdata::TMemData inherited;
	
private:
	TOraNestTable* FTable;
	System::Word FIndexOfs;
	bool FIncludeObjectField;
	void __fastcall SetTable(TOraNestTable* Value);
	
protected:
	int LastIndex;
	void __fastcall Check(int Status);
	virtual void __fastcall InternalPrepare();
	virtual void __fastcall InternalOpen(bool DisableInitFields = false);
	virtual void __fastcall CreateFieldDescs();
	virtual int __fastcall GetIndicatorSize();
	virtual void __fastcall InternalAppend(void * RecBuf);
	virtual void __fastcall InternalDelete();
	virtual void __fastcall InternalUpdate(void * RecBuf);
	virtual int __fastcall GetRecordCount();
	
public:
	__fastcall TTableData();
	__fastcall virtual ~TTableData();
	virtual bool __fastcall Fetch(bool FetchBack = false);
	virtual void __fastcall Reopen();
	virtual void __fastcall InitFields();
	__classmethod virtual bool __fastcall IsBlobDataType(System::Word DataType);
	virtual void __fastcall GetRecord(void * RecBuf);
	virtual void __fastcall GetNextRecord(void * RecBuf);
	virtual void __fastcall PutRecord(void * RecBuf);
	virtual void __fastcall CancelRecord(void * RecBuf);
	void __fastcall FetchComplexFields(void * RecBuf, bool WithBlob);
	void __fastcall FetchComplexField(void * RecBuf, Memdata::TFieldDesc* Field);
	virtual void __fastcall CreateComplexField(void * RecBuf, Memdata::TFieldDesc* Field);
	virtual void __fastcall FreeComplexField(void * RecBuf, Memdata::TFieldDesc* Field);
	virtual void __fastcall CopyComplexField(void * Source, void * Dest, Memdata::TFieldDesc* Field);
	__property TOraNestTable* Table = {read=FTable, write=SetTable};
	__property bool IncludeObjectField = {read=FIncludeObjectField, write=FIncludeObjectField, nodefault};
};


//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE TObjectTypes* ObjectTypes;
extern DELPHI_PACKAGE int __fastcall GetObjectCacheMaxSize(Oracall::TOCISvcCtx* OCISvcCtx);
extern DELPHI_PACKAGE void __fastcall SetObjectCacheMaxSize(Oracall::TOCISvcCtx* OCISvcCtx, int Value);
extern DELPHI_PACKAGE int __fastcall GetObjectCacheOptSize(Oracall::TOCISvcCtx* OCISvcCtx);
extern DELPHI_PACKAGE void __fastcall SetObjectCacheOptSize(Oracall::TOCISvcCtx* OCISvcCtx, int Value);
}	/* namespace Oraobjects */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_ORAOBJECTS)
using namespace Oraobjects;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// OraobjectsHPP
