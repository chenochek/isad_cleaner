﻿// CodeGear C++Builder
// Copyright (c) 1995, 2018 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'OraServices.pas' rev: 33.00 (iOS)

#ifndef OraservicesHPP
#define OraservicesHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.Classes.hpp>
#include <System.SysUtils.hpp>
#include <System.Variants.hpp>
#include <Data.DB.hpp>
#include <CLRClasses.hpp>
#include <CRTypes.hpp>
#include <CRXml.hpp>
#include <CRAccess.hpp>
#include <MemData.hpp>
#include <MemDS.hpp>
#include <DBAccess.hpp>
#include <DADump.hpp>
#include <DASQLGenerator.hpp>
#include <OraCall.hpp>
#include <OraSQLGenerator.hpp>

//-- user supplied -----------------------------------------------------------

namespace Oraservices
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TCustomOraFieldTypeMap;
class DELPHICLASS TCustomOraDataSetUpdater;
class DELPHICLASS TCustomOraDataSetService;
class DELPHICLASS TCustomDBOraSQLGenerator;
class DELPHICLASS TCustomOraDumpProcessor;
//-- type declarations -------------------------------------------------------
class PASCALIMPLEMENTATION TCustomOraFieldTypeMap : public Dbaccess::TDAFieldTypeMap
{
	typedef Dbaccess::TDAFieldTypeMap inherited;
	
public:
	__classmethod virtual int __fastcall GetDataType(Data::Db::TFieldType FieldType, System::Word SubDataType = (System::Word)(0x0));
	__classmethod virtual Data::Db::TFieldType __fastcall GetFieldType(System::Word DataType);
public:
	/* TObject.Create */ inline __fastcall TCustomOraFieldTypeMap() : Dbaccess::TDAFieldTypeMap() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TCustomOraFieldTypeMap() { }
	
};


class PASCALIMPLEMENTATION TCustomOraDataSetUpdater : public Dbaccess::TDADataSetUpdater
{
	typedef Dbaccess::TDADataSetUpdater inherited;
	
private:
	TCustomOraDataSetService* __fastcall GetDataSetService();
	
protected:
	void __fastcall GetSequenceNextVal();
	virtual bool __fastcall GetIdentityFieldValue(System::Variant &Value);
	virtual bool __fastcall UseParamType(Dbaccess::TDAParam* Param);
	virtual void __fastcall SetDefaultParamType(Dbaccess::TDAParam* Param);
	virtual bool __fastcall IsNeedInsertPreconnect();
	virtual bool __fastcall IsNeedEditPreconnect();
	virtual bool __fastcall IsPreconnected();
	virtual System::UnicodeString __fastcall PrepareBatch(const System::UnicodeString SQL);
	virtual void __fastcall CheckUpdateQuery(const Dbaccess::TStatementType StatementType);
	virtual void __fastcall SetUpdateQueryOptions(const Dbaccess::TStatementType StatementType);
	virtual void __fastcall UpdateExecute(const Dbaccess::TStatementTypes StatementTypes);
	virtual void __fastcall PrepareAppend();
	virtual bool __fastcall PerformAppend();
	__property System::Classes::TComponent* UpdateQuery = {read=FUpdateQuery};
	__property TCustomOraDataSetService* DataSetService = {read=GetDataSetService};
	
public:
	virtual bool __fastcall PerformSQL(const System::UnicodeString SQL, const Dbaccess::TStatementTypes StatementTypes);
	virtual bool __fastcall GetDefaultExpressionValue(System::UnicodeString DefExpr, /* out */ System::Variant &Value);
public:
	/* TDADataSetUpdater.Create */ inline __fastcall virtual TCustomOraDataSetUpdater(Memds::TDataSetService* AOwner) : Dbaccess::TDADataSetUpdater(AOwner) { }
	/* TDADataSetUpdater.Destroy */ inline __fastcall virtual ~TCustomOraDataSetUpdater() { }
	
};


class PASCALIMPLEMENTATION TCustomOraDataSetService : public Dbaccess::TDADataSetService
{
	typedef Dbaccess::TDADataSetService inherited;
	
private:
	TCustomOraDataSetUpdater* __fastcall GetUpdater();
	
protected:
	bool FScrollableCursor;
	virtual void __fastcall CreateDataSetUpdater();
	virtual void __fastcall CreateSQLGenerator();
	virtual Dbaccess::TFieldArray __fastcall DetectHiddenFields();
	virtual bool __fastcall DetectCanModify();
	virtual int __fastcall GetRecCount();
	virtual bool __fastcall CompatibilityMode();
	virtual Data::Db::TFieldClass __fastcall GetFieldClass(Data::Db::TFieldType FieldType, System::Word DataType);
	virtual System::UnicodeString __fastcall GetCurrentSchema();
	virtual void __fastcall WriteFieldXMLDataType(Data::Db::TField* Field, Memdata::TFieldDesc* FieldDesc, const System::UnicodeString FieldAlias, Crxml::XmlTextWriter* XMLWriter);
	virtual System::UnicodeString __fastcall GetFieldXMLValue(Data::Db::TField* Field, Memdata::TFieldDesc* FieldDesc);
	__property TCustomOraDataSetUpdater* Updater = {read=GetUpdater};
	
public:
	virtual bool __fastcall SetProp(int Prop, const System::Variant &Value);
	virtual bool __fastcall OpenNext();
	virtual bool __fastcall NeedParamValuesOnPrepare();
	virtual void __fastcall ResetTableKeyFields();
public:
	/* TDADataSetService.Create */ inline __fastcall virtual TCustomOraDataSetService(Memds::TMemDataSet* AOwner) : Dbaccess::TDADataSetService(AOwner) { }
	/* TDADataSetService.Destroy */ inline __fastcall virtual ~TCustomOraDataSetService() { }
	
};


class PASCALIMPLEMENTATION TCustomDBOraSQLGenerator : public Orasqlgenerator::TCustomOraSQLGenerator
{
	typedef Orasqlgenerator::TCustomOraSQLGenerator inherited;
	
public:
	virtual System::UnicodeString __fastcall GenerateSQL(Craccess::TDAParamsInfo* ParamsInfo, const Dasqlgenerator::_TStatementType StatementType, const bool ModifiedFieldsOnly, const int Index = 0xffffffff);
public:
	/* TDASQLGenerator.Create */ inline __fastcall virtual TCustomDBOraSQLGenerator(Dasqlgenerator::TSQLGeneratorServiceClass ServiceClass) : Orasqlgenerator::TCustomOraSQLGenerator(ServiceClass) { }
	/* TDASQLGenerator.Destroy */ inline __fastcall virtual ~TCustomDBOraSQLGenerator() { }
	
};


class PASCALIMPLEMENTATION TCustomOraDumpProcessor : public Dadump::TDADumpProcessor
{
	typedef Dadump::TDADumpProcessor inherited;
	
protected:
	virtual Dbaccess::TCustomDADataSet* __fastcall CreateQuery();
public:
	/* TDADumpProcessor.Create */ inline __fastcall virtual TCustomOraDumpProcessor(Dadump::TDADump* Owner) : Dadump::TDADumpProcessor(Owner) { }
	/* TDADumpProcessor.Destroy */ inline __fastcall virtual ~TCustomOraDumpProcessor() { }
	
};


//-- var, const, procedure ---------------------------------------------------
static constexpr System::Int8 prSequenceMode = System::Int8(0x65);
}	/* namespace Oraservices */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_ORASERVICES)
using namespace Oraservices;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// OraservicesHPP
