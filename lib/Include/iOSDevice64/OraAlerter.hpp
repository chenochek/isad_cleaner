﻿// CodeGear C++Builder
// Copyright (c) 1995, 2018 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'OraAlerter.pas' rev: 33.00 (iOS)

#ifndef OraalerterHPP
#define OraalerterHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.Classes.hpp>
#include <System.SysUtils.hpp>
#include <System.SyncObjs.hpp>
#include <Data.DB.hpp>
#include <System.Variants.hpp>
#include <CRTypes.hpp>
#include <MemData.hpp>
#include <CRAccess.hpp>
#include <DBAccess.hpp>
#include <DAAlerter.hpp>
#include <Ora.hpp>
#include <OraClasses.hpp>

//-- user supplied -----------------------------------------------------------

namespace Oraalerter
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TOraAlerter;
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TEventType : unsigned char { etAlert, etPipe };

typedef void __fastcall (__closure *TOnEventEvent)(System::TObject* Sender, System::UnicodeString Event, System::UnicodeString Message);

typedef void __fastcall (__closure *TOnTimeOutEvent)(System::TObject* Sender, bool &Continue);

class PASCALIMPLEMENTATION TOraAlerter : public Daalerter::TDAAlerter
{
	typedef Daalerter::TDAAlerter inherited;
	
private:
	int FTimeOut;
	int FInterval;
	TEventType FEventType;
	bool FSelfEvents;
	TOnEventEvent FOnEvent;
	TOnTimeOutEvent FOnTimeOut;
	Ora::TOraSession* __fastcall GetSession();
	void __fastcall SetSession(Ora::TOraSession* Value);
	void __fastcall SetTimeOut(int Value);
	void __fastcall SetInterval(int Value);
	void __fastcall SetEventType(TEventType Value);
	void __fastcall SetSelfEvents(bool Value);
	
protected:
	virtual Craccess::TCRAlerterClass __fastcall GetInternalAlerterClass();
	virtual void __fastcall SetInternalAlerter(Craccess::TCRAlerter* Value);
	virtual void __fastcall DoOnEvent(const System::UnicodeString EventName, const System::UnicodeString Message);
	void __fastcall DoOnTimeOut(bool &Continue);
	__property bool SelfEvents = {read=FSelfEvents, write=SetSelfEvents, nodefault};
	
public:
	__fastcall virtual TOraAlerter(System::Classes::TComponent* Owner);
	__fastcall virtual ~TOraAlerter();
	void __fastcall PackMessage(const System::Variant &Item);
	System::Variant __fastcall UnpackMessage()/* overload */;
	System::Variant __fastcall UnpackMessage(System::Variant &Item)/* overload */;
	Oraclasses::TMessageType __fastcall NextItemType();
	void __fastcall SendPipeMessage(System::UnicodeString Name = System::UnicodeString());
	void __fastcall PurgePipe();
	void __fastcall PutMessage(const System::Variant &Item);
	System::Variant __fastcall GetMessage()/* overload */;
	System::Variant __fastcall GetMessage(System::Variant &Item)/* overload */;
	Oraclasses::TMessageType __fastcall NextMessageType();
	void __fastcall SendMessage(System::UnicodeString Name = System::UnicodeString());
	
__published:
	__property Events = {default=0};
	__property AutoCommit = {default=1};
	__property Active = {default=0};
	__property AutoRegister = {default=0};
	__property OnError;
	__property Ora::TOraSession* Session = {read=GetSession, write=SetSession};
	__property int TimeOut = {read=FTimeOut, write=SetTimeOut, default=0};
	__property int Interval = {read=FInterval, write=SetInterval, default=0};
	__property TEventType EventType = {read=FEventType, write=SetEventType, nodefault};
	__property TOnEventEvent OnEvent = {read=FOnEvent, write=FOnEvent};
	__property TOnTimeOutEvent OnTimeOut = {read=FOnTimeOut, write=FOnTimeOut};
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Oraalerter */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_ORAALERTER)
using namespace Oraalerter;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// OraalerterHPP
