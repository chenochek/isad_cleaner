﻿// CodeGear C++Builder
// Copyright (c) 1995, 2018 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'OraInterval.pas' rev: 33.00 (iOS)

#ifndef OraintervalHPP
#define OraintervalHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.SysUtils.hpp>
#include <CRTypes.hpp>
#include <OraCall.hpp>

//-- user supplied -----------------------------------------------------------

namespace Orainterval
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TOCIInterval;
class DELPHICLASS TOCIIntervalYM;
class DELPHICLASS TOCIIntervalDS;
//-- type declarations -------------------------------------------------------
class PASCALIMPLEMENTATION TOCIInterval : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	System::TArray__1<System::Byte> FValue;
	System::Byte FLeadPrecision;
	System::Byte FFracPrecision;
	bool __fastcall GetIsNull();
	
protected:
	virtual void __fastcall Alloc() = 0 ;
	bool __fastcall GetSign(System::UnicodeString Value, int &CurPos);
	int __fastcall GetNextNumber(System::UnicodeString Value, int &CurPos, System::UnicodeString Delimiter, int MaxValue, System::UnicodeString RangeErrorMsg, bool FractionalPart = false);
	void __fastcall PutNextNumber(System::UnicodeString &ResStr, int Number, System::UnicodeString Delimiter, int MaxValue, int &Negative, bool FractionalPart = false);
	
public:
	__fastcall TOCIInterval();
	void __fastcall Assign(TOCIInterval* Interval);
	int __fastcall Compare(TOCIInterval* Interval);
	virtual unsigned __fastcall Check() = 0 ;
	virtual void __fastcall Parse(System::UnicodeString Value) = 0 ;
	HIDESBASE virtual System::UnicodeString __fastcall ToString(System::Byte Precision, System::Byte Scale) = 0 ;
	void __fastcall FromNumber(void * Number);
	__property bool IsNull = {read=GetIsNull, nodefault};
	__property System::TArray__1<System::Byte> Value = {read=FValue, write=FValue};
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TOCIInterval() { }
	
};


class PASCALIMPLEMENTATION TOCIIntervalYM : public TOCIInterval
{
	typedef TOCIInterval inherited;
	
protected:
	virtual void __fastcall Alloc();
	
public:
	virtual unsigned __fastcall Check();
	virtual void __fastcall Parse(System::UnicodeString Value);
	virtual System::UnicodeString __fastcall ToString(System::Byte Precision, System::Byte Scale);
	void __fastcall SetYearMonth(int Year, int Month);
	void __fastcall GetYearMonth(int &Year, int &Month);
public:
	/* TOCIInterval.Create */ inline __fastcall TOCIIntervalYM() : TOCIInterval() { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TOCIIntervalYM() { }
	
};


class PASCALIMPLEMENTATION TOCIIntervalDS : public TOCIInterval
{
	typedef TOCIInterval inherited;
	
protected:
	virtual void __fastcall Alloc();
	
public:
	virtual unsigned __fastcall Check();
	virtual void __fastcall Parse(System::UnicodeString Value);
	virtual System::UnicodeString __fastcall ToString(System::Byte Precision, System::Byte Scale);
	void __fastcall SetDaySecond(int Day, int Hour, int Minute, int Second, int fSecond);
	void __fastcall GetDaySecond(int &Day, int &Hour, int &minute, int &Second, int &fSecond);
public:
	/* TOCIInterval.Create */ inline __fastcall TOCIIntervalDS() : TOCIInterval() { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TOCIIntervalDS() { }
	
};


//-- var, const, procedure ---------------------------------------------------
#define SIntervalInvalid u"ORA-01867: the interval is invalid"
#define SLeadPricisionSmall u"ORA-01873: the leading pricision of interval is too small"
#define SInvalidMonth u"ORA-01843: not a valid month"
#define SInvalidHour u"ORA-01850 hour must be between 0 and 23"
#define SInvalidMinutes u"ORA-01851 minutes must be between 0 and 59"
#define SInvalidSeconds u"ORA-01852 seconds must be between 0 and 59"
#define SIntervalsIncomparable u"ORA-01870 the intervals or datetimes are not mutually comp"\
	u"arable"
}	/* namespace Orainterval */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_ORAINTERVAL)
using namespace Orainterval;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// OraintervalHPP
