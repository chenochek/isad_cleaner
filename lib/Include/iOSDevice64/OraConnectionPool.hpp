﻿// CodeGear C++Builder
// Copyright (c) 1995, 2018 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'OraConnectionPool.pas' rev: 33.00 (iOS)

#ifndef OraconnectionpoolHPP
#define OraconnectionpoolHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.Classes.hpp>
#include <System.SyncObjs.hpp>
#include <System.Variants.hpp>
#include <CRTypes.hpp>
#include <CLRClasses.hpp>
#include <CRConnectionPool.hpp>
#include <CRVio.hpp>
#include <CRAccess.hpp>
#include <OraCall.hpp>
#include <OraClasses.hpp>

//-- user supplied -----------------------------------------------------------

namespace Oraconnectionpool
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TOraConnectionParameters;
class DELPHICLASS TOraLocalConnectionPool;
class DELPHICLASS TOraOCIConnectionPool;
class DELPHICLASS TOraConnectionPoolManager;
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TOraPoolingType : unsigned char { optLocal, optOCI };

class PASCALIMPLEMENTATION TOraConnectionParameters : public Crconnectionpool::TCRConnectionParameters
{
	typedef Crconnectionpool::TCRConnectionParameters inherited;
	
protected:
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	
public:
	bool Direct;
	Crvio::TIPVersion IPVersion;
	System::UnicodeString HomeName;
	System::UnicodeString Charset;
	System::Word CharLength;
	bool UseUnicode;
	bool UnicodeEnvironment;
	int SubscriptionPort;
	Oraclasses::TConnectMode ConnectMode;
	bool UseOCI7;
	TOraPoolingType PoolingType;
	bool StatementCache;
	int StatementCacheSize;
	int ConnectionTimeout;
	Oraclasses::TOptimizerMode OptimizerMode;
	System::UnicodeString ClientIdentifier;
	System::UnicodeString Schema;
	System::UnicodeString ProxyUserName;
	System::UnicodeString ProxyPassword;
	virtual bool __fastcall Equals(Crconnectionpool::TCRConnectionParameters* Obj);
	virtual bool __fastcall SetProp(int Prop, const System::Variant &Value);
public:
	/* TCRConnectionParameters.Create */ inline __fastcall virtual TOraConnectionParameters() : Crconnectionpool::TCRConnectionParameters() { }
	/* TCRConnectionParameters.Destroy */ inline __fastcall virtual ~TOraConnectionParameters() { }
	
};


class PASCALIMPLEMENTATION TOraLocalConnectionPool : public Crconnectionpool::TCRLocalConnectionPool
{
	typedef Crconnectionpool::TCRLocalConnectionPool inherited;
	
protected:
	__classmethod virtual Craccess::TCRConnectionClass __fastcall GetConnectorClass();
	__classmethod void __fastcall InitConnectorOraParams(Craccess::TCRConnection* Connector, Crconnectionpool::TCRConnectionParameters* Parameters);
	virtual void __fastcall InitConnectorParams(Craccess::TCRConnection* Connector);
public:
	/* TCRLocalConnectionPool.Create */ inline __fastcall virtual TOraLocalConnectionPool(Crconnectionpool::TCRConnectionPoolManager* Manager, Crconnectionpool::TCRConnectionParameters* ConnectionParameters) : Crconnectionpool::TCRLocalConnectionPool(Manager, ConnectionParameters) { }
	/* TCRLocalConnectionPool.Destroy */ inline __fastcall virtual ~TOraLocalConnectionPool() { }
	
};


class PASCALIMPLEMENTATION TOraOCIConnectionPool : public Crconnectionpool::TCRConnectionPool
{
	typedef Crconnectionpool::TCRConnectionPool inherited;
	
private:
	Oracall::TOCIEnvironment* FEnvironment;
	void *FhOCIError;
	void *FhOCISPool;
	System::UnicodeString FPoolName;
	Oracall::TOCI8API* __fastcall GetOCI8();
	Oracall::TOracleHome* __fastcall GetHome();
	void * __fastcall AllocPoolHandle();
	void __fastcall FreePoolHandle(void * hOCISPool);
	void __fastcall CreateOCIPool();
	void __fastcall FreeOCIPool();
	
protected:
	virtual bool __fastcall InternalPutConnection(Craccess::TCRConnection* CRConnection);
	__property Oracall::TOCI8API* OCI8 = {read=GetOCI8};
	
public:
	__fastcall virtual ~TOraOCIConnectionPool();
	virtual Craccess::TCRConnection* __fastcall GetConnection();
public:
	/* TCRConnectionPool.Create */ inline __fastcall virtual TOraOCIConnectionPool(Crconnectionpool::TCRConnectionPoolManager* Manager, Crconnectionpool::TCRConnectionParameters* ConnectionParameters) : Crconnectionpool::TCRConnectionPool(Manager, ConnectionParameters) { }
	
};


class PASCALIMPLEMENTATION TOraConnectionPoolManager : public Crconnectionpool::TCRConnectionPoolManager
{
	typedef Crconnectionpool::TCRConnectionPoolManager inherited;
	
protected:
	__classmethod virtual int __fastcall GetPoolManagerIndex();
	__classmethod virtual void __fastcall SetPoolManagerIndex(int Value);
	virtual Crconnectionpool::TCRConnectionPool* __fastcall CreateConnectionPool(Crconnectionpool::TCRConnectionParameters* ConnectionParameters);
	virtual Craccess::TCRConnection* __fastcall InternalGetConnection(Crconnectionpool::TCRConnectionParameters* ConnectionParameters);
	virtual Craccess::TCRConnection* __fastcall InternalCheckConnection(Craccess::TCRConnection* &Connection);
public:
	/* TCRConnectionPoolManager.Create */ inline __fastcall TOraConnectionPoolManager() : Crconnectionpool::TCRConnectionPoolManager() { }
	/* TCRConnectionPoolManager.Destroy */ inline __fastcall virtual ~TOraConnectionPoolManager() { }
	
};


//-- var, const, procedure ---------------------------------------------------
static constexpr System::Int8 prPoolingType = System::Int8(0x65);
}	/* namespace Oraconnectionpool */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_ORACONNECTIONPOOL)
using namespace Oraconnectionpool;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// OraconnectionpoolHPP
