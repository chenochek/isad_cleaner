﻿// CodeGear C++Builder
// Copyright (c) 1995, 2018 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'CRHMAC.pas' rev: 33.00 (iOS)

#ifndef CrhmacHPP
#define CrhmacHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.SysUtils.hpp>
#include <CLRClasses.hpp>
#include <CRTypes.hpp>
#include <CRHashAlgorithm.hpp>
#include <CRCryptoTransformIntf.hpp>

//-- user supplied -----------------------------------------------------------

namespace Crhmac
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TKeyedHashAlgorithm;
class DELPHICLASS THMAC;
//-- type declarations -------------------------------------------------------
class PASCALIMPLEMENTATION TKeyedHashAlgorithm : public Crhashalgorithm::THashAlgorithm
{
	typedef Crhashalgorithm::THashAlgorithm inherited;
	
protected:
	System::TArray__1<System::Byte> FKeyValue;
	void __fastcall SetKey(const System::TArray__1<System::Byte> Value);
	
public:
	__fastcall virtual ~TKeyedHashAlgorithm();
public:
	/* THashAlgorithm.Create */ inline __fastcall virtual TKeyedHashAlgorithm() : Crhashalgorithm::THashAlgorithm() { }
	
};


class PASCALIMPLEMENTATION THMAC : public TKeyedHashAlgorithm
{
	typedef TKeyedHashAlgorithm inherited;
	
private:
	Crhashalgorithm::THashAlgorithm* FHashAlgorithm;
	bool FIsHashing;
	System::TArray__1<System::Byte> FPadded36;
	System::TArray__1<System::Byte> FPadded5C;
	int FBlockSize;
	
protected:
	virtual int __fastcall Get_HashSize();
	virtual void __fastcall HashCore(const char * Data, int Offset, int Count);
	virtual System::TArray__1<System::Byte> __fastcall HashFinal();
	
public:
	__fastcall THMAC(Crhashalgorithm::THashAlgorithm* Hash, const System::TArray__1<System::Byte> rgbKey);
	__fastcall virtual ~THMAC();
	virtual void __fastcall Initialize();
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Crhmac */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_CRHMAC)
using namespace Crhmac;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// CrhmacHPP
