﻿// CodeGear C++Builder
// Copyright (c) 1995, 2018 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'OdacFmx.pas' rev: 33.00 (iOS)

#ifndef OdacfmxHPP
#define OdacfmxHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.Classes.hpp>
#include <System.SysUtils.hpp>
#include <DBAccess.hpp>
#include <CRParser.hpp>
#include <CRServerEnumerator.hpp>
#include <DacFmx.hpp>
#include <ConnectFormFmx.hpp>
#include <CRAccess.hpp>
#include <OraProps.hpp>
#include <OraCall.hpp>
#include <OraClasses.hpp>
#include <Ora.hpp>
#include <OraServerEnumerator.hpp>

//-- user supplied -----------------------------------------------------------

namespace Odacfmx
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TConnectDialogFmx;
//-- type declarations -------------------------------------------------------
class PASCALIMPLEMENTATION TConnectDialogFmx : public Dbaccess::TCustomConnectDialog
{
	typedef Dbaccess::TCustomConnectDialog inherited;
	
private:
	Dbaccess::TConnectDialogOption* FRoleOption;
	Dbaccess::TConnectDialogOption* FDirectOption;
	Dbaccess::TConnectDialogOption* FHomeOption;
	Dbaccess::TConnectDialogOption* FSchemaOption;
	void __fastcall SetRoleOption(Dbaccess::TConnectDialogOption* Value);
	void __fastcall SetDirectOption(Dbaccess::TConnectDialogOption* Value);
	void __fastcall SetHomeOption(Dbaccess::TConnectDialogOption* Value);
	void __fastcall SetSchemaOption(Dbaccess::TConnectDialogOption* Value);
	Ora::TOraSession* __fastcall GetSession();
	bool __fastcall GetReadAliases();
	void __fastcall SetReadAliases(bool Value);
	
protected:
	virtual void __fastcall SetLabelSet(Dbaccess::TLabelSet Value);
	virtual Crserverenumerator::TCRServerEnumeratorClass __fastcall GetServerEnumeratorClass();
	virtual void __fastcall SetServerEnumerator(Crserverenumerator::TCRServerEnumerator* Value);
	virtual System::TClass __fastcall DefDialogClass();
	
public:
	__fastcall virtual TConnectDialogFmx(System::Classes::TComponent* Owner);
	__fastcall virtual ~TConnectDialogFmx();
	virtual bool __fastcall Execute();
	__property Ora::TOraSession* Session = {read=GetSession};
	virtual void __fastcall GetOptions(Dbaccess::TConnectDialogOptionArray &Options, bool Ordered = true);
	
__published:
	__property bool ReadAliases = {read=GetReadAliases, write=SetReadAliases, default=1};
	__property Retries = {default=3};
	__property SavePassword = {default=0};
	__property StoreLogInfo = {default=1};
	__property DialogClass = {default=0};
	__property Caption = {default=0};
	__property ConnectButton = {default=0};
	__property CancelButton = {default=0};
	__property Server;
	__property UserName;
	__property Password;
	__property Dbaccess::TConnectDialogOption* Home = {read=FHomeOption, write=SetHomeOption};
	__property Dbaccess::TConnectDialogOption* Direct = {read=FDirectOption, write=SetDirectOption};
	__property Dbaccess::TConnectDialogOption* Schema = {read=FSchemaOption, write=SetSchemaOption};
	__property Dbaccess::TConnectDialogOption* Role = {read=FRoleOption, write=SetRoleOption};
	__property LabelSet = {default=1};
};


//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE System::TClass __fastcall DefConnectDialogClass(void);
}	/* namespace Odacfmx */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_ODACFMX)
using namespace Odacfmx;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// OdacfmxHPP
