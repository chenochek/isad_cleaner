﻿// CodeGear C++Builder
// Copyright (c) 1995, 2018 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'OraSQLGenerator.pas' rev: 33.00 (iOS)

#ifndef OrasqlgeneratorHPP
#define OrasqlgeneratorHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.Classes.hpp>
#include <CLRClasses.hpp>
#include <CRTypes.hpp>
#include <CRAccess.hpp>
#include <MemData.hpp>
#include <DASQLGenerator.hpp>

//-- user supplied -----------------------------------------------------------

namespace Orasqlgenerator
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TCustomOraSQLGenerator;
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM _TSequenceMode : unsigned char { _smInsert, _smPost };

class PASCALIMPLEMENTATION TCustomOraSQLGenerator : public Dasqlgenerator::TDASQLGenerator
{
	typedef Dasqlgenerator::TDASQLGenerator inherited;
	
private:
	_TSequenceMode FSequenceMode;
	bool __fastcall GetTemporaryLobUpdate();
	
protected:
	bool FSeqReturning;
	__weak Craccess::TCRFieldDesc* FSeqFieldDesc;
	Clrclasses::WideStringBuilder* FReturnSB;
	Clrclasses::WideStringBuilder* FIntoSB;
	virtual bool __fastcall FieldIsNull(Craccess::TCRFieldDesc* FieldDesc, bool OldValue, Memdata::TData* Data, void * OldRecBuf, void * NewRecBuf)/* overload */;
	virtual System::UnicodeString __fastcall GenerateIndexName(const System::UnicodeString Name);
	virtual int __fastcall MaxIdentLength();
	virtual bool __fastcall IsSubstituteParamName();
	virtual void __fastcall AddParam(Craccess::TDAParamsInfo* ParamsInfo, Clrclasses::WideStringBuilder* SB, Craccess::TCRFieldDesc* FieldDesc, const Dasqlgenerator::_TStatementType StatementType, Craccess::TParamDirection ParamType, int Index = 0xffffffff, bool Old = false, bool AllowDuplicates = true);
	virtual void __fastcall AddFieldToInsertSQL(Craccess::TDAParamsInfo* ParamsInfo, Craccess::TCRFieldDesc* FieldDesc, const int Index = 0xffffffff);
	virtual void __fastcall AddFieldToUpdateSQL(Craccess::TDAParamsInfo* ParamsInfo, Craccess::TCRFieldDesc* FieldDesc, const bool ModifiedFieldsOnly, const int Index = 0xffffffff);
	virtual void __fastcall GenerateInsertSQL(Craccess::TDAParamsInfo* ParamsInfo, const Craccess::TKeyAndDataFields &KeyAndDataFields, const bool ModifiedFieldsOnly, const int Index = 0xffffffff);
	virtual void __fastcall GenerateUpdateSQL(Craccess::TDAParamsInfo* ParamsInfo, const Craccess::TKeyAndDataFields &KeyAndDataFields, const bool ModifiedFieldsOnly, const int Index = 0xffffffff);
	virtual void __fastcall GenerateLockSQL(Craccess::TDAParamsInfo* ParamsInfo, const Craccess::TKeyAndDataFields &KeyAndDataFields, const int Index = 0xffffffff);
	
public:
	virtual System::UnicodeString __fastcall GenerateTableSQL(const System::UnicodeString TableName, const System::UnicodeString OrderFields);
	virtual System::UnicodeString __fastcall GenerateSelectValues(const System::UnicodeString ValuesList);
	virtual System::UnicodeString __fastcall GenerateRecCountSQL(bool UseBaseSQL = false);
	virtual System::UnicodeString __fastcall GenerateEmptyTableSQL(const System::UnicodeString TableName);
	virtual System::UnicodeString __fastcall GenerateSmartFetchMetaInfoSQL();
	__property _TSequenceMode SequenceMode = {read=FSequenceMode, write=FSequenceMode, nodefault};
public:
	/* TDASQLGenerator.Create */ inline __fastcall virtual TCustomOraSQLGenerator(Dasqlgenerator::TSQLGeneratorServiceClass ServiceClass) : Dasqlgenerator::TDASQLGenerator(ServiceClass) { }
	/* TDASQLGenerator.Destroy */ inline __fastcall virtual ~TCustomOraSQLGenerator() { }
	
	/* Hoisted overloads: */
	
protected:
	inline bool __fastcall  FieldIsNull(Craccess::TCRFieldDesc* FieldDesc, bool OldValue){ return Dasqlgenerator::TDASQLGenerator::FieldIsNull(FieldDesc, OldValue); }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Orasqlgenerator */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_ORASQLGENERATOR)
using namespace Orasqlgenerator;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// OrasqlgeneratorHPP
