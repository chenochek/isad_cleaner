﻿// CodeGear C++Builder
// Copyright (c) 1995, 2018 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'OraNet.pas' rev: 33.00 (iOS)

#ifndef OranetHPP
#define OranetHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.Classes.hpp>
#include <System.Types.hpp>
#include <System.SysUtils.hpp>
#include <System.StrUtils.hpp>
#include <System.DateUtils.hpp>
#include <CRTypes.hpp>
#include <CRFunctions.hpp>
#include <CLRClasses.hpp>
#include <CRParser.hpp>
#include <CRVio.hpp>
#include <CRVioSocket.hpp>
#include <CRVioTcp.hpp>
#include <CRSspi.hpp>
#include <CRRNG.hpp>
#include <CRSymmetricAlgorithm.hpp>
#include <CRHashAlgorithm.hpp>
#include <CRCipher.hpp>
#include <CRHash.hpp>
#include <CRHMAC.hpp>
#include <CRBigInteger.hpp>
#include <MemUtils.hpp>
#include <OraCall.hpp>
#include <OraError.hpp>
#include <OraParser.hpp>
#include <OraNumber.hpp>
#include <OraDateTime.hpp>
#include <OraInterval.hpp>

//-- user supplied -----------------------------------------------------------

namespace Oranet
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS OracleIntArrayEncoding;
//-- type declarations -------------------------------------------------------
class PASCALIMPLEMENTATION OracleIntArrayEncoding : public Clrclasses::Encoding
{
	typedef Clrclasses::Encoding inherited;
	
private:
	bool O00QOCCOQ0;
	
protected:
	int __fastcall OO0QOCCOQ0(System::WideChar * OQ0QOCCOQ0, int OC0QOCCOQ0, int O0OQOCCOQ0, System::TArray__1<System::Byte> &OOOQOCCOQ0, int OQOQOCCOQ0, int OCOQOCCOQ0);
	int __fastcall OOCQOCCOQ0(const System::TArray__1<System::Byte> OQCQOCCOQ0, int OCCQOCCOQ0, int O00COCCOQ0, System::WideChar * OO0COCCOQ0, int OQ0COCCOQ0, int OC0COCCOQ0);
	
protected:
	virtual int __fastcall GetByteCount(System::WideChar * OOQCOCCOQ0, int O0C0OCC0Q0)/* overload */;
	virtual int __fastcall GetBytes(System::WideChar * O0O0OCCOQ0, int OQO0OCCOQ0, System::PByte OCO0OCCOQ0, int OQC0OCC0Q0)/* overload */;
	virtual int __fastcall GetCharCount(System::PByte OCCCOCCOQ0, int O00OOCC0Q0)/* overload */;
	virtual int __fastcall GetChars(System::PByte OQ0OOCC0Q0, int OC0OOCC0Q0, System::WideChar * O0OOOCC0Q0, int OOOOOCC0Q0)/* overload */;
	
public:
	__fastcall OracleIntArrayEncoding();
	virtual int __fastcall GetMaxByteCount(int OCOCOCCOQ0);
	virtual int __fastcall GetMaxCharCount(int O0QCOCCOQ0);
	virtual System::TArray__1<System::Byte> __fastcall GetPreamble();
	__classmethod OracleIntArrayEncoding* __fastcall GetInstance();
public:
	/* TObject.Destroy */ inline __fastcall virtual ~OracleIntArrayEncoding() { }
	
	/* Hoisted overloads: */
	
public:
	inline int __fastcall  GetByteCount(const System::WideChar Chars){ return System::Sysutils::TEncoding::GetByteCount(Chars); }
	inline int __fastcall  GetByteCount(const System::WideChar *Chars, const int Chars_High){ return System::Sysutils::TEncoding::GetByteCount(Chars, Chars_High); }
	inline int __fastcall  GetByteCount(const System::TArray__1<System::WideChar> Chars){ return System::Sysutils::TEncoding::GetByteCount(Chars); }
	inline int __fastcall  GetByteCount(const System::WideChar *Chars, const int Chars_High, int CharIndex, int CharCount){ return System::Sysutils::TEncoding::GetByteCount(Chars, Chars_High, CharIndex, CharCount); }
	inline int __fastcall  GetByteCount(const System::TArray__1<System::WideChar> Chars, int CharIndex, int CharCount){ return System::Sysutils::TEncoding::GetByteCount(Chars, CharIndex, CharCount); }
	inline int __fastcall  GetByteCount(const System::UnicodeString S){ return System::Sysutils::TEncoding::GetByteCount(S); }
	inline int __fastcall  GetByteCount(const System::UnicodeString S, int CharIndex, int CharCount){ return System::Sysutils::TEncoding::GetByteCount(S, CharIndex, CharCount); }
	inline int __fastcall  GetByteCount(const System::UnicodeString S, int CharIndex, int CharCount, const int StringBaseIndex){ return System::Sysutils::TEncoding::GetByteCount(S, CharIndex, CharCount, StringBaseIndex); }
	inline System::TArray__1<System::Byte> __fastcall  GetBytes(const System::WideChar Chars){ return System::Sysutils::TEncoding::GetBytes(Chars); }
	inline System::TArray__1<System::Byte> __fastcall  GetBytes(const System::WideChar *Chars, const int Chars_High){ return System::Sysutils::TEncoding::GetBytes(Chars, Chars_High); }
	inline System::TArray__1<System::Byte> __fastcall  GetBytes(const System::TArray__1<System::WideChar> Chars){ return System::Sysutils::TEncoding::GetBytes(Chars); }
	inline System::TArray__1<System::Byte> __fastcall  GetBytes(const System::WideChar *Chars, const int Chars_High, int CharIndex, int CharCount){ return System::Sysutils::TEncoding::GetBytes(Chars, Chars_High, CharIndex, CharCount); }
	inline System::TArray__1<System::Byte> __fastcall  GetBytes(const System::TArray__1<System::WideChar> Chars, int CharIndex, int CharCount){ return System::Sysutils::TEncoding::GetBytes(Chars, CharIndex, CharCount); }
	inline int __fastcall  GetBytes(const System::WideChar *Chars, const int Chars_High, int CharIndex, int CharCount, const System::TArray__1<System::Byte> Bytes, int ByteIndex){ return System::Sysutils::TEncoding::GetBytes(Chars, Chars_High, CharIndex, CharCount, Bytes, ByteIndex); }
	inline int __fastcall  GetBytes(const System::TArray__1<System::WideChar> Chars, int CharIndex, int CharCount, const System::TArray__1<System::Byte> Bytes, int ByteIndex){ return System::Sysutils::TEncoding::GetBytes(Chars, CharIndex, CharCount, Bytes, ByteIndex); }
	inline System::TArray__1<System::Byte> __fastcall  GetBytes(const System::UnicodeString S){ return System::Sysutils::TEncoding::GetBytes(S); }
	inline int __fastcall  GetBytes(const System::UnicodeString S, int CharIndex, int CharCount, const System::TArray__1<System::Byte> Bytes, int ByteIndex){ return System::Sysutils::TEncoding::GetBytes(S, CharIndex, CharCount, Bytes, ByteIndex); }
	inline int __fastcall  GetBytes(const System::UnicodeString S, int CharIndex, int CharCount, const System::TArray__1<System::Byte> Bytes, int ByteIndex, const int StringBaseIndex){ return System::Sysutils::TEncoding::GetBytes(S, CharIndex, CharCount, Bytes, ByteIndex, StringBaseIndex); }
	inline int __fastcall  GetCharCount(const System::Byte *Bytes, const int Bytes_High){ return System::Sysutils::TEncoding::GetCharCount(Bytes, Bytes_High); }
	inline int __fastcall  GetCharCount(const System::TArray__1<System::Byte> Bytes){ return System::Sysutils::TEncoding::GetCharCount(Bytes); }
	inline int __fastcall  GetCharCount(const System::Byte *Bytes, const int Bytes_High, int ByteIndex, int ByteCount){ return System::Sysutils::TEncoding::GetCharCount(Bytes, Bytes_High, ByteIndex, ByteCount); }
	inline int __fastcall  GetCharCount(const System::TArray__1<System::Byte> Bytes, int ByteIndex, int ByteCount){ return System::Sysutils::TEncoding::GetCharCount(Bytes, ByteIndex, ByteCount); }
	inline System::TArray__1<System::WideChar> __fastcall  GetChars(const System::Byte *Bytes, const int Bytes_High){ return System::Sysutils::TEncoding::GetChars(Bytes, Bytes_High); }
	inline System::TArray__1<System::WideChar> __fastcall  GetChars(const System::TArray__1<System::Byte> Bytes){ return System::Sysutils::TEncoding::GetChars(Bytes); }
	inline System::TArray__1<System::WideChar> __fastcall  GetChars(const System::Byte *Bytes, const int Bytes_High, int ByteIndex, int ByteCount){ return System::Sysutils::TEncoding::GetChars(Bytes, Bytes_High, ByteIndex, ByteCount); }
	inline System::TArray__1<System::WideChar> __fastcall  GetChars(const System::TArray__1<System::Byte> Bytes, int ByteIndex, int ByteCount){ return System::Sysutils::TEncoding::GetChars(Bytes, ByteIndex, ByteCount); }
	inline int __fastcall  GetChars(const System::Byte *Bytes, const int Bytes_High, int ByteIndex, int ByteCount, const System::WideChar *Chars, const int Chars_High, int CharIndex){ return System::Sysutils::TEncoding::GetChars(Bytes, Bytes_High, ByteIndex, ByteCount, Chars, Chars_High, CharIndex); }
	inline int __fastcall  GetChars(const System::TArray__1<System::Byte> Bytes, int ByteIndex, int ByteCount, const System::TArray__1<System::WideChar> Chars, int CharIndex){ return System::Sysutils::TEncoding::GetChars(Bytes, ByteIndex, ByteCount, Chars, CharIndex); }
	
};


enum DECLSPEC_DENUM TSecurityLevel : unsigned char { slAccepted, slRejected, slRequested, slRequired };

//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE bool UseDirectLobs;
extern DELPHI_PACKAGE bool UseFastSelect;
extern DELPHI_PACKAGE bool AdvancedSecurity;
extern DELPHI_PACKAGE TSecurityLevel EncryptionLevel;
extern DELPHI_PACKAGE TSecurityLevel DataIntegrityLevel;
extern DELPHI_PACKAGE int PacketSize;
extern DELPHI_PACKAGE int SDU;
extern DELPHI_PACKAGE int TDU;
extern DELPHI_PACKAGE bool OldAuthMode;
extern DELPHI_PACKAGE int __cdecl OCIEnvCreate_(void * &OCCQCCCOQ0, unsigned O00CCCCOQ0, const void * OO0CCCCOQ0, const void * OQ0CCCCOQ0, const void * OC0CCCCOQ0, const void * O0OCCCCOQ0, NativeUInt OOOCCCCOQ0, void * OQOCCCCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIErrorGet_(void * OCOCCCCOQ0, unsigned O0QCCCCOQ0, void * OOQCCCCOQ0, int &OQQCCCCOQ0, void * OCQCCCCOQ0, unsigned O0CCCCCOQ0, unsigned OOCCCCCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIHandleAlloc_(void * OQ00CCCOQ0, void * &OC00CCCOQ0, unsigned O0O0CCCOQ0, NativeUInt OOO0CCCOQ0, void * OQO0CCCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIHandleFree_(void * OCO0CCCOQ0, unsigned O0Q0CCCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIDescriptorAlloc_(void * OCOQCCCOQ0, void * &O0QQCCCOQ0, unsigned OOQQCCCOQ0, NativeUInt OQQQCCCOQ0, void * OCQQCCCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIDescriptorFree_(void * O0CQCCCOQ0, unsigned OOCQCCCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIAttrGet_(void * OO0OOCCOQ0, unsigned OQ0OOCCOQ0, void * OC0OOCCOQ0, Oracall::pub4 O0OOOCCOQ0, unsigned OOOOOCCOQ0, void * OQOOOCCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIAttrGet2_(void * OCOOOCCOQ0, unsigned O0QOOCCOQ0, int &OOQOOCCOQ0, Oracall::pub4 OQQOOCCOQ0, unsigned OCQOOCCOQ0, void * O0COOCCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIAttrSet_(void * OOCOOCCOQ0, unsigned OQCOOCCOQ0, void * OCCOOCCOQ0, unsigned O00Q0CCOQ0, unsigned OO0Q0CCOQ0, void * OQ0Q0CCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIAttrSet2_(void * OC0Q0CCOQ0, unsigned O0OQ0CCOQ0, int &OOOQ0CCOQ0, unsigned OQOQ0CCOQ0, unsigned OCOQ0CCOQ0, void * O0QQ0CCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIParamGet_(void * OQQ0CCCOQ0, unsigned OCQ0CCCOQ0, void * O0C0CCCOQ0, void * &OOC0CCCOQ0, unsigned OQC0CCCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIServerAttach_(void * O0QOCCCOQ0, void * OOQOCCCOQ0, void * OQQOCCCOQ0, int OCQOCCCOQ0, unsigned O0COCCCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIServerDetach_(void * OQCOCCCOQ0, void * OCCOCCCOQ0, unsigned O00QQCCOQ0);
extern DELPHI_PACKAGE int __cdecl OCISessionBegin_(void * OQOQQCCOQ0, void * OCOQQCCOQ0, void * O0QQQCCOQ0, unsigned OOQQQCCOQ0, unsigned OQQQQCCOQ0);
extern DELPHI_PACKAGE int __cdecl OCISessionEnd_(void * OCQQQCCOQ0, void * O0CQQCCOQ0, void * OOCQQCCOQ0, unsigned OQCQQCCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIServerVersion_(void * OO0QQCCOQ0, void * OQ0QQCCOQ0, void * OC0QQCCOQ0, unsigned O0OQQCCOQ0, System::Byte OOOQQCCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIBreak_(void * O00O0CCOQ0, void * OO0O0CCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIPasswordChange_(void * OCC0CCCOQ0, void * O00OCCCOQ0, const void * OO0OCCCOQ0, unsigned OQ0OCCCOQ0, const void * OC0OCCCOQ0, unsigned O0OOCCCOQ0, const void * OOOOCCCOQ0, int OQOOCCCOQ0, unsigned OCOOCCCOQ0);
extern DELPHI_PACKAGE int __cdecl OCITransCommit_(void * OOCOQCCOQ0, void * OQCOQCCOQ0, unsigned OCCOQCCOQ0);
extern DELPHI_PACKAGE int __cdecl OCITransRollback_(void * O00QOQCOQ0, void * OO0QOQCOQ0, unsigned OQ0QOQCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIStmtExecute_(void * OCCQQCCOQ0, void * O00CQCCOQ0, void * OO0CQCCOQ0, unsigned OQ0CQCCOQ0, unsigned OC0CQCCOQ0, void * O0OCQCCOQ0, void * OOOCQCCOQ0, unsigned OQOCQCCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIStmtFetch_(void * O0QCQCCOQ0, void * OOQCQCCOQ0, unsigned OQQCQCCOQ0, System::Word OCQCQCCOQ0, unsigned O0CCQCCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIStmtPrepare_(void * OQCCQCCOQ0, void * OCCCQCCOQ0, void * O000QCCOQ0, unsigned OO00QCCOQ0, unsigned OQ00QCCOQ0, unsigned OC00QCCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIStmtPrepare2_(void * OOO0QCCOQ0, void * &OQO0QCCOQ0, void * OCO0QCCOQ0, void * O0Q0QCCOQ0, unsigned OOQ0QCCOQ0, void * OQQ0QCCOQ0, unsigned OCQ0QCCOQ0, unsigned O0C0QCCOQ0, unsigned OOC0QCCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIStmtPrepare3_(void * OCC0QCCOQ0, void * O00OQCCOQ0, void * OO0OQCCOQ0, void * OQ0OQCCOQ0, unsigned OC0OQCCOQ0, unsigned O0OOQCCOQ0, unsigned OOOOQCCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIStmtGetNextResult_(void * OCOOQCCOQ0, void * O0QOQCCOQ0, Oracall::ppOCIStmt OOQOQCCOQ0, /* out */ unsigned &OQQOQCCOQ0, unsigned OCQOQCCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIBindByName_(void * OCCQ0CCOQ0, void * &O00C0CCOQ0, void * OO0C0CCOQ0, void * OQ0C0CCOQ0, int OC0C0CCOQ0, void * O0OC0CCOQ0, int OOOC0CCOQ0, System::Word OQOC0CCOQ0, void * OCOC0CCOQ0, Oracall::pub2 O0QC0CCOQ0, Oracall::pub2 OOQC0CCOQ0, unsigned OQQC0CCOQ0, Oracall::pub4 OCQC0CCOQ0, unsigned O0CC0CCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIBindByPos_(void * OC000CCOQ0, void * &O0O00CCOQ0, void * OOO00CCOQ0, unsigned OQO00CCOQ0, void * OCO00CCOQ0, int O0Q00CCOQ0, System::Word OOQ00CCOQ0, void * OQQ00CCOQ0, Oracall::pub2 OCQ00CCOQ0, Oracall::pub2 O0C00CCOQ0, unsigned OOC00CCOQ0, Oracall::pub4 OQC00CCOQ0, unsigned OCC00CCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIBindArrayOfStruct_(void * OOQQ0CCOQ0, void * OQQQ0CCOQ0, unsigned OCQQ0CCOQ0, unsigned O0CQ0CCOQ0, unsigned OOCQ0CCOQ0, unsigned OQCQ0CCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIBindDynamic_(void * OOCC0CCOQ0, void * OQCC0CCOQ0, void * OCCC0CCOQ0, void * O0000CCOQ0, void * OO000CCOQ0, void * OQ000CCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIDefineByPos_(void * O0QO0CCOQ0, void * &OOQO0CCOQ0, void * OQQO0CCOQ0, unsigned OCQO0CCOQ0, void * O0CO0CCOQ0, int OOCO0CCOQ0, System::Word OQCO0CCOQ0, void * OCCO0CCOQ0, Oracall::pub2 O00QCCCOQ0, Oracall::pub2 OO0QCCCOQ0, unsigned OQ0QCCCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIDefineArrayOfStruct_(void * OQ0O0CCOQ0, void * OC0O0CCOQ0, unsigned O0OO0CCOQ0, unsigned OOOO0CCOQ0, unsigned OQOO0CCOQ0, unsigned OCOO0CCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIDefineDynamic_(void * OC0QCCCOQ0, void * O0OQCCCOQ0, void * OOOQCCCOQ0, void * OQOQCCCOQ0);
extern DELPHI_PACKAGE int __cdecl OCILobCharSetForm_(void * OC0QOQCOQ0, void * O0OQOQCOQ0, const void * OOOQOQCOQ0, System::Byte &OQOQOQCOQ0);
extern DELPHI_PACKAGE int __cdecl OCILobGetLength_(void * OCOQOQCOQ0, void * O0QQOQCOQ0, void * OOQQOQCOQ0, unsigned &OQQQOQCOQ0);
extern DELPHI_PACKAGE int __cdecl OCILobCreateTemporary_(void * OCQQOQCOQ0, void * O0CQOQCOQ0, void * OOCQOQCOQ0, System::Word OQCQOQCOQ0, System::Byte OCCQOQCOQ0, System::Byte O00COQCOQ0, int OO0COQCOQ0, System::Word OQ0COQCOQ0);
extern DELPHI_PACKAGE int __cdecl OCILobFreeTemporary_(void * OC0COQCOQ0, void * O0OCOQCOQ0, void * OOOCOQCOQ0);
extern DELPHI_PACKAGE int __cdecl OCILobIsTemporary_(void * OQOCOQCOQ0, void * OCOCOQCOQ0, void * O0QCOQCOQ0, System::LongBool &OOQCOQCOQ0);
extern DELPHI_PACKAGE int __cdecl OCILobLocatorIsInit_(void * OQQCOQCOQ0, void * OCQCOQCOQ0, const void * O0CCOQCOQ0, int &OOCCOQCOQ0);
extern DELPHI_PACKAGE int __cdecl OCILobRead_(void * OQCCOQCOQ0, void * OCCCOQCOQ0, void * O000OQCOQ0, unsigned &OO00OQCOQ0, unsigned OQ00OQCOQ0, void * OC00OQCOQ0, unsigned O0O0OQCOQ0, void * OOO0OQCOQ0, void * OQO0OQCOQ0, System::Word OCO0OQCOQ0, System::Byte O0Q0OQCOQ0);
extern DELPHI_PACKAGE int __cdecl OCILobTrim_(void * OOQ0OQCOQ0, void * OQQ0OQCOQ0, void * OCQ0OQCOQ0, unsigned O0C0OQCOQ0);
extern DELPHI_PACKAGE int __cdecl OCILobWrite_(void * OOC0OQCOQ0, void * OQC0OQCOQ0, void * OCC0OQCOQ0, unsigned &O00OOQCOQ0, unsigned OO0OOQCOQ0, void * OQ0OOQCOQ0, unsigned OC0OOQCOQ0, System::Byte O0OOOQCOQ0, void * OOOOOQCOQ0, void * OQOOOQCOQ0, System::Word OCOOOQCOQ0, System::Byte O0QOOQCOQ0);
extern DELPHI_PACKAGE int __cdecl OCILobFileExists_(void * OOQOOQCOQ0, void * OQQOOQCOQ0, void * OCQOOQCOQ0, int &O0COOQCOQ0);
extern DELPHI_PACKAGE int __cdecl OCILobFileIsOpen_(void * OOCOOQCOQ0, void * OQCOOQCOQ0, void * OCCOOQCOQ0, int &O00Q0QCOQ0);
extern DELPHI_PACKAGE int __cdecl OCILobFileOpen_(void * OO0Q0QCOQ0, void * OQ0Q0QCOQ0, void * OC0Q0QCOQ0, System::Byte O0OQ0QCOQ0);
extern DELPHI_PACKAGE int __cdecl OCILobFileClose_(void * OOOQ0QCOQ0, void * OQOQ0QCOQ0, void * OCOQ0QCOQ0);
extern DELPHI_PACKAGE int __cdecl OCILobFileGetName_(void * O0QQ0QCOQ0, void * OOQQ0QCOQ0, const void * OQQQ0QCOQ0, void * OCQQ0QCOQ0, Oracall::pub2 O0CQ0QCOQ0, void * OOCQ0QCOQ0, Oracall::pub2 OQCQ0QCOQ0);
extern DELPHI_PACKAGE int __cdecl OCILobFileSetName_(void * OCCQ0QCOQ0, void * O00C0QCOQ0, Oracall::ppOCILobLocator OO0C0QCOQ0, const void * OQ0C0QCOQ0, System::Word OC0C0QCOQ0, const void * O0OC0QCOQ0, System::Word OOOC0QCOQ0);
extern DELPHI_PACKAGE int __cdecl OCINumberAssign_(void * OCOC0QCOQ0, const void * O0QC0QCOQ0, void * OOQC0QCOQ0);
extern DELPHI_PACKAGE int __cdecl OCINumberCmp_(void * OCQC0QCOQ0, const void * O0CC0QCOQ0, const void * OOCC0QCOQ0, int &OQCC0QCOQ0);
extern DELPHI_PACKAGE int __cdecl OCINumberFromInt_(void * OO000QCOQ0, __int64 &OQ000QCOQ0, unsigned OC000QCOQ0, unsigned O0O00QCOQ0, void * OOO00QCOQ0);
extern DELPHI_PACKAGE int __cdecl OCINumberFromReal_(void * OCO00QCOQ0, double &O0Q00QCOQ0, unsigned OOQ00QCOQ0, void * OQQ00QCOQ0);
extern DELPHI_PACKAGE int __cdecl OCINumberFromText_(void * O0C00QCOQ0, const void * OOC00QCOQ0, unsigned OQC00QCOQ0, const void * OCC00QCOQ0, unsigned O00O0QCOQ0, const void * OO0O0QCOQ0, unsigned OQ0O0QCOQ0, void * OC0O0QCOQ0);
extern DELPHI_PACKAGE int __cdecl OCINumberToInt_(void * O0QO0QCOQ0, void * OOQO0QCOQ0, unsigned OQQO0QCOQ0, unsigned OCQO0QCOQ0, __int64 &O0CO0QCOQ0);
extern DELPHI_PACKAGE int __cdecl OCINumberToReal_(void * OQCO0QCOQ0, const void * OCCO0QCOQ0, unsigned O00QCQCOQ0, double &OO0QCQCOQ0);
extern DELPHI_PACKAGE int __cdecl OCINumberToText_(void * OC0QCQCOQ0, void * O0OQCQCOQ0, const void * OOOQCQCOQ0, unsigned OQOQCQCOQ0, const void * OCOQCQCOQ0, unsigned O0QQCQCOQ0, unsigned &OOQQCQCOQ0, void * OQQQCQCOQ0);
extern DELPHI_PACKAGE int __cdecl OCINumberFromBCD_(void * OCCQCQCOQ0, const System::TArray__1<System::Byte> O00CCQCOQ0, unsigned OO0CCQCOQ0, void * OQ0CCQCOQ0);
extern DELPHI_PACKAGE int __cdecl OCINumberToBCD_(void * O0OCCQCOQ0, const void * OOOCCQCOQ0, unsigned OQOCCQCOQ0, void *OCOCCQCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIDateTimeAssign_(void * OOQCCQCOQ0, void * OQQCCQCOQ0, void * OCQCCQCOQ0, void * O0CCCQCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIDateTimeCheck_(void * OCCCCQCOQ0, void * O000CQCOQ0, void * OO00CQCOQ0, unsigned &OQ00CQCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIDateTimeCompare_(void * O0O0CQCOQ0, void * OOO0CQCOQ0, const void * OQO0CQCOQ0, const void * OCO0CQCOQ0, int &O0Q0CQCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIDateTimeConstruct_(void * OCQ0CQCOQ0, void * O0C0CQCOQ0, void * OOC0CQCOQ0, short OQC0CQCOQ0, System::Byte OCC0CQCOQ0, System::Byte O00OCQCOQ0, System::Byte OO0OCQCOQ0, System::Byte OQ0OCQCOQ0, System::Byte OC0OCQCOQ0, unsigned O0OOCQCOQ0, void * OOOOCQCOQ0, int OQOOCQCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIDateTimeFromText_(void * OQQOCQCOQ0, void * OCQOCQCOQ0, void * O0COCQCOQ0, int OOCOCQCOQ0, void * OQCOCQCOQ0, System::Byte OCCOCQCOQ0, void * O00QQQCOQ0, int OO0QQQCOQ0, void * OQ0QQQCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIDateTimeToText_(void * OCOQQQCOQ0, void * O0QQQQCOQ0, void * OOQQQQCOQ0, void * OQQQQQCOQ0, System::Byte OCQQQQCOQ0, System::Byte O0CQQQCOQ0, void * OOCQQQCOQ0, int OQCQQQCOQ0, unsigned &OCCQQQCOQ0, void * O00CQQCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIDateTimeGetDate_(void * OOOCQQCOQ0, void * OQOCQQCOQ0, void * OCOCQQCOQ0, short &O0QCQQCOQ0, System::Byte &OOQCQQCOQ0, System::Byte &OQQCQQCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIDateTimeGetTime_(void * O0CCQQCOQ0, void * OOCCQQCOQ0, void * OQCCQQCOQ0, System::Byte &OCCCQQCOQ0, System::Byte &O000QQCOQ0, System::Byte &OO00QQCOQ0, unsigned &OQ00QQCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIDateTimeGetTimeZoneOffset_(void * O0O0QQCOQ0, void * OOO0QQCOQ0, void * OQO0QQCOQ0, System::Int8 &OCO0QQCOQ0, System::Int8 &O0Q0QQCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIDateTimeGetTimeZoneName_(void * OQQ0QQCOQ0, void * OCQ0QQCOQ0, void * O0C0QQCOQ0, Oracall::pub1 OOC0QQCOQ0, unsigned &OQC0QQCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIIntervalAssign_(void * OO0OQQCOQ0, void * OQ0OQQCOQ0, void * OC0OQQCOQ0, void * O0OOQQCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIIntervalCheck_(void * OCOOQQCOQ0, void * O0QOQQCOQ0, void * OOQOQQCOQ0, unsigned &OQQOQQCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIIntervalCompare_(void * O0COQQCOQ0, void * OOCOQQCOQ0, void * OQCOQQCOQ0, void * OCCOQQCOQ0, int &O00QOOCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIIntervalFromText_(void * OC0QOOCOQ0, void * O0OQOOCOQ0, void * OOOQOOCOQ0, int OQOQOOCOQ0, void * OCOQOOCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIIntervalToText_(void * OCQQOOCOQ0, void * O0CQOOCOQ0, void * OOCQOOCOQ0, System::Byte OQCQOOCOQ0, System::Byte OCCQOOCOQ0, void * O00COOCOQ0, NativeUInt OO0COOCOQ0, NativeUInt &OQ0COOCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIIntervalSetYearMonth_(void * OOOCOOCOQ0, void * OQOCOOCOQ0, int OCOCOOCOQ0, int O0QCOOCOQ0, void * OOQCOOCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIIntervalGetYearMonth_(void * OCQCOOCOQ0, void * O0CCOOCOQ0, int &OOCCOOCOQ0, int &OQCCOOCOQ0, void * OCCCOOCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIIntervalSetDaySecond_(void * OO00OOCOQ0, void * OQ00OOCOQ0, int OC00OOCOQ0, int O0O0OOCOQ0, int OOO0OOCOQ0, int OQO0OOCOQ0, int OCO0OOCOQ0, void * O0Q0OOCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIIntervalGetDaySecond_(void * OQQ0OOCOQ0, void * OCQ0OOCOQ0, int &O0C0OOCOQ0, int &OOC0OOCOQ0, int &OQC0OOCOQ0, int &OCC0OOCOQ0, int &O00OOOCOQ0, void * OO0OOOCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIIntervalFromNumber_(void * OC0OOOCOQ0, void * O0OOOOCOQ0, void * OOOOOOCOQ0, void * OQOOOOCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIRowIdToChar_(void * O0QOOOCOQ0, void * OOQOOOCOQ0, System::Word &OQQOOOCOQ0, void * OCQOOOCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIStringAllocSize_(void * O0COOOCOQ0, void * OOCOOOCOQ0, const void * OQCOOOCOQ0, Oracall::pub4 OCCOOOCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIStringAssign_(void * OO0Q0OCOQ0, void * OQ0Q0OCOQ0, const void * OC0Q0OCOQ0, void * O0OQ0OCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIStringAssignText_(void * OCOQ0OCOQ0, void * O0QQ0OCOQ0, const void * OOQQ0OCOQ0, unsigned OQQQ0OCOQ0, void * OCQQ0OCOQ0);
extern DELPHI_PACKAGE void * __cdecl OCIStringPtr_(void * OQCQ0OCOQ0, const void * OCCQ0OCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIStringResize_(void * OO0C0OCOQ0, void * OQ0C0OCOQ0, unsigned OC0C0OCOQ0, void * O0OC0OCOQ0);
extern DELPHI_PACKAGE unsigned __cdecl OCIStringSize_(void * OQOC0OCOQ0, const void * OCOC0OCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIDescribeAny_(void * OOQC0OCOQ0, void * OQQC0OCOQ0, void * OCQC0OCOQ0, unsigned O0CC0OCOQ0, System::Byte OOCC0OCOQ0, System::Byte OQCC0OCOQ0, System::Byte OCCC0OCOQ0, void * O0000OCOQ0);
extern DELPHI_PACKAGE int __cdecl OCITypeByName_(void * O0O00OCOQ0, void * OOO00OCOQ0, const void * OQO00OCOQ0, const void * OCO00OCOQ0, unsigned O0Q00OCOQ0, const void * OOQ00OCOQ0, unsigned OQQ00OCOQ0, void * OCQ00OCOQ0, unsigned O0C00OCOQ0, System::Word OOC00OCOQ0, int OQC00OCOQ0, void * &OCC00OCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIObjectNew_(void * O0OO0OCOQ0, void * OOOO0OCOQ0, const void * OQOO0OCOQ0, System::Word OCOO0OCOQ0, void * O0QO0OCOQ0, void * OOQO0OCOQ0, System::Word OQQO0OCOQ0, int OCQO0OCOQ0, void * &O0CO0OCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIObjectCopy_(void * O00QCOCOQ0, void * OO0QCOCOQ0, const void * OQ0QCOCOQ0, void * OC0QCOCOQ0, void * O0OQCOCOQ0, void * OOOQCOCOQ0, void * OQOQCOCOQ0, void * OCOQCOCOQ0, System::Word O0QQCOCOQ0, System::Byte OOQQCOCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIObjectFree_(void * O0CQCOCOQ0, void * OOCQCOCOQ0, void * OQCQCOCOQ0, System::Word OCCQCOCOQ0);
extern DELPHI_PACKAGE void * __cdecl OCIObjectPtr_(void * OO0CCOCOQ0, void * OQ0CCOCOQ0, void * OC0CCOCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIObjectGetInd_(void * OQOCCOCOQ0, void * OCOCCOCOQ0, void * O0QCCOCOQ0, void * OOQCCOCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIObjectSetAttr_(void * OCQCCOCOQ0, void * O0CCCOCOQ0, void * OOCCCOCOQ0, void * OQCCCOCOQ0, void * OCCCCOCOQ0, void * O000COCOQ0, const Oracall::pub4 OO00COCOQ0, const unsigned OQ00COCOQ0, const Oracall::pub4 OC00COCOQ0, const unsigned O0O0COCOQ0, const short OOO0COCOQ0, const void * OQO0COCOQ0, const void * OCO0COCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIObjectGetAttr_(void * O00OCOCOQ0, void * OO0OCOCOQ0, void * OQ0OCOCOQ0, void * OC0OCOCOQ0, void * O0OOCOCOQ0, void * OOOOCOCOQ0, const Oracall::pub4 OQOOCOCOQ0, const unsigned OCOOCOCOQ0, const Oracall::pub4 O0QOCOCOQ0, const unsigned OOQOCOCOQ0, Oracall::pOCIInd OQQOCOCOQ0, void * OCQOCOCOQ0, void * O0COCOCOQ0, Oracall::ppOCIType OOCOCOCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIDefineObject_(void * OQOQQOCOQ0, void * OCOQQOCOQ0, const void * O0QQQOCOQ0, void * OOQQQOCOQ0, Oracall::pub4 OQQQQOCOQ0, void * OCQQQOCOQ0, Oracall::pub4 O0CQQOCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIBindObject_(void * OOCQQOCOQ0, void * OQCQQOCOQ0, const void * OCCQQOCOQ0, void * O00CQOCOQ0, Oracall::pub4 OO0CQOCOQ0, void * OQ0CQOCOQ0, Oracall::pub4 OC0CQOCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIRefIsNull_(void * O0OCQOCOQ0, const void * OOOCQOCOQ0);
extern DELPHI_PACKAGE int __cdecl OCIObjectPin2_(void * O0QCQOCOQ0, const void * OOQCQOCOQ0, void * OQQCQOCOQ0, void * OCQCQOCOQ0, void * O0CCQOCOQ0, int OOCCQOCOQ0, System::Word OQCCQOCOQ0, int OCCCQOCOQ0, void * O000QOCOQ0);
extern DELPHI_PACKAGE int __cdecl OCITableSize_(void * O0O0QOCOQ0, void * OOO0QOCOQ0, const void * OQO0QOCOQ0, int &OCO0QOCOQ0);
extern DELPHI_PACKAGE int __cdecl OCITableNext_(void * OOQ0QOCOQ0, void * OQQ0QOCOQ0, int OCQ0QOCOQ0, const void * O0C0QOCOQ0, int &OOC0QOCOQ0, int &OQC0QOCOQ0);
extern DELPHI_PACKAGE int __cdecl OCITableDelete_(void * O00OQOCOQ0, void * OO0OQOCOQ0, int OQ0OQOCOQ0, void * OC0OQOCOQ0);
extern DELPHI_PACKAGE int __cdecl OCICollSize_(void * OOOOQOCOQ0, void * OQOOQOCOQ0, const void * OCOOQOCOQ0, int &O0QOQOCOQ0);
extern DELPHI_PACKAGE int __cdecl OCICollTrim_(void * OQQOQOCOQ0, void * OCQOQOCOQ0, int O0COQOCOQ0, void * OOCOQOCOQ0);
extern DELPHI_PACKAGE int __cdecl OCICollAppend_(void * O00QO0COQ0, void * OO0QO0COQ0, const void * OQ0QO0COQ0, const void * OC0QO0COQ0, void * O0OQO0COQ0);
extern DELPHI_PACKAGE int __cdecl OCICollAssign_(void * OQQQO0COQ0, void * OCQQO0COQ0, const void * O0CQO0COQ0, void * OOCQO0COQ0);
extern DELPHI_PACKAGE int __cdecl OCICollAssignElem_(void * O00CO0COQ0, void * OO0CO0COQ0, int OQ0CO0COQ0, const void * OC0CO0COQ0, const void * O0OCO0COQ0, void * OOOCO0COQ0);
extern DELPHI_PACKAGE int __cdecl OCICollGetElem_(void * OCQCO0COQ0, void * O0CCO0COQ0, const void * OOCCO0COQ0, int OQCCO0COQ0, int &OCCCO0COQ0, void * &O000O0COQ0, void * &OO00O0COQ0);
extern DELPHI_PACKAGE int __cdecl OCIXMLTypeNew_(void * OOO0O0COQ0, void * OQO0O0COQ0, System::Word OCO0O0COQ0, char * O0Q0O0COQ0, unsigned OOQ0O0COQ0, char * OQQ0O0COQ0, unsigned OCQ0O0COQ0, void * &O0C0O0COQ0);
extern DELPHI_PACKAGE int __cdecl OCIXMLTypeCreateFromSrc_(void * O00OO0COQ0, void * OO0OO0COQ0, System::Word OQ0OO0COQ0, System::Byte OC0OO0COQ0, void * O0OOO0COQ0, int OOOOO0COQ0, void * &OQOOO0COQ0);
extern DELPHI_PACKAGE int __cdecl OCIPStreamFromXMLType_(void * OOCOO0COQ0, void * OQCOO0COQ0, void * OCCOO0COQ0, int O00Q00COQ0);
extern DELPHI_PACKAGE int __cdecl OCIPStreamRead_(void * O0OQ00COQ0, void * OOOQ00COQ0, void * OQOQ00COQ0, __int64 &OCOQ00COQ0, int O0QQ00COQ0);
extern DELPHI_PACKAGE int __cdecl OCIPStreamClose_(void * OOCQ00COQ0, void * OQCQ00COQ0);
extern DELPHI_PACKAGE int __cdecl OCIAnyDataAccess_(void * O00C00COQ0, void * OO0C00COQ0, void * OQ0C00COQ0, System::Word OC0C00COQ0, void * O0OC00COQ0, void * OOOC00COQ0, /* out */ void * &OQOC00COQ0, /* out */ unsigned &OCOC00COQ0);
extern DELPHI_PACKAGE int __cdecl OCIAnyDataConvert_(void * OQQC00COQ0, void * OCQC00COQ0, System::Word O0CC00COQ0, void * OOCC00COQ0, System::Word OQCC00COQ0, void * OCCC00COQ0, void * O00000COQ0, unsigned OO0000COQ0, void * OQ0000COQ0);
extern DELPHI_PACKAGE int __cdecl OCIAnyDataGetType_(void * OQO000COQ0, void * OCO000COQ0, void * O0Q000COQ0, /* out */ System::Word &OOQ000COQ0, /* out */ void * &OQQ000COQ0);
}	/* namespace Oranet */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_ORANET)
using namespace Oranet;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// OranetHPP
