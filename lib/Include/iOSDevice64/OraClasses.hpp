﻿// CodeGear C++Builder
// Copyright (c) 1995, 2018 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'OraClasses.pas' rev: 33.00 (iOS)

#ifndef OraclassesHPP
#define OraclassesHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.SysUtils.hpp>
#include <System.Classes.hpp>
#include <System.Types.hpp>
#include <System.SyncObjs.hpp>
#include <Data.FmtBcd.hpp>
#include <Data.SqlTimSt.hpp>
#include <CLRClasses.hpp>
#include <CRTypes.hpp>
#include <CRTimeStamp.hpp>
#include <CRAccess.hpp>
#include <CRParser.hpp>
#include <CRVio.hpp>
#include <MemData.hpp>
#include <CRDataTypeMap.hpp>
#include <OraCall.hpp>
#include <OraNumber.hpp>
#include <OraDateTime.hpp>
#include <OraInterval.hpp>
#include <OraError.hpp>

//-- user supplied -----------------------------------------------------------

namespace Oraclasses
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TOraParamDesc;
struct TNlsSessionParam;
class DELPHICLASS TOCIConnection;
class DELPHICLASS TOraCursor;
class DELPHICLASS TOCICommand;
class DELPHICLASS TOCISQLInfo;
class DELPHICLASS TOCIFieldDesc;
class DELPHICLASS TOCIRecordSet;
class DELPHICLASS TOraLob;
class DELPHICLASS TOraFile;
class DELPHICLASS TOraTimeStamp;
class DELPHICLASS TOraInterval;
class DELPHICLASS TOraNumber;
struct TTransactionLink;
class DELPHICLASS TOCITransaction;
class DELPHICLASS TOCIMetaData;
class DELPHICLASS TOCILoaderColumn;
class DELPHICLASS TOCILoader;
class DELPHICLASS TAlertEvent;
class DELPHICLASS TOCIAlerter;
class DELPHICLASS TOCIAlerterListenThread;
class DELPHICLASS TOraClassesUtils;
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TTransactionMode : unsigned char { tmReadOnly, tmReadWrite, tmReadCommitted, tmSerializable };

enum DECLSPEC_DENUM TConnectMode : unsigned char { cmNormal, cmSysOper, cmSysDBA, cmSysASM, cmSysBackup, cmSysDG, cmSysKM };

enum DECLSPEC_DENUM TStatementMode : unsigned char { smUnknown, smAllocated, smPrepared, smCached };

class PASCALIMPLEMENTATION TOraParamDesc : public Craccess::TParamDesc
{
	typedef Craccess::TParamDesc inherited;
	
private:
	__weak TOCICommand* FOwner;
	void *FValue;
	int FValueSize;
	void *FActualLengthPtr;
	void *FDefIndicator;
	void *FIndicator;
	bool FTable;
	void *FHandle;
	int FBindBufferSize;
	int FBlobPiece;
	int FLen;
	bool FTableIndicator;
	bool FHasDefault;
	bool FBufferAllocated;
	bool FCanBeOutput;
	Oracall::TOCISvcCtx* __fastcall GetOCISvcCtx();
	Oracall::TOCI8API* __fastcall GetOCI8();
	int __fastcall GetActualLength();
	void __fastcall SetActualLength(int Value);
	__property int ActualLength = {read=GetActualLength, write=SetActualLength, nodefault};
	
protected:
	virtual void __fastcall AllocBuffer();
	virtual void __fastcall FreeBuffer();
	void __fastcall ValidateParamValue();
	__property System::UnicodeString Name = {read=GetName, write=SetName};
	__property System::Word DataType = {read=FDataType, write=FDataType, nodefault};
	__property System::Word SubDataType = {read=FSubDataType, write=FSubDataType, nodefault};
	__property Craccess::TParamDirection ParamType = {read=FParamType, write=FParamType, nodefault};
	__property int Size = {read=FSize, write=FSize, nodefault};
	void __fastcall ClearBindData();
	__property Oracall::TOCISvcCtx* OCISvcCtx = {read=GetOCISvcCtx};
	__property Oracall::TOCI8API* OCI8 = {read=GetOCI8};
	
public:
	__fastcall virtual TOraParamDesc()/* overload */;
	__fastcall virtual ~TOraParamDesc();
	virtual int __fastcall GetMaxStringSize(Craccess::TCRConnection* Connection);
	virtual void __fastcall SetDataType(System::Word Value);
	virtual void __fastcall SetParamType(Craccess::TParamDirection Value);
	virtual void __fastcall SetSize(int Value);
	void __fastcall SetTable(bool Value);
	void __fastcall SetHasDefault(bool Value);
	bool __fastcall GetTable();
	int __fastcall GetLength();
	bool __fastcall GetHasDefault();
	bool __fastcall GetCanBeOutput();
	short __fastcall GetIndicator(int Index);
	void __fastcall SetIndicator(int Index, short Value);
	void __fastcall ExpandFixedChar();
	void __fastcall ExpandWideFixedChar();
	void __fastcall TrimFixedChar();
	void __fastcall TrimWideFixedChar();
	void * __fastcall ValuePtr();
	int __fastcall GetValueSize();
	void * __fastcall IndicatorPtr();
	void __fastcall SyncIndicator(TOCIConnection* Connection);
	System::TDateTime __fastcall GetItemAsDateTime(int Index);
	void __fastcall SetItemAsDateTime(int Index, System::TDateTime Value);
	double __fastcall GetItemAsFloat(int Index);
	void __fastcall SetItemAsFloat(int Index, double Value);
	float __fastcall GetItemAsSingle(int Index);
	void __fastcall SetItemAsSingle(int Index, float Value);
	System::Currency __fastcall GetItemAsCurrency(int Index);
	void __fastcall SetItemAsCurrency(int Index, System::Currency Value);
	int __fastcall GetItemAsInteger(int Index);
	void __fastcall SetItemAsInteger(int Index, int Value, System::Word DataType = (System::Word)(0x9));
	__int64 __fastcall GetItemAsLargeInt(int Index);
	void __fastcall SetItemAsLargeInt(int Index, __int64 Value);
	void __fastcall GetItemAsBcd(int Index, Data::Fmtbcd::TBcd &Value);
	void __fastcall SetItemAsBcd(int Index, const Data::Fmtbcd::TBcd &Value);
	void __fastcall GetItemAsSQLTimeStamp(int Index, Data::Sqltimst::TSQLTimeStamp &Value);
	void __fastcall SetItemAsSQLTimeStamp(int Index, const Data::Sqltimst::TSQLTimeStamp &Value);
	System::UnicodeString __fastcall GetItemAsString(int Index);
	void __fastcall SetItemAsString(int Index, const System::UnicodeString Value);
	
protected:
	Crtypes::AnsiString __fastcall GetItemAsAnsiString(int Index);
	void __fastcall SetItemAsAnsiString(int Index, const Crtypes::AnsiString &Value);
	
public:
	System::UnicodeString __fastcall GetItemAsWideString(int Index);
	void __fastcall SetItemAsWideString(int Index, const System::UnicodeString Value);
	bool __fastcall GetItemAsBoolean(int Index);
	void __fastcall SetItemAsBoolean(int Index, bool Value);
	virtual Memdata::TSharedObject* __fastcall GetItemObject(int Index);
	virtual void __fastcall SetItemObject(int Index, Memdata::TSharedObject* Value);
	virtual System::Variant __fastcall GetItemValue(int Index);
	virtual void __fastcall SetItemValue(int Index, const System::Variant &Value);
	virtual System::Variant __fastcall GetValue();
	virtual void __fastcall SetValue(const System::Variant &Value);
	Memdata::TBlob* __fastcall GetAsBlobRef();
	TOraCursor* __fastcall GetAsCursor();
	TOraLob* __fastcall GetAsOraBlob();
	TOraFile* __fastcall GetAsBFile();
	TOraTimeStamp* __fastcall GetAsTimeStamp();
	TOraInterval* __fastcall GetAsInterval();
	TOraNumber* __fastcall GetAsNumber();
	virtual Memdata::TSharedObject* __fastcall GetObject();
	virtual void __fastcall SetObject(Memdata::TSharedObject* Value);
	virtual bool __fastcall GetNull();
	virtual void __fastcall SetNull(const bool Value);
	virtual bool __fastcall GetItemNull(int Index);
	virtual void __fastcall SetItemNull(int Index, bool Value);
	__classmethod virtual bool __fastcall NeedAssignScalarValues();
	__classmethod virtual bool __fastcall NeedAssignObjectValues();
};


typedef void __fastcall (__closure *TRunMethod)(void);

typedef void __fastcall (__closure *TEndMethod)(System::Sysutils::Exception* E);

typedef int THandle;

enum DECLSPEC_DENUM TNlsParamType : unsigned char { nlsDateLanguage, nlsDateFormat, nlsNumericCharacters, nlsTimeStampFormat, nlsTimeStampTZFormat };

struct DECLSPEC_DRECORD TNlsSessionParam
{
public:
	System::UnicodeString Name;
	System::UnicodeString Value;
	bool IsUserDefined;
};


typedef void __fastcall (__closure *TFailoverCallback)(unsigned FailoverState, unsigned FailoverType, bool &Retry);

typedef void __fastcall (__closure *TInfoMessageCallback)(Oraerror::EOraError* Error);

enum DECLSPEC_DENUM TConnectionType : unsigned char { ctDefault, ctOCIPooled };

enum DECLSPEC_DENUM TOptimizerMode : unsigned char { omDefault, omFirstRows1000, omFirstRows100, omFirstRows10, omFirstRows1, omFirstRows, omAllRows, omChoose, omRule };

class PASCALIMPLEMENTATION TOCIConnection : public Craccess::TCRConnection
{
	typedef Craccess::TCRConnection inherited;
	
	
private:
	typedef System::StaticArray<TNlsSessionParam, 5> _TOCIConnection__1;
	
	
private:
	Oracall::TOCISvcCtx* FOCISvcCtx;
	Oracall::TOCICallStyle FOCICallStyle;
	Oracall::TOCICallStyle FOCICallStyleCommand;
	bool FThreadSafety;
	System::Word FMaxStringSize;
	int FLastError;
	TConnectMode FConnectMode;
	bool FEnableIntegers;
	bool FEnableLargeint;
	bool FEnableNumbers;
	bool FEnableWideOraClob;
	System::UnicodeString FInternalName;
	System::UnicodeString FOracleVersionSt;
	System::UnicodeString FOracleVersionFull;
	System::Word FOracleVersion;
	TOCIConnection* FProxyConnection;
	bool FDisconnectMode;
	int FConnectionTimeout;
	System::UnicodeString FOCIPoolName;
	bool FStatementCache;
	int FStatementCacheSize;
	System::UnicodeString FHomeName;
	bool FUnicodeEnvironment;
	bool FDirect;
	Crvio::TIPVersion FIPVersion;
	TConnectionType FConnectionType;
	System::UnicodeString FClientIdentifier;
	TOptimizerMode FOptimizerMode;
	System::UnicodeString FSchema;
	System::UnicodeString FCachedSchema;
	System::UnicodeString FCachedUser;
	int FSubscriptionPort;
	bool FEnableSQLTimeStamp;
	bool FIntervalAsString;
	bool FUnicodeAsNational;
	int FSmallintPrecision;
	int FIntegerPrecision;
	int FLargeIntPrecision;
	int FFloatPrecision;
	int FBCDPrecision;
	int FBCDScale;
	int FFmtBCDPrecision;
	int FFmtBCDScale;
	System::UnicodeString FCharset;
	System::Word FCharsetId;
	System::Word FInternalCharsetId;
	System::Word FCharLength;
	bool FQueryCharLength;
	System::Word FMaxCharLength;
	System::Word FMinCharLength;
	bool FUseUnicode;
	_TOCIConnection__1 FNlsParams;
	Oracall::TCDA *LDA;
	Oracall::THDA *HDA;
	void *hServer;
	void *hSession;
	void *hOCIAuthInfo;
	System::Syncobjs::TCriticalSection* FLock;
	System::Classes::TThread* FTimeoutThread;
	void __fastcall GetSessionParameters();
	void __fastcall SetNlsParameter(const System::UnicodeString Name, const System::UnicodeString Value);
	System::Word __fastcall GetMaxStringSize();
	Oracall::TOCI7API* __fastcall GetOCI7();
	Oracall::TOCI8API* __fastcall GetOCI8();
	
protected:
	TFailoverCallback FOnFailover;
	TInfoMessageCallback FOnInfoMessage;
	void __fastcall SetStatementCacheSize(int Size);
	void __fastcall SetupConnection();
	void __fastcall SetOptimizerMode();
	void __fastcall SetClientIdentifier();
	virtual void __fastcall RaiseError(const System::UnicodeString Msg);
	virtual void __fastcall InitCommandProp(Craccess::TCRCommand* Command);
	__property AutoCommit;
	__property EnableBCD;
	__property EnableFMTBCD;
	__property bool ConvertEOL = {read=FConvertEOL, nodefault};
	
public:
	__fastcall virtual TOCIConnection();
	__fastcall virtual ~TOCIConnection();
	virtual Craccess::TCRCommandClass __fastcall GetCommandClass();
	virtual Craccess::TCRRecordSetClass __fastcall GetRecordSetClass();
	virtual Craccess::TCRTransactionClass __fastcall GetTransactionClass();
	virtual Craccess::TCRLoaderClass __fastcall GetLoaderClass();
	virtual Craccess::TCRMetaDataClass __fastcall GetMetaDataClass();
	__classmethod virtual Crdatatypemap::TCRMapRulesClass __fastcall GetMapRulesClass();
	virtual Memdata::TDBObject* __fastcall CreateObject(System::Word DataType, Craccess::TCRTransaction* Transaction);
	void __fastcall CheckOCI();
	void __fastcall CheckOCI73();
	void __fastcall CheckOCI80();
	void __fastcall Check(int Status);
	virtual void __fastcall OraError(Oracall::TOCICallStyle FOCICallStyle, int ErrorCode, bool UseCallback, System::TObject* Component);
	Oracall::TOCIEnvironment* __fastcall AllocEnvironment();
	void __fastcall SetConnectionType(TConnectionType ConnectionType);
	void __fastcall InitSessionParameters();
	virtual void __fastcall Connect(const System::UnicodeString ConnectString);
	virtual void __fastcall Disconnect();
	virtual void __fastcall Ping();
	System::Word __fastcall GetOracleVersion();
	virtual System::UnicodeString __fastcall GetServerVersion();
	virtual System::UnicodeString __fastcall GetServerVersionFull();
	virtual System::UnicodeString __fastcall GetClientVersion();
	void __fastcall SetCurrentSchema(System::UnicodeString SchemaName);
	System::UnicodeString __fastcall GetCurrentSchema();
	System::UnicodeString __fastcall GetDefaultSchema();
	System::UnicodeString __fastcall GetCachedSchema();
	System::UnicodeString __fastcall GetCurrentUser();
	System::UnicodeString __fastcall GetCachedUser();
	void __fastcall BreakExec();
	void __fastcall Lock();
	void __fastcall Release();
	void __fastcall RunTimeoutThread(int Timeout);
	void __fastcall StopTimeoutThread();
	System::Classes::TThread* __fastcall RunThread(TRunMethod RunMethod, TEndMethod EndMethod);
	bool __fastcall StopThread(System::Classes::TThread* &hThread);
	Oracall::PCDA __fastcall GetLDA();
	void __fastcall SetLDA(Oracall::PCDA Value);
	void __fastcall SetOCISvcCtx(Oracall::TOCISvcCtx* Value);
	void __fastcall ChangePassword(System::UnicodeString NewPassword);
	virtual void __fastcall Assign(Craccess::TCRConnection* Source);
	virtual void __fastcall AssignConnect(Craccess::TCRConnection* Source);
	void __fastcall SetNonBlocking(bool Value);
	Oracall::TOCICallStyle __fastcall GetOCICallStyle();
	void __fastcall SetOCICallStyle(Oracall::TOCICallStyle Value);
	Oracall::TOCICallStyle __fastcall GetOCICallStyleCommand();
	int __fastcall GetLastError();
	void __fastcall SetLastError(int Value);
	void __fastcall GetTableFields(System::UnicodeString TableName, System::Classes::TStringList* Fields);
	virtual bool __fastcall SetProp(int Prop, const System::Variant &Value);
	virtual bool __fastcall GetProp(int Prop, /* out */ System::Variant &Value);
	virtual bool __fastcall CheckIsValid();
	virtual void __fastcall ReturnToPool();
	__property bool Direct = {read=FDirect, nodefault};
	__property Oracall::TOCISvcCtx* OCISvcCtx = {read=FOCISvcCtx};
	__property Oracall::TOCI7API* OCI7 = {read=GetOCI7};
	__property Oracall::TOCI8API* OCI8 = {read=GetOCI8};
	__property bool UnicodeEnvironment = {read=FUnicodeEnvironment, nodefault};
	__property TOCIConnection* ProxyConnection = {read=FProxyConnection, write=FProxyConnection};
	__property Oracall::TOCICallStyle OCICallStyle = {read=FOCICallStyle, nodefault};
	__property Oracall::TOCICallStyle OCICallStyleCommand = {read=FOCICallStyleCommand, nodefault};
	__property TFailoverCallback OnFailover = {read=FOnFailover, write=FOnFailover};
	__property TInfoMessageCallback OnInfoMessage = {read=FOnInfoMessage, write=FOnInfoMessage};
};


class PASCALIMPLEMENTATION TOraCursor : public Craccess::TCRCursor
{
	typedef Craccess::TCRCursor inherited;
	
private:
	Oracall::TCDA *FCDA;
	void * *FphOCIStmt;
	TStatementMode FStatementMode;
	Oracall::TOCISvcCtx* FOCISvcCtx;
	Oracall::TOCI8API* FOCI8;
	Oracall::TOCICallStyle FOCICallStyle;
	bool FScrollable;
	int FPrefetchRows;
	void __fastcall SetOCISvcCtx(Oracall::TOCISvcCtx* Value);
	void __fastcall DisablePrefetching();
	Oracall::PCDA __fastcall GetCDA();
	void * __fastcall GethOCIStmt();
	void __fastcall SethOCIStmt(void * Value);
	void * __fastcall GetOCIStmt();
	Oracall::ppOCIStmt __fastcall GetOCIStmtPtr();
	void __fastcall SetPrefetchRows(int Value);
	__property void * hOCIStmt = {read=GethOCIStmt, write=SethOCIStmt};
	
protected:
	Craccess::TCursorState FState;
	void __fastcall Check(int Status);
	void __fastcall CheckOCI();
	void __fastcall CheckOCI73();
	void __fastcall CheckOCI80();
	void __fastcall Init(Oracall::TOCISvcCtx* AOCISvcCtx, Oracall::TOCICallStyle AOCICallStyle);
	void __fastcall ReleaseOCISvcCtx();
	__property Oracall::TOCI8API* OCI8 = {read=FOCI8};
	
public:
	__fastcall TOraCursor();
	__fastcall virtual ~TOraCursor();
	void __fastcall AllocCursor(TStatementMode StatementMode = (TStatementMode)(0x1));
	void __fastcall ReleaseCursor();
	void __fastcall FreeCursor();
	virtual void __fastcall Disconnect();
	virtual bool __fastcall CanFetch();
	__property Oracall::PCDA CDA = {read=GetCDA};
	__property Oracall::TOCISvcCtx* OCISvcCtx = {read=FOCISvcCtx};
	__property Oracall::TOCICallStyle OCICallStyle = {read=FOCICallStyle, nodefault};
	__property void * OCIStmt = {read=GetOCIStmt};
	__property Oracall::ppOCIStmt OCIStmtPtr = {read=GetOCIStmtPtr};
	__property Craccess::TCursorState State = {read=FState, nodefault};
	__property int PrefetchRows = {read=FPrefetchRows, write=SetPrefetchRows, nodefault};
};


class PASCALIMPLEMENTATION TOCICommand : public Craccess::TCRCommand
{
	typedef Craccess::TCRCommand inherited;
	
private:
	Oracall::TOCISvcCtx* FOCISvcCtx;
	Oracall::TOCICallStyle FOCICallStyle;
	int FCommandTimeout;
	TOraCursor* FCursor;
	TOraCursor* FCursorRef;
	System::Classes::TList* FResultSets;
	bool FNonBlocking;
	System::Word FSQLType;
	System::Word FLastSQLType;
	int FRowsProcessed;
	int FFetchedRows;
	System::Word FErrorOffset;
	bool FFieldsAsString;
	bool FCacheLobs;
	bool FStoreRowId;
	System::UnicodeString FRowId;
	bool FRawAsString;
	bool FNumberAsString;
	bool FUseDefaultDataTypes;
	bool FProcNamedParams;
	int FSmallintPrecision;
	int FIntegerPrecision;
	int FLargeIntPrecision;
	int FFloatPrecision;
	int FBCDPrecision;
	int FBCDScale;
	int FFmtBCDPrecision;
	int FFmtBCDScale;
	bool FOpenNext;
	bool FForceUnprepare;
	void *FGCHandle;
	bool FTemporaryLobUpdate;
	System::Word FPrefetchLobLength;
	int FPrefetchLobSize;
	bool FStatementCache;
	bool FCheckParamHasDefault;
	bool FForceSPInit;
	void *FNullBuf;
	System::Syncobjs::TCriticalSection* FLock;
	void * __fastcall GetGCHandle();
	Oracall::TOCI7API* __fastcall GetOCI7();
	Oracall::TOCI8API* __fastcall GetOCI8();
	System::UnicodeString __fastcall RemoveCRSymbols(System::UnicodeString SQLText, int &ErrorOffset);
	
protected:
	__weak TOCIConnection* FConnection;
	void __fastcall DoExecute();
	void __fastcall EndExecute(System::Sysutils::Exception* E);
	virtual void __fastcall RaiseError(const System::UnicodeString Msg);
	void __fastcall AllocOCISvcCtx();
	void __fastcall ReleaseOCISvcCtx();
	void __fastcall CheckOCI();
	void __fastcall CheckOCI73();
	void __fastcall CheckOCI80();
	void __fastcall CheckActive();
	void __fastcall CheckInactive();
	void __fastcall CheckSession();
	void __fastcall Check(int Status);
	int __fastcall GetSmallintPrecision();
	int __fastcall GetIntegerPrecision();
	int __fastcall GetLargeintPrecision();
	int __fastcall GetFloatPrecision();
	int __fastcall GetBCDPrecision();
	int __fastcall GetBCDScale();
	int __fastcall GetFmtBCDPrecision();
	int __fastcall GetFmtBCDScale();
	void __fastcall DetectDataType(System::Word DBType, int DBLength, short DBScale, Memdata::TObjectType* ObjectType, bool LongStrings, /* out */ System::Word &DataType, /* out */ System::Word &SubDataType);
	int __fastcall DescribeStoredProc(const System::UnicodeString objnam, Oracall::pub2 ovrld, Oracall::pub2 pos, Oracall::pub2 level, void * argnam, Oracall::pub2 arnlen, Oracall::pub2 dtype, Oracall::pub1 defsup, Oracall::pub1 mode, Oracall::pub4 dtsiz, Oracall::psb2 prec, Oracall::psb2 scale, Oracall::pub1 radix, Oracall::pub4 spare, unsigned &arrsiz);
	void __fastcall DescribeObjectParams(const System::UnicodeString Name, int Overload, System::Classes::TStringList* ObjectParams);
	void __fastcall DescribeObjectParamType(const System::UnicodeString TypeName, TOraParamDesc* Param);
	int __fastcall GetOraType7(int DataType, int SubDataType);
	System::Word __fastcall InternalFetch7(System::Word Rows);
	int __fastcall InternalFetchPiece7();
	void __fastcall InitProcParams7(const System::UnicodeString Name, int Overload);
	int __fastcall GetInternalType8(System::Word DBType);
	int __fastcall GetOraType8(int DataType, int SubDataType, bool IsDefine);
	System::Word __fastcall InternalFetch8(System::Word Rows, int Orientation, int Offset);
	System::Word __fastcall InternalExecuteFetch8(System::Word Rows);
	int __fastcall InternalFetchPiece8(int Orientation, int Offset);
	void __fastcall InitProcParams8(System::UnicodeString Name, int Overload);
	int __fastcall CallbackInBind(void * Bind, unsigned Iter, unsigned Index, void * &Buf, unsigned &BufLen, System::Byte &PieceStatus, void * &Ind);
	int __fastcall CallbackOutBind(void * Bind, unsigned Iter, unsigned Index, void * &Buf, Oracall::pub4 &BufLen, System::Byte &PieceStatus, void * &Ind);
	void __fastcall SetArrayLength(int Value);
	bool __fastcall GetActive();
	virtual void __fastcall CreateBatchCommand();
	virtual bool __fastcall NeedBatchTransaction();
	virtual bool __fastcall NeedBatchRollback();
	virtual void __fastcall InternalExecuteBatch(int Iters, int Offset);
	__property Params;
	__property void * GCHandle = {read=GetGCHandle};
	
public:
	__fastcall virtual TOCICommand();
	__fastcall virtual ~TOCICommand();
	__classmethod virtual Craccess::TSQLInfoClass __fastcall GetSQLInfoClass();
	__classmethod virtual Crparser::TSQLParserClass __fastcall GetParserClass();
	__classmethod virtual Craccess::TParamDescClass __fastcall GetParamDescClass();
	__classmethod virtual Crdatatypemap::TCRMapRulesClass __fastcall GetMapRulesClass();
	int __fastcall GetOraType(int DataType, int SubDataType, bool IsDefine);
	void __fastcall InternalOpen();
	void __fastcall InternalParse();
	void __fastcall InternalPrepare();
	virtual System::UnicodeString __fastcall ParseSQL(const System::UnicodeString SQL, Craccess::TParamDescs* Params, const System::UnicodeString RenamePrefix = System::UnicodeString())/* overload */;
	virtual void __fastcall ParseSQLType()/* overload */;
	virtual bool __fastcall IsValidBatchSQL();
	void __fastcall InitProcParams(const System::UnicodeString Name, int Overload);
	virtual System::UnicodeString __fastcall CreateProcCall(const System::UnicodeString Name, bool NeedDescribe, bool IsQuery);
	virtual bool __fastcall ForceCreateSPParams();
	__classmethod virtual bool __fastcall IsAllowedArrayType(System::Word DataType);
	bool __fastcall NeedBindParam(TOraParamDesc* Param);
	void __fastcall BindParam(TOraParamDesc* Param);
	int __fastcall InternalExecute(int Mode, int Rows = 0x0);
	void __fastcall Exec();
	void __fastcall DefineData(TOCIFieldDesc* Field, void * DataPtr, void * IndPtr, void * LenPtr, int BufSkip, int IndSkip, int LenSkip);
	void __fastcall DefineDynamic7(TOCIFieldDesc* Field, void * Buf, Oracall::psb2 Ind);
	void __fastcall DefineDynamic8(TOCIFieldDesc* Field, void * Owner, void * Proc, int CharsetId);
	System::Word __fastcall InternalFetch(System::Word Rows, int Orientation = 0x2, int Offset = 0x0);
	int __fastcall InternalFetchPiece(int Orientation = 0x2, int Offset = 0x0);
	void __fastcall InternalCancel();
	void __fastcall InternalClose();
	void __fastcall Finish();
	virtual void __fastcall Close();
	void __fastcall GetPI(void * &Handle, System::Byte &Piece, void * &Buf, unsigned &Iteration, unsigned &Index, Craccess::TParamDirection &Mode);
	void __fastcall SetPI(void * Handle, unsigned HType, System::Byte Piece, void * Buf, unsigned &BufLen, Oracall::psb2 Ind);
	bool __fastcall NativeCursor();
	bool __fastcall RowsReturn();
	void __fastcall CheckRowsReturn();
	void __fastcall InitResultSets();
	void __fastcall ReleaseResultSets();
	virtual Craccess::TParamDesc* __fastcall AddParam();
	void __fastcall BindParams();
	TOraParamDesc* __fastcall GetParam(int Index);
	virtual void __fastcall BreakExec();
	void __fastcall HardBreak();
	void __fastcall Lock();
	void __fastcall Release();
	virtual void __fastcall Prepare();
	virtual void __fastcall Unprepare();
	virtual bool __fastcall GetPrepared();
	virtual void __fastcall Execute();
	virtual void __fastcall SetConnection(Craccess::TCRConnection* Value);
	virtual Craccess::TCRCursor* __fastcall GetCursor();
	virtual void __fastcall SetCursor(Craccess::TCRCursor* Value);
	TOraCursor* __fastcall GetNextCursor();
	void __fastcall SetOCICallStyle(Oracall::TOCICallStyle Value);
	virtual Craccess::TCursorState __fastcall GetCursorState();
	virtual void __fastcall SetCursorState(Craccess::TCursorState Value);
	int __fastcall GetSQLType();
	System::UnicodeString __fastcall GetRowId();
	virtual bool __fastcall SetProp(int Prop, const System::Variant &Value);
	virtual bool __fastcall GetProp(int Prop, /* out */ System::Variant &Value);
	__property EnableBCD;
	__property EnableFMTBCD;
	__property Oracall::TOCICallStyle OCICallStyle = {read=FOCICallStyle, nodefault};
	__property Oracall::TOCISvcCtx* OCISvcCtx = {read=FOCISvcCtx};
	__property Oracall::TOCI7API* OCI7 = {read=GetOCI7};
	__property Oracall::TOCI8API* OCI8 = {read=GetOCI8};
	/* Hoisted overloads: */
	
public:
	inline void __fastcall  ParseSQL(){ Craccess::TCRCommand::ParseSQL(); }
	inline void __fastcall  ParseSQLType(const System::UnicodeString SQL){ Craccess::TCRCommand::ParseSQLType(SQL); }
	
};


class PASCALIMPLEMENTATION TOCISQLInfo : public Craccess::TSQLInfo
{
	typedef Craccess::TSQLInfo inherited;
	
protected:
	virtual bool __fastcall NextCharQuotesNeed(System::WideChar Ch, Craccess::TIdentCase IdCase);
	virtual bool __fastcall HasAsLexem();
	virtual void __fastcall ParseExtTableInfo(Crparser::TSQLParser* Parser, int &CodeLexem, System::UnicodeString &StLex, System::UnicodeString &Name);
	
public:
	virtual Craccess::TIdentCase __fastcall IdentCase();
	virtual System::UnicodeString __fastcall NormalizeName(const System::UnicodeString Value, const System::WideChar LeftQ, const System::WideChar RightQ, bool QuoteNames = false, bool UnQuoteNames = false)/* overload */;
	virtual System::UnicodeString __fastcall NormalizeName(const System::UnicodeString Value, bool QuoteNames = false, bool UnQuoteNames = false)/* overload */;
	virtual void __fastcall SplitObjectName(const System::UnicodeString Name, Craccess::TSQLObjectInfo &Info);
	__classmethod bool __fastcall ParseSPName(System::UnicodeString FullName, System::UnicodeString &Name, int &Overload);
	__classmethod System::UnicodeString __fastcall GetFinalSPName(System::UnicodeString Name, int Overload);
public:
	/* TSQLInfo.Create */ inline __fastcall virtual TOCISQLInfo(Crparser::TSQLParserClass ParserClass) : Craccess::TSQLInfo(ParserClass) { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TOCISQLInfo() { }
	
};


class PASCALIMPLEMENTATION TOCIFieldDesc : public Craccess::TCRFieldDesc
{
	typedef Craccess::TCRFieldDesc inherited;
	
private:
	int FFetchBufferOffset;
	bool FIsConverted;
	bool FIsBlobSubType;
	
protected:
	virtual void __fastcall SetDataType(System::Word Value);
	virtual void __fastcall SetSubDataType(System::Word Value);
	
public:
	__property int FetchBufferOffset = {read=FFetchBufferOffset, nodefault};
	__property bool IsBlobSubType = {read=FIsBlobSubType, nodefault};
	__property bool IsConverted = {read=FIsConverted, nodefault};
public:
	/* TCRFieldDesc.Create */ inline __fastcall virtual TOCIFieldDesc(Memdata::TRecordSetClass RecordSetClass) : Craccess::TCRFieldDesc(RecordSetClass) { }
	
public:
	/* TFieldDesc.Destroy */ inline __fastcall virtual ~TOCIFieldDesc() { }
	
};


typedef void __fastcall (__closure *TModifyAction)(void);

class PASCALIMPLEMENTATION TOCIRecordSet : public Craccess::TCRRecordSet
{
	typedef Craccess::TCRRecordSet inherited;
	
private:
	Oracall::TOCISvcCtx* FOCISvcCtx;
	bool FAutoClose;
	bool FDeferredLobRead;
	System::Classes::TThread* hExecFetchThread;
	System::Classes::TThread* hFetchAllThread;
	TOraCursor* FFetchCursor;
	bool FPieceFetch;
	void *FFetchItems;
	bool FFetchAbsolute;
	int FFetchStart;
	int FFetchEnd;
	void *FGCHandle;
	bool FHasConvertedFields;
	bool FFetching;
	bool FHasObjectFields;
	System::UnicodeString FTempFilterText;
	bool FDisableInitFields;
	Memdata::TFieldDescs* FExpandedFields;
	bool FHideRowId;
	bool FDisconnectedMode;
	bool FUseUnicode;
	int FCharLength;
	void __fastcall AllocOCISvcCtx();
	void __fastcall ReleaseOCISvcCtx();
	bool __fastcall HasCursorParams();
	void __fastcall InitFetchCursor();
	bool __fastcall FetchArray(bool FetchBack = false);
	bool __fastcall FetchPiece(bool FetchBack = false);
	bool __fastcall GetNonBlocking();
	void * __fastcall GetGCHandle();
	bool __fastcall GetDisconnectedMode();
	bool __fastcall GetUseUnicode();
	int __fastcall GetCharLength();
	void __fastcall Check(System::Word Status);
	Oracall::TOCI7API* __fastcall GetOCI7();
	Oracall::TOCI8API* __fastcall GetOCI8();
	
protected:
	TOCICommand* FCommand;
	__weak TOCIConnection* FConnection;
	virtual void __fastcall CreateCommand();
	virtual void __fastcall SetCommand(Craccess::TCRCommand* Value);
	virtual bool __fastcall NeedInitFieldsOnPrepare();
	virtual void __fastcall InternalPrepare();
	virtual void __fastcall InternalUnPrepare();
	virtual void __fastcall InternalOpen(bool DisableInitFields = false);
	virtual void __fastcall InternalClose();
	virtual bool __fastcall ExtFieldsInfoIsInternal();
	virtual void __fastcall CreateFieldDescs();
	virtual void __fastcall FillDataFieldDescs(/* out */ Craccess::TFieldDescArray &DataFieldDescs, bool ForceUseAllKeyFields);
	virtual void __fastcall RequestFieldsInfo(Craccess::TSQLObjectsInfo Tables, Craccess::TCRColumnsInfo* Columns);
	virtual int __fastcall InternalCompareFieldValue(void * ValuePtr, System::Word ValueType, void * FieldBuf, System::Word FieldType, int FieldLength, bool HasParent, bool IsFixed, const Memdata::TCompareOptions Options);
	bool __fastcall GetFieldDesc(int FieldNo, TOCIFieldDesc* &Field, bool LongString, bool FlatBuffer);
	bool __fastcall GetFieldDesc7(int FieldNo, TOCIFieldDesc* &Field, bool LongString, bool FlatBuffer);
	bool __fastcall GetFieldDesc8(int FieldNo, TOCIFieldDesc* &Field, bool LongString, bool FlatBuffer);
	virtual int __fastcall GetIndicatorItemSize();
	virtual bool __fastcall GetEOF();
	virtual void __fastcall SetFilterText(const System::UnicodeString Value);
	virtual void __fastcall AllocFetchBuffer();
	virtual void __fastcall FreeFetchBuffer();
	virtual void __fastcall ProcessFetchedBlock(Memdata::PBlockHeader Block, bool FetchBack);
	virtual void __fastcall InitBlock(Memdata::PBlockHeader Block);
	void __fastcall SwapBlocks(Memdata::PBlockHeader Block1, Memdata::PBlockHeader Block2);
	virtual void __fastcall InitFetchedBlock(Memdata::PBlockHeader Block, int RowsObtained, bool FetchBack);
	virtual bool __fastcall CanFetchBack();
	virtual bool __fastcall InternalFetch(bool FetchBack = false);
	virtual bool __fastcall NeedInitFieldsOnFetch();
	virtual bool __fastcall NeedUnPrepareAfterFetch();
	virtual void __fastcall DoBeforeFetch(/* out */ bool &Cancel);
	virtual void __fastcall DoAfterFetch();
	virtual bool __fastcall IsSupportedDataType(System::Word DataType);
	virtual bool __fastcall IsSpecificType(Memdata::TFieldDesc* const Field, System::Word &DataType, System::UnicodeString &DataTypeName, int &Len, int &Scale);
	void __fastcall FreeAllItems();
	void __fastcall DoExecFetch();
	void __fastcall EndExecFetch(System::Sysutils::Exception* E);
	void __fastcall DoFetchAll();
	void __fastcall DoFetchAllPulse();
	void __fastcall EndFetchAll(System::Sysutils::Exception* E);
	int __fastcall CallbackDefine(void * Define, unsigned Iter, void * &Buf, Oracall::pub4 &BufLen, System::Byte &PieceStatus, void * &Ind);
	__property void * GCHandle = {read=GetGCHandle};
	__property bool DisconnectedMode = {read=GetDisconnectedMode, nodefault};
	__property bool UseUnicode = {read=GetUseUnicode, nodefault};
	__property int CharLength = {read=GetCharLength, nodefault};
	__property Oracall::TOCISvcCtx* OCISvcCtx = {read=FOCISvcCtx};
	__property Oracall::TOCI7API* OCI7 = {read=GetOCI7};
	__property Oracall::TOCI8API* OCI8 = {read=GetOCI8};
	
public:
	__fastcall virtual TOCIRecordSet();
	__fastcall virtual ~TOCIRecordSet();
	virtual bool __fastcall IsFullReopen();
	virtual void __fastcall Reopen();
	void __fastcall SetCommandType();
	virtual void __fastcall ExecCommand(int Iters = 0x1, int Offset = 0x0);
	virtual void __fastcall Disconnect();
	virtual void __fastcall ExecFetch(bool DisableInitFields);
	virtual void __fastcall FetchAll();
	virtual void __fastcall WaitForFetch();
	virtual bool __fastcall RowsReturn();
	__classmethod virtual int __fastcall GetBufferSize(System::Word DataType, int LengthInChars);
	virtual void __fastcall DetectIdentityField();
	virtual Memdata::TFieldDescClass __fastcall GetFieldDescType();
	void __fastcall DetectFieldType(const System::UnicodeString FieldName, System::Word DBType, int DBLength, short DBScale, Memdata::TObjectType* ObjectType, bool CharUsed, /* out */ System::Word &DataType, /* out */ System::Word &SubDataType, /* out */ int &Length, /* out */ int &Scale, /* out */ bool &Fixed);
	virtual void __fastcall SetNull(Memdata::TFieldDesc* Field, void * RecBuf, bool Value);
	virtual bool __fastcall GetNull(Memdata::TFieldDesc* Field, void * RecBuf);
	__classmethod virtual void __fastcall GetDateFromBuf(void * Buf, void * Date, bool HasParent, Memdata::TDateFormat Format);
	__classmethod virtual void __fastcall PutDateToBuf(void * Buf, void * Date, bool HasParent, Memdata::TDateFormat Format);
	__classmethod virtual bool __fastcall IsBlobDataType(System::Word DataType);
	__classmethod virtual bool __fastcall IsObjectDataType(System::Word DataType);
	__classmethod virtual bool __fastcall IsSharedObjectDataType(System::Word DataType);
	__classmethod virtual bool __fastcall IsComplexDataType(System::Word DataType);
	__classmethod bool __fastcall IsConvertedFieldType(System::Word DataType);
	virtual void __fastcall GetFieldData(Memdata::TFieldDesc* Field, void * FieldBuf, void * Dest, bool NeedConvert);
	virtual void __fastcall GetDataAsVariant(void * DataBuf, System::Word DataType, System::Word SubDataType, int FieldLength, bool HasParent, bool IsFixed, System::Variant &Value, bool UseRollback);
	virtual void __fastcall PutDataAsVariant(void * DataBuf, System::Word DataType, int DataSize, System::Word Length, System::Word Scale, bool HasParent, const System::Variant &Value, bool IsDatabaseValue);
	virtual bool __fastcall FieldListDependsOnParams();
	virtual System::Word __fastcall GetDecryptDataType(System::Word DataType);
	virtual bool __fastcall IsEncryptableDataType(System::Word DataType);
	virtual void __fastcall CreateComplexField(void * RecBuf, Memdata::TFieldDesc* Field);
	virtual void __fastcall FreeComplexField(void * RecBuf, Memdata::TFieldDesc* Field);
	virtual void __fastcall CopyComplexField(void * Source, void * Dest, Memdata::TFieldDesc* Field);
	virtual void __fastcall SortItems();
	virtual void __fastcall FilterUpdated();
	virtual void __fastcall SetToBegin();
	virtual void __fastcall SetToEnd();
	virtual void __fastcall GetBookmark(Memdata::PRecBookmark Bookmark);
	virtual void __fastcall SetToBookmark(Memdata::PRecBookmark Bookmark);
	int __fastcall GetBlockFetchPos(Memdata::PBlockHeader Block);
	int __fastcall GetItemFetchPos(Memdata::PItemHeader Item);
	virtual void __fastcall SetConnection(Craccess::TCRConnection* Value);
	virtual bool __fastcall SetProp(int Prop, const System::Variant &Value);
	virtual bool __fastcall GetProp(int Prop, /* out */ System::Variant &Value);
	__property Memdata::TFieldDescs* ExpandedFields = {read=FExpandedFields};
};


enum DECLSPEC_DENUM TLobType : unsigned char { ltBlob, ltClob, ltNClob };

class PASCALIMPLEMENTATION TOraLob : public Memdata::TCompressedBlob
{
	typedef Memdata::TCompressedBlob inherited;
	
private:
	Oracall::TOCISvcCtx* FOCISvcCtx;
	void * *phLobLocator;
	bool FNativeHandle;
	bool FCached;
	int FCharsetId;
	int FCharsetForm;
	TLobType FLobType;
	System::Byte FCharLength;
	void * __fastcall GetOCILobLocator();
	void * __fastcall GethOCILobLocator();
	void __fastcall SethOCILobLocator(void * Value);
	void __fastcall SetOCILobLocator(void * Value);
	Oracall::ppOCILobLocator __fastcall GetOCILobLocatorPtr();
	void __fastcall SetCached(const bool Value);
	void __fastcall SetOCISvcCtx(Oracall::TOCISvcCtx* const Value);
	Oracall::TOCI8API* __fastcall GetOCI8();
	
protected:
	bool FNeedReadLob;
	virtual Memdata::TBlob* __fastcall CreateClone();
	void __fastcall Check(int Status);
	virtual void __fastcall CheckValue();
	virtual unsigned __fastcall GetSize();
	virtual unsigned __fastcall GetSizeAnsi();
	virtual unsigned __fastcall GetSizeUni();
	void __fastcall CheckAlloc();
	void __fastcall CheckSession();
	void __fastcall CheckInit();
	void __fastcall CheckCharSetForm();
	virtual System::Byte __fastcall CharSize();
	void __fastcall ReadLob(Memdata::PPieceHeader &SharedPiece, int PrefetchLobSize)/* overload */;
	__property Oracall::TOCI8API* OCI8 = {read=GetOCI8};
	__property void * hLobLocator = {read=GethOCILobLocator, write=SethOCILobLocator};
	
public:
	__fastcall TOraLob(Oracall::TOCISvcCtx* AOCISvcCtx);
	__fastcall virtual ~TOraLob();
	virtual void __fastcall AllocLob();
	virtual void __fastcall FreeBlob();
	void __fastcall FreeLob();
	virtual void __fastcall Disconnect();
	void __fastcall Init();
	void __fastcall CreateTemporary(TLobType LobType);
	void __fastcall FreeTemporary();
	System::LongBool __fastcall IsTemporary();
	bool __fastcall IsInit();
	unsigned __fastcall LengthLob();
	void __fastcall EnableBuffering();
	void __fastcall DisableBuffering();
	void __fastcall ReadLob()/* overload */;
	void __fastcall WriteLob();
	virtual unsigned __fastcall Read(unsigned Position, unsigned Count, void * Dest);
	virtual void __fastcall Write(unsigned Position, unsigned Count, void * Source);
	virtual void __fastcall Clear();
	virtual void __fastcall Truncate(unsigned NewSize);
	__property Oracall::TOCISvcCtx* OCISvcCtx = {read=FOCISvcCtx, write=SetOCISvcCtx};
	__property void * OCILobLocator = {read=GetOCILobLocator, write=SetOCILobLocator};
	__property Oracall::ppOCILobLocator OCILobLocatorPtr = {read=GetOCILobLocatorPtr};
	__property bool Cached = {read=FCached, write=SetCached, nodefault};
	__property TLobType LobType = {read=FLobType, write=FLobType, nodefault};
};


class PASCALIMPLEMENTATION TOraFile : public TOraLob
{
	typedef TOraLob inherited;
	
private:
	bool FNeedRollback;
	System::UnicodeString FRollbackFileDir;
	System::UnicodeString FRollbackFileName;
	System::UnicodeString __fastcall GetFileDir();
	void __fastcall SetFileDir(const System::UnicodeString Value);
	System::UnicodeString __fastcall GetFileName();
	void __fastcall SetFileName(const System::UnicodeString Value);
	void __fastcall SetFileDirAndName(const System::UnicodeString FileDir, const System::UnicodeString FileName);
	
protected:
	bool CanRollback;
	virtual void __fastcall CheckValue();
	virtual System::Byte __fastcall CharSize();
	virtual void __fastcall SaveToRollback();
	
public:
	__fastcall virtual ~TOraFile();
	virtual void __fastcall AllocLob();
	virtual void __fastcall FreeBlob();
	void __fastcall Open();
	void __fastcall Close();
	HIDESBASE void __fastcall EnableRollback();
	virtual void __fastcall Commit();
	virtual void __fastcall Cancel();
	void __fastcall Refresh();
	bool __fastcall IsOpen();
	bool __fastcall Exists();
	__property System::UnicodeString FileDir = {read=GetFileDir, write=SetFileDir};
	__property System::UnicodeString FileName = {read=GetFileName, write=SetFileName};
public:
	/* TOraLob.Create */ inline __fastcall TOraFile(Oracall::TOCISvcCtx* AOCISvcCtx) : TOraLob(AOCISvcCtx) { }
	
};


enum DECLSPEC_DENUM THandleType : unsigned char { htLocal, htNative, htShared };

class PASCALIMPLEMENTATION TOraTimeStamp : public Memdata::TSharedObject
{
	typedef Memdata::TSharedObject inherited;
	
private:
	Oracall::TOCIEnvironment* FEnvironment;
	void * *FOCIDateTimePtr;
	Oradatetime::TOCIDateTime* FLocalDateTime;
	short *FIndicatorPtr;
	short FIndicator;
	unsigned FDescriptorType;
	System::Byte FPrecision;
	System::UnicodeString FFormat;
	THandleType FHandleType;
	System::UnicodeString __fastcall GetAsString();
	void __fastcall SetAsString(const System::UnicodeString Value);
	System::TDateTime __fastcall GetAsDateTime();
	void __fastcall SetAsDateTime(System::TDateTime Value);
	System::UnicodeString __fastcall GetTimeZone();
	void * __fastcall GethOCIDateTime();
	void __fastcall SethOCIDateTime(void * Value);
	void * __fastcall GetOCIDateTime();
	void __fastcall SetOCIDateTime(const void * Value);
	void __fastcall SetDescriptorType(const unsigned Value);
	Oracall::ppOCIDateTime __fastcall GetOCIDateTimePtr();
	void __fastcall SetAsSQLTimeStamp(const Data::Sqltimst::TSQLTimeStamp &Value);
	Data::Sqltimst::TSQLTimeStamp __fastcall GetAsSQLTimeStamp();
	Oradatetime::TOCIDateTime* __fastcall GetLocalDateTime();
	HIDESBASE void __fastcall CheckValid();
	
protected:
	void __fastcall Check(int Res);
	void __fastcall Init(System::Word ADataType);
	void __fastcall AllocDateTime();
	void __fastcall FreeDateTime();
	Oradatetime::TOCIDateTime* __fastcall ToLocalDateTime();
	void __fastcall FromLocalDateTime(Oradatetime::TOCIDateTime* Value);
	void __fastcall SetFormat(const System::UnicodeString AFormat);
	__property void * hOCIDateTime = {read=GethOCIDateTime, write=SethOCIDateTime};
	__property Oradatetime::TOCIDateTime* LocalDateTime = {read=GetLocalDateTime};
	
public:
	__fastcall TOraTimeStamp(System::Word ADataType)/* overload */;
	__fastcall TOraTimeStamp(Oracall::TOCIEnvironment* AOCIEnvironment, System::Word ADataType)/* overload */;
	__fastcall TOraTimeStamp(Oracall::TOCISvcCtx* AOCISvcCtx, System::Word ADataType)/* overload */;
	__fastcall virtual ~TOraTimeStamp();
	void __fastcall SetEnvironment(Oracall::TOCIEnvironment* AEnvironment);
	virtual void __fastcall Disconnect();
	void __fastcall Assign(TOraTimeStamp* Source);
	void __fastcall AssignTo(TOraTimeStamp* Dest);
	int __fastcall Compare(TOraTimeStamp* Dest);
	bool __fastcall GetIsNull();
	void __fastcall SetIsNull(bool Value);
	void __fastcall Construct(short Year, System::Byte Month, System::Byte Day, System::Byte Hour, System::Byte Min, System::Byte Sec, unsigned FSec, System::UnicodeString TimeZone);
	void __fastcall GetDate(short &Year, System::Byte &Month, System::Byte &Day);
	void __fastcall SetDate(short Year, System::Byte Month, System::Byte Day);
	void __fastcall GetTime(System::Byte &Hour, System::Byte &Min, System::Byte &Sec, unsigned &FSec);
	void __fastcall SetTime(System::Byte Hour, System::Byte Min, System::Byte Sec, unsigned FSec);
	void __fastcall GetTimeZoneOffset(System::Int8 &TZHour, System::Int8 &TZMin);
	void __fastcall SetTimeZoneOffset(System::Int8 TZHour, System::Int8 TZMin);
	__property unsigned DescriptorType = {read=FDescriptorType, write=SetDescriptorType, nodefault};
	__property void * OCIDateTime = {read=GetOCIDateTime, write=SetOCIDateTime};
	__property Oracall::ppOCIDateTime OCIDateTimePtr = {read=GetOCIDateTimePtr};
	__property Oracall::pOCIInd IndicatorPtr = {read=FIndicatorPtr};
	__property System::UnicodeString Format = {read=FFormat, write=SetFormat};
	__property System::Byte Precision = {read=FPrecision, write=FPrecision, nodefault};
	__property System::UnicodeString TimeZone = {read=GetTimeZone};
	__property System::UnicodeString AsString = {read=GetAsString, write=SetAsString};
	__property System::TDateTime AsDateTime = {read=GetAsDateTime, write=SetAsDateTime};
	__property Data::Sqltimst::TSQLTimeStamp AsTimeStamp = {read=GetAsSQLTimeStamp, write=SetAsSQLTimeStamp};
	__property bool IsNull = {read=GetIsNull, write=SetIsNull, nodefault};
};


class PASCALIMPLEMENTATION TOraInterval : public Memdata::TSharedObject
{
	typedef Memdata::TSharedObject inherited;
	
private:
	Oracall::TOCIEnvironment* FEnvironment;
	void * *FOCIIntervalPtr;
	Orainterval::TOCIInterval* FLocalInterval;
	short *FIndicatorPtr;
	short FIndicator;
	unsigned FDescriptorType;
	THandleType FHandleType;
	System::Byte FFracPrecision;
	System::Byte FLeadPrecision;
	void __fastcall InitInterval();
	HIDESBASE void __fastcall CheckValid();
	System::UnicodeString __fastcall GetAsString();
	void * __fastcall GethOCIInterval();
	void __fastcall SethOCIInterval(void * Value);
	void * __fastcall GetOCIInterval();
	Oracall::ppOCIInterval __fastcall GetOCIIntervalPtr();
	void __fastcall SetAsString(const System::UnicodeString Value);
	void __fastcall SetDescriptorType(const unsigned Value);
	void __fastcall SetOCIInterval(const void * Value);
	Orainterval::TOCIInterval* __fastcall GetLocalInterval();
	
protected:
	void __fastcall Check(int Res);
	void __fastcall Init(System::Word ADataType);
	void __fastcall AllocInterval();
	void __fastcall FreeInterval();
	Orainterval::TOCIInterval* __fastcall ToLocalInterval();
	void __fastcall FromLocalInterval(Orainterval::TOCIInterval* Value);
	__property void * hOCIInterval = {read=GethOCIInterval, write=SethOCIInterval};
	__property Orainterval::TOCIInterval* LocalInterval = {read=GetLocalInterval};
	
public:
	__fastcall TOraInterval(System::Word ADataType)/* overload */;
	__fastcall TOraInterval(Oracall::TOCIEnvironment* AOCIEnvironment, System::Word ADataType)/* overload */;
	__fastcall TOraInterval(Oracall::TOCISvcCtx* AOCISvcCtx, System::Word ADataType)/* overload */;
	__fastcall virtual ~TOraInterval();
	void __fastcall SetEnvironment(Oracall::TOCIEnvironment* AEnvironment);
	virtual void __fastcall Disconnect();
	void __fastcall Assign(TOraInterval* Source);
	void __fastcall AssignTo(TOraInterval* Dest);
	int __fastcall Compare(TOraInterval* Dest);
	bool __fastcall GetIsNull();
	void __fastcall SetIsNull(bool Value);
	void __fastcall GetYearMonth(int &Year, int &Month);
	void __fastcall SetYearMonth(int Year, int Month);
	void __fastcall GetDaySecond(int &Day, int &Hour, int &Min, int &Sec, int &FSec);
	void __fastcall SetDaySecond(int Day, int Hour, int Min, int Sec, int FSec);
	__property unsigned DescriptorType = {read=FDescriptorType, write=SetDescriptorType, nodefault};
	__property void * OCIInterval = {read=GetOCIInterval, write=SetOCIInterval};
	__property Oracall::ppOCIInterval OCIIntervalPtr = {read=GetOCIIntervalPtr};
	__property Oracall::pOCIInd IndicatorPtr = {read=FIndicatorPtr};
	__property System::Byte LeadPrecision = {read=FLeadPrecision, write=FLeadPrecision, nodefault};
	__property System::Byte FracPrecision = {read=FFracPrecision, write=FFracPrecision, nodefault};
	__property System::UnicodeString AsString = {read=GetAsString, write=SetAsString};
	__property bool IsNull = {read=GetIsNull, write=SetIsNull, nodefault};
};


class PASCALIMPLEMENTATION TOraNumber : public Memdata::TSharedObject
{
	typedef Memdata::TSharedObject inherited;
	
private:
	Oracall::TOCIEnvironment* FEnvironment;
	void *FOCINumberPtr;
	short *FIndicatorPtr;
	short FIndicator;
	THandleType FHandleType;
	void __fastcall SetOCINumberPtr(void * Value);
	Oracall::OCINumber __fastcall GetOCINumber();
	void __fastcall SetOCINumber(const Oracall::OCINumber &Value);
	System::UnicodeString __fastcall GetAsString();
	void __fastcall SetAsString(const System::UnicodeString Value);
	int __fastcall GetAsInteger();
	void __fastcall SetAsInteger(const int Value);
	__int64 __fastcall GetAsLargeInt();
	void __fastcall SetAsLargeInt(const __int64 Value);
	double __fastcall GetAsFloat();
	void __fastcall SetAsFloat(const double Value);
	bool __fastcall GetIsNull();
	void __fastcall SetIsNull(bool Value);
	Data::Fmtbcd::TBcd __fastcall GetAsBCD();
	void __fastcall SetAsBCD(const Data::Fmtbcd::TBcd &Value);
	
protected:
	void __fastcall Init();
	void __fastcall Check(int Res);
	
public:
	__fastcall TOraNumber()/* overload */;
	__fastcall TOraNumber(Oracall::TOCIEnvironment* AOCIEnvironment)/* overload */;
	__fastcall TOraNumber(Oracall::TOCISvcCtx* AOCISvcCtx)/* overload */;
	__fastcall virtual ~TOraNumber();
	void __fastcall SetEnvironment(Oracall::TOCIEnvironment* AEnvironment);
	virtual void __fastcall Disconnect();
	void __fastcall Assign(TOraNumber* Source);
	void __fastcall AssignTo(TOraNumber* Dest);
	int __fastcall Compare(TOraNumber* Dest);
	__property Oracall::OCINumber OCINumber = {read=GetOCINumber, write=SetOCINumber};
	__property void * OCINumberPtr = {read=FOCINumberPtr, write=SetOCINumberPtr};
	__property System::UnicodeString AsString = {read=GetAsString, write=SetAsString};
	__property int AsInteger = {read=GetAsInteger, write=SetAsInteger, nodefault};
	__property __int64 AsLargeInt = {read=GetAsLargeInt, write=SetAsLargeInt};
	__property double AsFloat = {read=GetAsFloat, write=SetAsFloat};
	__property bool IsNull = {read=GetIsNull, write=SetIsNull, nodefault};
	__property Data::Fmtbcd::TBcd AsBCD = {read=GetAsBCD, write=SetAsBCD};
};


enum DECLSPEC_DENUM TOraTransactionState : unsigned char { tsInactive, tsActive, tsPrepared, tsFinished };

struct DECLSPEC_DRECORD TTransactionLink
{
public:
	System::TArray__1<System::Byte> BranchQualifier;
	TOraTransactionState State;
	void *OCITrans;
};


class PASCALIMPLEMENTATION TOCITransaction : public Craccess::TCRTransaction
{
	typedef Craccess::TCRTransaction inherited;
	
	
private:
	typedef System::DynamicArray<TTransactionLink> _TOCITransaction__1;
	
	
private:
	System::UnicodeString FLocalTransactionId;
	System::UnicodeString FTransactionName;
	System::UnicodeString FRollbackSegment;
	_TOCITransaction__1 FTransactionLinks;
	int FInactiveTimeOut;
	int FResumeTimeOut;
	void *FXID;
	System::TArray__1<System::Byte> FTransactionId;
	bool FResume;
	
protected:
	void __fastcall WriteTransactionId();
	void __fastcall WriteBranchQualifier(const TTransactionLink &TransactionLink);
	void __fastcall FreeTransaction();
	System::UnicodeString __fastcall LocalTransactionId(bool CreateTransaction = false);
	void __fastcall StartTransactionLocal();
	void __fastcall CommitLocal();
	void __fastcall RollbackLocal();
	
public:
	__fastcall virtual TOCITransaction();
	__fastcall virtual ~TOCITransaction();
	void __fastcall Check(int Status, TOCIConnection* Connection);
	void __fastcall OraError(Oracall::TOCISvcCtx* OCISvcCtx, int &ErrorCode, bool UseCallback);
	void __fastcall SetTransactionId(System::TArray__1<System::Byte> TransactionId);
	void __fastcall SetBranchQualifier(int Index, System::TArray__1<System::Byte> BranchQualifier);
	virtual bool __fastcall SetProp(int Prop, const System::Variant &Value);
	virtual bool __fastcall GetProp(int Prop, System::Variant &Value);
	virtual bool __fastcall DetectInTransaction(bool CanActivate);
	virtual void __fastcall AssignConnect(Craccess::TCRTransaction* Source);
	virtual void __fastcall StartTransaction();
	virtual void __fastcall Commit();
	virtual void __fastcall Rollback();
	virtual void __fastcall Savepoint(const System::UnicodeString Name);
	virtual void __fastcall RollbackToSavepoint(const System::UnicodeString Name);
	void __fastcall Detach();
};


class PASCALIMPLEMENTATION TOCIMetaData : public Craccess::TCRMetaData
{
	typedef Craccess::TCRMetaData inherited;
	
protected:
	void __fastcall AddOrderBy(System::UnicodeString &OrderByClause, const System::UnicodeString Value);
	System::UnicodeString __fastcall GetTypesForSQL(const System::UnicodeString ObjectTypes, System::UnicodeString *AllTypes, const int AllTypes_High);
	virtual Craccess::TCRRecordSet* __fastcall CreateRecordSet();
	virtual Memdata::TData* __fastcall InternalGetMetaData(const System::UnicodeString MetaDataKind, System::Classes::TStrings* Restrictions)/* overload */;
	virtual void __fastcall InternalGetMetaDataKindsList(System::Classes::TStringList* List);
	virtual void __fastcall InternalGetRestrictionsList(System::Classes::TStringList* List, const System::UnicodeString MetaDataKind);
	virtual Memdata::TData* __fastcall GetTables(System::Classes::TStrings* Restrictions);
	virtual Memdata::TData* __fastcall GetColumns(System::Classes::TStrings* Restrictions);
	virtual Memdata::TData* __fastcall GetProcedures(System::Classes::TStrings* Restrictions);
	virtual Memdata::TData* __fastcall GetProcedureParameters(System::Classes::TStrings* Restrictions);
	virtual Memdata::TData* __fastcall GetIndexes(System::Classes::TStrings* Restrictions);
	virtual Memdata::TData* __fastcall GetIndexColumns(System::Classes::TStrings* Restrictions);
	virtual Memdata::TData* __fastcall GetConstraints(System::Classes::TStrings* Restrictions);
	virtual Memdata::TData* __fastcall GetConstraintColumns(System::Classes::TStrings* Restrictions);
	virtual Memdata::TData* __fastcall GetUsers(System::Classes::TStrings* Restrictions);
	virtual Memdata::TData* __fastcall GetUDTs(System::Classes::TStrings* Restrictions);
	virtual Memdata::TData* __fastcall GetPackages(System::Classes::TStrings* Restrictions);
	Memdata::TData* __fastcall GetSequences(System::Classes::TStrings* Restrictions);
	Memdata::TData* __fastcall GetSynonyms(System::Classes::TStrings* Restrictions);
public:
	/* TCRMetaData.Create */ inline __fastcall virtual TOCIMetaData() : Craccess::TCRMetaData() { }
	/* TCRMetaData.Destroy */ inline __fastcall virtual ~TOCIMetaData() { }
	
};


class PASCALIMPLEMENTATION TOCILoaderColumn : public Craccess::TCRLoaderColumn
{
	typedef Craccess::TCRLoaderColumn inherited;
	
private:
	int FColumnType;
	System::UnicodeString FDateFormat;
	bool FIsNational;
	bool FIsLob;
	int FOffset;
	
protected:
	void __fastcall VerifySize();
	
public:
	__fastcall virtual TOCILoaderColumn();
	virtual void __fastcall UpdateDataType(System::Word Value);
	__property int ColumnType = {read=FColumnType, write=FColumnType, nodefault};
	__property System::UnicodeString DateFormat = {read=FDateFormat, write=FDateFormat};
	__property bool IsNational = {read=FIsNational, write=FIsNational, nodefault};
	__property bool IsLob = {read=FIsLob, write=FIsLob, nodefault};
	__property int Offset = {read=FOffset, write=FOffset, nodefault};
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TOCILoaderColumn() { }
	
};


enum DECLSPEC_DENUM _TDPErrorAction : unsigned char { _dpAbort, _dpFail, _dpIgnore };

typedef void __fastcall (__closure *_TDPErrorEvent)(System::Sysutils::Exception* E, int Col, int Row, _TDPErrorAction &Action);

class PASCALIMPLEMENTATION TOCILoader : public Craccess::TCRLoader
{
	typedef Craccess::TCRLoader inherited;
	
private:
	TOCIConnection* FConnection;
	bool FIsDirectMode;
	_TDPErrorEvent FOnError;
	TOCICommand* FDML;
	Oracall::OCIDirPathCtx *hDirPathCtx;
	Oracall::OCIDirPathColArray *hColumnArray;
	Oracall::OCIDirPathStream *hStream;
	int FBufNumRows;
	System::Word FBufNumCols;
	System::Word FRowSize;
	void *FBuffer;
	System::Classes::TList* FLobBuffer;
	Oracall::TOCISvcCtx* __fastcall GetOCISvcCtx();
	Oracall::TOCI7API* __fastcall GetOCI7();
	Oracall::TOCI8API* __fastcall GetOCI8();
	void __fastcall Check(System::Word Status);
	
protected:
	virtual void __fastcall SetConnection(Craccess::TCRConnection* Value);
	virtual void __fastcall FillColumn(Craccess::TCRLoaderColumn* Column, Memdata::TFieldDesc* FieldDesc);
	bool __fastcall ConverToUnicode(TOCILoaderColumn* Column);
	void __fastcall CheckLobBuffer();
	void __fastcall ClearLobBuffer();
	void __fastcall FreeLobBuffer();
	__property Oracall::TOCISvcCtx* OCISvcCtx = {read=GetOCISvcCtx};
	__property Oracall::TOCI7API* OCI7 = {read=GetOCI7};
	__property Oracall::TOCI8API* OCI8 = {read=GetOCI8};
	
public:
	__fastcall virtual TOCILoader();
	__fastcall virtual ~TOCILoader();
	virtual bool __fastcall SetProp(int Prop, const System::Variant &Value);
	virtual bool __fastcall GetProp(int Prop, System::Variant &Value);
	__classmethod virtual Craccess::TCRLoaderColumnClass __fastcall GetColumnClass();
	virtual void __fastcall Prepare()/* overload */;
	virtual void __fastcall Reset();
	virtual void __fastcall PutColumnData(int Col, int Row, const System::Variant &Value);
	virtual void __fastcall DoLoad();
	virtual void __fastcall Finish();
	__property _TDPErrorEvent OnError = {read=FOnError, write=FOnError};
	/* Hoisted overloads: */
	
public:
	inline void __fastcall  Prepare(int RecordCount){ Craccess::TCRLoader::Prepare(RecordCount); }
	
};


enum DECLSPEC_DENUM _TEventType : unsigned char { _etAlert, _etPipe };

enum DECLSPEC_DENUM TMessageType : unsigned char { mtNone, mtNumber, mtString, mtDate };

class PASCALIMPLEMENTATION TAlertEvent : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	System::UnicodeString Name;
	System::UnicodeString Message;
public:
	/* TObject.Create */ inline __fastcall TAlertEvent() : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TAlertEvent() { }
	
};


typedef void __fastcall (__closure *TOCIAlerterTimeoutCallback)(bool &Continue);

class PASCALIMPLEMENTATION TOCIAlerter : public Craccess::TCRAlerter
{
	typedef Craccess::TCRAlerter inherited;
	
private:
	void *FGCHandle;
	TOCIConnection* FListenConnection;
	TOCICommand* FWaitCommand;
	TOCIAlerterListenThread* FListenThread;
	int FTimeOut;
	TOCIAlerterTimeoutCallback FOnTimeOut;
	int FInterval;
	_TEventType FEventType;
	System::UnicodeString FSelfMessage;
	bool FRegistered;
	bool FAutoCommit;
	bool FSelfEvents;
	TAlertEvent* FLastEvent;
	void * __fastcall GetGCHandle();
	Oracall::TOCISvcCtx* __fastcall GetOCISvcCtx();
	void __fastcall RegisterEvents();
	void __fastcall RemoveEvents();
	void __fastcall SendAlertMessage(const System::UnicodeString EventName, const System::UnicodeString Message);
	bool __fastcall ProcessWaitResult();
	bool __fastcall ProcessMessage();
	bool __fastcall ProcessError(int Status);
	void __fastcall DoOnEvent();
	void __fastcall CallEvent(TAlertEvent* Event);
	void __fastcall DoOnTimeout(bool &Continue);
	
protected:
	__property void * GCHandle = {read=GetGCHandle};
	__property Oracall::TOCISvcCtx* OCISvcCtx = {read=GetOCISvcCtx};
	
public:
	__fastcall virtual TOCIAlerter();
	__fastcall virtual ~TOCIAlerter();
	virtual bool __fastcall SetProp(int Prop, const System::Variant &Value);
	virtual bool __fastcall GetProp(int Prop, System::Variant &Value);
	virtual void __fastcall SendEvent(const System::UnicodeString EventName, const System::UnicodeString Message);
	virtual void __fastcall Start();
	virtual void __fastcall Stop();
	void __fastcall PackMessage(const System::Variant &Item);
	bool __fastcall UnpackMessage(System::Variant &Item);
	TMessageType __fastcall NextItemType();
	void __fastcall SendPipeMessage(const System::UnicodeString Name);
	void __fastcall PurgePipe();
	__property TOCIAlerterTimeoutCallback OnTimeOut = {read=FOnTimeOut, write=FOnTimeOut};
};


class PASCALIMPLEMENTATION TOCIAlerterListenThread : public System::Classes::TThread
{
	typedef System::Classes::TThread inherited;
	
private:
	TOCIAlerter* FAlerter;
	
protected:
	virtual void __fastcall Execute();
	
public:
	__fastcall TOCIAlerterListenThread(TOCIAlerter* Alerter);
	HIDESBASE void __fastcall Terminate();
public:
	/* TThread.Destroy */ inline __fastcall virtual ~TOCIAlerterListenThread() { }
	
};


class PASCALIMPLEMENTATION TOraClassesUtils : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	__classmethod void __fastcall InternalUnPrepare(TOCIRecordSet* Obj);
public:
	/* TObject.Create */ inline __fastcall TOraClassesUtils() : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TOraClassesUtils() { }
	
};


//-- var, const, procedure ---------------------------------------------------
static constexpr System::Int8 dtUndefined = System::Int8(0x64);
static constexpr System::Int8 dtRowId = System::Int8(0x65);
static constexpr System::Int8 dtURowId = System::Int8(0x66);
static constexpr System::Int8 dtOraBlob = System::Int8(0x67);
static constexpr System::Int8 dtOraClob = System::Int8(0x68);
static constexpr System::Int8 dtWideOraClob = System::Int8(0x69);
static constexpr System::Int8 dtBFILE = System::Int8(0x6a);
static constexpr System::Int8 dtCFILE = System::Int8(0x6b);
static constexpr System::Int8 dtLabel = System::Int8(0x6c);
static constexpr System::Int8 dtTimeStamp = System::Int8(0x6d);
static constexpr System::Int8 dtTimeStampTZ = System::Int8(0x6e);
static constexpr System::Int8 dtTimeStampLTZ = System::Int8(0x6f);
static constexpr System::Int8 dtIntervalYM = System::Int8(0x70);
static constexpr System::Int8 dtIntervalDS = System::Int8(0x71);
static constexpr System::Int8 dtNumber = System::Int8(0x72);
static constexpr System::Int8 dtNumberFloating = System::Int8(0x73);
static constexpr System::Int8 dtBFloat = System::Int8(0x74);
static constexpr System::Int8 dtBDouble = System::Int8(0x75);
static constexpr System::Int8 dtFixedNChar = System::Int8(0x76);
static constexpr System::Int8 dtFixedNWideChar = System::Int8(0x77);
static constexpr System::Int8 dtNString = System::Int8(0x78);
static constexpr System::Int8 dtNWideString = System::Int8(0x79);
static constexpr System::Int8 dtNClob = System::Int8(0x7a);
static constexpr System::Int8 dtAnyData = System::Int8(0x7b);
static constexpr System::Int8 dtBLOBLocator = System::Int8(0x67);
static constexpr System::Int8 dtCLOBLocator = System::Int8(0x68);
static constexpr System::Int8 RowIdSize = System::Int8(0x12);
extern DELPHI_PACKAGE int MaxBlobSize;
static constexpr System::Int8 MaxTransactionIdLength = System::Int8(0x40);
static const TConnectMode DefValConnectMode = (TConnectMode)(0);
extern DELPHI_PACKAGE TOCISQLInfo* OCISQLInfo;
extern DELPHI_PACKAGE int SmallintPrecision;
extern DELPHI_PACKAGE int IntegerPrecision;
extern DELPHI_PACKAGE int LargeIntPrecision;
extern DELPHI_PACKAGE int FloatPrecision;
extern DELPHI_PACKAGE System::UnicodeString BCDPrecision;
extern DELPHI_PACKAGE System::UnicodeString FmtBCDPrecision;
extern DELPHI_PACKAGE bool EnableWideOraClob;
extern DELPHI_PACKAGE bool RemoveCRInStringLiterals;
extern DELPHI_PACKAGE bool UseMaxDataSize;
extern DELPHI_PACKAGE bool NumberAsInteger;
extern DELPHI_PACKAGE bool UseOCI7ProcDesc;
extern DELPHI_PACKAGE bool ForceProcNamedParams;
extern DELPHI_PACKAGE bool UniqueEnvironments;
extern DELPHI_PACKAGE System::TDateTime __fastcall OraDateToDateTime(void * Buf);
extern DELPHI_PACKAGE System::Sysutils::TTimeStamp __fastcall OraDateToTimeStamp(void * Buf);
extern DELPHI_PACKAGE double __fastcall OraDateToMSecs(void * Buf);
extern DELPHI_PACKAGE void __fastcall DateTimeToOraDate(System::TDateTime DateTime, void * Buf);
extern DELPHI_PACKAGE void __fastcall MSecsToOraDate(double MSecs, void * Buf);
extern DELPHI_PACKAGE void __fastcall OraTimeStampToSQLTimeStamp(TOraTimeStamp* OraTimeStamp, Data::Sqltimst::TSQLTimeStamp &SQLTimeStamp);
extern DELPHI_PACKAGE void __fastcall SQLTimeStampToOraTimeStamp(TOraTimeStamp* OraTimeStamp, const Data::Sqltimst::TSQLTimeStamp &SQLTimeStamp);
extern DELPHI_PACKAGE void __fastcall ShareOraTimeStamp(TOraTimeStamp* OraTimeStamp);
extern DELPHI_PACKAGE void __fastcall ShareOraInterval(TOraInterval* OraInterval);
extern DELPHI_PACKAGE void __fastcall ParseConnectString(const System::UnicodeString ConnectString, System::UnicodeString &Username, System::UnicodeString &Password, System::UnicodeString &Server, TConnectMode &ConnectMode);
}	/* namespace Oraclasses */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_ORACLASSES)
using namespace Oraclasses;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// OraclassesHPP
