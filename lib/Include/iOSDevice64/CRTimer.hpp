﻿// CodeGear C++Builder
// Copyright (c) 1995, 2018 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'CRTimer.pas' rev: 33.00 (iOS)

#ifndef CrtimerHPP
#define CrtimerHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.SysUtils.hpp>
#include <System.Classes.hpp>
#include <System.SyncObjs.hpp>
#include <MemData.hpp>
#include <DAConsts.hpp>
#include <CRFunctions.hpp>

//-- user supplied -----------------------------------------------------------

namespace Crtimer
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TCRIntervalThread;
class DELPHICLASS TCRIntervalProcessor;
//-- type declarations -------------------------------------------------------
class PASCALIMPLEMENTATION TCRIntervalThread : public System::Classes::TThread
{
	typedef System::Classes::TThread inherited;
	
private:
	System::Syncobjs::TEvent* FIntervalEvent;
	unsigned FInterval;
	System::Classes::TNotifyEvent FOnTimer;
	bool FEnabled;
	void __fastcall SetEnabled(bool Value);
	void __fastcall SetInterval(unsigned Value);
	void __fastcall SetOnTimer(System::Classes::TNotifyEvent Value);
	void __fastcall UpdateThread();
	
protected:
	virtual void __fastcall Execute();
	
public:
	__fastcall TCRIntervalThread(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TCRIntervalThread();
	__property bool Enabled = {read=FEnabled, write=SetEnabled, default=1};
	__property unsigned Interval = {read=FInterval, write=SetInterval, default=1000};
	__property System::Classes::TNotifyEvent OnTimer = {read=FOnTimer, write=SetOnTimer};
};


class PASCALIMPLEMENTATION TCRIntervalProcessor : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	TCRIntervalThread* FIntervalThread;
	bool __fastcall GetEnabled();
	void __fastcall SetEnabled(bool Value);
	unsigned __fastcall GetInterval();
	void __fastcall SetInterval(unsigned Value);
	System::Classes::TNotifyEvent __fastcall GetOnTimer();
	void __fastcall SetOnTimer(System::Classes::TNotifyEvent Value);
	
public:
	__fastcall TCRIntervalProcessor(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TCRIntervalProcessor();
	__property bool Enabled = {read=GetEnabled, write=SetEnabled, nodefault};
	__property unsigned Interval = {read=GetInterval, write=SetInterval, nodefault};
	__property System::Classes::TNotifyEvent OnTimer = {read=GetOnTimer, write=SetOnTimer};
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Crtimer */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_CRTIMER)
using namespace Crtimer;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// CrtimerHPP
