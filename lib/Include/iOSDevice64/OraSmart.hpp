﻿// CodeGear C++Builder
// Copyright (c) 1995, 2018 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'OraSmart.pas' rev: 33.00 (iOS)

#ifndef OrasmartHPP
#define OrasmartHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.Classes.hpp>
#include <System.SysUtils.hpp>
#include <Data.DB.hpp>
#include <System.Variants.hpp>
#include <CLRClasses.hpp>
#include <CRTypes.hpp>
#include <MemData.hpp>
#include <MemDS.hpp>
#include <CRAccess.hpp>
#include <DBAccess.hpp>
#include <DASQLGenerator.hpp>
#include <OraCall.hpp>
#include <OraClasses.hpp>
#include <Ora.hpp>
#include <OraServices.hpp>
#include <OraSQLGenerator.hpp>
#include <System.Generics.Collections.hpp>
#include <System.Generics.Defaults.hpp>
#include <System.Types.hpp>

//-- user supplied -----------------------------------------------------------

namespace Orasmart
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TSmartQueryOptions;
class DELPHICLASS TSmartSQLGenerator;
class DELPHICLASS TSmartDataSetUpdater;
class DELPHICLASS TSmartDataSetService;
class DELPHICLASS TCustomSmartQuery;
class DELPHICLASS TSmartQuery;
class DELPHICLASS TOraTableOptions;
class DELPHICLASS TOraTable;
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TSmartState : unsigned char { dsState, dsView };

class PASCALIMPLEMENTATION TSmartQueryOptions : public Ora::TOraDataSetOptions
{
	typedef Ora::TOraDataSetOptions inherited;
	
public:
	__fastcall TSmartQueryOptions(Dbaccess::TCustomDADataSet* Owner);
	
__published:
	__property SetFieldsReadOnly = {default=1};
	__property ExtendedFieldsInfo = {default=1};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TSmartQueryOptions() { }
	
};


class PASCALIMPLEMENTATION TSmartSQLGenerator : public Ora::TOraSQLGenerator
{
	typedef Ora::TOraSQLGenerator inherited;
	
protected:
	virtual void __fastcall AddFieldToRefreshSQL(Craccess::TCRFieldDesc* FieldDesc);
	virtual void __fastcall GenerateConditions(Craccess::TDAParamsInfo* ParamsInfo, Clrclasses::WideStringBuilder* SB, const Dasqlgenerator::_TStatementType StatementType, const Craccess::TKeyAndDataFields &KeyAndDataFields, const int Index = 0xffffffff);
	virtual bool __fastcall FieldIsNull(Craccess::TCRFieldDesc* FieldDesc, bool OldValue, Memdata::TData* Data, void * OldRecBuf, void * NewRecBuf)/* overload */;
	virtual bool __fastcall FieldModified(Craccess::TCRFieldDesc* FieldDesc, Memdata::TData* Data, void * OldRecBuf, void * NewRecBuf)/* overload */;
public:
	/* TDASQLGenerator.Create */ inline __fastcall virtual TSmartSQLGenerator(Dasqlgenerator::TSQLGeneratorServiceClass ServiceClass) : Ora::TOraSQLGenerator(ServiceClass) { }
	/* TDASQLGenerator.Destroy */ inline __fastcall virtual ~TSmartSQLGenerator() { }
	
	/* Hoisted overloads: */
	
protected:
	inline bool __fastcall  FieldIsNull(Craccess::TCRFieldDesc* FieldDesc, bool OldValue){ return Dasqlgenerator::TDASQLGenerator::FieldIsNull(FieldDesc, OldValue); }
	inline bool __fastcall  FieldModified(Craccess::TCRFieldDesc* FieldDesc){ return Ora::TOraSQLGenerator::FieldModified(FieldDesc); }
	
};


class PASCALIMPLEMENTATION TSmartDataSetUpdater : public Ora::TOraDataSetUpdater
{
	typedef Ora::TOraDataSetUpdater inherited;
	
protected:
	TSmartDataSetService* FDataSetService;
	TCustomSmartQuery* FDataSet;
	virtual bool __fastcall PerformAppend();
	virtual bool __fastcall PerformUpdate();
	virtual bool __fastcall PerformDelete();
	virtual void __fastcall PrepareUpdate();
	
public:
	__fastcall virtual TSmartDataSetUpdater(Memds::TDataSetService* AOwner);
public:
	/* TDADataSetUpdater.Destroy */ inline __fastcall virtual ~TSmartDataSetUpdater() { }
	
};


class PASCALIMPLEMENTATION TSmartDataSetService : public Ora::TOraDataSetService
{
	typedef Ora::TOraDataSetService inherited;
	
protected:
	TSmartSQLGenerator* FSQLGenerator;
	TSmartDataSetUpdater* FUpdater;
	TCustomSmartQuery* FDataSet;
	virtual void __fastcall CreateSQLGenerator();
	virtual void __fastcall SetSQLGenerator(Dasqlgenerator::TDASQLGenerator* Value);
	virtual void __fastcall CreateDataSetUpdater();
	virtual void __fastcall SetDataSetUpdater(Memds::TDataSetUpdater* Value);
	virtual void __fastcall SetFieldsReadOnly();
	void __fastcall PrepareExpandDataSet(bool Empty, System::UnicodeString &SQL);
	__property bool IsAnyFieldCanBeModified = {read=FIsAnyFieldCanBeModified, write=FIsAnyFieldCanBeModified, nodefault};
	
public:
	__fastcall virtual TSmartDataSetService(Memds::TMemDataSet* AOwner);
public:
	/* TDADataSetService.Destroy */ inline __fastcall virtual ~TSmartDataSetService() { }
	
};


class PASCALIMPLEMENTATION TCustomSmartQuery : public Ora::TCustomOraQuery
{
	typedef Ora::TCustomOraQuery inherited;
	
private:
	bool FExpand;
	Data::Db::TDataSetNotifyEvent FRefreshFields;
	bool FExpanded;
	System::Word FExpandFlagOfs;
	TSmartState FSmartState;
	void __fastcall SetExpand(bool Value);
	bool __fastcall GetCachedUpdates();
	HIDESBASE void __fastcall SetCachedUpdates(const bool Value);
	bool __fastcall GetFullRefresh();
	void __fastcall SetFullRefresh(bool Value);
	
protected:
	Oraclasses::TOCIRecordSet* FRecordData;
	void *FExpandRecBuf;
	void *FOldExpandRecBuf;
	TSmartDataSetService* FDataSetService;
	virtual Memds::TDataSetServiceClass __fastcall GetDataSetServiceClass();
	virtual void __fastcall SetDataSetService(Memds::TDataSetService* Value);
	virtual Dbaccess::TDADataSetOptions* __fastcall CreateOptions();
	virtual void __fastcall InternalOpen();
	virtual void __fastcall InternalClose();
	virtual void __fastcall CreateFieldDefs();
	virtual void __fastcall SetUpdatingTable(const System::UnicodeString Value);
	virtual System::PByte __fastcall AllocRecordBuffer();
	bool __fastcall GetExpandFlag(System::PByte RecBuf);
	void __fastcall SetExpandFlag(System::PByte RecBuf, bool Value);
	void __fastcall AllocExpandRecBufs();
	void __fastcall FreeExpandRecBufs();
	virtual void __fastcall InitRecord(System::PByte Buffer)/* overload */;
	virtual void __fastcall InternalPost();
	virtual void __fastcall InternalCancel();
	virtual bool __fastcall CanRefreshField(Data::Db::TField* Field);
	virtual bool __fastcall GetCanModify();
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	HIDESBASE TSmartQueryOptions* __fastcall GetOptions();
	HIDESBASE void __fastcall SetOptions(TSmartQueryOptions* Value);
	
public:
	__fastcall virtual TCustomSmartQuery(System::Classes::TComponent* Owner);
	__fastcall virtual ~TCustomSmartQuery();
	virtual Memdata::TFieldDesc* __fastcall GetFieldDesc(Data::Db::TField* const Field)/* overload */;
	virtual Memdata::TFieldDesc* __fastcall GetFieldDesc(const int FieldNo)/* overload */;
	virtual System::Classes::TStream* __fastcall CreateBlobStream(Data::Db::TField* Field, Data::Db::TBlobStreamMode Mode);
	virtual bool __fastcall GetFieldData(Data::Db::TField* Field, void * Buffer)/* overload */;
	
protected:
	virtual void __fastcall SetFieldData(Data::Db::TField* Field, void * Buffer)/* overload */;
	
public:
	virtual void __fastcall Post();
	virtual void __fastcall Cancel();
	void __fastcall View();
	virtual int __fastcall GetDataType(const System::UnicodeString FieldName);
	__property bool Expand = {read=FExpand, write=SetExpand, default=0};
	__property TSmartState SmartState = {read=FSmartState, nodefault};
	__property bool FullRefresh = {read=GetFullRefresh, write=SetFullRefresh, default=0};
	__property Data::Db::TDataSetNotifyEvent RefreshFields = {read=FRefreshFields, write=FRefreshFields};
	__property bool CachedUpdates = {read=GetCachedUpdates, write=SetCachedUpdates, default=0};
	__property TSmartQueryOptions* Options = {read=GetOptions, write=SetOptions};
	/* Hoisted overloads: */
	
protected:
	inline void __fastcall  InitRecord(NativeInt Buffer){ Memds::TMemDataSet::InitRecord(Buffer); }
	
public:
	inline Memdata::TFieldDesc* __fastcall  GetFieldDesc(const System::UnicodeString FieldName, const System::UnicodeString TableName){ return Dbaccess::TCustomDADataSet::GetFieldDesc(FieldName, TableName); }
	inline Memdata::TFieldDesc* __fastcall  GetFieldDesc(const System::UnicodeString FieldName){ return Memds::TMemDataSet::GetFieldDesc(FieldName); }
	inline bool __fastcall  GetFieldData(int FieldNo, System::TArray__1<System::Byte> &Buffer){ return Memds::TMemDataSet::GetFieldData(FieldNo, Buffer); }
	inline bool __fastcall  GetFieldData(Data::Db::TField* Field, System::TArray__1<System::Byte> &Buffer){ return Memds::TMemDataSet::GetFieldData(Field, Buffer); }
	inline bool __fastcall  GetFieldData(Data::Db::TField* Field, System::TArray__1<System::Byte> &Buffer, bool NativeFormat){ return Memds::TMemDataSet::GetFieldData(Field, Buffer, NativeFormat); }
	inline bool __fastcall  GetFieldData(int FieldNo, void * Buffer){ return Memds::TMemDataSet::GetFieldData(FieldNo, Buffer); }
	inline bool __fastcall  GetFieldData(Data::Db::TField* Field, void * Buffer, bool NativeFormat){ return Memds::TMemDataSet::GetFieldData(Field, Buffer, NativeFormat); }
	
protected:
	inline void __fastcall  SetFieldData(Data::Db::TField* Field, System::TArray__1<System::Byte> Buffer){ Memds::TMemDataSet::SetFieldData(Field, Buffer); }
	inline void __fastcall  SetFieldData(Data::Db::TField* Field, System::TArray__1<System::Byte> Buffer, bool NativeFormat){ Memds::TMemDataSet::SetFieldData(Field, Buffer, NativeFormat); }
	inline void __fastcall  SetFieldData(Data::Db::TField* Field, void * Buffer, bool NativeFormat){ Memds::TMemDataSet::SetFieldData(Field, Buffer, NativeFormat); }
	
};


class PASCALIMPLEMENTATION TSmartQuery : public TCustomSmartQuery
{
	typedef TCustomSmartQuery inherited;
	
__published:
	__property UpdatingTable = {default=0};
	__property CheckMode = {default=0};
	__property Expand = {default=0};
	__property KeyFields = {default=0};
	__property FullRefresh = {default=0};
	__property DMLRefresh = {default=0};
	__property KeySequence = {default=0};
	__property SequenceMode = {default=1};
	__property RefreshFields;
	__property SQLInsert;
	__property SQLDelete;
	__property SQLUpdate;
	__property SQLRefresh;
	__property SQLLock;
	__property SQLRecCount;
	__property CommandTimeout = {default=0};
	__property DataTypeMap;
	__property Encryption;
	__property SmartFetch;
	__property Session;
	__property SQL;
	__property MasterFields = {default=0};
	__property DetailFields = {default=0};
	__property MasterSource;
	__property Debug = {default=0};
	__property Macros;
	__property Params;
	__property FetchRows = {default=25};
	__property FetchAll = {default=0};
	__property NonBlocking = {default=0};
	__property ReadOnly = {default=0};
	__property UniDirectional = {default=0};
	__property CachedUpdates = {default=0};
	__property AutoCommit = {default=1};
	__property FilterSQL = {default=0};
	__property LockMode = {default=1};
	__property RefreshOptions = {default=0};
	__property Options;
	__property ReturnParams;
	__property LocalConstraints = {default=1};
	__property RefreshMode;
	__property OptionsDS;
	__property BeforeExecute;
	__property AfterExecute;
	__property BeforeUpdateExecute;
	__property AfterUpdateExecute;
	__property OnUpdateError;
	__property OnUpdateRecord;
	__property UpdateObject;
	__property Active = {default=0};
	__property AutoCalcFields = {default=1};
	__property Constraints = {stored=IsConstraintsStored};
	__property Filtered = {default=0};
	__property Filter = {default=0};
	__property FilterOptions = {default=0};
	__property IndexFieldNames = {default=0};
	__property ObjectView = {default=0};
	__property BeforeOpen;
	__property AfterOpen;
	__property BeforeFetch;
	__property AfterFetch;
	__property BeforeClose;
	__property AfterClose;
	__property BeforeInsert;
	__property AfterInsert;
	__property BeforeEdit;
	__property AfterEdit;
	__property BeforePost;
	__property AfterPost;
	__property BeforeCancel;
	__property AfterCancel;
	__property BeforeDelete;
	__property AfterDelete;
	__property BeforeScroll;
	__property AfterScroll;
	__property OnCalcFields;
	__property OnDeleteError;
	__property OnEditError;
	__property OnFilterRecord;
	__property OnNewRecord;
	__property OnPostError;
	__property AfterRefresh;
	__property BeforeRefresh;
	__property Fields;
public:
	/* TCustomSmartQuery.Create */ inline __fastcall virtual TSmartQuery(System::Classes::TComponent* Owner) : TCustomSmartQuery(Owner) { }
	/* TCustomSmartQuery.Destroy */ inline __fastcall virtual ~TSmartQuery() { }
	
};


class PASCALIMPLEMENTATION TOraTableOptions : public TSmartQueryOptions
{
	typedef TSmartQueryOptions inherited;
	
public:
	__fastcall TOraTableOptions(Dbaccess::TCustomDADataSet* Owner);
	
__published:
	__property SetFieldsReadOnly = {default=0};
	__property ExtendedFieldsInfo = {default=0};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TOraTableOptions() { }
	
};


class PASCALIMPLEMENTATION TOraTable : public TCustomSmartQuery
{
	typedef TCustomSmartQuery inherited;
	
private:
	System::UnicodeString FTableName;
	System::UnicodeString FOrderFields;
	void __fastcall SetTableName(const System::UnicodeString Value);
	void __fastcall SetOrderFields(const System::UnicodeString Value);
	
protected:
	virtual System::UnicodeString __fastcall PSGetTableName();
	virtual void __fastcall PSSetParams(Data::Db::TParams* AParams);
	virtual void __fastcall PSSetCommandText(const System::UnicodeString CommandText);
	virtual void __fastcall OpenCursor(bool InfoQuery);
	virtual Dbaccess::TDADataSetOptions* __fastcall CreateOptions();
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	HIDESBASE TOraTableOptions* __fastcall GetOptions();
	HIDESBASE void __fastcall SetOptions(TOraTableOptions* Value);
	virtual bool __fastcall SQLAutoGenerated();
	virtual void __fastcall CreateFields();
	virtual void __fastcall CheckSQL();
	virtual void __fastcall SetFilterSQL(const System::UnicodeString Value);
	
public:
	void __fastcall PrepareSQL();
	virtual void __fastcall Prepare();
	virtual void __fastcall Execute()/* overload */;
	HIDESBASE void __fastcall EmptyTable();
	
__published:
	__property System::UnicodeString TableName = {read=FTableName, write=SetTableName};
	__property System::UnicodeString OrderFields = {read=FOrderFields, write=SetOrderFields};
	__property MasterFields = {default=0};
	__property DetailFields = {default=0};
	__property MasterSource;
	__property ReadOnly = {default=0};
	__property CheckMode = {default=0};
	__property KeyFields = {default=0};
	__property KeySequence = {default=0};
	__property SequenceMode = {default=1};
	__property DMLRefresh = {default=0};
	__property RefreshFields;
	__property CommandTimeout = {default=0};
	__property DataTypeMap;
	__property Encryption;
	__property SmartFetch;
	__property Session;
	__property FilterSQL = {default=0};
	__property Debug = {default=0};
	__property Params;
	__property FetchRows = {default=25};
	__property FetchAll = {default=0};
	__property NonBlocking = {default=0};
	__property UniDirectional = {default=0};
	__property CachedUpdates = {default=0};
	__property AutoCommit = {default=1};
	__property LockMode = {default=1};
	__property RefreshOptions = {default=0};
	__property LocalConstraints = {default=1};
	__property RefreshMode;
	__property OptionsDS;
	__property OnUpdateError;
	__property OnUpdateRecord;
	__property UpdateObject;
	__property Active = {default=0};
	__property AutoCalcFields = {default=1};
	__property Constraints = {stored=IsConstraintsStored};
	__property Filtered = {default=0};
	__property Filter = {default=0};
	__property FilterOptions = {default=0};
	__property IndexFieldNames = {default=0};
	__property ObjectView = {default=0};
	__property BeforeOpen;
	__property AfterOpen;
	__property BeforeClose;
	__property AfterClose;
	__property BeforeInsert;
	__property AfterInsert;
	__property BeforeEdit;
	__property AfterEdit;
	__property BeforePost;
	__property AfterPost;
	__property BeforeCancel;
	__property AfterCancel;
	__property BeforeDelete;
	__property AfterDelete;
	__property BeforeScroll;
	__property AfterScroll;
	__property OnCalcFields;
	__property OnDeleteError;
	__property OnEditError;
	__property OnFilterRecord;
	__property OnNewRecord;
	__property OnPostError;
	__property AfterRefresh;
	__property BeforeRefresh;
	__property Fields;
	__property TOraTableOptions* Options = {read=GetOptions, write=SetOptions};
public:
	/* TCustomSmartQuery.Create */ inline __fastcall virtual TOraTable(System::Classes::TComponent* Owner) : TCustomSmartQuery(Owner) { }
	/* TCustomSmartQuery.Destroy */ inline __fastcall virtual ~TOraTable() { }
	
	/* Hoisted overloads: */
	
public:
	inline void __fastcall  Execute(int Iters, int Offset = 0x0){ Dbaccess::TCustomDADataSet::Execute(Iters, Offset); }
	
};


//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE int RefreshInterval;
}	/* namespace Orasmart */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_ORASMART)
using namespace Orasmart;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// OrasmartHPP
