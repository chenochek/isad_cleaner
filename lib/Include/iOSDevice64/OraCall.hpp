﻿// CodeGear C++Builder
// Copyright (c) 1995, 2018 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'OraCall.pas' rev: 33.00 (iOS)

#ifndef OracallHPP
#define OracallHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.SysUtils.hpp>
#include <System.Classes.hpp>
#include <System.SyncObjs.hpp>
#include <System.Types.hpp>
#include <CLRClasses.hpp>
#include <CRTypes.hpp>
#include <MemUtils.hpp>
#include <DAConsts.hpp>
#include <System.Generics.Collections.hpp>

//-- user supplied -----------------------------------------------------------
namespace Oracall {
  #pragma option push -b-
  enum TChangeNotifyOperation { cnoInsert, cnoUpdate, cnoDelete, cnoAllRows, cnoAlter, cnoDrop };
  typedef Set<TChangeNotifyOperation, cnoInsert, cnoDrop> TChangeNotifyOperations;
  typedef TChangeNotifyOperation TChangeNotifyDMLOperation;
  typedef Set<TChangeNotifyDMLOperation, cnoInsert, cnoDelete> TChangeNotifyDMLOperations;
  #pragma option pop
}

namespace Oracall
{
//-- forward type declarations -----------------------------------------------
struct TRD;
struct TRowId7;
struct TCDAHead;
struct TCDA;
struct DECLSPEC_DRECORD OCIDirPathCtx
{
};


struct DECLSPEC_DRECORD OCIDirPathColArray
{
};


struct DECLSPEC_DRECORD OCIDirPathStream
{
};


struct DECLSPEC_DRECORD OCIDirPathDesc
{
};


struct TRowId8;
struct TRowId81;
struct OCIRowid;
struct OCIRowid81;
struct OCINumber;
struct OCITime;
struct OCIDate;
struct TOCIFoCbkStruct;
#pragma pack(push,1)
struct DECLSPEC_DRECORD OCIString
{
};
#pragma pack(pop)


#pragma pack(push,1)
struct DECLSPEC_DRECORD OCIRaw
{
};
#pragma pack(pop)


#pragma pack(push,1)
struct DECLSPEC_DRECORD OCIRef
{
};
#pragma pack(pop)


#pragma pack(push,1)
struct DECLSPEC_DRECORD OCIColl
{
};
#pragma pack(pop)


#pragma pack(push,1)
struct DECLSPEC_DRECORD OCIArray
{
};
#pragma pack(pop)


#pragma pack(push,1)
struct DECLSPEC_DRECORD OCITable
{
};
#pragma pack(pop)


#pragma pack(push,1)
struct DECLSPEC_DRECORD OCIIter
{
};
#pragma pack(pop)


struct TXID;
struct DECLSPEC_DRECORD OCIXMLType
{
};


struct DECLSPEC_DRECORD OCIDOMDocument
{
};


class DELPHICLASS TOCIAPI;
class DELPHICLASS TOCI7API;
class DELPHICLASS TOCI8API;
class DELPHICLASS TDirectAPI;
class DELPHICLASS TOracleHome;
class DELPHICLASS TDirectHome;
class DELPHICLASS TOracleHomes;
class DELPHICLASS TOCIEnvironment;
class DELPHICLASS TDirectEnvironment;
class DELPHICLASS TOCISvcCtx;
class DELPHICLASS TDirectServerInfo;
class DELPHICLASS EOCIInitError;
//-- type declarations -------------------------------------------------------
typedef int eword;

typedef unsigned uword;

typedef int sword;

typedef System::Int8 eb1;

typedef System::Byte ub1;

typedef System::Int8 sb1;

typedef System::Byte *pub1;

typedef System::Int8 *psb1;

typedef pub1 *ppub1;

typedef ppub1 *pppub1;

typedef System::Word *pub2;

typedef short *psb2;

typedef pub2 *ppub2;

typedef int *psb4;

typedef unsigned *pub4;

typedef pub4 *ppub4;

typedef __int64 *pub8;

typedef pub8 *ppub8;

typedef void * *PPointer;

typedef int *pbool;

typedef short eb2;

typedef System::Word ub2;

typedef short sb2;

typedef int eb4;

typedef unsigned ub4;

typedef int sb4;

typedef __int64 ub8;

typedef int tbool;

#pragma pack(push,1)
struct DECLSPEC_DRECORD TRD
{
public:
	unsigned rcs4;
	System::Word rcs5;
	System::Byte rcs6;
};
#pragma pack(pop)


typedef TRD *PTRD;

#pragma pack(push,1)
struct DECLSPEC_DRECORD TRowId7
{
public:
	TRD rd;
	unsigned rcs7;
	unsigned rcs8;
};
#pragma pack(pop)


typedef TRowId7 *PRowId7;

#pragma pack(push,1)
struct DECLSPEC_DRECORD TCDAHead
{
public:
	short v2_rc;
	System::Word ft;
	unsigned rpc;
	System::Word peo;
	System::Byte fc;
	System::Byte rcs1;
	System::Word rc;
	System::Byte wrn;
	System::Byte rcs2;
	int rcs3;
	TRowId7 rid;
	int ose;
	System::Byte chk;
	void *rcsp;
};
#pragma pack(pop)


#pragma pack(push,1)
struct DECLSPEC_DRECORD TCDA
{
public:
	short v2_rc;
	System::Word ft;
	unsigned rpc;
	System::Word peo;
	System::Byte fc;
	System::Byte rcs1;
	System::Word rc;
	System::Byte wrn;
	System::Byte rcs2;
	int rcs3;
	TRowId7 rid;
	int ose;
	System::Byte chk;
	void *rcsp;
	System::StaticArray<System::Byte, 16> rcs9;
};
#pragma pack(pop)


typedef TCDA *PCDA;

typedef TCDA TLDA;

typedef PCDA PLDA;

typedef System::StaticArray<System::Byte, 256> THDA;

typedef THDA *PHDA;

typedef int __cdecl (*_obindps)(void * cursor, System::Byte opcode, char * sqlvar, int sqlvl, pub1 pvctx, int progvl, int ftype, int scale, psb2 indp, pub2 alen, pub2 arcode, int pv_skip, int ind_skip, int alen_skip, int rc_skip, unsigned maxsiz, pub4 cursiz, void * fmt, int fmtl, int fmtt);

typedef int __cdecl (*_obreak)(void * lda);

typedef int __cdecl (*_ocan)(void * cursor);

typedef int __cdecl (*_oclose)(void * cursor);

typedef int __cdecl (*_ocof)(void * lda);

typedef int __cdecl (*_ocom)(void * lda);

typedef int __cdecl (*_ocon)(void * lda);

typedef int __cdecl (*_odefinps)(void * cursor, System::Byte opcode, int pos, void * bufctx, int bufl, int ftype, int scale, psb2 indp, void * fmt, int fmtl, int fmtt, pub2 rlen, pub2 rcode, int pv_skip, int ind_skip, int alen_skip, int rc_skip);

typedef int __cdecl (*_odessp)(void * lda, char * objnam, NativeUInt onlen, pub1 rsv1, NativeUInt rsv1ln, pub1 rsv2, NativeUInt rsv2ln, pub2 ovrld, pub2 pos, pub2 level, void * argnam, pub2 arnlen, pub2 dtype, pub1 defsup, pub1 mode, pub4 dtsiz, psb2 prec, psb2 scale, pub1 radix, pub4 spare, unsigned &arrsiz);

typedef int __cdecl (*_odescr)(void * cursor, int pos, int &dbsize, short &dbtype, void * cbuf, int &cbufl, int &dsize, short &prec, short &scale, short &nullok);

typedef int __cdecl (*_oerhms)(void * lda, short rcode, void * buf, int bufsiz);

typedef int __cdecl (*_oermsg)(short rcode, char * buf);

typedef int __cdecl (*_oexec)(void * cursor);

typedef int __cdecl (*_oexfet)(void * cursor, unsigned nrows, int cancel, int exact);

typedef int __cdecl (*_oexn)(void * cursor, int iters, int rowoff);

typedef int __cdecl (*_ofen)(void * cursor, int nrows);

typedef int __cdecl (*_ofetch)(void * cursor);

typedef int __cdecl (*_oflng)(void * cursor, int pos, pub1 buf, int bufl, int dtype, pub4 retl, int offset);

typedef int __cdecl (*_ogetpi)(void * cursor, System::Byte &piecep, void * &ctxpp, unsigned &iterp, unsigned &indexp);

typedef int __cdecl (*_oopt)(void * cursor, int rbopt, int waitopt);

typedef int __cdecl (*_opinit)(unsigned mode);

typedef int __cdecl (*_olog)(void * lda, PHDA hda, char * uid, int uidl, char * pswd, int pswdl, char * conn, int connl, unsigned mode);

typedef int __cdecl (*_ologof)(void * lda);

typedef int __cdecl (*_oopen)(void * cursor, void * lda, void * dbn, int dbnl, int arsize, void * uid, int uidl);

typedef int __cdecl (*_oparse)(void * cursor, char * sqlstm, int sqllen, int defflg, unsigned lngflg);

typedef int __cdecl (*_orol)(void * lda);

typedef int __cdecl (*_osetpi)(void * cursor, System::Byte piece, void * bufp, unsigned &lenp);

typedef void __cdecl (*_sqlld2)(void * lda, char * cname, psb4 cnlen);

typedef void __cdecl (*_sqllda)(void * lda);

typedef int __cdecl (*_onbset)(void * lda);

typedef int __cdecl (*_onbtst)(void * lda);

typedef int __cdecl (*_onbclr)(void * lda);

typedef int __cdecl (*_ognfd)(void * lda, void * fdp);

typedef int __cdecl (*_obndra)(void * cursor, char * sqlvar, int sqlvl, pub1 progv, int progvl, int ftype, int scale, psb2 indp, pub2 alen, pub2 arcode, unsigned maxsiz, pub4 cursiz, void * fmt, int fmtl, int fmtt);

typedef int __cdecl (*_obndrn)(void * cursor, int sqlvn, pub1 progv, int progvl, int ftype, int scale, psb2 indp, char * fmt, int fmtl, int fmtt);

typedef int __cdecl (*_obndrv)(void * cursor, char * sqlvar, int sqlvl, void * progv, int progvl, int ftype, int scale, psb2 indp, void * fmt, int fmtl, int fmtt);

typedef int __cdecl (*_odefin)(void * cursor, int pos, void * buf, int bufl, int ftype, int scale, psb2 indp, void * fmt, int fmtl, int fmtt, pub2 rlen, pub2 rcode);

typedef OCIDirPathCtx *pOCIDirPathCtx;

typedef OCIDirPathColArray *pOCIDirPathColArray;

typedef OCIDirPathStream *pOCIDirPathStream;

typedef OCIDirPathDesc *pOCIDirPathDesc;

#pragma pack(push,1)
struct DECLSPEC_DRECORD TRowId8
{
public:
	unsigned ridobjnum;
	System::Word ridfilenum;
	System::Word filler;
	unsigned ridblocknum;
	System::Word ridslotnum;
};
#pragma pack(pop)


typedef TRowId8 *PRowid8;

#pragma pack(push,1)
struct DECLSPEC_DRECORD TRowId81
{
public:
	System::Byte filler;
	unsigned ridobjnum;
	System::Word ridfilenum;
	unsigned ridblocknum;
	System::Word ridslotnum;
};
#pragma pack(pop)


typedef TRowId81 *PRowid81;

#pragma pack(push,1)
struct DECLSPEC_DRECORD OCIRowid
{
public:
	System::StaticArray<System::Byte, 8> Unknown;
	TRowId8 RowId;
};
#pragma pack(pop)


typedef OCIRowid *POCIRowid;

#pragma pack(push,1)
struct DECLSPEC_DRECORD OCIRowid81
{
public:
	System::StaticArray<System::Byte, 8> Unknown;
	TRowId81 *RowId;
};
#pragma pack(pop)


typedef OCIRowid81 *POCIRowid81;

struct DECLSPEC_DRECORD OCINumber
{
public:
	System::StaticArray<System::Byte, 22> OCINumberPart;
};


#pragma pack(push,1)
struct DECLSPEC_DRECORD OCITime
{
public:
	System::Byte OCITimeHH;
	System::Byte OCITimeMI;
	System::Byte OCITimeSS;
};
#pragma pack(pop)


#pragma pack(push,1)
struct DECLSPEC_DRECORD OCIDate
{
public:
	short OCIDateYYYY;
	System::Byte OCIDateMM;
	System::Byte OCIDateDD;
	OCITime OCIDateTime;
};
#pragma pack(pop)


typedef int __cdecl (*TOCICallbackFailover)(void * svchp, void * envhp, void * fo_ctx, unsigned fo_type, unsigned fo_event);

#pragma pack(push,1)
struct DECLSPEC_DRECORD TOCIFoCbkStruct
{
public:
	void *callback_function;
	void *fo_ctx;
};
#pragma pack(pop)


typedef void * pOCIBind;

typedef void * pOCIDefine;

typedef void * pOCIDescribe;

typedef void * pOCIEnv;

typedef void * pOCIError;

typedef void * pOCIServer;

typedef void * pOCISession;

typedef void * pOCIStmt;

typedef void * pOCISvcCtx;

typedef void * pOCITrans;

typedef void * pOCIComplexObject;

typedef void * pOCISPool;

typedef void * pOCIAuthInfo;

typedef void * pOCIParam;

typedef void * pOCISnapshot;

typedef void * pOCILobLocator;

typedef void * pOCIDateTime;

typedef void * pOCIInterval;

typedef void * pOCIType;

typedef void * ppOCIString;

typedef void * pOCINumber;

typedef void * pOCIDate;

typedef void * pOCIString;

typedef void * pOCIRaw;

typedef void * pOCIRef;

typedef void * pOCIColl;

typedef void * pOCIArray;

typedef void * pOCITable;

typedef void * pOCIExtProcContext;

typedef void * pOCISubscription;

typedef void * *ppOCIEnv;

typedef void * *ppOCIBind;

typedef void * *ppOCIDefine;

typedef void * *ppOCIStmt;

typedef void * *ppOCISvcCtx;

typedef void * pOCIHandle;

typedef void * *ppOCIHandle;

typedef int OCIParam;

typedef int OCISnapshot;

typedef int OCILobLocator;

typedef int OCIDateTime;

typedef int OCIInterval;

typedef void * pOCIDescriptor;

typedef void * *ppOCIParam;

typedef void * *ppOCILobLocator;

typedef void * *ppOCIDescriptor;

typedef void * *ppOCIDateTime;

typedef void * *ppOCIInterval;

typedef int OCIType;

typedef void * *ppOCIType;

typedef void * *ppOCIRef;

typedef int tenum;

typedef System::Word OCIDuration;

typedef short OCIInd;

typedef int OCILockOpt;

typedef int OCIMarkOpt;

typedef int OCIObjectEvent;

typedef int OCIObjectProperty;

typedef int OCIPinOpt;

typedef int OCIRefreshOpt;

typedef System::Word OCITypeCode;

typedef int OCITypeEncap;

typedef int OCITypeGetOpt;

typedef int OCITypeMethodFlag;

typedef int OCITypeParamMode;

typedef System::Byte OCIObjectPropId;

typedef int OCIObjectLifetime;

typedef unsigned OCIObjectMarkstatus;

typedef short *pOCIInd;

#pragma pack(push,1)
struct DECLSPEC_DRECORD TXID
{
public:
	int FormatID;
	int Gtrid_length;
	int Bqual_length;
	System::StaticArray<System::Byte, 128> Data;
};
#pragma pack(pop)


typedef int __cdecl (*TOCISubscriptionNotify)(void * pCtx, void * pSubscrHp, void * pPayload, unsigned iPayloadLen, void * pDescriptor, unsigned iMode);

typedef int __cdecl (*_OCIAttrGet1)(void * trgthndlp, unsigned trghndltyp, void * attributep, pub4 sizep, unsigned attrtype, void * errhp);

typedef int __cdecl (*_OCIAttrGet2)(void * trgthndlp, unsigned trghndltyp, int &attributep, pub4 sizep, unsigned attrtype, void * errhp);

typedef int __cdecl (*_OCIAttrSet1)(void * trgthndlp, unsigned trghndltyp, void * attributep, unsigned size, unsigned attrtype, void * errhp);

typedef int __cdecl (*_OCIAttrSet2)(void * trgthndlp, unsigned trghndltyp, int &attributep, unsigned size, unsigned attrtype, void * errhp);

typedef int __cdecl (*_OCIBindArrayOfStruct)(void * bindp, void * errhp, unsigned pvskip, unsigned indskip, unsigned alskip, unsigned rcskip);

typedef int __cdecl (*_OCIBindByName)(void * stmtp, void * &bindpp, void * errhp, void * placeholder, int placeh_len, void * valuep, int value_sz, System::Word dty, void * indp, pub2 alenp, pub2 rcodep, unsigned maxarr_len, pub4 curelep, unsigned mode);

typedef int __cdecl (*_OCIBindByPos)(void * stmtp, void * &bindpp, void * errhp, unsigned position, void * valuep, int value_sz, System::Word dty, void * indp, pub2 alenp, pub2 rcodep, unsigned maxarr_len, pub4 curelep, unsigned mode);

typedef int __cdecl (*TOCICallbackInBind)(void * ictxp, void * bindp, unsigned iter, unsigned index, void * &bufpp, unsigned &alenp, System::Byte &piecep, pOCIInd &indpp);

typedef int __cdecl (*TOCICallbackOutBind)(void * octxp, void * bindp, unsigned iter, unsigned index, void * &bufpp, pub4 &alenpp, System::Byte &piecep, pOCIInd &indpp, pub2 &rcodepp);

typedef int __cdecl (*_OCIBindDynamic)(void * bindp, void * errhp, void * ictxp, void * icbfp, void * octxp, void * ocbfp);

typedef int __cdecl (*_OCIBindObject)(void * bindp, void * errhp, const void * otype, void * pgvpp, pub4 pvszsp, pOCIInd indpp, pub4 indszp);

typedef int __cdecl (*_OCIBreak)(void * hndlp, void * errhp);

typedef int __cdecl (*_OCIDefineArrayOfStruct)(void * defnp, void * errhp, unsigned pvskip, unsigned indskip, unsigned rlskip, unsigned rcskip);

typedef int __cdecl (*_OCIDefineByPos)(void * stmtp, void * &defnpp, void * errhp, unsigned position, void * valuep, int value_sz, System::Word dty, void * indp, pub2 rlenp, pub2 rcodep, unsigned mode);

typedef int __cdecl (*TOCICallbackDefine)(void * octxp, void * defnp, unsigned iter, void * &bufpp, pub4 &alenpp, System::Byte &piecep, pOCIInd &indpp, pub2 &rcodep);

typedef int __cdecl (*_OCIDefineDynamic)(void * defnp, void * errhp, void * octxp, void * ocbfp);

typedef int __cdecl (*_OCIDefineObject)(void * defnp, void * errhp, const void * otype, void * pgvpp, pub4 pvszsp, pOCIInd indpp, pub4 indszp);

typedef int __cdecl (*_OCIDescribeAny)(void * svchp, void * errhp, void * objptr, unsigned objnm_len, System::Byte objptr_typ, System::Byte info_level, System::Byte objtyp, void * dschp);

typedef int __cdecl (*_OCIDescriptorAlloc)(void * parenth, void * &descpp, unsigned dtype, NativeUInt xtramem_sz, void * usrmempp);

typedef int __cdecl (*_OCIDescriptorFree)(void * descp, unsigned dtype);

typedef int __cdecl (*_OCIEnvInit)(void * &envhpp, unsigned mode, NativeUInt xtramemsz, void * usrmempp);

typedef int __cdecl (*_OCIErrorGet)(void * hndlp, unsigned recordno, void * sqlstate, int &errcodep, void * bufp, unsigned bufsiz, unsigned htype);

typedef int __cdecl (*_OCIHandleAlloc)(void * parenth, void * &hndlpp, unsigned htype, NativeUInt xtramem_sz, void * usrmempp);

typedef int __cdecl (*_OCIHandleFree)(void * hndlp, unsigned htype);

typedef int __cdecl (*_OCIInitialize)(unsigned mode, void * ctxp, void * malocfp, void * ralocfp, void * mfreefp);

typedef int __cdecl (*_OCILdaToSvcCtx)(void * &svchpp, void * errhp, void * ldap);

typedef int __cdecl (*_OCIParamGet)(void * hndlp, unsigned htype, void * errhp, void * &parmdpp, unsigned pos);

typedef int __cdecl (*_OCIPasswordChange)(void * svchp, void * errhp, const void * user_name, unsigned usernm_len, const void * opasswd, unsigned opasswd_len, const void * npasswd, int npasswd_len, unsigned mode);

typedef int __cdecl (*_OCIReset)(void * hndlp, void * errhp);

typedef int __cdecl (*_OCIServerAttach)(void * srvhp, void * errhp, void * dblink, int dblink_len, unsigned mode);

typedef int __cdecl (*_OCIServerDetach)(void * srvhp, void * errhp, unsigned mode);

typedef int __cdecl (*_OCIServerVersion)(void * hndlp, void * errhp, void * bufp, unsigned bufsz, System::Byte hndltype);

typedef int __cdecl (*_OCISessionBegin)(void * svchp, void * errhp, void * usrhp, unsigned credt, unsigned mode);

typedef int __cdecl (*_OCISessionEnd)(void * svchp, void * errhp, void * usrhp, unsigned mode);

typedef int __cdecl (*_OCISessionGet)(void * envhp, void * errhp, void * &svchp, void * authhp, void * poolName, unsigned poolName_len, void * tagInfo, unsigned tagInfo_len, void * &retTagInfo, unsigned &retTagInfo_len, System::LongBool &found, unsigned mode);

typedef int __cdecl (*_OCISessionRelease)(void * svchp, void * errhp, void * tag, unsigned tag_len, unsigned mode);

typedef int __cdecl (*_OCISessionPoolCreate)(void * envhp, void * errhp, void * spoolhp, void * &poolName, unsigned &poolNameLen, void * connStr, unsigned connStrLen, unsigned sessMin, unsigned sessMax, unsigned sessIncr, void * userid, unsigned useridLen, void * password, unsigned passwordLen, unsigned mode);

typedef int __cdecl (*_OCISessionPoolDestroy)(void * spoolhp, void * errhp, unsigned mode);

typedef int __cdecl (*_OCITransStart)(void * svchp, void * errhp, System::Word timeout, unsigned flags);

typedef int __cdecl (*_OCITransRollback)(void * svchp, void * errhp, unsigned flags);

typedef int __cdecl (*_OCITransCommit)(void * svchp, void * errhp, unsigned flags);

typedef int __cdecl (*_OCITransDetach)(void * svchp, void * errhp, unsigned flags);

typedef int __cdecl (*_OCITransPrepare)(void * svchp, void * errhp, unsigned flags);

typedef int __cdecl (*_OCITransForget)(void * svchp, void * errhp, unsigned flags);

typedef int __cdecl (*_OCIStmtExecute)(void * svchp, void * stmtp, void * errhp, unsigned iters, unsigned rowoff, void * snap_in, void * snap_out, unsigned mode);

typedef int __cdecl (*_OCIStmtFetch)(void * stmtp, void * errhp, unsigned nrows, System::Word orientation, unsigned mode);

typedef int __cdecl (*_OCIStmtGetPieceInfo)(void * stmtp, void * errhp, void * &hndlpp, pub4 htypep, pub1 in_outp, pub4 iterp, pub4 idxp, pub1 piecep);

typedef int __cdecl (*_OCIStmtPrepare)(void * stmtp, void * errhp, void * stmt, unsigned stmt_len, unsigned language, unsigned mode);

typedef int __cdecl (*_OCIStmtPrepare2)(void * svchp, void * &stmtp, void * errhp, void * stmt, unsigned stmt_len, void * key, unsigned key_len, unsigned language, unsigned mode);

typedef int __cdecl (*_OCIStmtPrepare3)(void * svchp, void * stmtp, void * errhp, void * stmt, unsigned stmt_len, unsigned language, unsigned mode);

typedef int __cdecl (*_OCIStmtRelease)(void * stmtp, void * errhp, void * key, unsigned key_len, unsigned mode);

typedef int __cdecl (*_OCIStmtGetNextResult)(void * stmtp, void * errhp, ppOCIStmt ppresult, /* out */ unsigned &rtype, unsigned mode);

typedef int __cdecl (*_OCIStmtSetPieceInfo)(void * hndlp, unsigned htype, void * errhp, const void * bufp, pub4 alenp, System::Byte piece, const void * indp, pub2 rcodep);

typedef int __cdecl (*_OCISvcCtxToLda)(void * srvhp, void * errhp, void * ldap);

typedef int __cdecl (*_OCILobAppend)(void * svchp, void * errhp, void * dst_locp, void * src_locp);

typedef int __cdecl (*_OCILobAssign)(void * envhp, void * errhp, const void * src_locp, void * &dst_locpp);

typedef int __cdecl (*_OCILobCharSetForm)(void * envhp, void * errhp, const void * locp, System::Byte &csfrm);

typedef int __cdecl (*_OCILobCharSetId)(void * envhp, void * errhp, const void * locp, pub2 csid);

typedef int __cdecl (*_OCILobCopy)(void * svchp, void * errhp, void * dst_locp, void * src_locp, unsigned amount, unsigned dst_offset, unsigned src_offset);

typedef int __cdecl (*_OCILobOpen)(void * svchp, void * errhp, void * locp, System::Byte mode);

typedef int __cdecl (*_OCILobClose)(void * svchp, void * errhp, void * locp);

typedef int __cdecl (*_OCILobIsOpen)(void * svchp, void * errhp, void * locp, System::LongBool &flag);

typedef int __cdecl (*_OCILobCreateTemporary)(void * svchp, void * errhp, void * locp, System::Word csid, System::Byte csfrm, System::Byte lobtype, int cache, System::Word duration);

typedef int __cdecl (*_OCILobFreeTemporary)(void * svchp, void * errhp, void * locp);

typedef int __cdecl (*_OCILobIsTemporary)(void * envhp, void * errhp, void * locp, System::LongBool &is_temporary);

typedef int __cdecl (*_OCILobDisableBuffering)(void * svchp, void * errhp, void * locp);

typedef int __cdecl (*_OCILobEnableBuffering)(void * svchp, void * errhp, void * locp);

typedef int __cdecl (*_OCILobErase)(void * svchp, void * errhp, void * locp, pub4 amount, unsigned offset);

typedef int __cdecl (*_OCILobFileClose)(void * svchp, void * errhp, void * filep);

typedef int __cdecl (*_OCILobFileExists)(void * svchp, void * errhp, void * filep, int &flag);

typedef int __cdecl (*_OCILobFileGetName)(void * envhp, void * errhp, const void * filep, void * dir_alias, pub2 d_length, void * filename, pub2 f_length);

typedef int __cdecl (*_OCILobFileIsOpen)(void * svchp, void * errhp, void * filep, int &flag);

typedef int __cdecl (*_OCILobFileOpen)(void * svchp, void * errhp, void * filep, System::Byte mode);

typedef int __cdecl (*_OCILobFileSetName)(void * envhp, void * errhp, ppOCILobLocator filepp, const void * dir_alias, System::Word d_length, const void * filename, System::Word f_length);

typedef int __cdecl (*_OCILobFlushBuffer)(void * svchp, void * errhp, void * locp, unsigned flag);

typedef int __cdecl (*_OCILobGetLength)(void * svchp, void * errhp, void * locp, unsigned &lenp);

typedef int __cdecl (*_OCILobIsEqual)(void * envhp, const void * x, const void * y, pbool is_equal);

typedef int __cdecl (*_OCILobLoadFromFile)(void * svchp, void * errhp, void * dst_locp, void * src_locp, unsigned amount, unsigned dst_offset, unsigned src_offset);

typedef int __cdecl (*_OCILobLocatorIsInit)(void * envhp, void * errhp, const void * locp, int &is_initialized);

typedef int __cdecl (*_OCILobRead)(void * svchp, void * errhp, void * locp, unsigned &amtp, unsigned offset, void * bufp, unsigned bufl, void * ctxp, void * cbfp, System::Word csid, System::Byte csfrm);

typedef int __cdecl (*_OCILobRead2)(void * svchp, void * errhp, void * locp, __int64 &byte_amtp, __int64 &char_amtp, __int64 offset, void * bufp, __int64 bufl, System::Byte piece, void * ctxp, void * cbfp, System::Word csid, System::Byte csfrm);

typedef int __cdecl (*_OCILobTrim)(void * svchp, void * errhp, void * locp, unsigned newlen);

typedef int __cdecl (*_OCILobWrite)(void * svchp, void * errhp, void * locp, unsigned &amtp, unsigned offset, void * bufp, unsigned bufl, System::Byte piece, void * ctxp, void * cbfp, System::Word csid, System::Byte csfrm);

typedef void * __cdecl (*TGetFlushRef)(void * context, pub1 last);

typedef int __cdecl (*_OCICacheFlush)(void * env, void * err, const void * svc, void * context, TGetFlushRef get, void * &ref);

typedef int __cdecl (*_OCICacheFree)(void * env, void * err, const void * svc);

typedef void * __cdecl (*TGetRefreshRef)(void * context);

typedef int __cdecl (*_OCICacheRefresh)(void * env, void * err, const void * svc, int option, void * context, TGetRefreshRef get, void * &ref);

typedef int __cdecl (*_OCICacheUnmark)(void * env, void * err, const void * svc);

typedef int __cdecl (*_OCICacheUnpin)(void * env, void * err, const void * svc);

typedef int __cdecl (*_OCIObjectCopy)(void * env, void * err, const void * svc, void * source, void * null_source, void * target, void * null_target, void * tdo, System::Word duration, System::Byte option);

typedef int __cdecl (*_OCIObjectExists)(void * env, void * err, void * ins, int &exist);

typedef int __cdecl (*_OCIObjectFlush)(void * env, void * err, void * pobject);

typedef int __cdecl (*_OCIObjectFree)(void * env, void * err, void * instance, System::Word flags);

typedef void * __cdecl (*_OCIObjectPtr)(void * env, void * err, void * instance);

typedef int __cdecl (*_OCIObjectGetAttr)(void * env, void * err, void * instance, void * null_struct, void * tdo, void * namesp, const pub4 lengths, const unsigned name_count, const pub4 indexes, const unsigned index_count, pOCIInd attr_null_status, void * attr_null_structp, void * attr_valuep, ppOCIType attr_tdop);

typedef int __cdecl (*_OCIObjectGetInd)(void * env, void * err, void * instance, void * null_structp);

typedef int __cdecl (*_OCIObjectGetObjectRef)(void * env, void * err, void * pobject, void * object_ref);

typedef int __cdecl (*_OCIObjectGetProperty)(void * env, void * err, const void * obj, System::Byte propertyId, System::Byte prop, pub4 size);

typedef int __cdecl (*_OCIObjectGetTypeRef)(void * env, void * err, void * instance, void * type_ref);

typedef int __cdecl (*_OCIObjectIsDirty)(void * env, void * err, void * ins, int &dirty);

typedef int __cdecl (*_OCIObjectIsLocked)(void * env, void * err, void * ins, int &lock);

typedef int __cdecl (*_OCIObjectLock)(void * env, void * err, void * pobject);

typedef int __cdecl (*_OCIObjectMarkDelete)(void * env, void * err, void * instance);

typedef int __cdecl (*_OCIObjectMarkDeleteByRef)(void * env, void * err, void * object_ref);

typedef int __cdecl (*_OCIObjectMarkUpdate)(void * env, void * err, void * pobject);

typedef int __cdecl (*_OCIObjectNew)(void * env, void * err, const void * svc, System::Word typecode, void * tdo, void * table, System::Word duration, int value, void * &instance);

typedef int __cdecl (*_OCIObjectPin)(void * env, void * err, void * object_ref, void * corhdl, int pin_option, System::Word pin_duration, int lock_option, void * pobjectp);

typedef int __cdecl (*_OCIObjectPin2)(void * env, const void * svc, void * err, void * object_ref, void * corhdl, int pin_option, System::Word pin_duration, int lock_option, void * pobjectp);

typedef int __cdecl (*_OCIObjectPinCountReset)(void * env, void * err, void * pobject);

typedef int __cdecl (*_OCIObjectPinTable)(void * env, void * err, const void * svc, const void * schema_name, unsigned s_n_length, const void * object_name, unsigned o_n_length, void * not_used, System::Word pin_duration, void * &pobject);

typedef int __cdecl (*_OCIObjectRefresh)(void * env, void * err, void * pobject);

typedef int __cdecl (*_OCIObjectSetAttr)(void * env, void * err, void * instance, void * null_struct, void * tdo, void * namesp, const pub4 lengths, const unsigned name_count, const pub4 indexes, const unsigned index_count, const short null_status, const void * attr_null_struct, const void * attr_value);

typedef int __cdecl (*_OCIObjectUnmark)(void * env, void * err, void * pobject);

typedef int __cdecl (*_OCIObjectUnmarkByRef)(void * env, void * err, void * ref);

typedef int __cdecl (*_OCIObjectUnpin)(void * env, void * err, void * pobject);

typedef int __cdecl (*_OCITypeByName)(void * env, void * err, const void * svc, const void * schema_name, unsigned s_length, const void * type_name, unsigned t_length, void * version_name, unsigned v_length, System::Word pin_duration, int get_option, void * &tdo);

typedef int __cdecl (*_OCITypeByRef)(void * env, void * err, const void * type_ref, System::Word pin_duration, int get_option, void * tdo);

typedef int __cdecl (*_OCICollAppend)(void * env, void * err, const void * elem, const void * elemind, void * coll);

typedef int __cdecl (*_OCICollAssign)(void * env, void * err, const void * rhs, void * lhs);

typedef int __cdecl (*_OCICollAssignElem)(void * env, void * err, int index, const void * elem, const void * elemind, void * coll);

typedef int __cdecl (*_OCICollGetElem)(void * env, void * err, const void * coll, int index, int &exists, void * &elem, void * &elemind);

typedef int __cdecl (*_OCICollMax)(void * env, const void * coll);

typedef int __cdecl (*_OCICollSize)(void * env, void * err, const void * coll, int &size);

typedef int __cdecl (*_OCICollTrim)(void * env, void * err, int trim_num, void * coll);

typedef int __cdecl (*_OCIDateAssign)(void * err, const void * from, void * todate);

typedef int __cdecl (*_OCIDateFromText)(void * err, void * date_str, unsigned d_str_length, const void * fmt, System::Byte fmt_length, const void * lang_name, unsigned lang_length, void * date);

typedef int __cdecl (*_OCIDateGetDate)(const void * date, psb2 year, pub1 month, pub1 day);

typedef int __cdecl (*_OCIDateGetTime)(const void * date, pub1 hour, pub1 min, pub1 sec);

typedef int __cdecl (*_OCIDateSetDate)(void * date, short year, System::Byte month, System::Byte day);

typedef int __cdecl (*_OCIDateSetTime)(void * date, System::Byte hour, System::Byte min, System::Byte sec);

typedef int __cdecl (*_OCIDateToText)(void * err, const void * date, const void * fmt, System::Byte fmt_length, const void * lang_name, unsigned lang_length, pub4 buf_size, void * buf);

typedef int __cdecl (*_OCINumberAssign)(void * err, const void * from, void * tonum);

typedef int __cdecl (*_OCINumberCmp)(void * err, const void * number1, const void * number2, int &result);

typedef int __cdecl (*_OCINumberFromInt)(void * err, __int64 &inum, unsigned inum_length, unsigned inum_s_flag, void * number);

typedef int __cdecl (*_OCINumberFromReal)(void * err, double &rnum, unsigned rnum_length, void * number);

typedef int __cdecl (*_OCINumberFromText)(void * err, const void * str, unsigned str_length, const void * fmt, unsigned fmt_length, const void * nls_params, unsigned nls_p_length, void * number);

typedef int __cdecl (*_OCINumberToInt)(void * err, void * number, unsigned rsl_length, unsigned rsl_flag, __int64 &rsl);

typedef int __cdecl (*_OCINumberToReal)(void * err, const void * number, unsigned rsl_length, double &rsl);

typedef int __cdecl (*_OCINumberToText)(void * err, void * number, const void * fmt, unsigned fmt_length, const void * nls_params, unsigned nls_p_length, unsigned &buf_size, void * buf);

typedef int __cdecl (*_OCIRefAssign)(void * env, void * err, const void * source, void * &target);

typedef int __cdecl (*_OCIRefClear)(void * env, void * ref);

typedef int __cdecl (*_OCIRefIsEqual)(void * env, const void * x, const void * y);

typedef int __cdecl (*_OCIRefIsNull)(void * env, const void * ref);

typedef int __cdecl (*_OCIRefToHex)(void * env, void * err, const void * ref, void * hex, unsigned &hex_length);

typedef int __cdecl (*_OCIStringAllocSize)(void * env, void * err, const void * vs, pub4 allocsize);

typedef int __cdecl (*_OCIStringAssign)(void * env, void * err, const void * rhs, void * lhs);

typedef int __cdecl (*_OCIStringAssignText)(void * env, void * err, const void * rhs, unsigned rhs_len, void * lhs);

typedef void * __cdecl (*_OCIStringPtr)(void * env, const void * vs);

typedef int __cdecl (*_OCIStringResize)(void * env, void * err, unsigned new_size, void * str);

typedef unsigned __cdecl (*_OCIStringSize)(void * env, const void * vs);

typedef int __cdecl (*_OCITableDelete)(void * env, void * err, int index, void * tbl);

typedef int __cdecl (*_OCITableExists)(void * env, void * err, const void * tbl, int index, pbool exists);

typedef int __cdecl (*_OCITableFirst)(void * env, void * err, const void * tbl, psb4 index);

typedef int __cdecl (*_OCITableLast)(void * env, void * err, const void * tbl, psb4 index);

typedef int __cdecl (*_OCITableNext)(void * env, void * err, int index, const void * tbl, int &next_index, int &exists);

typedef int __cdecl (*_OCITablePrev)(void * env, void * err, int index, const void * tbl, psb4 prev_index, pbool exists);

typedef int __cdecl (*_OCITableSize)(void * env, void * err, const void * tbl, int &size);

typedef int __cdecl (*_OCIEnvCreate)(void * &envhpp, unsigned mode, const void * ctxp, const void * malocfp, const void * ralocfp, const void * mfreefp, NativeUInt xtramemsz, void * usrmempp);

typedef int __cdecl (*_OCIEnvNlsCreate)(void * &envhpp, unsigned mode, const void * ctxp, const void * malocfp, const void * ralocfp, const void * mfreefp, NativeUInt xtramemsz, void * usrmempp, System::Word charset, System::Word ncharset);

typedef int __cdecl (*_OCIDirPathAbort)(pOCIDirPathCtx dpctx, void * errhp);

typedef int __cdecl (*_OCIDirPathColArrayEntryGet)(pOCIDirPathColArray dpca, void * errhp, unsigned rownum, System::Word colIdx, pub1 &cvalpp, unsigned clenp, pub1 cflgp);

typedef int __cdecl (*_OCIDirPathColArrayEntrySet)(pOCIDirPathColArray dpca, void * errhp, unsigned rownum, System::Word colIdx, pub1 cvalp, unsigned clen, System::Byte cflg);

typedef int __cdecl (*_OCIDirPathColArrayRowGet)(pOCIDirPathColArray dpca, void * errhp, unsigned rownum, pub1 &cvalppp, pub4 &clenpp, ppub1 cflgpp);

typedef int __cdecl (*_OCIDirPathColArrayReset)(pOCIDirPathColArray dpca, void * errhp);

typedef int __cdecl (*_OCIDirPathColArrayToStream)(pOCIDirPathColArray dpca, const pOCIDirPathCtx dpctx, pOCIDirPathStream dpstr, void * errhp, unsigned rowcnt, unsigned rowoff);

typedef int __cdecl (*_OCIDirPathFinish)(pOCIDirPathCtx dpctx, void * errhp);

typedef int __cdecl (*_OCIDirPathLoadStream)(pOCIDirPathCtx dpctx, pOCIDirPathStream dpstr, void * errhp);

typedef int __cdecl (*_OCIDirPathPrepare)(pOCIDirPathCtx dpctx, void * svchp, void * errhp);

typedef int __cdecl (*_OCIDirPathStreamReset)(pOCIDirPathStream dpstr, void * errhp);

typedef int __cdecl (*_OCIDateTimeConstruct)(void * hndl, void * err, void * datetime, short year, System::Byte month, System::Byte day, System::Byte hour, System::Byte min, System::Byte sec, unsigned fsec, void * timezone, int timezone_length);

typedef int __cdecl (*_OCIDateTimeCheck)(void * hndl, void * err, void * date, unsigned &valid);

typedef int __cdecl (*_OCIDateTimeFromText)(void * hndl, void * err, void * date_str, int d_str_length, void * fmt, System::Byte fmt_length, void * lang_name, int lang_length, void * date);

typedef int __cdecl (*_OCIDateTimeToText)(void * hndl, void * err, void * date, void * fmt, System::Byte fmt_length, System::Byte fsprec, void * lang_name, int lang_length, unsigned &buf_size, void * buf);

typedef int __cdecl (*_OCIDateTimeGetDate)(void * hndl, void * err, void * date, short &year, System::Byte &month, System::Byte &day);

typedef int __cdecl (*_OCIDateTimeGetTime)(void * hndl, void * err, void * datetime, System::Byte &hour, System::Byte &minute, System::Byte &sec, unsigned &fsec);

typedef int __cdecl (*_OCIDateTimeGetTimeZoneOffset)(void * hndl, void * err, void * datetime, System::Int8 &hour, System::Int8 &minute);

typedef int __cdecl (*_OCIDateTimeGetTimeZoneName)(void * hndl, void * err, void * datetime, pub1 buf, unsigned &buflen);

typedef int __cdecl (*_OCIDateTimeAssign)(void * hndl, void * err, void * src, void * dst);

typedef int __cdecl (*_OCIDateTimeCompare)(void * hndl, void * err, const void * date1, const void * date2, int &result);

typedef int __cdecl (*_OCIIntervalFromText)(void * hndl, void * err, void * inpstr, int str_len, void * result);

typedef int __cdecl (*_OCIIntervalToText)(void * hndl, void * err, void * inter, System::Byte lfprec, System::Byte fsprec, void * buffer, NativeUInt buflen, NativeUInt &resultlen);

typedef int __cdecl (*_OCIIntervalCheck)(void * hndl, void * err, void * interval, unsigned &valid);

typedef int __cdecl (*_OCIIntervalAssign)(void * hndl, void * err, void * ininter, void * outinter);

typedef int __cdecl (*_OCIIntervalCompare)(void * hndl, void * err, void * inter1, void * inter2, int &result);

typedef int __cdecl (*_OCIIntervalSetYearMonth)(void * hndl, void * err, int yr, int mnth, void * result);

typedef int __cdecl (*_OCIIntervalGetYearMonth)(void * hndl, void * err, int &yr, int &mnth, void * result);

typedef int __cdecl (*_OCIIntervalSetDaySecond)(void * hndl, void * err, int dy, int hr, int mm, int ss, int fsec, void * result);

typedef int __cdecl (*_OCIIntervalGetDaySecond)(void * hndl, void * err, int &dy, int &hr, int &mm, int &ss, int &fsec, void * result);

typedef int __cdecl (*_OCIIntervalFromNumber)(void * hndl, void * err, void * interval, void * number);

typedef int __cdecl (*_OCIStmtFetch2)(void * stmtp, void * errhp, unsigned nrows, System::Word orientation, int scrollOffset, unsigned mode);

typedef int __cdecl (*_OCIClientVersion)(int &major_version, int &minor_version, int &update_num, int &patch_num, int &port_update_num);

typedef int __cdecl (*_OCIPing)(void * svchp, void * errhp, unsigned mode);

typedef void * pOCIXMLType;

typedef void * pOCIDOMDocument;

typedef void * ppOCIXMLType;

typedef void * ppOCIDOMDocument;

typedef int __cdecl (*_OCIXMLTypeNew)(void * svchp, void * errhp, System::Word dur, char * elname, unsigned elname_Len, char * schemaURL, unsigned schemaURL_Len, void * &retInstance);

typedef int __cdecl (*_OCIXMLTypeCreateFromSrc)(void * svchp, void * errhp, System::Word dur, System::Byte src_type, void * src_ptr, int ind, void * &retInstance);

typedef int __cdecl (*_OCIXMLTypeExtract)(void * errhp, void * doc, System::Word dur, char * xpathexpr, unsigned xpathexpr_Len, char * nsmap, unsigned nsmap_Len, void * &retDoc);

typedef int __cdecl (*_OCIXMLTypeTransform)(void * errhp, System::Word dur, void * doc, void * xsldoc, void * &retDoc);

typedef int __cdecl (*_OCIXMLTypeExists)(void * errhp, void * doc, char * xpathexpr, unsigned xpathexpr_Len, char * nsmap, unsigned nsmap_Len, unsigned &retval);

typedef int __cdecl (*_OCIXMLTypeIsSchemaBased)(void * errhp, void * doc, unsigned &retval);

typedef int __cdecl (*_OCIXMLTypeGetSchema)(void * errhp, void * doc, void * &schemadoc, void * &schemaURL, unsigned &schemaURL_Len, void * &rootelem, unsigned &rootelem_Len);

typedef int __cdecl (*_OCIXMLTypeValidate)(void * errhp, void * doc, char * schemaURL, unsigned schemaURL_Len, unsigned &retval);

typedef int __cdecl (*_OCIXMLTypeGetDOM)(void * errhp, void * doc, System::Word dur, void * &retDom);

typedef int __cdecl (*_OCIXMLTypeGetFromDOM)(void * errhp, void * domdoc, void * &retXMLType);

typedef int __cdecl (*_OCIDOMFree)(void * errhp, void * domdoc);

typedef int __cdecl (*_OCIPStreamFromXMLType)(void * errhp, void * phOCIDescriptor, void * pobject, int res);

typedef int __cdecl (*_OCIPStreamRead)(void * errhp, void * phOCIDescriptor, void * pStr, __int64 &Len, int res);

typedef int __cdecl (*_OCIPStreamClose)(void * errhp, void * phOCIDescriptor);

typedef void * pOCIAnyData;

typedef int __cdecl (*_OCIAnyDataAccess)(void * svchp, void * errhp, void * sdata, System::Word typecode, void * tdo, void * null_ind, /* out */ void * &data_value, /* out */ unsigned &length);

typedef int __cdecl (*_OCIAnyDataConvert)(void * svchp, void * errhp, System::Word typecode, void * tdo, System::Word duration, void * null_ind, void * data_value, unsigned length, void * sdata);

typedef int __cdecl (*_OCIAnyDataGetType)(void * svchp, void * errhp, void * data, /* out */ System::Word &typecode, /* out */ void * &tdo);

typedef int __cdecl (*_OCIRowidToChar)(void * rowidDesc, void * outbfp, System::Word &outbflp, void * errhp);

typedef int __cdecl (*_OCIExtProcGetEnv)(void * with_context, void * &envh, void * &svch, void * &errh);

typedef int __cdecl (*_OCIExtProcAllocCallMemory)(void * with_context, unsigned amount);

typedef int __cdecl (*_OCIExtProcRaiseExcpWithMsg)(void * with_context, int errnum, char * errmsg, int msglen);

typedef int __cdecl (*_OCISubscriptionRegister)(void * svchp, void * &subscrhpp, System::Word count, void * errhp, unsigned mode);

typedef int __cdecl (*_OCISubscriptionUnRegister)(void * svchp, void * subscrhp, void * errhp, unsigned mode);

typedef int __cdecl (*_OCISubscriptionEnable)(void * subscrhp, void * errhp, unsigned mode);

typedef int __cdecl (*_OCISubscriptionDisable)(void * subscrhp, void * errhp, unsigned mode);

enum DECLSPEC_DENUM TOCICallStyle : unsigned char { None, OCI73, OCI80 };

typedef System::Set<TOCICallStyle, TOCICallStyle::None, TOCICallStyle::OCI80> TOCICallStyleSet;

_DECLARE_METACLASS(System::TMetaClass, TEnvironmentClass);

class PASCALIMPLEMENTATION TOCIAPI : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	System::Syncobjs::TCriticalSection* hInitLock;
	__weak TOracleHome* FHome;
	
protected:
	bool FInited;
	void * __fastcall GetProc(const System::UnicodeString Name);
	void * __fastcall GetOraClientProc(const System::UnicodeString Name);
	__classmethod bool __fastcall CheckProc(NativeUInt OCILib, const System::UnicodeString Name);
	
public:
	__fastcall virtual TOCIAPI(TOracleHome* AHome);
	__fastcall virtual ~TOCIAPI();
	virtual void __fastcall Init() = 0 ;
	virtual void __fastcall Release() = 0 ;
	__property TOracleHome* Home = {read=FHome};
	__property bool Inited = {read=FInited, nodefault};
};


class PASCALIMPLEMENTATION TOCI7API : public TOCIAPI
{
	typedef TOCIAPI inherited;
	
public:
	_obindps obindps;
	_obndra obndra;
	_obndrn obndrn;
	_obndrv obndrv;
	_obreak obreak;
	_ocan ocan;
	_oclose oclose;
	_ocof ocof;
	_ocom ocom;
	_ocon ocon;
	_odefin odefin;
	_odefinps odefinps;
	_odescr odescr;
	_odessp odessp;
	_oerhms oerhms;
	_oermsg oermsg;
	_oexec oexec;
	_oexfet oexfet;
	_oexn oexn;
	_ofen ofen;
	_ofetch ofetch;
	_oflng oflng;
	_ognfd ognfd;
	_olog olog;
	_ologof ologof;
	_onbclr onbclr;
	_onbset onbset;
	_onbtst onbtst;
	_oopt oopt;
	_oopen oopen;
	_oparse oparse;
	_opinit opinit;
	_orol orol;
	_ogetpi ogetpi;
	_osetpi osetpi;
	_sqllda sqllda;
	_sqlld2 sqlld2;
	virtual void __fastcall Init();
	virtual void __fastcall Release();
	bool __fastcall GetThreadSafety();
	void __fastcall SetThreadSafety(bool Value);
public:
	/* TOCIAPI.Create */ inline __fastcall virtual TOCI7API(TOracleHome* AHome) : TOCIAPI(AHome) { }
	/* TOCIAPI.Destroy */ inline __fastcall virtual ~TOCI7API() { }
	
};


class PASCALIMPLEMENTATION TOCI8API : public TOCIAPI
{
	typedef TOCIAPI inherited;
	
public:
	_OCIAttrGet1 OCIAttrGet1;
	_OCIAttrGet2 OCIAttrGet2;
	_OCIAttrSet1 OCIAttrSet1;
	_OCIAttrSet2 OCIAttrSet2;
	_OCIBindArrayOfStruct OCIBindArrayOfStruct;
	_OCIBindByName OCIBindByName;
	_OCIBindByPos OCIBindByPos;
	_OCIBindDynamic OCIBindDynamic;
	_OCIBindObject OCIBindObject;
	_OCIBreak OCIBreak;
	_OCIDefineArrayOfStruct OCIDefineArrayOfStruct;
	_OCIDefineByPos OCIDefineByPos;
	_OCIDefineDynamic OCIDefineDynamic;
	_OCIDefineObject OCIDefineObject;
	_OCIDescribeAny OCIDescribeAny;
	_OCIDescriptorAlloc OCIDescriptorAlloc;
	_OCIDescriptorFree OCIDescriptorFree;
	_OCIEnvInit OCIEnvInit;
	_OCIErrorGet OCIErrorGet;
	_OCIHandleAlloc OCIHandleAlloc;
	_OCIHandleFree OCIHandleFree;
	_OCIInitialize OCIInitialize;
	_OCILdaToSvcCtx OCILdaToSvcCtx;
	_OCIParamGet OCIParamGet;
	_OCIPasswordChange OCIPasswordChange;
	_OCIReset OCIReset;
	_OCIServerAttach OCIServerAttach;
	_OCIServerDetach OCIServerDetach;
	_OCIServerVersion OCIServerVersion;
	_OCISessionBegin OCISessionBegin;
	_OCISessionEnd OCISessionEnd;
	_OCISessionGet OCISessionGet;
	_OCISessionRelease OCISessionRelease;
	_OCISessionPoolCreate OCISessionPoolCreate;
	_OCISessionPoolDestroy OCISessionPoolDestroy;
	_OCITransStart OCITransStart;
	_OCITransRollback OCITransRollback;
	_OCITransCommit OCITransCommit;
	_OCITransDetach OCITransDetach;
	_OCITransPrepare OCITransPrepare;
	_OCITransForget OCITransForget;
	_OCIStmtExecute OCIStmtExecute;
	_OCIStmtFetch OCIStmtFetch;
	_OCIStmtFetch2 OCIStmtFetch2;
	_OCIStmtGetPieceInfo OCIStmtGetPieceInfo;
	_OCIStmtPrepare OCIStmtPrepare;
	_OCIStmtPrepare2 OCIStmtPrepare2;
	_OCIStmtPrepare3 OCIStmtPrepare3;
	_OCIStmtRelease OCIStmtRelease;
	_OCIStmtGetNextResult OCIStmtGetNextResult;
	_OCIStmtSetPieceInfo OCIStmtSetPieceInfo;
	_OCISvcCtxToLda OCISvcCtxToLda;
	_OCILobAppend OCILobAppend;
	_OCILobAssign OCILobAssign;
	_OCILobCharSetForm OCILobCharSetForm;
	_OCILobCharSetId OCILobCharSetId;
	_OCILobCopy OCILobCopy;
	_OCILobOpen OCILobOpen;
	_OCILobClose OCILobClose;
	_OCILobIsOpen OCILobIsOpen;
	_OCILobCreateTemporary OCILobCreateTemporary;
	_OCILobFreeTemporary OCILobFreeTemporary;
	_OCILobIsTemporary OCILobIsTemporary;
	_OCILobDisableBuffering OCILobDisableBuffering;
	_OCILobEnableBuffering OCILobEnableBuffering;
	_OCILobErase OCILobErase;
	_OCILobFileClose OCILobFileClose;
	_OCILobFileExists OCILobFileExists;
	_OCILobFileGetName OCILobFileGetName;
	_OCILobFileIsOpen OCILobFileIsOpen;
	_OCILobFileOpen OCILobFileOpen;
	_OCILobFileSetName OCILobFileSetName;
	_OCILobFlushBuffer OCILobFlushBuffer;
	_OCILobGetLength OCILobGetLength;
	_OCILobIsEqual OCILobIsEqual;
	_OCILobLoadFromFile OCILobLoadFromFile;
	_OCILobLocatorIsInit OCILobLocatorIsInit;
	_OCILobRead OCILobRead;
	_OCILobRead2 OCILobRead2;
	_OCILobTrim OCILobTrim;
	_OCILobWrite OCILobWrite;
	_OCICacheFlush OCICacheFlush;
	_OCICacheFree OCICacheFree;
	_OCICacheRefresh OCICacheRefresh;
	_OCICacheUnmark OCICacheUnmark;
	_OCICacheUnpin OCICacheUnpin;
	_OCIObjectCopy OCIObjectCopy;
	_OCIObjectExists OCIObjectExists;
	_OCIObjectFlush OCIObjectFlush;
	_OCIObjectFree OCIObjectFree;
	_OCIObjectPtr OCIObjectPtr;
	_OCIObjectGetAttr OCIObjectGetAttr;
	_OCIObjectGetInd OCIObjectGetInd;
	_OCIObjectGetObjectRef OCIObjectGetObjectRef;
	_OCIObjectGetProperty OCIObjectGetProperty;
	_OCIObjectGetTypeRef OCIObjectGetTypeRef;
	_OCIObjectIsDirty OCIObjectIsDirty;
	_OCIObjectIsLocked OCIObjectIsLocked;
	_OCIObjectLock OCIObjectLock;
	_OCIObjectMarkDelete OCIObjectMarkDelete;
	_OCIObjectMarkDeleteByRef OCIObjectMarkDeleteByRef;
	_OCIObjectMarkUpdate OCIObjectMarkUpdate;
	_OCIObjectNew OCIObjectNew;
	_OCIObjectPin OCIObjectPin;
	_OCIObjectPin2 OCIObjectPin2;
	_OCIObjectPinCountReset OCIObjectPinCountReset;
	_OCIObjectPinTable OCIObjectPinTable;
	_OCIObjectRefresh OCIObjectRefresh;
	_OCIObjectSetAttr OCIObjectSetAttr;
	_OCIObjectUnmark OCIObjectUnmark;
	_OCIObjectUnmarkByRef OCIObjectUnmarkByRef;
	_OCIObjectUnpin OCIObjectUnpin;
	_OCITypeByName OCITypeByName;
	_OCITypeByRef OCITypeByRef;
	_OCICollAppend OCICollAppend;
	_OCICollAssign OCICollAssign;
	_OCICollAssignElem OCICollAssignElem;
	_OCICollGetElem OCICollGetElem;
	_OCICollMax OCICollMax;
	_OCICollSize OCICollSize;
	_OCICollTrim OCICollTrim;
	_OCIDateAssign OCIDateAssign;
	_OCIDateFromText OCIDateFromText;
	_OCIDateGetDate OCIDateGetDate;
	_OCIDateGetTime OCIDateGetTime;
	_OCIDateSetDate OCIDateSetDate;
	_OCIDateSetTime OCIDateSetTime;
	_OCIDateToText OCIDateToText;
	_OCINumberAssign OCINumberAssign;
	_OCINumberCmp OCINumberCmp;
	_OCINumberFromInt OCINumberFromInt;
	_OCINumberFromReal OCINumberFromReal;
	_OCINumberFromText OCINumberFromText;
	_OCINumberToInt OCINumberToInt;
	_OCINumberToReal OCINumberToReal;
	_OCINumberToText OCINumberToText;
	_OCIRefAssign OCIRefAssign;
	_OCIRefClear OCIRefClear;
	_OCIRefIsEqual OCIRefIsEqual;
	_OCIRefIsNull OCIRefIsNull;
	_OCIRefToHex OCIRefToHex;
	_OCIStringAllocSize OCIStringAllocSize;
	_OCIStringAssign OCIStringAssign;
	_OCIStringAssignText OCIStringAssignText;
	_OCIStringPtr OCIStringPtr;
	_OCIStringResize OCIStringResize;
	_OCIStringSize OCIStringSize;
	_OCITableDelete OCITableDelete;
	_OCITableExists OCITableExists;
	_OCITableFirst OCITableFirst;
	_OCITableLast OCITableLast;
	_OCITableNext OCITableNext;
	_OCITablePrev OCITablePrev;
	_OCITableSize OCITableSize;
	_OCIEnvCreate OCIEnvCreate;
	_OCIEnvNlsCreate OCIEnvNlsCreate;
	_OCIDirPathAbort OCIDirPathAbort;
	_OCIDirPathColArrayEntryGet OCIDirPathColArrayEntryGet;
	_OCIDirPathColArrayEntrySet OCIDirPathColArrayEntrySet;
	_OCIDirPathColArrayRowGet OCIDirPathColArrayRowGet;
	_OCIDirPathColArrayReset OCIDirPathColArrayReset;
	_OCIDirPathColArrayToStream OCIDirPathColArrayToStream;
	_OCIDirPathFinish OCIDirPathFinish;
	_OCIDirPathLoadStream OCIDirPathLoadStream;
	_OCIDirPathPrepare OCIDirPathPrepare;
	_OCIDirPathStreamReset OCIDirPathStreamReset;
	_OCIDateTimeConstruct OCIDateTimeConstruct;
	_OCIDateTimeCheck OCIDateTimeCheck;
	_OCIDateTimeFromText OCIDateTimeFromText;
	_OCIDateTimeToText OCIDateTimeToText;
	_OCIDateTimeGetDate OCIDateTimeGetDate;
	_OCIDateTimeGetTime OCIDateTimeGetTime;
	_OCIDateTimeGetTimeZoneOffset OCIDateTimeGetTimeZoneOffset;
	_OCIDateTimeGetTimeZoneName OCIDateTimeGetTimeZoneName;
	_OCIDateTimeAssign OCIDateTimeAssign;
	_OCIDateTimeCompare OCIDateTimeCompare;
	_OCIIntervalFromText OCIIntervalFromText;
	_OCIIntervalToText OCIIntervalToText;
	_OCIIntervalCheck OCIIntervalCheck;
	_OCIIntervalAssign OCIIntervalAssign;
	_OCIIntervalCompare OCIIntervalCompare;
	_OCIIntervalSetYearMonth OCIIntervalSetYearMonth;
	_OCIIntervalGetYearMonth OCIIntervalGetYearMonth;
	_OCIIntervalSetDaySecond OCIIntervalSetDaySecond;
	_OCIIntervalGetDaySecond OCIIntervalGetDaySecond;
	_OCIIntervalFromNumber OCIIntervalFromNumber;
	_OCIPing OCIPing;
	_OCIXMLTypeNew OCIXMLTypeNew;
	_OCIXMLTypeCreateFromSrc OCIXMLTypeCreateFromSrc;
	_OCIXMLTypeExtract OCIXMLTypeExtract;
	_OCIXMLTypeTransform OCIXMLTypeTransform;
	_OCIXMLTypeExists OCIXMLTypeExists;
	_OCIXMLTypeIsSchemaBased OCIXMLTypeIsSchemaBased;
	_OCIXMLTypeGetSchema OCIXMLTypeGetSchema;
	_OCIXMLTypeValidate OCIXMLTypeValidate;
	_OCIXMLTypeGetDOM OCIXMLTypeGetDOM;
	_OCIXMLTypeGetFromDOM OCIXMLTypeGetFromDOM;
	_OCIDOMFree OCIDOMFree;
	_OCIAnyDataAccess OCIAnyDataAccess;
	_OCIAnyDataConvert OCIAnyDataConvert;
	_OCIAnyDataGetType OCIAnyDataGetType;
	_OCIPStreamFromXMLType OCIPStreamFromXMLType;
	_OCIPStreamRead OCIPStreamRead;
	_OCIPStreamClose OCIPStreamClose;
	_OCIRowidToChar OCIRowidToChar;
	_OCIExtProcGetEnv OCIExtProcGetEnv;
	_OCIExtProcAllocCallMemory OCIExtProcAllocCallMemory;
	_OCIExtProcRaiseExcpWithMsg OCIExtProcRaiseExcpWithMsg;
	_OCISubscriptionRegister OCISubscriptionRegister;
	_OCISubscriptionUnRegister OCISubscriptionUnRegister;
	_OCISubscriptionEnable OCISubscriptionEnable;
	_OCISubscriptionDisable OCISubscriptionDisable;
	virtual void __fastcall Init();
	virtual void __fastcall Release();
	void __fastcall DoOraError(int ErrorCode, TOCISvcCtx* OCISvcCtx)/* overload */;
	void __fastcall DoOraError(int ErrorCode, bool UnicodeEnv, void * hOCIError)/* overload */;
	int __fastcall GetOraError(int ErrorCode, TOCISvcCtx* OCISvcCtx, System::UnicodeString &ErrorMsg)/* overload */;
	int __fastcall GetOraError(int ErrorCode, TOCISvcCtx* OCISvcCtx)/* overload */;
	void __fastcall Check(int Status, TOCISvcCtx* OCISvcCtx)/* overload */;
	void __fastcall Check(int Status, bool UnicodeEnv, void * hOCIError)/* overload */;
	int __fastcall GetHeapAlloc(TOCISvcCtx* OCISvcCtx);
	int __fastcall GetSharedHeapAlloc(TOCISvcCtx* OCISvcCtx);
	bool __fastcall IsUnicodeEnv(void * hOCIEnv, void * hOCIError);
public:
	/* TOCIAPI.Create */ inline __fastcall virtual TOCI8API(TOracleHome* AHome) : TOCIAPI(AHome) { }
	/* TOCIAPI.Destroy */ inline __fastcall virtual ~TOCI8API() { }
	
};


class PASCALIMPLEMENTATION TDirectAPI : public TOCI8API
{
	typedef TOCI8API inherited;
	
public:
	virtual void __fastcall Init();
public:
	/* TOCIAPI.Create */ inline __fastcall virtual TDirectAPI(TOracleHome* AHome) : TOCI8API(AHome) { }
	/* TOCIAPI.Destroy */ inline __fastcall virtual ~TDirectAPI() { }
	
};


class PASCALIMPLEMENTATION TOracleHome : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	__weak TOracleHomes* FOwner;
	NativeUInt FOCILib;
	NativeUInt FOCIClientLib;
	TOCI7API* FOCI7;
	TOCI8API* FOCI8;
	System::Generics::Collections::TObjectList__1<System::TObject*> * FEnvironments;
	bool __fastcall GetDirect();
	TOCIEnvironment* __fastcall GetEnvironment(int Index);
	int __fastcall GetEnvironmentCount();
	TOCI7API* __fastcall GetOCI7();
	TOCI8API* __fastcall GetOCI8();
	
protected:
	System::UnicodeString FName;
	System::UnicodeString FPath;
	System::UnicodeString FTNSPath;
	System::UnicodeString FNLSLang;
	System::UnicodeString FOCIDLL;
	System::UnicodeString FOCIClientDLL;
	System::UnicodeString FOCIVersionSt;
	System::Word FOCIVersion;
	TOCICallStyle FOCICallStyle;
	bool FOCILite;
	TOCICallStyleSet FPossibleOCICallStyles;
	virtual TEnvironmentClass __fastcall GetEnvironmentClass();
	virtual bool __fastcall GetInited();
	virtual TOCI8API* __fastcall CreateOCI8API();
	void __fastcall DetectOCIClientVersion();
	void __fastcall AddEnvironment(TOCIEnvironment* Environment);
	void __fastcall RemoveEnvironment(TOCIEnvironment* Environment);
	void __fastcall ClearEnvironments();
	TOCIEnvironment* __fastcall FindEnvironment(bool Unique, bool UnicodeEnv, int SubscriptionPort)/* overload */;
	TOCIEnvironment* __fastcall FindEnvironment(void * hOCIEnv)/* overload */;
	TOCIEnvironment* __fastcall CreateEnvironment(bool Unique, bool UnicodeEnv, int SubscriptionPort)/* overload */;
	TOCIEnvironment* __fastcall CreateEnvironment(void * hOCIEnv)/* overload */;
	void __fastcall LoadOCI();
	void __fastcall FreeOCI();
	__property NativeUInt OCILib = {read=FOCILib};
	__property NativeUInt OCIClientLib = {read=FOCIClientLib};
	
public:
	__fastcall virtual TOracleHome(TOracleHomes* AOwner);
	__fastcall virtual ~TOracleHome();
	virtual void __fastcall Init();
	virtual void __fastcall Release();
	TOCIEnvironment* __fastcall AllocEnvironment(bool Unique, bool UnicodeEnv, int SubscriptionPort)/* overload */;
	TOCIEnvironment* __fastcall AllocEnvironment(void * hOCIEnv)/* overload */;
	void __fastcall CheckOCI();
	void __fastcall CheckOCI73();
	void __fastcall CheckOCI80();
	void __fastcall CheckOCI81();
	void __fastcall CheckOCI90();
	__property TOracleHomes* Owner = {read=FOwner};
	__property bool Direct = {read=GetDirect, nodefault};
	__property bool Inited = {read=GetInited, nodefault};
	__property System::UnicodeString Name = {read=FName};
	__property System::UnicodeString Path = {read=FPath};
	__property System::UnicodeString TNSPath = {read=FTNSPath};
	__property System::UnicodeString NLSLang = {read=FNLSLang};
	__property System::UnicodeString OCIDLL = {read=FOCIDLL};
	__property System::UnicodeString OCIClientDLL = {read=FOCIClientDLL};
	__property System::UnicodeString OCIVersionSt = {read=FOCIVersionSt};
	__property System::Word OCIVersion = {read=FOCIVersion, nodefault};
	__property TOCICallStyle OCICallStyle = {read=FOCICallStyle, nodefault};
	__property TOCICallStyleSet PossibleOCICallStyles = {read=FPossibleOCICallStyles, nodefault};
	__property TOCI7API* OCI7 = {read=GetOCI7};
	__property TOCI8API* OCI8 = {read=GetOCI8};
	__property TOCIEnvironment* Environments[int Index] = {read=GetEnvironment};
	__property int EnvironmentCount = {read=GetEnvironmentCount, nodefault};
};


class PASCALIMPLEMENTATION TDirectHome : public TOracleHome
{
	typedef TOracleHome inherited;
	
protected:
	virtual TEnvironmentClass __fastcall GetEnvironmentClass();
	virtual bool __fastcall GetInited();
	virtual TOCI8API* __fastcall CreateOCI8API();
	
public:
	__fastcall virtual TDirectHome(TOracleHomes* AOwner);
	virtual void __fastcall Init();
public:
	/* TOracleHome.Destroy */ inline __fastcall virtual ~TDirectHome() { }
	
};


class PASCALIMPLEMENTATION TOracleHomes : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	TOracleHome* operator[](int Index) { return this->Homes[Index]; }
	
private:
	System::Syncobjs::TCriticalSection* hInitLock;
	bool FInited;
	bool FInDestroying;
	System::Generics::Collections::TObjectList__1<System::TObject*> * FHomes;
	TOracleHome* FDirect;
	__weak TOracleHome* FDefault;
	int __fastcall GetCount();
	TOracleHome* __fastcall GetOracleHome(int Index);
	TOracleHome* __fastcall GetDirect();
	TOracleHome* __fastcall GetDefault();
	void __fastcall SetDefault(TOracleHome* Value);
	
protected:
	TOracleHome* __fastcall Add(const System::UnicodeString Name, const System::UnicodeString HomePath, const System::UnicodeString OCIDLL, const System::UnicodeString TNSPath, const System::UnicodeString NLSLang, bool IsLite);
	void __fastcall Clear();
	void __fastcall OCINotFound(const System::UnicodeString Path);
	
public:
	__fastcall TOracleHomes();
	__fastcall virtual ~TOracleHomes();
	void __fastcall Init();
	void __fastcall Release();
	TOracleHome* __fastcall AddHome(const System::UnicodeString Path)/* overload */;
	TOracleHome* __fastcall AddHome(const System::UnicodeString Name, const System::UnicodeString Path)/* overload */;
	TOracleHome* __fastcall AddHome(const System::UnicodeString Name, const System::UnicodeString Path, const System::UnicodeString TNSPath, bool IsLite = false)/* overload */;
	TOracleHome* __fastcall AddHome(const System::UnicodeString Name, const System::UnicodeString Path, const System::UnicodeString TNSPath, const System::UnicodeString NLSLang, bool IsLite = false)/* overload */;
	TOracleHome* __fastcall FindHome(const System::UnicodeString HomeName);
	TOracleHome* __fastcall GetHome(const System::UnicodeString HomeName);
	__property bool Inited = {read=FInited, nodefault};
	__property TOracleHome* Homes[int Index] = {read=GetOracleHome/*, default*/};
	__property int Count = {read=GetCount, nodefault};
	__property TOracleHome* Direct = {read=GetDirect};
	__property TOracleHome* Default = {read=GetDefault, write=SetDefault};
};


class PASCALIMPLEMENTATION TOCIEnvironment : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	System::Syncobjs::TCriticalSection* hInitLock;
	TOracleHome* FHome;
	bool FUnique;
	bool FUnicodeEnv;
	int FSubscriptionPort;
	void *FhOCIEnv;
	void *FhOCIError;
	bool FNativeHandle;
	System::Classes::TList* FOCISvcCtxs;
	
protected:
	bool FInited;
	void __fastcall AddOCISvcCtx(TOCISvcCtx* Value);
	void __fastcall RemoveOCISvcCtx(TOCISvcCtx* Value);
	void __fastcall ClearOCISvcCtxs();
	void __fastcall OCIError(int Res);
	void __fastcall SetSubscriptionPort(int Port);
	
public:
	__fastcall TOCIEnvironment(TOracleHome* AHome, bool AUnique, bool AUnicodeEnv, int ASubscriptionPort)/* overload */;
	__fastcall TOCIEnvironment(TOracleHome* AHome, void * hOCIEnv)/* overload */;
	__fastcall virtual ~TOCIEnvironment();
	virtual void __fastcall Init();
	virtual void __fastcall Release();
	void * __fastcall AllocErrorHandle();
	void __fastcall FreeErrorHandle(void * hOCIError);
	void * __fastcall AllocSvcCtxHandle();
	void * __fastcall AllocPooledSvcCtxHandle(const System::UnicodeString PoolName, void * hOCIAuthInfo);
	void __fastcall FreeSvcCtxHandle(void * hOCISvcCtx);
	__property TOracleHome* Home = {read=FHome};
	__property bool Inited = {read=FInited, nodefault};
	__property void * hOCIEnv = {read=FhOCIEnv};
	__property bool Unique = {read=FUnique, nodefault};
	__property bool UnicodeEnv = {read=FUnicodeEnv, nodefault};
	__property int SubscriptionPort = {read=FSubscriptionPort, nodefault};
	__property void * hOCIError = {read=FhOCIError};
};


class PASCALIMPLEMENTATION TDirectEnvironment : public TOCIEnvironment
{
	typedef TOCIEnvironment inherited;
	
public:
	virtual void __fastcall Init();
public:
	/* TOCIEnvironment.Create */ inline __fastcall TDirectEnvironment(TOracleHome* AHome, bool AUnique, bool AUnicodeEnv, int ASubscriptionPort)/* overload */ : TOCIEnvironment(AHome, AUnique, AUnicodeEnv, ASubscriptionPort) { }
	/* TOCIEnvironment.Create */ inline __fastcall TDirectEnvironment(TOracleHome* AHome, void * hOCIEnv)/* overload */ : TOCIEnvironment(AHome, hOCIEnv) { }
	/* TOCIEnvironment.Destroy */ inline __fastcall virtual ~TDirectEnvironment() { }
	
};


class PASCALIMPLEMENTATION TOCISvcCtx : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	TOCIEnvironment* FEnvironment;
	TOracleHome* FHome;
	TOCI7API* FOCI7;
	TOCI8API* FOCI8;
	void *FhOCIEnv;
	void *FhOCISvcCtx;
	void *FhOCIError;
	bool FNativeHandle;
	bool FUnicodeEnv;
	bool FUseUnicode;
	void __fastcall ReleaseEnvironment();
	
protected:
	void __fastcall ResetEnvironment();
	void __fastcall SetEnvironment(TOCIEnvironment* Value);
	
public:
	__fastcall TOCISvcCtx(TOCIEnvironment* AEnvironment, bool AUseUnicode)/* overload */;
	__fastcall TOCISvcCtx(TOCIEnvironment* AEnvironment, void * AhOCISvcCtx, bool AUseUnicode)/* overload */;
	__fastcall virtual ~TOCISvcCtx();
	void __fastcall AllocSvcCtxHandle();
	void __fastcall AllocPooledSvcCtxHandle(const System::UnicodeString PoolName, void * hOCIAuthInfo);
	void __fastcall FreeSvcCtxHandle();
	void __fastcall Release();
	void __fastcall GetLDA(PCDA Value);
	void __fastcall SetLDA(PCDA Value);
	__property TOCIEnvironment* Environment = {read=FEnvironment};
	__property TOracleHome* Home = {read=FHome};
	__property TOCI7API* OCI7 = {read=FOCI7};
	__property TOCI8API* OCI8 = {read=FOCI8};
	__property void * hOCIEnv = {read=FhOCIEnv};
	__property void * hOCIError = {read=FhOCIError};
	__property void * hOCISvcCtx = {read=FhOCISvcCtx};
	__property bool UnicodeEnv = {read=FUnicodeEnv, nodefault};
	__property bool UseUnicode = {read=FUseUnicode, nodefault};
};


class PASCALIMPLEMENTATION TDirectServerInfo : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	bool FFullInfo;
	System::UnicodeString FHost;
	System::UnicodeString FPort;
	System::UnicodeString FSID;
	System::UnicodeString FServiceName;
	System::UnicodeString __fastcall GetHost();
	System::UnicodeString __fastcall GetPort();
	System::UnicodeString __fastcall GetSID();
	System::UnicodeString __fastcall GetServiceName();
	
public:
	__fastcall TDirectServerInfo();
	virtual void __fastcall Clear();
	System::UnicodeString __fastcall GetServerInfo();
	void __fastcall SetServerInfo(const System::UnicodeString Value);
	__property bool FullInfo = {read=FFullInfo, write=FFullInfo, nodefault};
	__property System::UnicodeString Host = {read=GetHost, write=FHost};
	__property System::UnicodeString Port = {read=GetPort, write=FPort};
	__property System::UnicodeString SID = {read=GetSID, write=FSID};
	__property System::UnicodeString ServiceName = {read=GetServiceName, write=FServiceName};
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TDirectServerInfo() { }
	
};


class PASCALIMPLEMENTATION EOCIInitError : public System::Sysutils::Exception
{
	typedef System::Sysutils::Exception inherited;
	
public:
	/* Exception.Create */ inline __fastcall EOCIInitError(const System::UnicodeString Msg) : System::Sysutils::Exception(Msg) { }
	/* Exception.CreateFmt */ inline __fastcall EOCIInitError(const System::UnicodeString Msg, const System::TVarRec *Args, const int Args_High) : System::Sysutils::Exception(Msg, Args, Args_High) { }
	/* Exception.CreateRes */ inline __fastcall EOCIInitError(System::PResStringRec ResStringRec) : System::Sysutils::Exception(ResStringRec) { }
	/* Exception.CreateResFmt */ inline __fastcall EOCIInitError(System::PResStringRec ResStringRec, const System::TVarRec *Args, const int Args_High) : System::Sysutils::Exception(ResStringRec, Args, Args_High) { }
	/* Exception.CreateHelp */ inline __fastcall EOCIInitError(const System::UnicodeString Msg, int AHelpContext) : System::Sysutils::Exception(Msg, AHelpContext) { }
	/* Exception.CreateFmtHelp */ inline __fastcall EOCIInitError(const System::UnicodeString Msg, const System::TVarRec *Args, const int Args_High, int AHelpContext) : System::Sysutils::Exception(Msg, Args, Args_High, AHelpContext) { }
	/* Exception.CreateResHelp */ inline __fastcall EOCIInitError(System::PResStringRec ResStringRec, int AHelpContext) : System::Sysutils::Exception(ResStringRec, AHelpContext) { }
	/* Exception.CreateResFmtHelp */ inline __fastcall EOCIInitError(System::PResStringRec ResStringRec, const System::TVarRec *Args, const int Args_High, int AHelpContext) : System::Sysutils::Exception(ResStringRec, Args, Args_High, AHelpContext) { }
	/* Exception.Destroy */ inline __fastcall virtual ~EOCIInitError() { }
	
};


//-- var, const, procedure ---------------------------------------------------
static constexpr System::Int8 SQLT_UNK = System::Int8(0x0);
static constexpr System::Int8 SQLT_CHR = System::Int8(0x1);
static constexpr System::Int8 SQLT_NUM = System::Int8(0x2);
static constexpr System::Int8 SQLT_INT = System::Int8(0x3);
static constexpr System::Int8 SQLT_FLT = System::Int8(0x4);
static constexpr System::Int8 SQLT_STR = System::Int8(0x5);
static constexpr System::Int8 SQLT_VNU = System::Int8(0x6);
static constexpr System::Int8 SQLT_PDN = System::Int8(0x7);
static constexpr System::Int8 SQLT_LNG = System::Int8(0x8);
static constexpr System::Int8 SQLT_VCS = System::Int8(0x9);
static constexpr System::Int8 SQLT_NON = System::Int8(0xa);
static constexpr System::Int8 SQLT_RID = System::Int8(0xb);
static constexpr System::Int8 SQLT_DAT = System::Int8(0xc);
static constexpr System::Int8 SQLT_VBI = System::Int8(0xf);
static constexpr System::Int8 SQLT_BFLOAT = System::Int8(0x15);
static constexpr System::Int8 SQLT_BDOUBLE = System::Int8(0x16);
static constexpr System::Int8 SQLT_BIN = System::Int8(0x17);
static constexpr System::Int8 SQLT_LBI = System::Int8(0x18);
static constexpr System::Int8 SQLT_UND = System::Int8(0x19);
static constexpr System::Int8 SQLT_OPAQUE = System::Int8(0x3a);
static constexpr System::Int8 SQLT_UIN = System::Int8(0x44);
static constexpr System::Int8 SQLT_SLS = System::Int8(0x5b);
static constexpr System::Int8 SQLT_LVC = System::Int8(0x5e);
static constexpr System::Int8 SQLT_LVB = System::Int8(0x5f);
static constexpr System::Int8 SQLT_AFC = System::Int8(0x60);
static constexpr System::Int8 SQLT_AVC = System::Int8(0x61);
static constexpr System::Int8 SQLT_IBFLOAT = System::Int8(0x64);
static constexpr System::Int8 SQLT_IBDOUBLE = System::Int8(0x65);
static constexpr System::Int8 SQLT_CUR = System::Int8(0x66);
static constexpr System::Int8 SQLT_RDD = System::Int8(0x68);
static constexpr System::Int8 SQLT_LAB = System::Int8(0x69);
static constexpr System::Int8 SQLT_OSL = System::Int8(0x6a);
static constexpr System::Int8 SQLT_NTY = System::Int8(0x6c);
static constexpr System::Int8 SQLT_REF = System::Int8(0x6e);
static constexpr System::Int8 SQLT_CLOB = System::Int8(0x70);
static constexpr System::Int8 SQLT_BLOB = System::Int8(0x71);
static constexpr System::Int8 SQLT_BFILEE = System::Int8(0x72);
static constexpr System::Int8 SQLT_CFILEE = System::Int8(0x73);
static constexpr System::Int8 SQLT_RSET = System::Int8(0x74);
static constexpr System::Int8 SQLT_NCO = System::Int8(0x7a);
static constexpr System::Int8 SQLT_VARRAY = System::Int8(0x7b);
static constexpr System::Byte SQLT_VST = System::Byte(0x9b);
static constexpr System::Byte SQLT_ODT = System::Byte(0x9c);
static constexpr System::Int8 SQLT_FILE = System::Int8(0x72);
static constexpr System::Int8 SQLT_CFILE = System::Int8(0x73);
static constexpr System::Int8 SQLT_BFILE = System::Int8(0x72);
static constexpr System::Byte SQLT_DATE = System::Byte(0xb8);
static constexpr System::Byte SQLT_TIME = System::Byte(0xb9);
static constexpr System::Byte SQLT_TIME_TZ = System::Byte(0xba);
static constexpr System::Byte SQLT_TIMESTAMP = System::Byte(0xbb);
static constexpr System::Byte SQLT_TIMESTAMP_TZ = System::Byte(0xbc);
static constexpr System::Byte SQLT_INTERVAL_YM = System::Byte(0xbd);
static constexpr System::Byte SQLT_INTERVAL_DS = System::Byte(0xbe);
static constexpr System::Byte SQLT_URID = System::Byte(0xd0);
static constexpr System::Byte SQLT_TIMESTAMP_LTZ = System::Byte(0xe8);
static constexpr System::Byte SQLT_REC = System::Byte(0xfa);
static constexpr System::Byte SQLT_TAB = System::Byte(0xfb);
static constexpr System::Byte SQLT_BOL = System::Byte(0xfc);
static constexpr System::Int8 CDA_SIZE = System::Int8(0x40);
static constexpr System::Word HDA_SIZE = System::Word(0x100);
static constexpr System::Int8 UNKNOWN_TYPE = System::Int8(0x0);
static constexpr System::Int8 VARCHAR2_TYPE = System::Int8(0x1);
static constexpr System::Int8 NUMBER_TYPE = System::Int8(0x2);
static constexpr System::Int8 INTEGER_TYPE = System::Int8(0x3);
static constexpr System::Int8 FLOAT_TYPE = System::Int8(0x4);
static constexpr System::Int8 STRING_TYPE = System::Int8(0x5);
static constexpr System::Int8 LONG_TYPE = System::Int8(0x8);
static constexpr System::Int8 ROWID_TYPE = System::Int8(0xb);
static constexpr System::Int8 DATE_TYPE = System::Int8(0xc);
static constexpr System::Int8 RAW_TYPE = System::Int8(0x17);
static constexpr System::Int8 LONGRAW_TYPE = System::Int8(0x18);
static constexpr System::Int8 CHAR_TYPE = System::Int8(0x60);
static constexpr System::Int8 CHARZ_TYPE = System::Int8(0x61);
static constexpr System::Int8 CURSOR_TYPE = System::Int8(0x66);
static constexpr short OCI_VAR_NOT_IN_LIST = short(-303);
static constexpr System::Int8 OCI_NO_DATA_FOUND = System::Int8(0x4);
static constexpr System::Word OCI_NULL_VALUE_RETURNED = System::Word(0x57d);
static constexpr short OCI_BLOCKED = short(-3123);
static constexpr short OCI_CONNECTION_BUSY = short(-3127);
static constexpr System::Int8 OCI_SESSION_KILLED = System::Int8(0x1c);
static constexpr System::Word OCI_NOT_LOGGEDON = System::Word(0x3f4);
static constexpr System::Word OCI_EOF_COMMUNICATION = System::Word(0xc29);
static constexpr System::Word OCI_NOT_CONNECTED = System::Word(0xc2a);
static constexpr System::Word OCI_NO_INTERFACE = System::Word(0xc31);
static constexpr short OCI_STILL_IS_PIECE = short(-3130);
static constexpr short OCI_STILL_IS_PIECE1 = short(-3129);
static constexpr short OCI_BREAKED = short(-1013);
static constexpr System::Int8 SQL_UNKNOWN = System::Int8(0x0);
static constexpr System::Int8 SQL_CREATE_TABLE = System::Int8(0x1);
static constexpr System::Int8 SQL_SET_ROLE = System::Int8(0x2);
static constexpr System::Int8 SQL_INSERT = System::Int8(0x3);
static constexpr System::Int8 SQL_SELECT = System::Int8(0x4);
static constexpr System::Int8 SQL_UPDATE = System::Int8(0x5);
static constexpr System::Int8 SQL_DROP_ROLE = System::Int8(0x6);
static constexpr System::Int8 SQL_DROP_VIEW = System::Int8(0x7);
static constexpr System::Int8 SQL_DROP_TABLE = System::Int8(0x8);
static constexpr System::Int8 SQL_DELETE = System::Int8(0x9);
static constexpr System::Int8 SQL_CREATE_VIEW = System::Int8(0xa);
static constexpr System::Int8 SQL_DROP_USER = System::Int8(0xb);
static constexpr System::Int8 SQL_CREATE_ROLE = System::Int8(0xc);
static constexpr System::Int8 SQL_CREATE_SEQUENCE = System::Int8(0xd);
static constexpr System::Int8 SQL_ALTER_SEQUENCE = System::Int8(0xe);
static constexpr System::Int8 SQL_DROP_SEQUENCE = System::Int8(0x10);
static constexpr System::Int8 SQL_CREATE_SCHEMA = System::Int8(0x11);
static constexpr System::Int8 SQL_CREATE_CLUSTER = System::Int8(0x12);
static constexpr System::Int8 SQL_CREATE_USER = System::Int8(0x13);
static constexpr System::Int8 SQL_CREATE_INDEX = System::Int8(0x14);
static constexpr System::Int8 SQL_DROP_INDEX = System::Int8(0x15);
static constexpr System::Int8 SQL_DROP_CLUSTER = System::Int8(0x16);
static constexpr System::Int8 SQL_VALIDATE_INDEX = System::Int8(0x17);
static constexpr System::Int8 SQL_CREATE_PROCEDURE = System::Int8(0x18);
static constexpr System::Int8 SQL_ALTER_PROCEDURE = System::Int8(0x19);
static constexpr System::Int8 SQL_ALTER_TABLE = System::Int8(0x1a);
static constexpr System::Int8 SQL_EXPLAIN = System::Int8(0x1b);
static constexpr System::Int8 SQL_GRANT = System::Int8(0x1c);
static constexpr System::Int8 SQL_REVOKE = System::Int8(0x1d);
static constexpr System::Int8 SQL_CREATE_SYNONYM = System::Int8(0x1e);
static constexpr System::Int8 SQL_DROP_SYNONYM = System::Int8(0x1f);
static constexpr System::Int8 SQL_ALTER_SYSTEM_SWITCH = System::Int8(0x20);
static constexpr System::Int8 SQL_SET_TRANSACTION = System::Int8(0x21);
static constexpr System::Int8 SQL_PLSQL = System::Int8(0x22);
static constexpr System::Int8 SQL_LOCK = System::Int8(0x23);
static constexpr System::Int8 SQL_RENAME = System::Int8(0x25);
static constexpr System::Int8 SQL_COMMENT = System::Int8(0x26);
static constexpr System::Int8 SQL_AUDIT = System::Int8(0x27);
static constexpr System::Int8 SQL_NOAUDIT = System::Int8(0x28);
static constexpr System::Int8 SQL_ALTER_INDEX = System::Int8(0x29);
static constexpr System::Int8 SQL_CREATE_EXTERNAL_DATABASE = System::Int8(0x2a);
static constexpr System::Int8 SQL_DROP_EXTERNAL_DATABASE = System::Int8(0x2b);
static constexpr System::Int8 SQL_CREATE_DATABASE = System::Int8(0x2c);
static constexpr System::Int8 SQL_ALTER_DATABASE = System::Int8(0x2d);
static constexpr System::Int8 SQL_CREATE_ROLLBACK_SEGMENT = System::Int8(0x2e);
static constexpr System::Int8 SQL_ALTER_ROLLBACK_SEGMENT = System::Int8(0x2f);
static constexpr System::Int8 SQL_DROP_ROLLBACK_SEGMENT = System::Int8(0x30);
static constexpr System::Int8 SQL_CREATE_TABLESPACE = System::Int8(0x31);
static constexpr System::Int8 SQL_ALTER_TABLESPACE = System::Int8(0x32);
static constexpr System::Int8 SQL_DROP_TABLESPACE = System::Int8(0x33);
static constexpr System::Int8 SQL_ALTER_SESSION = System::Int8(0x34);
static constexpr System::Int8 SQL_ALTER_USER = System::Int8(0x35);
static constexpr System::Int8 SQL_COMMIT = System::Int8(0x36);
static constexpr System::Int8 SQL_ROLLBACK = System::Int8(0x37);
static constexpr System::Int8 SQL_SAVEPOINT = System::Int8(0x38);
static constexpr System::Int8 SQL_CREATE_CONTROL_FILE = System::Int8(0x39);
static constexpr System::Int8 SQL_ALTER_TRACING = System::Int8(0x3a);
static constexpr System::Int8 SQL_CREATE_TRIGGER = System::Int8(0x3b);
static constexpr System::Int8 SQL_ALTER_TRIGGER = System::Int8(0x3c);
static constexpr System::Int8 SQL_DROP_TRIGGER = System::Int8(0x3d);
static constexpr System::Int8 SQL_ANALYZE_TABLE = System::Int8(0x3e);
static constexpr System::Int8 SQL_ANALYZE_INDEX = System::Int8(0x3f);
static constexpr System::Int8 SQL_ANALYZE_CLUSTER = System::Int8(0x40);
static constexpr System::Int8 SQL_CREATE_PROFILE = System::Int8(0x41);
static constexpr System::Int8 SQL_DROP_PROFILE = System::Int8(0x42);
static constexpr System::Int8 SQL_ALTER_PROFILE = System::Int8(0x43);
static constexpr System::Int8 SQL_DROP_PROCEDURE = System::Int8(0x44);
static constexpr System::Int8 SQL_ALTER_RESOURCE_COST = System::Int8(0x46);
static constexpr System::Int8 SQL_CREATE_SNAPSHOT_LOG = System::Int8(0x47);
static constexpr System::Int8 SQL_ALTER_SNAPSHOT_LOG = System::Int8(0x48);
static constexpr System::Int8 SQL_DROP_SNAPSHOT_LOG = System::Int8(0x49);
static constexpr System::Int8 SQL_CREATE_SNAPSHOT = System::Int8(0x4a);
static constexpr System::Int8 SQL_ALTER_SNAPSHOT = System::Int8(0x4b);
static constexpr System::Int8 SQL_DROP_SNAPSHOT = System::Int8(0x4c);
static constexpr System::Int8 SQL_CREATE_TYPE = System::Int8(0x4d);
static constexpr System::Int8 SQL_DROP_TYPE = System::Int8(0x4e);
static constexpr System::Int8 SQL_ALTER_ROLE = System::Int8(0x4f);
static constexpr System::Int8 SQL_ALTER_TYPE = System::Int8(0x50);
static constexpr System::Int8 SQL_CREATE_TYPE_BODY = System::Int8(0x51);
static constexpr System::Int8 SQL_ALTER_TYPE_BODY = System::Int8(0x52);
static constexpr System::Int8 SQL_DROP_TYPE_BODY = System::Int8(0x53);
static constexpr System::Int8 SQL_DROP_LIBRARY = System::Int8(0x54);
static constexpr System::Int8 SQL_TRUNCATE_TABLE = System::Int8(0x55);
static constexpr System::Int8 SQL_TRUNCATE_CLUSTER = System::Int8(0x56);
static constexpr System::Int8 SQL_CREATE_BITMAPFILE = System::Int8(0x57);
static constexpr System::Int8 SQL_ALTER_VIEW = System::Int8(0x58);
static constexpr System::Int8 SQL_DROP_BITMAPFILE = System::Int8(0x59);
static constexpr System::Int8 SQL_SET_CONSTRAINTS = System::Int8(0x5a);
static constexpr System::Int8 SQL_CREATE_FUNCTION = System::Int8(0x5b);
static constexpr System::Int8 SQL_ALTER_FUNCTION = System::Int8(0x5c);
static constexpr System::Int8 SQL_DROP_FUNCTION = System::Int8(0x5d);
static constexpr System::Int8 SQL_CREATE_PACKAGE = System::Int8(0x5e);
static constexpr System::Int8 SQL_ALTER_PACKAGE = System::Int8(0x5f);
static constexpr System::Int8 SQL_DROP_PACKAGE = System::Int8(0x60);
static constexpr System::Int8 SQL_CREATE_PACKAGE_BODY = System::Int8(0x61);
static constexpr System::Int8 SQL_ALTER_PACKAGE_BODY = System::Int8(0x62);
static constexpr System::Int8 SQL_DROP_PACKAGE_BODY = System::Int8(0x63);
static constexpr System::Byte SQL_CREATE_DIRECTORY = System::Byte(0x9d);
static constexpr System::Byte SQL_DROP_DIRECTORY = System::Byte(0x9e);
static constexpr System::Byte SQL_CREATE_LIBRARY = System::Byte(0x9f);
static constexpr System::Byte SQL_CREATE_JAVA = System::Byte(0xa0);
static constexpr System::Byte SQL_ALTER_JAVA = System::Byte(0xa1);
static constexpr System::Byte SQL_DROP_JAVA = System::Byte(0xa2);
static constexpr System::Byte SQL_CREATE_OPERATOR = System::Byte(0xa3);
static constexpr System::Byte SQL_CREATE_INDEXTYPE = System::Byte(0xa4);
static constexpr System::Byte SQL_DROP_INDEXTYPE = System::Byte(0xa5);
static constexpr System::Byte SQL_ALTER_INDEXTYPE = System::Byte(0xa6);
static constexpr System::Byte SQL_DROP_OPERATOR = System::Byte(0xa7);
static constexpr System::Byte SQL_ASSOCIATE_STATISTICS = System::Byte(0xa8);
static constexpr System::Byte SQL_DISASSOCIATE_STATISTICS = System::Byte(0xa9);
static constexpr System::Byte SQL_CALL_METHOD = System::Byte(0xaa);
static constexpr System::Byte SQL_CREATE_SUMMARY = System::Byte(0xab);
static constexpr System::Byte SQL_ALTER_SUMMARY = System::Byte(0xac);
static constexpr System::Byte SQL_DROP_SUMMARY = System::Byte(0xad);
static constexpr System::Byte SQL_CREATE_DIMENSION = System::Byte(0xae);
static constexpr System::Byte SQL_ALTER_DIMENSION = System::Byte(0xaf);
static constexpr System::Byte SQL_DROP_DIMENSION = System::Byte(0xb0);
static constexpr System::Byte SQL_CREATE_CONTEXT = System::Byte(0xb1);
static constexpr System::Byte SQL_DROP_CONTEXT = System::Byte(0xb2);
static constexpr System::Byte SQL_ALTER_OUTLINE = System::Byte(0xb3);
static constexpr System::Byte SQL_CREATE_OUTLINE = System::Byte(0xb4);
static constexpr System::Byte SQL_DROP_OUTLINE = System::Byte(0xb5);
static constexpr System::Byte SQL_UPDATE_INDEXES = System::Byte(0xb6);
static constexpr System::Byte SQL_ALTER_OPERATOR = System::Byte(0xb7);
static constexpr System::Byte SQL_ALTER_SYNONYM = System::Byte(0xc0);
static constexpr System::Byte SQL_PURGE_USER_RECYCLEBIN = System::Byte(0xc5);
static constexpr System::Byte SQL_PURGE_DBA_RECYCLEBIN = System::Byte(0xc6);
static constexpr System::Byte SQL_PURGE_TABLESAPCE = System::Byte(0xc7);
static constexpr System::Byte SQL_PURGE_TABLE = System::Byte(0xc8);
static constexpr System::Byte SQL_PURGE_INDEX = System::Byte(0xc9);
static constexpr System::Byte SQL_UNDROP_OBJECT = System::Byte(0xca);
static constexpr System::Byte SQL_FLASHBACK_DATABASE = System::Byte(0xcc);
static constexpr System::Byte SQL_FLASHBACK_TABLE = System::Byte(0xcd);
static constexpr System::Byte SQL_CREATE_RESTORE_POINT = System::Byte(0xce);
static constexpr System::Byte SQL_DROP_RESTORE_POINT = System::Byte(0xcf);
static constexpr System::Byte SQL_PROXY_AUTHENTICATION_ONLY = System::Byte(0xd0);
static constexpr System::Byte SQL_DECLARE_REWRITE_EQUIVALENCE = System::Byte(0xd1);
static constexpr System::Byte SQL_ALTER_REWRITE_EQUIVALENCE = System::Byte(0xd2);
static constexpr System::Byte SQL_DROP_REWRITE_EQUIVALENCE = System::Byte(0xd3);
static constexpr System::Int8 FC_OOPEN = System::Int8(0xe);
static constexpr System::Int8 OCI_EV_DEF = System::Int8(0x0);
static constexpr System::Int8 OCI_EV_TSF = System::Int8(0x1);
static constexpr System::Int8 OCI_LM_DEF = System::Int8(0x0);
static constexpr System::Int8 OCI_LM_NBL = System::Int8(0x1);
static constexpr System::Int8 OCI_ONE_PIECE = System::Int8(0x0);
static constexpr System::Int8 OCI_FIRST_PIECE = System::Int8(0x1);
static constexpr System::Int8 OCI_NEXT_PIECE = System::Int8(0x2);
static constexpr System::Int8 OCI_LAST_PIECE = System::Int8(0x3);
static constexpr System::Int8 OCI_PARSE_NODEFER = System::Int8(0x0);
static constexpr System::Int8 OCI_PARSE_DEFER = System::Int8(0x1);
static constexpr System::Int8 OCI_LANG_V6 = System::Int8(0x0);
static constexpr System::Int8 OCI_LANG_NORM = System::Int8(0x1);
static constexpr System::Int8 OCI_LANG_V7 = System::Int8(0x2);
static constexpr System::Int8 SQLCS_IMPLICIT = System::Int8(0x1);
static constexpr System::Int8 SQLCS_NCHAR = System::Int8(0x2);
static constexpr System::Int8 SQLCS_EXPLICIT = System::Int8(0x3);
static constexpr System::Int8 SQLCS_FLEXIBLE = System::Int8(0x4);
static constexpr System::Int8 SQLCS_LIT_NULL = System::Int8(0x5);
static constexpr int SizeOfTCDAHead = int(0x30);
static constexpr System::Int8 OCI_DEFAULT = System::Int8(0x0);
static constexpr System::Int8 OCI_THREADED = System::Int8(0x1);
static constexpr System::Int8 OCI_OBJECT = System::Int8(0x2);
static constexpr System::Int8 OCI_EVENTS = System::Int8(0x4);
static constexpr System::Int8 OCI_ENV_NO_MUTEX = System::Int8(0x8);
static constexpr System::Int8 OCI_SHARED = System::Int8(0x10);
static constexpr System::Int8 OCI_RESERVED2 = System::Int8(0x20);
static constexpr System::Int8 OCI_NO_UCB = System::Int8(0x40);
static constexpr System::Byte OCI_NO_MUTEX = System::Byte(0x80);
static constexpr System::Word OCI_SHARED_EXT = System::Word(0x100);
static constexpr System::Word OCI_CACHE = System::Word(0x200);
static constexpr System::Word OCI_NO_CACHE = System::Word(0x400);
static constexpr System::Word OCI_UTF16 = System::Word(0x4000);
static constexpr int OCI_NEW_LENGTH_SEMANTICS = int(0x20000);
static constexpr int OCI_NCHAR_LITERAL_REPLACE_ON = int(0x400000);
static constexpr int OCI_NCHAR_LITERAL_REPLACE_OFF = int(0x800000);
static constexpr System::Int8 OCI_SESSGET_SPOOL = System::Int8(0x1);
static constexpr System::Int8 OCI_SESSGET_STMTCACHE = System::Int8(0x4);
static constexpr System::Int8 OCI_SPC_REINITIALIZE = System::Int8(0x1);
static constexpr System::Int8 OCI_SPC_HOMOGENEOUS = System::Int8(0x2);
static constexpr System::Int8 OCI_SPC_STMTCACHE = System::Int8(0x4);
static constexpr System::Int8 OCI_HTYPE_FIRST = System::Int8(0x1);
static constexpr System::Int8 OCI_HTYPE_ENV = System::Int8(0x1);
static constexpr System::Int8 OCI_HTYPE_ERROR = System::Int8(0x2);
static constexpr System::Int8 OCI_HTYPE_SVCCTX = System::Int8(0x3);
static constexpr System::Int8 OCI_HTYPE_STMT = System::Int8(0x4);
static constexpr System::Int8 OCI_HTYPE_BIND = System::Int8(0x5);
static constexpr System::Int8 OCI_HTYPE_DEFINE = System::Int8(0x6);
static constexpr System::Int8 OCI_HTYPE_DESCRIBE = System::Int8(0x7);
static constexpr System::Int8 OCI_HTYPE_SERVER = System::Int8(0x8);
static constexpr System::Int8 OCI_HTYPE_SESSION = System::Int8(0x9);
static constexpr System::Int8 OCI_HTYPE_AUTHINFO = System::Int8(0x9);
static constexpr System::Int8 OCI_HTYPE_TRANS = System::Int8(0xa);
static constexpr System::Int8 OCI_HTYPE_COMPLEXOBJECT = System::Int8(0xb);
static constexpr System::Int8 OCI_HTYPE_SECURITY = System::Int8(0xc);
static constexpr System::Int8 OCI_HTYPE_SUBSCRIPTION = System::Int8(0xd);
static constexpr System::Int8 OCI_HTYPE_DIRPATH_CTX = System::Int8(0xe);
static constexpr System::Int8 OCI_HTYPE_DIRPATH_COLUMN_ARRAY = System::Int8(0xf);
static constexpr System::Int8 OCI_HTYPE_DIRPATH_STREAM = System::Int8(0x10);
static constexpr System::Int8 OCI_HTYPE_PROC = System::Int8(0x11);
static constexpr System::Int8 OCI_HTYPE_LAST = System::Int8(0x11);
static constexpr System::Int8 OCI_HTYPE_SPOOL = System::Int8(0x1b);
static constexpr System::Int8 OCI_DTYPE_FIRST = System::Int8(0x32);
static constexpr System::Int8 OCI_DTYPE_LOB = System::Int8(0x32);
static constexpr System::Int8 OCI_DTYPE_SNAP = System::Int8(0x33);
static constexpr System::Int8 OCI_DTYPE_RSET = System::Int8(0x34);
static constexpr System::Int8 OCI_DTYPE_PARAM = System::Int8(0x35);
static constexpr System::Int8 OCI_DTYPE_ROWID = System::Int8(0x36);
static constexpr System::Int8 OCI_DTYPE_COMPLEXOBJECTCOMP = System::Int8(0x37);
static constexpr System::Int8 OCI_DTYPE_FILE = System::Int8(0x38);
static constexpr System::Int8 OCI_DTYPE_AQENQ_OPTIONS = System::Int8(0x39);
static constexpr System::Int8 OCI_DTYPE_AQDEQ_OPTIONS = System::Int8(0x3a);
static constexpr System::Int8 OCI_DTYPE_AQMSG_PROPERTIES = System::Int8(0x3b);
static constexpr System::Int8 OCI_DTYPE_AQAGENT = System::Int8(0x3c);
static constexpr System::Int8 OCI_DTYPE_INTERVAL_YM = System::Int8(0x3e);
static constexpr System::Int8 OCI_DTYPE_INTERVAL_DS = System::Int8(0x3f);
static constexpr System::Int8 OCI_DTYPE_AQNFY_DESCRIPTOR = System::Int8(0x40);
static constexpr System::Int8 OCI_DTYPE_DATE = System::Int8(0x41);
static constexpr System::Int8 OCI_DTYPE_TIME = System::Int8(0x42);
static constexpr System::Int8 OCI_DTYPE_TIME_TZ = System::Int8(0x43);
static constexpr System::Int8 OCI_DTYPE_TIMESTAMP = System::Int8(0x44);
static constexpr System::Int8 OCI_DTYPE_TIMESTAMP_TZ = System::Int8(0x45);
static constexpr System::Int8 OCI_DTYPE_TIMESTAMP_LTZ = System::Int8(0x46);
static constexpr System::Int8 OCI_DTYPE_UCB = System::Int8(0x47);
static constexpr System::Int8 OCI_DTYPE_SRVDN = System::Int8(0x48);
static constexpr System::Int8 OCI_DTYPE_SIGNATURE = System::Int8(0x49);
static constexpr System::Int8 OCI_DTYPE_XML_STREAM = System::Int8(0x4a);
static constexpr System::Int8 OCI_DTYPE_AQLIS_OPTIONS = System::Int8(0x4b);
static constexpr System::Int8 OCI_DTYPE_AQLIS_MSG_PROPERTIES = System::Int8(0x4c);
static constexpr System::Int8 OCI_DTYPE_CHDES = System::Int8(0x4d);
static constexpr System::Int8 OCI_DTYPE_TABLE_CHDES = System::Int8(0x4e);
static constexpr System::Int8 OCI_DTYPE_ROW_CHDES = System::Int8(0x4f);
static constexpr System::Int8 OCI_OTYPE_NAME = System::Int8(0x1);
static constexpr System::Int8 OCI_OTYPE_REF = System::Int8(0x2);
static constexpr System::Int8 OCI_OTYPE_PTR = System::Int8(0x3);
static constexpr System::Int8 OCI_ATTR_FNCODE = System::Int8(0x1);
static constexpr System::Int8 OCI_ATTR_OBJECT = System::Int8(0x2);
static constexpr System::Int8 OCI_ATTR_NONBLOCKING_MODE = System::Int8(0x3);
static constexpr System::Int8 OCI_ATTR_SQLCODE = System::Int8(0x4);
static constexpr System::Int8 OCI_ATTR_ENV = System::Int8(0x5);
static constexpr System::Int8 OCI_ATTR_SERVER = System::Int8(0x6);
static constexpr System::Int8 OCI_ATTR_SESSION = System::Int8(0x7);
static constexpr System::Int8 OCI_ATTR_TRANS = System::Int8(0x8);
static constexpr System::Int8 OCI_ATTR_ROW_COUNT = System::Int8(0x9);
static constexpr System::Int8 OCI_ATTR_SQLFNCODE = System::Int8(0xa);
static constexpr System::Int8 OCI_ATTR_PREFETCH_ROWS = System::Int8(0xb);
static constexpr System::Int8 OCI_ATTR_NESTED_PREFETCH_ROWS = System::Int8(0xc);
static constexpr System::Int8 OCI_ATTR_PREFETCH_MEMORY = System::Int8(0xd);
static constexpr System::Int8 OCI_ATTR_NESTED_PREFETCH_MEMORY = System::Int8(0xe);
static constexpr System::Int8 OCI_ATTR_CHAR_COUNT = System::Int8(0xf);
static constexpr System::Int8 OCI_ATTR_PDSCL = System::Int8(0x10);
static constexpr System::Int8 OCI_ATTR_FSPRECISION = System::Int8(0x10);
static constexpr System::Int8 OCI_ATTR_PDFMT = System::Int8(0x11);
static constexpr System::Int8 OCI_ATTR_PARAM_COUNT = System::Int8(0x12);
static constexpr System::Int8 OCI_ATTR_ROWID = System::Int8(0x13);
static constexpr System::Int8 OCI_ATTR_CHARSET = System::Int8(0x14);
static constexpr System::Int8 OCI_ATTR_NCHAR = System::Int8(0x15);
static constexpr System::Int8 OCI_ATTR_USERNAME = System::Int8(0x16);
static constexpr System::Int8 OCI_ATTR_PASSWORD = System::Int8(0x17);
static constexpr System::Int8 OCI_ATTR_STMT_TYPE = System::Int8(0x18);
static constexpr System::Int8 OCI_ATTR_INTERNAL_NAME = System::Int8(0x19);
static constexpr System::Int8 OCI_ATTR_EXTERNAL_NAME = System::Int8(0x1a);
static constexpr System::Int8 OCI_ATTR_XID = System::Int8(0x1b);
static constexpr System::Int8 OCI_ATTR_TRANS_LOCK = System::Int8(0x1c);
static constexpr System::Int8 OCI_ATTR_TRANS_NAME = System::Int8(0x1d);
static constexpr System::Int8 OCI_ATTR_HEAPALLOC = System::Int8(0x1e);
static constexpr System::Int8 OCI_ATTR_CHARSET_ID = System::Int8(0x1f);
static constexpr System::Int8 OCI_ATTR_CHARSET_FORM = System::Int8(0x20);
static constexpr System::Int8 OCI_ATTR_MAXDATA_SIZE = System::Int8(0x21);
static constexpr System::Int8 OCI_ATTR_CACHE_OPT_SIZE = System::Int8(0x22);
static constexpr System::Int8 OCI_ATTR_CACHE_MAX_SIZE = System::Int8(0x23);
static constexpr System::Int8 OCI_ATTR_PINOPTION = System::Int8(0x24);
static constexpr System::Int8 OCI_ATTR_ALLOC_DURATION = System::Int8(0x25);
static constexpr System::Int8 OCI_ATTR_PIN_DURATION = System::Int8(0x26);
static constexpr System::Int8 OCI_ATTR_FDO = System::Int8(0x27);
static constexpr System::Int8 OCI_ATTR_POSTPROCESSING_CALLBACK = System::Int8(0x28);
static constexpr System::Int8 OCI_ATTR_POSTPROCESSING_CONTEXT = System::Int8(0x29);
static constexpr System::Int8 OCI_ATTR_ROWS_RETURNED = System::Int8(0x2a);
static constexpr System::Int8 OCI_ATTR_FOCBK = System::Int8(0x2b);
static constexpr System::Int8 OCI_ATTR_IN_V8_MODE = System::Int8(0x2c);
static constexpr System::Int8 OCI_ATTR_LOBEMPTY = System::Int8(0x2d);
static constexpr System::Int8 OCI_ATTR_SESSLANG = System::Int8(0x2e);
static constexpr System::Int8 OCI_ATTR_VISIBILITY = System::Int8(0x2f);
static constexpr System::Int8 OCI_ATTR_RELATIVE_MSGID = System::Int8(0x30);
static constexpr System::Int8 OCI_ATTR_SEQUENCE_DEVIATION = System::Int8(0x31);
static constexpr System::Int8 OCI_ATTR_CONSUMER_NAME = System::Int8(0x32);
static constexpr System::Int8 OCI_ATTR_DEQ_MODE = System::Int8(0x33);
static constexpr System::Int8 OCI_ATTR_NAVIGATION = System::Int8(0x34);
static constexpr System::Int8 OCI_ATTR_WAIT = System::Int8(0x35);
static constexpr System::Int8 OCI_ATTR_DEQ_MSGID = System::Int8(0x36);
static constexpr System::Int8 OCI_ATTR_PRIORITY = System::Int8(0x37);
static constexpr System::Int8 OCI_ATTR_DELAY = System::Int8(0x38);
static constexpr System::Int8 OCI_ATTR_EXPIRATION = System::Int8(0x39);
static constexpr System::Int8 OCI_ATTR_CORRELATION = System::Int8(0x3a);
static constexpr System::Int8 OCI_ATTR_ATTEMPTS = System::Int8(0x3b);
static constexpr System::Int8 OCI_ATTR_RECIPIENT_LIST = System::Int8(0x3c);
static constexpr System::Int8 OCI_ATTR_EXCEPTION_QUEUE = System::Int8(0x3d);
static constexpr System::Int8 OCI_ATTR_ENQ_TIME = System::Int8(0x3e);
static constexpr System::Int8 OCI_ATTR_MSG_STATE = System::Int8(0x3f);
static constexpr System::Int8 OCI_ATTR_AGENT_NAME = System::Int8(0x40);
static constexpr System::Int8 OCI_ATTR_AGENT_ADDRESS = System::Int8(0x41);
static constexpr System::Int8 OCI_ATTR_AGENT_PROTOCOL = System::Int8(0x42);
static constexpr System::Int8 OCI_ATTR_SENDER_ID = System::Int8(0x44);
static constexpr System::Int8 OCI_ATTR_ORIGINAL_MSGID = System::Int8(0x45);
static constexpr System::Int8 OCI_ATTR_QUEUE_NAME = System::Int8(0x46);
static constexpr System::Int8 OCI_ATTR_NFY_MSGID = System::Int8(0x47);
static constexpr System::Int8 OCI_ATTR_MSG_PROP = System::Int8(0x48);
static constexpr System::Int8 OCI_ATTR_NUM_DML_ERRORS = System::Int8(0x49);
static constexpr System::Int8 OCI_ATTR_DML_ROW_OFFSET = System::Int8(0x4a);
static constexpr System::Int8 OCI_ATTR_DATEFORMAT = System::Int8(0x4b);
static constexpr System::Int8 OCI_ATTR_BUF_ADDR = System::Int8(0x4c);
static constexpr System::Int8 OCI_ATTR_BUF_SIZE = System::Int8(0x4d);
static constexpr System::Int8 OCI_ATTR_DIRPATH_MODE = System::Int8(0x4e);
static constexpr System::Int8 OCI_ATTR_DIRPATH_NOLOG = System::Int8(0x4f);
static constexpr System::Int8 OCI_ATTR_DIRPATH_PARALLEL = System::Int8(0x50);
static constexpr System::Int8 OCI_ATTR_NUM_ROWS = System::Int8(0x51);
static constexpr System::Int8 OCI_ATTR_COL_COUNT = System::Int8(0x52);
static constexpr System::Int8 OCI_ATTR_STREAM_OFFSET = System::Int8(0x53);
static constexpr System::Int8 OCI_ATTR_SHARED_HEAPALLOC = System::Int8(0x54);
static constexpr System::Int8 OCI_ATTR_SERVER_GROUP = System::Int8(0x55);
static constexpr System::Int8 OCI_ATTR_MIGSESSION = System::Int8(0x56);
static constexpr System::Int8 OCI_ATTR_NOCACHE = System::Int8(0x57);
static constexpr System::Int8 OCI_ATTR_MEMPOOL_SIZE = System::Int8(0x58);
static constexpr System::Int8 OCI_ATTR_MEMPOOL_INSTNAME = System::Int8(0x59);
static constexpr System::Int8 OCI_ATTR_MEMPOOL_APPNAME = System::Int8(0x5a);
static constexpr System::Int8 OCI_ATTR_MEMPOOL_HOMENAME = System::Int8(0x5b);
static constexpr System::Int8 OCI_ATTR_MEMPOOL_MODEL = System::Int8(0x5c);
static constexpr System::Int8 OCI_ATTR_MODES = System::Int8(0x5d);
static constexpr System::Int8 OCI_ATTR_SUBSCR_NAME = System::Int8(0x5e);
static constexpr System::Int8 OCI_ATTR_SUBSCR_CALLBACK = System::Int8(0x5f);
static constexpr System::Int8 OCI_ATTR_SUBSCR_CTX = System::Int8(0x60);
static constexpr System::Int8 OCI_ATTR_SUBSCR_PAYLOAD = System::Int8(0x61);
static constexpr System::Int8 OCI_ATTR_SUBSCR_NAMESPACE = System::Int8(0x62);
static constexpr System::Int8 OCI_ATTR_PROXY_CREDENTIALS = System::Int8(0x63);
static constexpr System::Int8 OCI_ATTR_INITIAL_CLIENT_ROLES = System::Int8(0x64);
static constexpr System::Int8 OCI_ATTR_UNK = System::Int8(0x65);
static constexpr System::Int8 OCI_ATTR_NUM_COLS = System::Int8(0x66);
static constexpr System::Int8 OCI_ATTR_LIST_COLUMNS = System::Int8(0x67);
static constexpr System::Int8 OCI_ATTR_RDBA = System::Int8(0x68);
static constexpr System::Int8 OCI_ATTR_CLUSTERED = System::Int8(0x69);
static constexpr System::Int8 OCI_ATTR_PARTITIONED = System::Int8(0x6a);
static constexpr System::Int8 OCI_ATTR_INDEX_ONLY = System::Int8(0x6b);
static constexpr System::Int8 OCI_ATTR_LIST_ARGUMENTS = System::Int8(0x6c);
static constexpr System::Int8 OCI_ATTR_LIST_SUBPROGRAMS = System::Int8(0x6d);
static constexpr System::Int8 OCI_ATTR_REF_TDO = System::Int8(0x6e);
static constexpr System::Int8 OCI_ATTR_LINK = System::Int8(0x6f);
static constexpr System::Int8 OCI_ATTR_MIN = System::Int8(0x70);
static constexpr System::Int8 OCI_ATTR_MAX = System::Int8(0x71);
static constexpr System::Int8 OCI_ATTR_INCR = System::Int8(0x72);
static constexpr System::Int8 OCI_ATTR_CACHE = System::Int8(0x73);
static constexpr System::Int8 OCI_ATTR_ORDER = System::Int8(0x74);
static constexpr System::Int8 OCI_ATTR_HW_MARK = System::Int8(0x75);
static constexpr System::Int8 OCI_ATTR_TYPE_SCHEMA = System::Int8(0x76);
static constexpr System::Int8 OCI_ATTR_TIMESTAMP = System::Int8(0x77);
static constexpr System::Int8 OCI_ATTR_NUM_ATTRS = System::Int8(0x78);
static constexpr System::Int8 OCI_ATTR_NUM_PARAMS = System::Int8(0x79);
static constexpr System::Int8 OCI_ATTR_OBJID = System::Int8(0x7a);
static constexpr System::Int8 OCI_ATTR_PTYPE = System::Int8(0x7b);
static constexpr System::Int8 OCI_ATTR_PARAM = System::Int8(0x7c);
static constexpr System::Int8 OCI_ATTR_OVERLOAD_ID = System::Int8(0x7d);
static constexpr System::Int8 OCI_ATTR_TABLESPACE = System::Int8(0x7e);
static constexpr System::Int8 OCI_ATTR_TDO = System::Int8(0x7f);
static constexpr System::Byte OCI_ATTR_LTYPE = System::Byte(0x80);
static constexpr System::Byte OCI_ATTR_PARSE_ERROR_OFFSET = System::Byte(0x81);
static constexpr System::Byte OCI_ATTR_IS_TEMPORARY = System::Byte(0x82);
static constexpr System::Byte OCI_ATTR_IS_TYPED = System::Byte(0x83);
static constexpr System::Byte OCI_ATTR_DURATION = System::Byte(0x84);
static constexpr System::Byte OCI_ATTR_IS_INVOKER_RIGHTS = System::Byte(0x85);
static constexpr System::Byte OCI_ATTR_OBJ_NAME = System::Byte(0x86);
static constexpr System::Byte OCI_ATTR_OBJ_SCHEMA = System::Byte(0x87);
static constexpr System::Byte OCI_ATTR_OBJ_ID = System::Byte(0x88);
static constexpr System::Byte OCI_ATTR_MAXCHAR_SIZE = System::Byte(0xa3);
static constexpr System::Byte OCI_ATTR_CURRENT_POSITION = System::Byte(0xa4);
static constexpr System::Byte OCI_ATTR_STMTCACHESIZE = System::Byte(0xb0);
static constexpr System::Byte OCI_ATTR_STMT_STATE = System::Byte(0xb6);
static constexpr System::Byte OCI_ATTR_ENV_UTF16 = System::Byte(0xd1);
static constexpr System::Int8 OCI_SUBSCR_CQ_QOS_QUERY = System::Int8(0x1);
static constexpr System::Int8 OCI_SUBSCR_CQ_QOS_BEST_EFFORT = System::Int8(0x2);
static constexpr System::Byte OCI_ATTR_SUBSCR_QOSFLAGS = System::Byte(0xe1);
static constexpr System::Byte OCI_ATTR_SUBSCR_PAYLOADCBK = System::Byte(0xe2);
static constexpr System::Byte OCI_ATTR_SUBSCR_TIMEOUT = System::Byte(0xe3);
static constexpr System::Byte OCI_ATTR_SUBSCR_NAMESPACE_CTX = System::Byte(0xe4);
static constexpr System::Byte OCI_ATTR_SUBSCR_CQ_QOSFLAGS = System::Byte(0xe5);
static constexpr System::Word OCI_ATTR_CLIENT_IDENTIFIER = System::Word(0x116);
static constexpr System::Word OCI_ATTR_IS_XMLTYPE = System::Word(0x13b);
static constexpr System::Word OCI_ATTR_XMLSCHEMA_NAME = System::Word(0x13c);
static constexpr System::Word OCI_ATTR_XMLELEMENT_NAME = System::Word(0x13d);
static constexpr System::Word OCI_ATTR_XMLSQLTYPSCH_NAME = System::Word(0x13e);
static constexpr System::Word OCI_ATTR_XMLSQLTYPE_NAME = System::Word(0x13f);
static constexpr System::Word OCI_ATTR_XMLTYPE_STORED_OBJ = System::Word(0x140);
static constexpr System::Word OCI_ATTR_TRANSACTION_NO = System::Word(0x16d);
static constexpr System::Word OCI_ATTR_SUBSCR_PORTNO = System::Word(0x186);
static constexpr System::Word OCI_ATTR_CHNF_TABLENAMES = System::Word(0x191);
static constexpr System::Word OCI_ATTR_CHNF_ROWIDS = System::Word(0x192);
static constexpr System::Word OCI_ATTR_CHNF_OPERATIONS = System::Word(0x193);
static constexpr System::Word OCI_ATTR_CHNF_CHANGELAG = System::Word(0x194);
static constexpr System::Word OCI_ATTR_CHDES_DBNAME = System::Word(0x195);
static constexpr System::Word OCI_ATTR_CHDES_NFYTYPE = System::Word(0x196);
static constexpr System::Word OCI_ATTR_CHDES_XID = System::Word(0x197);
static constexpr System::Word OCI_ATTR_CHDES_TABLE_CHANGES = System::Word(0x198);
static constexpr System::Word OCI_ATTR_CHDES_TABLE_NAME = System::Word(0x199);
static constexpr System::Word OCI_ATTR_CHDES_TABLE_OPFLAGS = System::Word(0x19a);
static constexpr System::Word OCI_ATTR_CHDES_TABLE_ROW_CHANGES = System::Word(0x19b);
static constexpr System::Word OCI_ATTR_CHDES_ROW_ROWID = System::Word(0x19c);
static constexpr System::Word OCI_ATTR_CHDES_ROW_OPFLAGS = System::Word(0x19d);
static constexpr System::Word OCI_ATTR_CHNF_REGHANDLE = System::Word(0x19e);
static constexpr System::Word OCI_ATTR_DEFAULT_LOBPREFETCH_SIZE = System::Word(0x1b6);
static constexpr System::Word OCI_ATTR_LOBPREFETCH_SIZE = System::Word(0x1b7);
static constexpr System::Word OCI_ATTR_LOBPREFETCH_LENGTH = System::Word(0x1b8);
static constexpr System::Word OCI_ATTR_MSG_DELIVERY_MODE = System::Word(0x197);
static constexpr System::Word OCI_ATTR_IMPLICIT_RESULT_COUNT = System::Word(0x1cf);
static constexpr System::Word OCI_ATTR_CONNECTION_TIMEOUT = System::Word(0x2711);
static constexpr System::Word OCI_ATTR_IP_VERSION = System::Word(0x2712);
static constexpr System::Int8 OCI_RESULT_TYPE_SELECT = System::Int8(0x1);
static constexpr System::Int8 OCI_EVENT_NONE = System::Int8(0x0);
static constexpr System::Int8 OCI_EVENT_STARTUP = System::Int8(0x1);
static constexpr System::Int8 OCI_EVENT_SHUTDOWN = System::Int8(0x2);
static constexpr System::Int8 OCI_EVENT_SHUTDOWN_ANY = System::Int8(0x3);
static constexpr System::Int8 OCI_EVENT_DROP_DB = System::Int8(0x4);
static constexpr System::Int8 OCI_EVENT_DEREG = System::Int8(0x5);
static constexpr System::Int8 OCI_EVENT_OBJCHANGE = System::Int8(0x6);
static constexpr System::Int8 OCI_EVENT_QUERYCHANGE = System::Int8(0x7);
static constexpr System::Int8 OCI_OPCODE_ALLROWS = System::Int8(0x1);
static constexpr System::Int8 OCI_OPCODE_ALLOPS = System::Int8(0x0);
static constexpr System::Int8 OCI_OPCODE_INSERT = System::Int8(0x2);
static constexpr System::Int8 OCI_OPCODE_UPDATE = System::Int8(0x4);
static constexpr System::Int8 OCI_OPCODE_DELETE = System::Int8(0x8);
static constexpr System::Int8 OCI_OPCODE_ALTER = System::Int8(0x10);
static constexpr System::Int8 OCI_OPCODE_DROP = System::Int8(0x20);
static constexpr System::Int8 OCI_OPCODE_UNKNOWN = System::Int8(0x40);
static constexpr System::Int8 OCI_SUBSCR_QOS_RELIABLE = System::Int8(0x1);
static constexpr System::Int8 OCI_SUBSCR_QOS_PAYLOAD = System::Int8(0x2);
static constexpr System::Int8 OCI_SUBSCR_QOS_REPLICATE = System::Int8(0x4);
static constexpr System::Int8 OCI_SUBSCR_QOS_SECURE = System::Int8(0x8);
static constexpr System::Int8 OCI_SUBSCR_QOS_PURGE_ON_NTFN = System::Int8(0x10);
static constexpr System::Int8 OCI_SUBSCR_QOS_MULTICBK = System::Int8(0x20);
static constexpr System::Word OCI_AL24UTFFSSID = System::Word(0x366);
static constexpr System::Word OCI_AL24UTF8ID = System::Word(0x367);
static constexpr System::Word OCI_UTFEID = System::Word(0x368);
static constexpr System::Word OCI_AL32UTF8ID = System::Word(0x369);
static constexpr System::Word OCI_UCS2ID = System::Word(0x3e8);
static constexpr System::Word OCI_UTF16ID = System::Word(0x3e8);
static constexpr System::Word OCI_AL16UTF16ID = System::Word(0x7d0);
static constexpr System::Int8 OCI_SUBSCR_NAMESPACE_ANONYMOUS = System::Int8(0x0);
static constexpr System::Int8 OCI_SUBSCR_NAMESPACE_AQ = System::Int8(0x1);
static constexpr System::Int8 OCI_SUBSCR_NAMESPACE_DBCHANGE = System::Int8(0x2);
static constexpr System::Int8 OCI_CRED_RDBMS = System::Int8(0x1);
static constexpr System::Int8 OCI_CRED_EXT = System::Int8(0x2);
static constexpr System::Int8 OCI_CRED_PROXY = System::Int8(0x3);
static constexpr System::Int8 OCI_SUCCESS = System::Int8(0x0);
static constexpr System::Int8 OCI_SUCCESS_WITH_INFO = System::Int8(0x1);
static constexpr System::Int8 OCI_NO_DATA = System::Int8(0x64);
static constexpr System::Int8 OCI_ERROR = System::Int8(-1);
static constexpr System::Int8 OCI_INVALID_HANDLE = System::Int8(-2);
static constexpr System::Int8 OCI_NEED_DATA = System::Int8(0x63);
static constexpr short OCI_STILL_EXECUTING = short(-3123);
static constexpr short OCI_CONTINUE = short(-24200);
static constexpr System::Int8 OCI_V7_SYNTAX = System::Int8(0x2);
static constexpr System::Int8 OCI_V8_SYNTAX = System::Int8(0x3);
static constexpr System::Int8 OCI_NTV_SYNTAX = System::Int8(0x1);
static constexpr System::Int8 OCI_FETCH_NEXT = System::Int8(0x2);
static constexpr System::Int8 OCI_FETCH_FIRST = System::Int8(0x4);
static constexpr System::Int8 OCI_FETCH_LAST = System::Int8(0x8);
static constexpr System::Int8 OCI_FETCH_PRIOR = System::Int8(0x10);
static constexpr System::Int8 OCI_FETCH_ABSOLUTE = System::Int8(0x20);
static constexpr System::Int8 OCI_FETCH_RELATIVE = System::Int8(0x40);
static constexpr System::Int8 OCI_SB2_IND_PTR = System::Int8(0x1);
static constexpr System::Int8 OCI_DATA_AT_EXEC = System::Int8(0x2);
static constexpr System::Int8 OCI_DYNAMIC_FETCH = System::Int8(0x2);
static constexpr System::Int8 OCI_PIECEWISE = System::Int8(0x4);
static constexpr System::Int8 OCI_STMT_STATE_INITIALIZED = System::Int8(0x1);
static constexpr System::Int8 OCI_STMT_STATE_EXECUTED = System::Int8(0x2);
static constexpr System::Int8 OCI_STMT_STATE_END_OF_FETCH = System::Int8(0x3);
static constexpr System::Int8 OCI_DEFAULT_MODE = System::Int8(0x0);
static constexpr System::Int8 OCI_BATCH_MODE = System::Int8(0x1);
static constexpr System::Int8 OCI_EXACT_FETCH = System::Int8(0x2);
static constexpr System::Int8 OCI_KEEP_FETCH_STATE = System::Int8(0x4);
static constexpr System::Int8 OCI_SCROLLABLE_CURSOR = System::Int8(0x8);
static constexpr System::Int8 OCI_DESCRIBE_ONLY = System::Int8(0x10);
static constexpr System::Int8 OCI_COMMIT_ON_SUCCESS = System::Int8(0x20);
static constexpr System::Int8 OCI_NON_BLOCKING = System::Int8(0x40);
static constexpr System::Byte OCI_BATCH_ERRORS = System::Byte(0x80);
static constexpr System::Word OCI_PARSE_ONLY = System::Word(0x100);
static constexpr System::Word OCI_SHOW_DML_WARNINGS = System::Word(0x400);
static constexpr int OCI_RETURN_ROW_COUNT_ARRAY = int(0x100000);
static constexpr System::Int8 OCI_MIGRATE = System::Int8(0x1);
static constexpr System::Int8 OCI_SYSDBA = System::Int8(0x2);
static constexpr System::Int8 OCI_SYSOPER = System::Int8(0x4);
static constexpr System::Int8 OCI_PRELIM_AUTH = System::Int8(0x8);
static constexpr System::Int8 OCI_STMT_CACHE = System::Int8(0x40);
static constexpr System::Word OCI_SYSASM = System::Word(0x8000);
static constexpr int OCI_SYSBKP = int(0x20000);
static constexpr int OCI_SYSDGD = int(0x40000);
static constexpr int OCI_SYSKMT = int(0x80000);
static constexpr System::Int8 OCI_PARAM_IN = System::Int8(0x1);
static constexpr System::Int8 OCI_PARAM_OUT = System::Int8(0x2);
static constexpr System::Int8 OCI_TRANS_OLD = System::Int8(0x0);
static constexpr System::Int8 OCI_TRANS_NEW = System::Int8(0x1);
static constexpr System::Int8 OCI_TRANS_JOIN = System::Int8(0x2);
static constexpr System::Int8 OCI_TRANS_RESUME = System::Int8(0x4);
static constexpr System::Byte OCI_TRANS_STARTMASK = System::Byte(0xff);
static constexpr System::Word OCI_TRANS_READONLY = System::Word(0x100);
static constexpr System::Word OCI_TRANS_READWRITE = System::Word(0x200);
static constexpr System::Word OCI_TRANS_SERIALIZABLE = System::Word(0x400);
static constexpr System::Word OCI_TRANS_ISOLMASK = System::Word(0xff00);
static constexpr int OCI_TRANS_LOOSE = int(0x10000);
static constexpr int OCI_TRANS_TIGHT = int(0x20000);
static constexpr int OCI_TRANS_TYPEMASK = int(0xf0000);
static constexpr int OCI_TRANS_NOMIGRATE = int(0x100000);
static constexpr int OCI_TRANS_TWOPHASE = int(0x1000000);
static constexpr System::Int8 OCI_ENQ_IMMEDIATE = System::Int8(0x1);
static constexpr System::Int8 OCI_ENQ_ON_COMMIT = System::Int8(0x2);
static constexpr System::Int8 OCI_DEQ_BROWSE = System::Int8(0x1);
static constexpr System::Int8 OCI_DEQ_LOCKED = System::Int8(0x2);
static constexpr System::Int8 OCI_DEQ_REMOVE = System::Int8(0x3);
static constexpr System::Int8 OCI_DEQ_FIRST_MSG = System::Int8(0x1);
static constexpr System::Int8 OCI_DEQ_NEXT_MSG = System::Int8(0x3);
static constexpr System::Int8 OCI_DEQ_NEXT_TRANSACTION = System::Int8(0x2);
static constexpr System::Int8 OCI_MSG_WAITING = System::Int8(0x1);
static constexpr System::Int8 OCI_MSG_READY = System::Int8(0x0);
static constexpr System::Int8 OCI_MSG_PROCESSED = System::Int8(0x2);
static constexpr System::Int8 OCI_MSG_EXPIRED = System::Int8(0x3);
static constexpr System::Int8 OCI_ENQ_BEFORE = System::Int8(0x2);
static constexpr System::Int8 OCI_ENQ_TOP = System::Int8(0x3);
static constexpr System::Int8 OCI_DEQ_IMMEDIATE = System::Int8(0x1);
static constexpr System::Int8 OCI_DEQ_ON_COMMIT = System::Int8(0x2);
static constexpr System::Int8 OCI_DEQ_WAIT_FOREVER = System::Int8(-1);
static constexpr System::Int8 OCI_DEQ_NO_WAIT = System::Int8(0x0);
static constexpr System::Int8 OCI_MSG_NO_DELAY = System::Int8(0x0);
static constexpr System::Int8 OCI_MSG_NO_EXPIRATION = System::Int8(-1);
static constexpr System::Int8 OCI_MSG_PERSISTENT_OR_BUFFERED = System::Int8(0x3);
static constexpr System::Int8 OCI_MSG_BUFFERED = System::Int8(0x2);
static constexpr System::Int8 OCI_MSG_PERSISTENT = System::Int8(0x1);
static constexpr System::Int8 OCI_OTYPE_UNK = System::Int8(0x0);
static constexpr System::Int8 OCI_OTYPE_TABLE = System::Int8(0x1);
static constexpr System::Int8 OCI_OTYPE_VIEW = System::Int8(0x2);
static constexpr System::Int8 OCI_OTYPE_SYN = System::Int8(0x3);
static constexpr System::Int8 OCI_OTYPE_PROC = System::Int8(0x4);
static constexpr System::Int8 OCI_OTYPE_FUNC = System::Int8(0x5);
static constexpr System::Int8 OCI_OTYPE_PKG = System::Int8(0x6);
static constexpr System::Int8 OCI_OTYPE_STMT = System::Int8(0x7);
static constexpr System::Word OCI_ATTR_CHAR_USED = System::Word(0x11d);
static constexpr System::Word OCI_ATTR_CHAR_SIZE = System::Word(0x11e);
static constexpr System::Int8 OCI_ATTR_DATA_SIZE = System::Int8(0x1);
static constexpr System::Int8 OCI_ATTR_DATA_TYPE = System::Int8(0x2);
static constexpr System::Int8 OCI_ATTR_DISP_SIZE = System::Int8(0x3);
static constexpr System::Int8 OCI_ATTR_NAME = System::Int8(0x4);
static constexpr System::Int8 OCI_ATTR_PRECISION = System::Int8(0x5);
static constexpr System::Int8 OCI_ATTR_SCALE = System::Int8(0x6);
static constexpr System::Int8 OCI_ATTR_IS_NULL = System::Int8(0x7);
static constexpr System::Int8 OCI_ATTR_TYPE_NAME = System::Int8(0x8);
static constexpr System::Int8 OCI_ATTR_SCHEMA_NAME = System::Int8(0x9);
static constexpr System::Int8 OCI_ATTR_SUB_NAME = System::Int8(0xa);
static constexpr System::Int8 OCI_ATTR_POSITION = System::Int8(0xb);
static constexpr System::Int8 OCI_ATTR_COMPLEXOBJECTCOMP_TYPE = System::Int8(0x32);
static constexpr System::Int8 OCI_ATTR_COMPLEXOBJECTCOMP_TYPE_LEVEL = System::Int8(0x33);
static constexpr System::Int8 OCI_ATTR_COMPLEXOBJECT_LEVEL = System::Int8(0x34);
static constexpr System::Int8 OCI_ATTR_COMPLEXOBJECT_COLL_OUTOFLINE = System::Int8(0x35);
static constexpr System::Int8 OCI_ATTR_DISP_NAME = System::Int8(0x64);
static constexpr System::Byte OCI_ATTR_OVERLOAD = System::Byte(0xd2);
static constexpr System::Byte OCI_ATTR_LEVEL = System::Byte(0xd3);
static constexpr System::Byte OCI_ATTR_HAS_DEFAULT = System::Byte(0xd4);
static constexpr System::Byte OCI_ATTR_IOMODE = System::Byte(0xd5);
static constexpr System::Byte OCI_ATTR_RADIX = System::Byte(0xd6);
static constexpr System::Byte OCI_ATTR_NUM_ARGS = System::Byte(0xd7);
static constexpr System::Byte OCI_ATTR_TYPECODE = System::Byte(0xd8);
static constexpr System::Byte OCI_ATTR_COLLECTION_TYPECODE = System::Byte(0xd9);
static constexpr System::Byte OCI_ATTR_VERSION = System::Byte(0xda);
static constexpr System::Byte OCI_ATTR_IS_INCOMPLETE_TYPE = System::Byte(0xdb);
static constexpr System::Byte OCI_ATTR_IS_SYSTEM_TYPE = System::Byte(0xdc);
static constexpr System::Byte OCI_ATTR_IS_PREDEFINED_TYPE = System::Byte(0xdd);
static constexpr System::Byte OCI_ATTR_IS_TRANSIENT_TYPE = System::Byte(0xde);
static constexpr System::Byte OCI_ATTR_IS_SYSTEM_GENERATED_TYPE = System::Byte(0xdf);
static constexpr System::Byte OCI_ATTR_HAS_NESTED_TABLE = System::Byte(0xe0);
static constexpr System::Byte OCI_ATTR_HAS_LOB = System::Byte(0xe1);
static constexpr System::Byte OCI_ATTR_HAS_FILE = System::Byte(0xe2);
static constexpr System::Byte OCI_ATTR_COLLECTION_ELEMENT = System::Byte(0xe3);
static constexpr System::Byte OCI_ATTR_NUM_TYPE_ATTRS = System::Byte(0xe4);
static constexpr System::Byte OCI_ATTR_LIST_TYPE_ATTRS = System::Byte(0xe5);
static constexpr System::Byte OCI_ATTR_NUM_TYPE_METHODS = System::Byte(0xe6);
static constexpr System::Byte OCI_ATTR_LIST_TYPE_METHODS = System::Byte(0xe7);
static constexpr System::Byte OCI_ATTR_MAP_METHOD = System::Byte(0xe8);
static constexpr System::Byte OCI_ATTR_ORDER_METHOD = System::Byte(0xe9);
static constexpr System::Byte OCI_ATTR_NUM_ELEMS = System::Byte(0xea);
static constexpr System::Word OCI_ATTR_IS_SUBTYPE = System::Word(0x102);
static constexpr System::Word OCI_ATTR_SUPERTYPE_SCHEMA_NAME = System::Word(0x103);
static constexpr System::Word OCI_ATTR_SUPERTYPE_NAME = System::Word(0x104);
static constexpr System::Word OCI_ATTR_LIST_OBJECTS = System::Word(0x105);
static constexpr System::Word OCI_ATTR_NCHARSET_ID = System::Word(0x106);
static constexpr System::Word OCI_ATTR_LIST_SCHEMAS = System::Word(0x107);
static constexpr System::Word OCI_ATTR_MAX_PROC_LEN = System::Word(0x108);
static constexpr System::Word OCI_ATTR_MAX_COLUMN_LEN = System::Word(0x109);
static constexpr System::Word OCI_ATTR_CURSOR_COMMIT_BEHAVIOR = System::Word(0x10a);
static constexpr System::Word OCI_ATTR_MAX_CATALOG_NAMELEN = System::Word(0x10b);
static constexpr System::Word OCI_ATTR_CATALOG_LOCATION = System::Word(0x10c);
static constexpr System::Word OCI_ATTR_SAVEPOINT_SUPPORT = System::Word(0x10d);
static constexpr System::Word OCI_ATTR_NOWAIT_SUPPORT = System::Word(0x10e);
static constexpr System::Word OCI_ATTR_AUTOCOMMIT_DDL = System::Word(0x10f);
static constexpr System::Word OCI_ATTR_LOCKING_MODE = System::Word(0x110);
static constexpr System::Word OCI_ATTR_IS_FINAL_TYPE = System::Word(0x117);
static constexpr System::Word OCI_ATTR_IS_INSTANTIABLE_TYPE = System::Word(0x118);
static constexpr System::Word OCI_ATTR_IS_FINAL_METHOD = System::Word(0x119);
static constexpr System::Word OCI_ATTR_IS_INSTANTIABLE_METHOD = System::Word(0x11a);
static constexpr System::Word OCI_ATTR_IS_OVERRIDING_METHOD = System::Word(0x11b);
static constexpr System::Byte OCI_ATTR_NUM_ELEMENTS = System::Byte(0xea);
static constexpr System::Byte OCI_ATTR_ENCAPSULATION = System::Byte(0xeb);
static constexpr System::Byte OCI_ATTR_IS_SELFISH = System::Byte(0xec);
static constexpr System::Byte OCI_ATTR_IS_VIRTUAL = System::Byte(0xed);
static constexpr System::Byte OCI_ATTR_IS_INLINE = System::Byte(0xee);
static constexpr System::Byte OCI_ATTR_IS_CONSTANT = System::Byte(0xef);
static constexpr System::Byte OCI_ATTR_HAS_RESULT = System::Byte(0xf0);
static constexpr System::Byte OCI_ATTR_IS_CONSTRUCTOR = System::Byte(0xf1);
static constexpr System::Byte OCI_ATTR_IS_DESTRUCTOR = System::Byte(0xf2);
static constexpr System::Byte OCI_ATTR_IS_OPERATOR = System::Byte(0xf3);
static constexpr System::Byte OCI_ATTR_IS_MAP = System::Byte(0xf4);
static constexpr System::Byte OCI_ATTR_IS_ORDER = System::Byte(0xf5);
static constexpr System::Byte OCI_ATTR_IS_RNDS = System::Byte(0xf6);
static constexpr System::Byte OCI_ATTR_IS_RNPS = System::Byte(0xf7);
static constexpr System::Byte OCI_ATTR_IS_WNDS = System::Byte(0xf8);
static constexpr System::Byte OCI_ATTR_IS_WNPS = System::Byte(0xf9);
static constexpr System::Byte OCI_ATTR_DESC_PUBLIC = System::Byte(0xfa);
static constexpr System::Int8 OCI_AUTH = System::Int8(0x8);
static constexpr System::Int8 OCI_MAX_FNS = System::Int8(0x64);
static constexpr System::Int8 OCI_SQLSTATE_SIZE = System::Int8(0x5);
static constexpr System::Word OCI_ERROR_MAXMSG_SIZE = System::Word(0x400);
static constexpr System::Int8 OCI_ROWID_LEN = System::Int8(0x17);
static constexpr System::Int8 OCI_FO_END = System::Int8(0x1);
static constexpr System::Int8 OCI_FO_ABORT = System::Int8(0x2);
static constexpr System::Int8 OCI_FO_REAUTH = System::Int8(0x4);
static constexpr System::Int8 OCI_FO_BEGIN = System::Int8(0x8);
static constexpr System::Int8 OCI_FO_ERROR = System::Int8(0x10);
static constexpr System::Word OCI_FO_RETRY = System::Word(0x6342);
static constexpr System::Int8 OCI_FO_NONE = System::Int8(0x1);
static constexpr System::Int8 OCI_FO_SESSION = System::Int8(0x2);
static constexpr System::Int8 OCI_FO_SELECT = System::Int8(0x4);
static constexpr System::Int8 OCI_FO_TXNAL = System::Int8(0x8);
static constexpr System::Int8 OCI_FILE_READONLY = System::Int8(0x1);
static constexpr System::Int8 OCI_LOB_BUFFER_FREE = System::Int8(0x1);
static constexpr System::Int8 OCI_LOB_BUFFER_NOFREE = System::Int8(0x2);
static constexpr System::Int8 OCI_TEMP_BLOB = System::Int8(0x1);
static constexpr System::Int8 OCI_TEMP_CLOB = System::Int8(0x2);
static constexpr System::Int8 OCI_TEMP_NCLOB = System::Int8(0x3);
static constexpr System::Int8 OCI_STMT_SELECT = System::Int8(0x1);
static constexpr System::Int8 OCI_STMT_UPDATE = System::Int8(0x2);
static constexpr System::Int8 OCI_STMT_DELETE = System::Int8(0x3);
static constexpr System::Int8 OCI_STMT_INSERT = System::Int8(0x4);
static constexpr System::Int8 OCI_STMT_CREATE = System::Int8(0x5);
static constexpr System::Int8 OCI_STMT_DROP = System::Int8(0x6);
static constexpr System::Int8 OCI_STMT_ALTER = System::Int8(0x7);
static constexpr System::Int8 OCI_STMT_BEGIN = System::Int8(0x8);
static constexpr System::Int8 OCI_STMT_DECLARE = System::Int8(0x9);
static constexpr System::Int8 OCI_STMT_EXPLAIN = System::Int8(0xa);
static constexpr System::Int8 OCI_PTYPE_UNK = System::Int8(0x0);
static constexpr System::Int8 OCI_PTYPE_TABLE = System::Int8(0x1);
static constexpr System::Int8 OCI_PTYPE_VIEW = System::Int8(0x2);
static constexpr System::Int8 OCI_PTYPE_PROC = System::Int8(0x3);
static constexpr System::Int8 OCI_PTYPE_FUNC = System::Int8(0x4);
static constexpr System::Int8 OCI_PTYPE_PKG = System::Int8(0x5);
static constexpr System::Int8 OCI_PTYPE_TYPE = System::Int8(0x6);
static constexpr System::Int8 OCI_PTYPE_SYN = System::Int8(0x7);
static constexpr System::Int8 OCI_PTYPE_SEQ = System::Int8(0x8);
static constexpr System::Int8 OCI_PTYPE_COL = System::Int8(0x9);
static constexpr System::Int8 OCI_PTYPE_ARG = System::Int8(0xa);
static constexpr System::Int8 OCI_PTYPE_LIST = System::Int8(0xb);
static constexpr System::Int8 OCI_PTYPE_TYPE_ATTR = System::Int8(0xc);
static constexpr System::Int8 OCI_PTYPE_TYPE_COLL = System::Int8(0xd);
static constexpr System::Int8 OCI_PTYPE_TYPE_METHOD = System::Int8(0xe);
static constexpr System::Int8 OCI_PTYPE_TYPE_ARG = System::Int8(0xf);
static constexpr System::Int8 OCI_PTYPE_TYPE_RESULT = System::Int8(0x10);
static constexpr System::Int8 OCI_LTYPE_UNK = System::Int8(0x0);
static constexpr System::Int8 OCI_LTYPE_COLUMN = System::Int8(0x1);
static constexpr System::Int8 OCI_LTYPE_ARG_PROC = System::Int8(0x2);
static constexpr System::Int8 OCI_LTYPE_ARG_FUNC = System::Int8(0x3);
static constexpr System::Int8 OCI_LTYPE_SUBPRG = System::Int8(0x4);
static constexpr System::Int8 OCI_LTYPE_TYPE_ATTR = System::Int8(0x5);
static constexpr System::Int8 OCI_LTYPE_TYPE_METHOD = System::Int8(0x6);
static constexpr System::Int8 OCI_LTYPE_TYPE_ARG_PROC = System::Int8(0x7);
static constexpr System::Int8 OCI_LTYPE_TYPE_ARG_FUNC = System::Int8(0x8);
static constexpr System::Int8 OCI_TYPECODE_DATE = System::Int8(0xc);
static constexpr System::Int8 OCI_TYPECODE_SIGNED8 = System::Int8(0x1b);
static constexpr System::Int8 OCI_TYPECODE_SIGNED16 = System::Int8(0x1c);
static constexpr System::Int8 OCI_TYPECODE_SIGNED32 = System::Int8(0x1d);
static constexpr System::Int8 OCI_TYPECODE_REAL = System::Int8(0x15);
static constexpr System::Int8 OCI_TYPECODE_DOUBLE = System::Int8(0x16);
static constexpr System::Int8 OCI_TYPECODE_FLOAT = System::Int8(0x4);
static constexpr System::Int8 OCI_TYPECODE_NUMBER = System::Int8(0x2);
static constexpr System::Int8 OCI_TYPECODE_DECIMAL = System::Int8(0x7);
static constexpr System::Int8 OCI_TYPECODE_UNSIGNED8 = System::Int8(0x17);
static constexpr System::Int8 OCI_TYPECODE_UNSIGNED16 = System::Int8(0x19);
static constexpr System::Int8 OCI_TYPECODE_UNSIGNED32 = System::Int8(0x1a);
static constexpr System::Byte OCI_TYPECODE_OCTET = System::Byte(0xf5);
static constexpr System::Byte OCI_TYPECODE_SMALLINT = System::Byte(0xf6);
static constexpr System::Int8 OCI_TYPECODE_INTEGER = System::Int8(0x3);
static constexpr System::Int8 OCI_TYPECODE_RAW = System::Int8(0x5f);
static constexpr System::Int8 OCI_TYPECODE_PTR = System::Int8(0x20);
static constexpr System::Int8 OCI_TYPECODE_VARCHAR2 = System::Int8(0x9);
static constexpr System::Int8 OCI_TYPECODE_CHAR = System::Int8(0x60);
static constexpr System::Int8 OCI_TYPECODE_VARCHAR = System::Int8(0x1);
static constexpr System::Int8 OCI_TYPECODE_MLSLABEL = System::Int8(0x69);
static constexpr System::Byte OCI_TYPECODE_VARRAY = System::Byte(0xf7);
static constexpr System::Byte OCI_TYPECODE_TABLE = System::Byte(0xf8);
static constexpr System::Int8 OCI_TYPECODE_OBJECT = System::Int8(0x6c);
static constexpr System::Int8 OCI_TYPECODE_REF = System::Int8(0x6e);
static constexpr System::Int8 OCI_TYPECODE_NAMEDCOLLECTION = System::Int8(0x7a);
static constexpr System::Int8 OCI_TYPECODE_OPAQUE = System::Int8(0x3a);
static constexpr System::Int8 OCI_TYPECODE_BLOB = System::Int8(0x71);
static constexpr System::Int8 OCI_TYPECODE_CLOB = System::Int8(0x70);
static constexpr System::Int8 OCI_TYPECODE_BFILE = System::Int8(0x72);
static constexpr System::Int8 OCI_TYPECODE_CFILE = System::Int8(0x73);
static constexpr System::Byte OCI_TYPECODE_TIME = System::Byte(0xb9);
static constexpr System::Byte OCI_TYPECODE_TIME_TZ = System::Byte(0xba);
static constexpr System::Byte OCI_TYPECODE_TIMESTAMP = System::Byte(0xbb);
static constexpr System::Byte OCI_TYPECODE_TIMESTAMP_TZ = System::Byte(0xbc);
static constexpr System::Byte OCI_TYPECODE_TIMESTAMP_LTZ = System::Byte(0xe8);
static constexpr System::Byte OCI_TYPECODE_INTERVAL_YM = System::Byte(0xbd);
static constexpr System::Byte OCI_TYPECODE_INTERVAL_DS = System::Byte(0xbe);
static constexpr System::Int8 OCI_TYPECODE_BDOUBLE = System::Int8(0x65);
static constexpr System::Int8 OCI_TYPECODE_BFLOAT = System::Int8(0x64);
static constexpr System::Int8 OCI_TYPECODE_UROWID = System::Int8(0x68);
static constexpr System::Word OCI_TYPECODE_NCHAR = System::Word(0x11e);
static constexpr System::Word OCI_TYPECODE_NVARCHAR2 = System::Word(0x11f);
static constexpr System::Word OCI_TYPECODE_NCLOB = System::Word(0x120);
static constexpr System::Byte OCI_TYPECODE_OTMFIRST = System::Byte(0xe4);
static constexpr System::Word OCI_TYPECODE_OTMLAST = System::Word(0x140);
static constexpr System::Byte OCI_TYPECODE_SYSFIRST = System::Byte(0xe4);
static constexpr System::Byte OCI_TYPECODE_SYSLAST = System::Byte(0xeb);
static constexpr System::Byte OCI_TYPECODE_ITABLE = System::Byte(0xfb);
static constexpr System::Byte OCI_TYPECODE_RECORD = System::Byte(0xfa);
static constexpr System::Byte OCI_TYPECODE_BOOLEAN = System::Byte(0xfc);
static constexpr System::Int8 OCI_IND_NOTNULL = System::Int8(0x0);
static constexpr System::Int8 OCI_IND_NULL = System::Int8(-1);
static constexpr System::Int8 OCI_IND_BADNULL = System::Int8(-2);
static constexpr System::Int8 OCI_IND_NOTNULLABLE = System::Int8(-3);
static constexpr System::Int8 OCI_PIN_DEFAULT = System::Int8(0x1);
static constexpr System::Int8 OCI_PIN_ANY = System::Int8(0x3);
static constexpr System::Int8 OCI_PIN_RECENT = System::Int8(0x4);
static constexpr System::Int8 OCI_PIN_LATEST = System::Int8(0x5);
static constexpr System::Int8 OCI_LOCK_NONE = System::Int8(0x1);
static constexpr System::Int8 OCI_LOCK_X = System::Int8(0x2);
static constexpr System::Int8 OCI_MARK_DEFAULT = System::Int8(0x1);
static constexpr System::Int8 OCI_MARK_NONE = System::Int8(0x1);
static constexpr System::Int8 OCI_MARK_UPDATE = System::Int8(0x2);
static constexpr System::Word OCI_DURATION_INVALID = System::Word(0xffff);
static constexpr System::Int8 OCI_DURATION_BEGIN = System::Int8(0xa);
static constexpr System::Int8 OCI_DURATION_SESSION = System::Int8(0xa);
static constexpr System::Int8 OCI_DURATION_TRANS = System::Int8(0xb);
static constexpr System::Int8 OCI_DURATION_CALL = System::Int8(0xc);
static constexpr System::Int8 OCI_DURATION_STATEMENT = System::Int8(0xd);
static constexpr System::Int8 OCI_DURATION_CALLOUT = System::Int8(0xe);
static constexpr System::Int8 OCI_DURATION_NULL = System::Int8(0x9);
static constexpr System::Int8 OCI_DURATION_DEFAULT = System::Int8(0x8);
static constexpr System::Int8 OCI_DURATION_USER_CALLBACK = System::Int8(0x7);
static constexpr System::Int8 OCI_DURATION_NEXT = System::Int8(0x6);
static constexpr System::Int8 OCI_DURATION_PROCESS = System::Int8(0x5);
static constexpr System::Int8 OCI_DURATION_LAST = System::Int8(0xe);
static constexpr System::Int8 OCI_OBJECTFREE_FORCE = System::Int8(0x1);
static constexpr System::Int8 OCI_OBJECTFREE_NONULL = System::Int8(0x2);
static constexpr System::Int8 OCI_OBJECT_PERSISTENT = System::Int8(0x1);
static constexpr System::Int8 OCI_OBJECT_TRANSIENT = System::Int8(0x2);
static constexpr System::Int8 OCI_OBJECT_VALUE = System::Int8(0x3);
static constexpr System::Int8 OCI_OBJECT_NEW = System::Int8(0x1);
static constexpr System::Int8 OCI_OBJECT_DELETED = System::Int8(0x2);
static constexpr System::Int8 OCI_OBJECT_UPDATED = System::Int8(0x4);
static constexpr System::Int8 OCI_TYPEGET_HEADER = System::Int8(0x0);
static constexpr System::Int8 OCI_TYPEGET_ALL = System::Int8(0x1);
static constexpr System::Int8 OCI_NUMBER_SIZE = System::Int8(0x16);
static constexpr System::Int8 OCI_NUMBER_UNSIGNED = System::Int8(0x0);
static constexpr System::Int8 OCI_NUMBER_SIGNED = System::Int8(0x2);
static constexpr System::Int8 OCI_INTER_INVALID_DAY = System::Int8(0x1);
static constexpr System::Int8 OCI_INTER_DAY_BELOW_VALID = System::Int8(0x2);
static constexpr System::Int8 OCI_INTER_INVALID_MONTH = System::Int8(0x4);
static constexpr System::Int8 OCI_INTER_MONTH_BELOW_VALID = System::Int8(0x8);
static constexpr System::Int8 OCI_INTER_INVALID_YEAR = System::Int8(0x10);
static constexpr System::Int8 OCI_INTER_YEAR_BELOW_VALID = System::Int8(0x20);
static constexpr System::Int8 OCI_INTER_INVALID_HOUR = System::Int8(0x40);
static constexpr System::Byte OCI_INTER_HOUR_BELOW_VALID = System::Byte(0x80);
static constexpr System::Word OCI_INTER_INVALID_MINUTE = System::Word(0x100);
static constexpr System::Word OCI_INTER_MINUTE_BELOW_VALID = System::Word(0x200);
static constexpr System::Word OCI_INTER_INVALID_SECOND = System::Word(0x400);
static constexpr System::Word OCI_INTER_SECOND_BELOW_VALID = System::Word(0x800);
static constexpr System::Word OCI_INTER_INVALID_FRACSEC = System::Word(0x1000);
static constexpr System::Word OCI_INTER_FRACSEC_BELOW_VALID = System::Word(0x2000);
static constexpr System::Int8 OCI_DT_INVALID_DAY = System::Int8(0x1);
static constexpr System::Int8 OCI_DT_DAY_BELOW_VALID = System::Int8(0x2);
static constexpr System::Int8 OCI_DT_INVALID_MONTH = System::Int8(0x4);
static constexpr System::Int8 OCI_DT_MONTH_BELOW_VALID = System::Int8(0x8);
static constexpr System::Int8 OCI_DT_INVALID_YEAR = System::Int8(0x10);
static constexpr System::Int8 OCI_DT_YEAR_BELOW_VALID = System::Int8(0x20);
static constexpr System::Int8 OCI_DT_INVALID_HOUR = System::Int8(0x40);
static constexpr System::Byte OCI_DT_HOUR_BELOW_VALID = System::Byte(0x80);
static constexpr System::Word OCI_DT_INVALID_MINUTE = System::Word(0x100);
static constexpr System::Word OCI_DT_MINUTE_BELOW_VALID = System::Word(0x200);
static constexpr System::Word OCI_DT_INVALID_SECOND = System::Word(0x400);
static constexpr System::Word OCI_DT_SECOND_BELOW_VALID = System::Word(0x800);
static constexpr System::Word OCI_DT_DAY_MISSING_FROM_1582 = System::Word(0x1000);
static constexpr System::Word OCI_DT_YEAR_ZERO = System::Word(0x2000);
static constexpr System::Word OCI_DT_INVALID_TIMEZONE = System::Word(0x4000);
static constexpr System::Word OCI_DT_INVALID_FORMAT = System::Word(0x8000);
static constexpr System::Int8 OCI_XMLTYPE_CREATE_OCISTRING = System::Int8(0x1);
static constexpr System::Int8 OCI_XMLTYPE_CREATE_CLOB = System::Int8(0x2);
static constexpr System::Int8 OCI_XMLTYPE_CREATE_BLOB = System::Int8(0x3);
static constexpr System::Int8 OCI_DIRPATH_LOAD = System::Int8(0x1);
static constexpr System::Int8 OCI_DIRPATH_UNLOAD = System::Int8(0x2);
static constexpr System::Int8 OCI_DIRPATH_CONVERT = System::Int8(0x3);
static constexpr System::Int8 OCI_DIRPATH_NORMAL = System::Int8(0x1);
static constexpr System::Int8 OCI_DIRPATH_PARTIAL = System::Int8(0x2);
static constexpr System::Int8 OCI_DIRPATH_NOT_PREPARED = System::Int8(0x3);
static constexpr System::Int8 OCI_DIRPATH_COL_COMPLETE = System::Int8(0x0);
static constexpr System::Int8 OCI_DIRPATH_COL_NULL = System::Int8(0x1);
static constexpr System::Int8 OCI_DIRPATH_COL_PARTIAL = System::Int8(0x2);
static constexpr System::Int8 sizeof_TRowId8 = System::Int8(0xe);
static constexpr System::Int8 sizeof_TRowId81 = System::Int8(0xd);
static constexpr System::Int8 sizeof_OCIRowid = System::Int8(0x16);
static constexpr System::Int8 sizeof_OCIRowid81 = System::Int8(0xc);
static constexpr System::Byte XID_SIZE = System::Byte(0x8c);
extern DELPHI_PACKAGE TOracleHomes* OracleHomes;
extern DELPHI_PACKAGE int SubscriptionPort;
extern DELPHI_PACKAGE bool ObjectVersion;
extern DELPHI_PACKAGE bool OCI7ThreadSafety;
extern DELPHI_PACKAGE bool OCIUnicode;
extern DELPHI_PACKAGE bool OCIThreaded;
extern DELPHI_PACKAGE bool OCIMutexed;
extern DELPHI_PACKAGE bool OCIShared;
extern DELPHI_PACKAGE bool OCIEvents;
extern DELPHI_PACKAGE System::Word OCIEventsVersion;
extern DELPHI_PACKAGE bool OCIUnicodeAsNational;
extern DELPHI_PACKAGE bool OCILite;
extern DELPHI_PACKAGE bool OCINCharLiteralReplace;
#define DACProductName u"ODAC"
extern DELPHI_PACKAGE void * __fastcall StringToHGlobalOCI(const System::UnicodeString S, int &Size, bool UnicodeEnv);
extern DELPHI_PACKAGE void __fastcall FreeStringOCI(void * P, bool UnicodeEnv);
extern DELPHI_PACKAGE System::UnicodeString __fastcall PtrToStringOCI(void * P, bool UnicodeEnv)/* overload */;
extern DELPHI_PACKAGE System::UnicodeString __fastcall PtrToStringOCI(void * P, int Size, bool UnicodeEnv)/* overload */;
extern DELPHI_PACKAGE int __fastcall SizeOfCharOCI(bool UnicodeEnv);
extern DELPHI_PACKAGE System::Word __fastcall VersionStrToWord(System::UnicodeString VersionSt);
extern DELPHI_PACKAGE System::Byte __fastcall GetMaxCharLength(System::Word Charset);
extern DELPHI_PACKAGE System::Byte __fastcall GetFixedCharLength(System::Word Charset);
extern DELPHI_PACKAGE int __fastcall GetUTF8Charset(System::Word OracleVersion);
extern DELPHI_PACKAGE System::Word __fastcall GetCodePageCharset(int CodePage, System::Word OracleVersion);
extern DELPHI_PACKAGE void __fastcall OCINumberFromBCD(const System::TArray__1<System::Byte> rnum, unsigned rnum_length, void * number);
extern DELPHI_PACKAGE void __fastcall OCINumberToBCD(void * Number, void *Bcd);
extern DELPHI_PACKAGE System::UnicodeString __fastcall DefaultOCIDateFormat(void);
extern DELPHI_PACKAGE System::UnicodeString __fastcall DefaultOCITimeFormat(System::Byte Precision = (System::Byte)(0x6));
extern DELPHI_PACKAGE System::UnicodeString __fastcall DefaultOCITimeStampFormat(System::Byte Precision = (System::Byte)(0x6));
extern DELPHI_PACKAGE System::UnicodeString __fastcall DefaultOCITimeStampWithTZFormat(System::Byte Precision = (System::Byte)(0x6));
}	/* namespace Oracall */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_ORACALL)
using namespace Oracall;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// OracallHPP
