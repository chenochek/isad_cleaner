﻿// CodeGear C++Builder
// Copyright (c) 1995, 2018 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'OraTransaction.pas' rev: 33.00 (iOS)

#ifndef OratransactionHPP
#define OratransactionHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.SysUtils.hpp>
#include <System.Classes.hpp>
#include <System.Variants.hpp>
#include <CLRClasses.hpp>
#include <CRTypes.hpp>
#include <CRAccess.hpp>
#include <DBAccess.hpp>
#include <OraError.hpp>
#include <OraClasses.hpp>
#include <Ora.hpp>

//-- user supplied -----------------------------------------------------------

namespace Oratransaction
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TOraTransaction;
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TGlobalCoordinator : unsigned char { gcInternal };

typedef Craccess::TCRTransactionAction TOraTransactionAction;

class PASCALIMPLEMENTATION TOraTransaction : public Dbaccess::TDATransaction
{
	typedef Dbaccess::TDATransaction inherited;
	
	
private:
	typedef System::DynamicArray<System::TArray__1<System::Byte> > _TOraTransaction__1;
	
	
private:
	System::UnicodeString FTransactionName;
	System::UnicodeString FRollbackSegment;
	int FInactiveTimeOut;
	int FResumeTimeOut;
	System::TArray__1<System::Byte> FTransactionId;
	_TOraTransaction__1 FBranchQualifiers;
	bool FResume;
	Ora::TOraSession* __fastcall GetSession(int Index);
	int __fastcall GetSessionsCount();
	Ora::TOraSession* __fastcall GetDefaultSession();
	void __fastcall SetDefaultSession(Ora::TOraSession* Value);
	Ora::TOraIsolationLevel __fastcall GetIsolationLevel();
	HIDESBASE void __fastcall SetIsolationLevel(Ora::TOraIsolationLevel Value);
	void __fastcall SetTransactionName(const System::UnicodeString Value);
	void __fastcall SetRollbackSegment(const System::UnicodeString Value);
	System::TArray__1<System::Byte> __fastcall GetTransactionId();
	void __fastcall SetTransactionId(System::TArray__1<System::Byte> Value);
	System::TArray__1<System::Byte> __fastcall GetBranchQualifier(int Index);
	TGlobalCoordinator __fastcall GetGlobalCoordinator();
	void __fastcall SetGlobalCoordinator(TGlobalCoordinator Value);
	void __fastcall SetInactiveTimeOut(int Value);
	void __fastcall SetResumeTimeOut(int Value);
	
protected:
	virtual Craccess::TCRTransactionClass __fastcall GetITransactionClass();
	virtual void __fastcall SetITransaction(Craccess::TCRTransaction* Value);
	virtual System::TClass __fastcall SQLMonitorClass();
	virtual bool __fastcall DetectInTransaction(bool CanActivate = false);
	
public:
	__fastcall virtual TOraTransaction(System::Classes::TComponent* Owner);
	void __fastcall AddSession(Ora::TOraSession* Session, System::TArray__1<System::Byte> BranchQualifier)/* overload */;
	void __fastcall AddSession(Ora::TOraSession* Session)/* overload */;
	void __fastcall RemoveSession(Ora::TOraSession* Session);
	void __fastcall ClearSessions();
	virtual void __fastcall StartTransaction()/* overload */;
	HIDESBASE void __fastcall StartTransaction(bool Resume)/* overload */;
	void __fastcall Detach();
	void __fastcall Resume();
	void __fastcall Savepoint(const System::UnicodeString Savepoint);
	void __fastcall RollbackToSavepoint(const System::UnicodeString Savepoint);
	__property Active;
	__property Ora::TOraSession* Sessions[int Index] = {read=GetSession};
	__property int SessionsCount = {read=GetSessionsCount, nodefault};
	__property System::UnicodeString RollbackSegment = {read=FRollbackSegment, write=SetRollbackSegment};
	__property System::TArray__1<System::Byte> TransactionId = {read=GetTransactionId, write=SetTransactionId};
	__property System::TArray__1<System::Byte> BranchQualifiers[int Index] = {read=GetBranchQualifier};
	
__published:
	__property System::UnicodeString TransactionName = {read=FTransactionName, write=SetTransactionName};
	__property Ora::TOraIsolationLevel IsolationLevel = {read=GetIsolationLevel, write=SetIsolationLevel, default=0};
	__property Ora::TOraSession* DefaultSession = {read=GetDefaultSession, write=SetDefaultSession};
	__property TGlobalCoordinator GlobalCoordinator = {read=GetGlobalCoordinator, write=SetGlobalCoordinator, default=0};
	__property int InactiveTimeOut = {read=FInactiveTimeOut, write=SetInactiveTimeOut, default=0};
	__property int ResumeTimeOut = {read=FResumeTimeOut, write=SetResumeTimeOut, default=0};
	__property DefaultCloseAction = {default=0};
	__property OnError;
	__property OnStart;
	__property OnCommit;
	__property OnRollback;
public:
	/* TDATransaction.Destroy */ inline __fastcall virtual ~TOraTransaction() { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Oratransaction */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_ORATRANSACTION)
using namespace Oratransaction;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// OratransactionHPP
