﻿// CodeGear C++Builder
// Copyright (c) 1995, 2018 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'OraSQLMonitor.pas' rev: 33.00 (iOS)

#ifndef OrasqlmonitorHPP
#define OrasqlmonitorHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.SysUtils.hpp>
#include <System.Classes.hpp>
#include <Data.DB.hpp>
#include <CRTypes.hpp>
#include <MemUtils.hpp>
#include <MemDS.hpp>
#include <DBAccess.hpp>
#include <DASQLMonitor.hpp>

//-- user supplied -----------------------------------------------------------

namespace Orasqlmonitor
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TOraSQLMonitor;
//-- type declarations -------------------------------------------------------
_DECLARE_METACLASS(System::TMetaClass, TOraSQLMonitorClass);

class PASCALIMPLEMENTATION TOraSQLMonitor : public Dasqlmonitor::TCustomDASQLMonitor
{
	typedef Dasqlmonitor::TCustomDASQLMonitor inherited;
	
protected:
	__classmethod virtual Dasqlmonitor::TCustomDASQLMonitor* __fastcall GetMonitor();
	virtual void __fastcall SetMonitor();
	
public:
	__fastcall virtual TOraSQLMonitor(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TOraSQLMonitor();
	__classmethod virtual System::UnicodeString __fastcall GetParamDataType(Dbaccess::TDAParam* Param);
	__classmethod virtual System::UnicodeString __fastcall GetParamValue(Dbaccess::TDAParam* Param);
	__classmethod virtual System::UnicodeString __fastcall GetCaption();
	
__published:
	__property Active = {default=1};
	__property Options = {default=15};
	__property DBMonitorOptions;
	__property TraceFlags = {default=1643};
	__property OnSQL;
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Orasqlmonitor */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_ORASQLMONITOR)
using namespace Orasqlmonitor;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// OrasqlmonitorHPP
