﻿// CodeGear C++Builder
// Copyright (c) 1995, 2018 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'ConnectFormFmx.pas' rev: 33.00 (iOS)

#ifndef ConnectformfmxHPP
#define ConnectformfmxHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.Classes.hpp>
#include <System.SysUtils.hpp>
#include <System.UITypes.hpp>
#include <FMX.Types.hpp>
#include <FMX.Platform.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Edit.hpp>
#include <FMX.ListBox.hpp>
#include <FMX.Memo.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.ComboEdit.hpp>
#include <CRTypes.hpp>
#include <DBAccess.hpp>
#include <System.Generics.Collections.hpp>
#include <System.Generics.Defaults.hpp>
#include <System.Types.hpp>

//-- user supplied -----------------------------------------------------------

namespace Connectformfmx
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TConnectForm;
//-- type declarations -------------------------------------------------------
class PASCALIMPLEMENTATION TConnectForm : public Fmx::Forms::TForm
{
	typedef Fmx::Forms::TForm inherited;
	
__published:
	Fmx::Stdctrls::TPanel* pMain;
	Fmx::Stdctrls::TPanel* pButtons;
	Fmx::Stdctrls::TButton* btConnect;
	Fmx::Stdctrls::TButton* btCancel;
	Fmx::Stdctrls::TPanel* pUserName;
	Fmx::Stdctrls::TPanel* pPassword;
	Fmx::Stdctrls::TPanel* pServer;
	Fmx::Stdctrls::TPanel* pHome;
	Fmx::Stdctrls::TPanel* pRole;
	Fmx::Stdctrls::TPanel* pSchema;
	Fmx::Stdctrls::TPanel* pDirect;
	Fmx::Stdctrls::TLabel* lbUsername;
	Fmx::Edit::TEdit* edUsername;
	Fmx::Stdctrls::TLabel* lbPassword;
	Fmx::Edit::TEdit* edPassword;
	Fmx::Stdctrls::TLabel* lbServer;
	Fmx::Comboedit::TComboEdit* edServer;
	Fmx::Stdctrls::TLabel* lbHome;
	Fmx::Comboedit::TComboEdit* cbHome;
	Fmx::Stdctrls::TLabel* lbRole;
	Fmx::Comboedit::TComboEdit* cbRole;
	Fmx::Stdctrls::TLabel* lbSchema;
	Fmx::Edit::TEdit* edSchema;
	Fmx::Stdctrls::TLabel* lbDirect;
	Fmx::Stdctrls::TCheckBox* cbDirect;
	void __fastcall btConnectClick(System::TObject* Sender);
	void __fastcall edServerPopup(System::TObject* Sender);
	void __fastcall cbHomeChange(System::TObject* Sender);
	void __fastcall cbHomePopup(System::TObject* Sender);
	void __fastcall cbDirectChange(System::TObject* Sender);
	
private:
	Dbaccess::TCustomConnectDialog* FConnectDialog;
	int FRetries;
	bool FRetry;
	bool FInDoInit;
	void __fastcall SetConnectDialog(Dbaccess::TCustomConnectDialog* Value);
	void __fastcall AssignHomeName(const System::UnicodeString Value);
	void __fastcall AssignDirect(const bool Value);
	System::UnicodeString __fastcall GetHomeName();
	void __fastcall FillServerList();
	void __fastcall FillHomeName();
	
protected:
	virtual void __fastcall DoInit();
	virtual void __fastcall DoConnect();
	
__published:
	__property Dbaccess::TCustomConnectDialog* ConnectDialog = {read=FConnectDialog, write=SetConnectDialog};
public:
	/* TCustomForm.Create */ inline __fastcall virtual TConnectForm(System::Classes::TComponent* AOwner) : Fmx::Forms::TForm(AOwner) { }
	/* TCustomForm.CreateNew */ inline __fastcall virtual TConnectForm(System::Classes::TComponent* AOwner, NativeInt Dummy) : Fmx::Forms::TForm(AOwner, Dummy) { }
	/* TCustomForm.Destroy */ inline __fastcall virtual ~TConnectForm() { }
	
};


//-- var, const, procedure ---------------------------------------------------
}	/* namespace Connectformfmx */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_CONNECTFORMFMX)
using namespace Connectformfmx;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// ConnectformfmxHPP
