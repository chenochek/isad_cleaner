﻿// CodeGear C++Builder
// Copyright (c) 1995, 2018 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'OraScriptProcessor.pas' rev: 33.00 (iOS)

#ifndef OrascriptprocessorHPP
#define OrascriptprocessorHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.Classes.hpp>
#include <System.SysUtils.hpp>
#include <Data.DB.hpp>
#include <CRTypes.hpp>
#include <DBAccess.hpp>
#include <DAScript.hpp>
#include <CRParser.hpp>
#include <CRAccess.hpp>
#include <OraClasses.hpp>

//-- user supplied -----------------------------------------------------------

namespace Orascriptprocessor
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TCustomOraScriptProcessor;
//-- type declarations -------------------------------------------------------
class PASCALIMPLEMENTATION TCustomOraScriptProcessor : public Dascript::TDAScriptProcessor
{
	typedef Dascript::TDAScriptProcessor inherited;
	
	
private:
	typedef System::DynamicArray<int> _TCustomOraScriptProcessor__1;
	
	
protected:
	_TCustomOraScriptProcessor__1 FCodes;
	int FStatementType;
	bool FInWrapped;
	virtual bool __fastcall ExecuteNext();
	virtual bool __fastcall SlashIsDelimiter();
	virtual void __fastcall SetConnectMode(Dbaccess::TCustomDAConnection* Connection, Oraclasses::TConnectMode ConnectMode) = 0 ;
	virtual Crparser::TSQLParserClass __fastcall GetParserClass();
	virtual void __fastcall CheckLexem(int Code, int &StatementType, bool &Omit);
	virtual bool __fastcall GetReady(int Code);
	virtual void __fastcall DoBeforeStatementExecute(System::UnicodeString &SQL, int StatementType, bool &Omit);
	virtual bool __fastcall IsSpecificSQL(int StatementType);
	
public:
	__fastcall virtual TCustomOraScriptProcessor(Dascript::TDAScript* Owner);
public:
	/* TDAScriptProcessor.Destroy */ inline __fastcall virtual ~TCustomOraScriptProcessor() { }
	
};


//-- var, const, procedure ---------------------------------------------------
static constexpr System::Int8 ST_NORMAL = System::Int8(0x64);
static constexpr System::Word ST_SQLPLUS = System::Word(0x100);
static constexpr System::Word ST_IGNORED = System::Word(0x101);
static constexpr System::Word ST_CONNECT = System::Word(0x102);
static constexpr System::Word ST_DISCONNECT = System::Word(0x103);
static constexpr System::Word ST_DEFINE = System::Word(0x104);
static constexpr System::Word ST_EXECUTE = System::Word(0x105);
static constexpr System::Word ST_PRIVILEGES = System::Word(0x106);
}	/* namespace Orascriptprocessor */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_ORASCRIPTPROCESSOR)
using namespace Orascriptprocessor;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// OrascriptprocessorHPP
