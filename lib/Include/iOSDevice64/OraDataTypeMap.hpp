﻿// CodeGear C++Builder
// Copyright (c) 1995, 2018 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'OraDataTypeMap.pas' rev: 33.00 (iOS)

#ifndef OradatatypemapHPP
#define OradatatypemapHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.SysUtils.hpp>
#include <Data.FmtBcd.hpp>
#include <CLRClasses.hpp>
#include <CRDataTypeMap.hpp>
#include <CRTypes.hpp>
#include <MemData.hpp>
#include <System.Classes.hpp>

//-- user supplied -----------------------------------------------------------

namespace Oradatatypemap
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TOraConverterManager;
class DELPHICLASS TOraMapRules;
class DELPHICLASS TOraDataConverters;
//-- type declarations -------------------------------------------------------
class PASCALIMPLEMENTATION TOraConverterManager : public Crdatatypemap::TConverterManager
{
	typedef Crdatatypemap::TConverterManager inherited;
	
public:
	__fastcall TOraConverterManager();
	__classmethod virtual System::Word __fastcall GetDBProvider();
	__classmethod System::Word __fastcall GetDBType(System::Word SQLType, System::Word ObjectDataType, bool IsNational = false);
public:
	/* TConverterManager.Destroy */ inline __fastcall virtual ~TOraConverterManager() { }
	
};


class PASCALIMPLEMENTATION TOraMapRules : public Crdatatypemap::TCRMapRules
{
	typedef Crdatatypemap::TCRMapRules inherited;
	
public:
	__classmethod virtual Crdatatypemap::TConverterManager* __fastcall GetConverterManager();
public:
	/* TCRMapRules.Create */ inline __fastcall virtual TOraMapRules() : Crdatatypemap::TCRMapRules() { }
	
public:
	/* TCollection.Destroy */ inline __fastcall virtual ~TOraMapRules() { }
	
};


class PASCALIMPLEMENTATION TOraDataConverters : public Crdatatypemap::TDataConverters
{
	typedef Crdatatypemap::TDataConverters inherited;
	
protected:
	__classmethod Memdata::TConvertStatus __fastcall InternalNumberToStr(void * Source, /* out */ System::UnicodeString &Str, int DestLen);
	__classmethod Memdata::TConvertStatus __fastcall InternalStrToNumber(const System::UnicodeString Str, void * Dest, bool IgnoreConvertErrors);
	
public:
	__classmethod Memdata::TConvertStatus __fastcall NumberToInt8(Memdata::TConvertInfo &AConvertInfo);
	__classmethod Memdata::TConvertStatus __fastcall NumberToUInt8(Memdata::TConvertInfo &AConvertInfo);
	__classmethod Memdata::TConvertStatus __fastcall NumberToInt16(Memdata::TConvertInfo &AConvertInfo);
	__classmethod Memdata::TConvertStatus __fastcall NumberToUInt16(Memdata::TConvertInfo &AConvertInfo);
	__classmethod Memdata::TConvertStatus __fastcall NumberToInt32(Memdata::TConvertInfo &AConvertInfo);
	__classmethod Memdata::TConvertStatus __fastcall NumberToUInt32(Memdata::TConvertInfo &AConvertInfo);
	__classmethod Memdata::TConvertStatus __fastcall NumberToInt64(Memdata::TConvertInfo &AConvertInfo);
	__classmethod Memdata::TConvertStatus __fastcall NumberToFloat(Memdata::TConvertInfo &AConvertInfo);
	__classmethod Memdata::TConvertStatus __fastcall NumberToSingle(Memdata::TConvertInfo &AConvertInfo);
	__classmethod Memdata::TConvertStatus __fastcall NumberToBCD(Memdata::TConvertInfo &AConvertInfo);
	__classmethod Memdata::TConvertStatus __fastcall NumberToFMTBCD(Memdata::TConvertInfo &AConvertInfo);
	__classmethod Memdata::TConvertStatus __fastcall NumberToAStr(Memdata::TConvertInfo &AConvertInfo);
	__classmethod Memdata::TConvertStatus __fastcall NumberToWStr(Memdata::TConvertInfo &AConvertInfo);
	__classmethod Memdata::TConvertStatus __fastcall NumberToExtAStr(Memdata::TConvertInfo &AConvertInfo);
	__classmethod Memdata::TConvertStatus __fastcall NumberToExtWStr(Memdata::TConvertInfo &AConvertInfo);
	__classmethod Memdata::TConvertStatus __fastcall Int8ToNumber(Memdata::TConvertInfo &AConvertInfo);
	__classmethod Memdata::TConvertStatus __fastcall UInt8ToNumber(Memdata::TConvertInfo &AConvertInfo);
	__classmethod Memdata::TConvertStatus __fastcall Int16ToNumber(Memdata::TConvertInfo &AConvertInfo);
	__classmethod Memdata::TConvertStatus __fastcall UInt16ToNumber(Memdata::TConvertInfo &AConvertInfo);
	__classmethod Memdata::TConvertStatus __fastcall Int32ToNumber(Memdata::TConvertInfo &AConvertInfo);
	__classmethod Memdata::TConvertStatus __fastcall UInt32ToNumber(Memdata::TConvertInfo &AConvertInfo);
	__classmethod Memdata::TConvertStatus __fastcall Int64ToNumber(Memdata::TConvertInfo &AConvertInfo);
	__classmethod Memdata::TConvertStatus __fastcall FloatToNumber(Memdata::TConvertInfo &AConvertInfo);
	__classmethod Memdata::TConvertStatus __fastcall SingleToNumber(Memdata::TConvertInfo &AConvertInfo);
	__classmethod Memdata::TConvertStatus __fastcall BCDToNumber(Memdata::TConvertInfo &AConvertInfo);
	__classmethod Memdata::TConvertStatus __fastcall FMTBCDToNumber(Memdata::TConvertInfo &AConvertInfo);
	__classmethod Memdata::TConvertStatus __fastcall AStrToNumber(Memdata::TConvertInfo &AConvertInfo);
	__classmethod Memdata::TConvertStatus __fastcall WStrToNumber(Memdata::TConvertInfo &AConvertInfo);
	__classmethod Memdata::TConvertStatus __fastcall ExtAStrToNumber(Memdata::TConvertInfo &AConvertInfo);
	__classmethod Memdata::TConvertStatus __fastcall ExtWStrToNumber(Memdata::TConvertInfo &AConvertInfo);
	__classmethod Memdata::TConvertStatus __fastcall XmlToAStr(Memdata::TConvertInfo &AConvertInfo);
	__classmethod Memdata::TConvertStatus __fastcall XmlToWStr(Memdata::TConvertInfo &AConvertInfo);
	__classmethod Memdata::TConvertStatus __fastcall AStrToXml(Memdata::TConvertInfo &AConvertInfo);
	__classmethod Memdata::TConvertStatus __fastcall WStrToXml(Memdata::TConvertInfo &AConvertInfo);
	__classmethod Memdata::TConvertStatus __fastcall IntervalToAStr(Memdata::TConvertInfo &AConvertInfo);
	__classmethod Memdata::TConvertStatus __fastcall IntervalToWStr(Memdata::TConvertInfo &AConvertInfo);
	__classmethod Memdata::TConvertStatus __fastcall AStrToInterval(Memdata::TConvertInfo &AConvertInfo);
	__classmethod Memdata::TConvertStatus __fastcall WStrToInterval(Memdata::TConvertInfo &AConvertInfo);
public:
	/* TObject.Create */ inline __fastcall TOraDataConverters() : Crdatatypemap::TDataConverters() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TOraDataConverters() { }
	
};


//-- var, const, procedure ---------------------------------------------------
static constexpr System::Int8 oraBase = System::Int8(0x64);
static constexpr System::Int8 oraChar = System::Int8(0x65);
static constexpr System::Int8 oraVarchar2 = System::Int8(0x66);
static constexpr System::Int8 oraNChar = System::Int8(0x67);
static constexpr System::Int8 oraNVarchar2 = System::Int8(0x68);
static constexpr System::Int8 oraNumber = System::Int8(0x69);
static constexpr System::Int8 oraInteger = System::Int8(0x6a);
static constexpr System::Int8 oraFloat = System::Int8(0x6b);
static constexpr System::Int8 oraBinaryFloat = System::Int8(0x6c);
static constexpr System::Int8 oraBinaryDouble = System::Int8(0x6d);
static constexpr System::Int8 oraDoublePrecision = System::Int8(0x6e);
static constexpr System::Int8 oraDate = System::Int8(0x6f);
static constexpr System::Int8 oraTimeStamp = System::Int8(0x70);
static constexpr System::Int8 oraTimeStampWithTimeZone = System::Int8(0x71);
static constexpr System::Int8 oraTimeStampWithLocalTimeZone = System::Int8(0x72);
static constexpr System::Int8 oraIntervalYM = System::Int8(0x73);
static constexpr System::Int8 oraIntervalDS = System::Int8(0x74);
static constexpr System::Int8 oraBlob = System::Int8(0x75);
static constexpr System::Int8 oraClob = System::Int8(0x76);
static constexpr System::Int8 oraNClob = System::Int8(0x77);
static constexpr System::Int8 oraBFile = System::Int8(0x78);
static constexpr System::Int8 oraCFile = System::Int8(0x79);
static constexpr System::Int8 oraLong = System::Int8(0x7a);
static constexpr System::Int8 oraRaw = System::Int8(0x7b);
static constexpr System::Int8 oraLongRaw = System::Int8(0x7c);
static constexpr System::Int8 oraRowID = System::Int8(0x7d);
static constexpr System::Int8 oraURowID = System::Int8(0x7e);
static constexpr System::Int8 oraCursor = System::Int8(0x7f);
static constexpr System::Byte oraObject = System::Byte(0x80);
static constexpr System::Byte oraReference = System::Byte(0x81);
static constexpr System::Byte oraXML = System::Byte(0x82);
static constexpr System::Byte oraAnyData = System::Byte(0x83);
static constexpr System::Byte oraLabel = System::Byte(0x84);
static constexpr System::Byte oraUndefined = System::Byte(0x85);
extern DELPHI_PACKAGE void __fastcall InitOraTypes(void);
}	/* namespace Oradatatypemap */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_ORADATATYPEMAP)
using namespace Oradatatypemap;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// OradatatypemapHPP
