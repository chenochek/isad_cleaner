﻿// CodeGear C++Builder
// Copyright (c) 1995, 2018 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'OraConsts.pas' rev: 33.00 (iOS)

#ifndef OraconstsHPP
#define OraconstsHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>

//-- user supplied -----------------------------------------------------------

namespace Oraconsts
{
//-- forward type declarations -----------------------------------------------
//-- type declarations -------------------------------------------------------
//-- var, const, procedure ---------------------------------------------------
static constexpr System::Int8 DefValOraConnectionTimeout = System::Int8(0x0);
static constexpr bool DefValDirect = false;
extern DELPHI_PACKAGE System::ResourceString _SDirectModeNotSupported;
#define Oraconsts_SDirectModeNotSupported System::LoadResourceString(&Oraconsts::_SDirectModeNotSupported)
extern DELPHI_PACKAGE System::ResourceString _SOciModeNotAvailable;
#define Oraconsts_SOciModeNotAvailable System::LoadResourceString(&Oraconsts::_SOciModeNotAvailable)
extern DELPHI_PACKAGE System::ResourceString _SSessionNotDefined;
#define Oraconsts_SSessionNotDefined System::LoadResourceString(&Oraconsts::_SSessionNotDefined)
extern DELPHI_PACKAGE System::ResourceString _SSessionNotConnected;
#define Oraconsts_SSessionNotConnected System::LoadResourceString(&Oraconsts::_SSessionNotConnected)
extern DELPHI_PACKAGE System::ResourceString _SUnsupportedDataType;
#define Oraconsts_SUnsupportedDataType System::LoadResourceString(&Oraconsts::_SUnsupportedDataType)
extern DELPHI_PACKAGE System::ResourceString _SInvalidBufferSize;
#define Oraconsts_SInvalidBufferSize System::LoadResourceString(&Oraconsts::_SInvalidBufferSize)
extern DELPHI_PACKAGE System::ResourceString _SParamIsNotStorable;
#define Oraconsts_SParamIsNotStorable System::LoadResourceString(&Oraconsts::_SParamIsNotStorable)
extern DELPHI_PACKAGE System::ResourceString _SCannotChangeBufferSize;
#define Oraconsts_SCannotChangeBufferSize System::LoadResourceString(&Oraconsts::_SCannotChangeBufferSize)
extern DELPHI_PACKAGE System::ResourceString _SInvalidIndex;
#define Oraconsts_SInvalidIndex System::LoadResourceString(&Oraconsts::_SInvalidIndex)
extern DELPHI_PACKAGE System::ResourceString _SInvalidTableType;
#define Oraconsts_SInvalidTableType System::LoadResourceString(&Oraconsts::_SInvalidTableType)
extern DELPHI_PACKAGE System::ResourceString _SInvalidNationalType;
#define Oraconsts_SInvalidNationalType System::LoadResourceString(&Oraconsts::_SInvalidNationalType)
extern DELPHI_PACKAGE System::ResourceString _SInvalidDmlArrayType;
#define Oraconsts_SInvalidDmlArrayType System::LoadResourceString(&Oraconsts::_SInvalidDmlArrayType)
extern DELPHI_PACKAGE System::ResourceString _SCursorNotOpened;
#define Oraconsts_SCursorNotOpened System::LoadResourceString(&Oraconsts::_SCursorNotOpened)
extern DELPHI_PACKAGE System::ResourceString _SCursorOpened;
#define Oraconsts_SCursorOpened System::LoadResourceString(&Oraconsts::_SCursorOpened)
extern DELPHI_PACKAGE System::ResourceString _SArrayParam;
#define Oraconsts_SArrayParam System::LoadResourceString(&Oraconsts::_SArrayParam)
extern DELPHI_PACKAGE System::ResourceString _STooManyInputParams;
#define Oraconsts_STooManyInputParams System::LoadResourceString(&Oraconsts::_STooManyInputParams)
extern DELPHI_PACKAGE System::ResourceString _SNotEnoughInputParams;
#define Oraconsts_SNotEnoughInputParams System::LoadResourceString(&Oraconsts::_SNotEnoughInputParams)
extern DELPHI_PACKAGE System::ResourceString _STooNanyOutputParams;
#define Oraconsts_STooNanyOutputParams System::LoadResourceString(&Oraconsts::_STooNanyOutputParams)
extern DELPHI_PACKAGE System::ResourceString _SNotEnoughOutputParams;
#define Oraconsts_SNotEnoughOutputParams System::LoadResourceString(&Oraconsts::_SNotEnoughOutputParams)
extern DELPHI_PACKAGE System::ResourceString _SPackageNotExist;
#define Oraconsts_SPackageNotExist System::LoadResourceString(&Oraconsts::_SPackageNotExist)
extern DELPHI_PACKAGE System::ResourceString _SNoOCISvcCtx;
#define Oraconsts_SNoOCISvcCtx System::LoadResourceString(&Oraconsts::_SNoOCISvcCtx)
extern DELPHI_PACKAGE System::ResourceString _SObjectNotExist;
#define Oraconsts_SObjectNotExist System::LoadResourceString(&Oraconsts::_SObjectNotExist)
extern DELPHI_PACKAGE System::ResourceString _SNoObjectType;
#define Oraconsts_SNoObjectType System::LoadResourceString(&Oraconsts::_SNoObjectType)
extern DELPHI_PACKAGE System::ResourceString _SObjectNotAllocated;
#define Oraconsts_SObjectNotAllocated System::LoadResourceString(&Oraconsts::_SObjectNotAllocated)
extern DELPHI_PACKAGE System::ResourceString _SObjectIsCached;
#define Oraconsts_SObjectIsCached System::LoadResourceString(&Oraconsts::_SObjectIsCached)
extern DELPHI_PACKAGE System::ResourceString _SNeedThreadSafety;
#define Oraconsts_SNeedThreadSafety System::LoadResourceString(&Oraconsts::_SNeedThreadSafety)
extern DELPHI_PACKAGE System::ResourceString _SParamNotFound;
#define Oraconsts_SParamNotFound System::LoadResourceString(&Oraconsts::_SParamNotFound)
extern DELPHI_PACKAGE System::ResourceString _SReopenNotAllowed;
#define Oraconsts_SReopenNotAllowed System::LoadResourceString(&Oraconsts::_SReopenNotAllowed)
extern DELPHI_PACKAGE System::ResourceString _SLobNotAllocatted;
#define Oraconsts_SLobNotAllocatted System::LoadResourceString(&Oraconsts::_SLobNotAllocatted)
extern DELPHI_PACKAGE System::ResourceString _SSvcCtxNotDefined;
#define Oraconsts_SSvcCtxNotDefined System::LoadResourceString(&Oraconsts::_SSvcCtxNotDefined)
extern DELPHI_PACKAGE System::ResourceString _SNeedParamType;
#define Oraconsts_SNeedParamType System::LoadResourceString(&Oraconsts::_SNeedParamType)
extern DELPHI_PACKAGE System::ResourceString _SLobNotInited;
#define Oraconsts_SLobNotInited System::LoadResourceString(&Oraconsts::_SLobNotInited)
extern DELPHI_PACKAGE System::ResourceString _SCannotDisableLobCache;
#define Oraconsts_SCannotDisableLobCache System::LoadResourceString(&Oraconsts::_SCannotDisableLobCache)
extern DELPHI_PACKAGE System::ResourceString _SInvalidConnectString;
#define Oraconsts_SInvalidConnectString System::LoadResourceString(&Oraconsts::_SInvalidConnectString)
extern DELPHI_PACKAGE System::ResourceString _SHomeNameDiffers;
#define Oraconsts_SHomeNameDiffers System::LoadResourceString(&Oraconsts::_SHomeNameDiffers)
extern DELPHI_PACKAGE System::ResourceString _SDirectDiffers;
#define Oraconsts_SDirectDiffers System::LoadResourceString(&Oraconsts::_SDirectDiffers)
extern DELPHI_PACKAGE System::ResourceString _SOCIUnicodeDiffers;
#define Oraconsts_SOCIUnicodeDiffers System::LoadResourceString(&Oraconsts::_SOCIUnicodeDiffers)
extern DELPHI_PACKAGE System::ResourceString _SCannotChangeParams;
#define Oraconsts_SCannotChangeParams System::LoadResourceString(&Oraconsts::_SCannotChangeParams)
extern DELPHI_PACKAGE System::ResourceString _SNoCorrespondParam;
#define Oraconsts_SNoCorrespondParam System::LoadResourceString(&Oraconsts::_SNoCorrespondParam)
extern DELPHI_PACKAGE System::ResourceString _SNeedBlobType;
#define Oraconsts_SNeedBlobType System::LoadResourceString(&Oraconsts::_SNeedBlobType)
extern DELPHI_PACKAGE System::ResourceString _SNeedLobType;
#define Oraconsts_SNeedLobType System::LoadResourceString(&Oraconsts::_SNeedLobType)
extern DELPHI_PACKAGE System::ResourceString _SNeedFileType;
#define Oraconsts_SNeedFileType System::LoadResourceString(&Oraconsts::_SNeedFileType)
extern DELPHI_PACKAGE System::ResourceString _SNeedObjectType;
#define Oraconsts_SNeedObjectType System::LoadResourceString(&Oraconsts::_SNeedObjectType)
extern DELPHI_PACKAGE System::ResourceString _SNeedTimeStampType;
#define Oraconsts_SNeedTimeStampType System::LoadResourceString(&Oraconsts::_SNeedTimeStampType)
extern DELPHI_PACKAGE System::ResourceString _SNeedIntervalType;
#define Oraconsts_SNeedIntervalType System::LoadResourceString(&Oraconsts::_SNeedIntervalType)
extern DELPHI_PACKAGE System::ResourceString _SNeedNumberType;
#define Oraconsts_SNeedNumberType System::LoadResourceString(&Oraconsts::_SNeedNumberType)
extern DELPHI_PACKAGE System::ResourceString _SMissingDataSetField;
#define Oraconsts_SMissingDataSetField System::LoadResourceString(&Oraconsts::_SMissingDataSetField)
extern DELPHI_PACKAGE System::ResourceString _SColumnNotFound;
#define Oraconsts_SColumnNotFound System::LoadResourceString(&Oraconsts::_SColumnNotFound)
extern DELPHI_PACKAGE System::ResourceString _SUpdatingTableNotDef;
#define Oraconsts_SUpdatingTableNotDef System::LoadResourceString(&Oraconsts::_SUpdatingTableNotDef)
extern DELPHI_PACKAGE System::ResourceString _SCannotWriteExpandedField;
#define Oraconsts_SCannotWriteExpandedField System::LoadResourceString(&Oraconsts::_SCannotWriteExpandedField)
extern DELPHI_PACKAGE System::ResourceString _SCannotReadKeyFields;
#define Oraconsts_SCannotReadKeyFields System::LoadResourceString(&Oraconsts::_SCannotReadKeyFields)
extern DELPHI_PACKAGE System::ResourceString _SInvalidTimeStamp;
#define Oraconsts_SInvalidTimeStamp System::LoadResourceString(&Oraconsts::_SInvalidTimeStamp)
extern DELPHI_PACKAGE System::ResourceString _SInvalidInterval;
#define Oraconsts_SInvalidInterval System::LoadResourceString(&Oraconsts::_SInvalidInterval)
extern DELPHI_PACKAGE System::ResourceString _SCannotConvertStrToBool;
#define Oraconsts_SCannotConvertStrToBool System::LoadResourceString(&Oraconsts::_SCannotConvertStrToBool)
extern DELPHI_PACKAGE System::ResourceString _SCannotConvertNumberToBCD;
#define Oraconsts_SCannotConvertNumberToBCD System::LoadResourceString(&Oraconsts::_SCannotConvertNumberToBCD)
extern DELPHI_PACKAGE System::ResourceString _SNoTimers;
#define Oraconsts_SNoTimers System::LoadResourceString(&Oraconsts::_SNoTimers)
extern DELPHI_PACKAGE System::ResourceString _SBlobNotExpanded;
#define Oraconsts_SBlobNotExpanded System::LoadResourceString(&Oraconsts::_SBlobNotExpanded)
extern DELPHI_PACKAGE System::ResourceString _SAlerterReceiveFailed;
#define Oraconsts_SAlerterReceiveFailed System::LoadResourceString(&Oraconsts::_SAlerterReceiveFailed)
extern DELPHI_PACKAGE System::ResourceString _SAlerterSendFailed;
#define Oraconsts_SAlerterSendFailed System::LoadResourceString(&Oraconsts::_SAlerterSendFailed)
extern DELPHI_PACKAGE System::ResourceString _SOLiteSchemaNotSupported;
#define Oraconsts_SOLiteSchemaNotSupported System::LoadResourceString(&Oraconsts::_SOLiteSchemaNotSupported)
extern DELPHI_PACKAGE System::ResourceString _SUseUnicodeRequired;
#define Oraconsts_SUseUnicodeRequired System::LoadResourceString(&Oraconsts::_SUseUnicodeRequired)
extern DELPHI_PACKAGE System::ResourceString _SClobMustBeUnicode;
#define Oraconsts_SClobMustBeUnicode System::LoadResourceString(&Oraconsts::_SClobMustBeUnicode)
extern DELPHI_PACKAGE System::ResourceString _SCannotInitOCI;
#define Oraconsts_SCannotInitOCI System::LoadResourceString(&Oraconsts::_SCannotInitOCI)
extern DELPHI_PACKAGE System::ResourceString _SCheckOCI;
#define Oraconsts_SCheckOCI System::LoadResourceString(&Oraconsts::_SCheckOCI)
extern DELPHI_PACKAGE System::ResourceString _SCheckOCI73;
#define Oraconsts_SCheckOCI73 System::LoadResourceString(&Oraconsts::_SCheckOCI73)
extern DELPHI_PACKAGE System::ResourceString _SCheckOCI80;
#define Oraconsts_SCheckOCI80 System::LoadResourceString(&Oraconsts::_SCheckOCI80)
extern DELPHI_PACKAGE System::ResourceString _SCheckOCI81;
#define Oraconsts_SCheckOCI81 System::LoadResourceString(&Oraconsts::_SCheckOCI81)
extern DELPHI_PACKAGE System::ResourceString _SCheckOCI90;
#define Oraconsts_SCheckOCI90 System::LoadResourceString(&Oraconsts::_SCheckOCI90)
extern DELPHI_PACKAGE System::ResourceString _SDirectPathLoadingNotSupportedWithDirect;
#define Oraconsts_SDirectPathLoadingNotSupportedWithDirect System::LoadResourceString(&Oraconsts::_SDirectPathLoadingNotSupportedWithDirect)
extern DELPHI_PACKAGE System::ResourceString _SProxyPoolNotSupportedWithDirect;
#define Oraconsts_SProxyPoolNotSupportedWithDirect System::LoadResourceString(&Oraconsts::_SProxyPoolNotSupportedWithDirect)
extern DELPHI_PACKAGE System::ResourceString _SOCIPoolNotSupportedWithDirect;
#define Oraconsts_SOCIPoolNotSupportedWithDirect System::LoadResourceString(&Oraconsts::_SOCIPoolNotSupportedWithDirect)
extern DELPHI_PACKAGE System::ResourceString _SOCIPoolNotSupported;
#define Oraconsts_SOCIPoolNotSupported System::LoadResourceString(&Oraconsts::_SOCIPoolNotSupported)
extern DELPHI_PACKAGE System::ResourceString _SMTSPoolNotSupportedWithDirect;
#define Oraconsts_SMTSPoolNotSupportedWithDirect System::LoadResourceString(&Oraconsts::_SMTSPoolNotSupportedWithDirect)
extern DELPHI_PACKAGE System::ResourceString _SStmtCacheNotSupportedWithDirect;
#define Oraconsts_SStmtCacheNotSupportedWithDirect System::LoadResourceString(&Oraconsts::_SStmtCacheNotSupportedWithDirect)
extern DELPHI_PACKAGE System::ResourceString _SClientIdentifierFirstCharShouldNotBeColon;
#define Oraconsts_SClientIdentifierFirstCharShouldNotBeColon System::LoadResourceString(&Oraconsts::_SClientIdentifierFirstCharShouldNotBeColon)
extern DELPHI_PACKAGE System::ResourceString _SCannotStartTransactionWithoutId;
#define Oraconsts_SCannotStartTransactionWithoutId System::LoadResourceString(&Oraconsts::_SCannotStartTransactionWithoutId)
extern DELPHI_PACKAGE System::ResourceString _STransactionIdTooLong;
#define Oraconsts_STransactionIdTooLong System::LoadResourceString(&Oraconsts::_STransactionIdTooLong)
extern DELPHI_PACKAGE System::ResourceString _SDetachNotSupportedWithMTS;
#define Oraconsts_SDetachNotSupportedWithMTS System::LoadResourceString(&Oraconsts::_SDetachNotSupportedWithMTS)
extern DELPHI_PACKAGE System::ResourceString _STransactionNotSupportedWithDirect;
#define Oraconsts_STransactionNotSupportedWithDirect System::LoadResourceString(&Oraconsts::_STransactionNotSupportedWithDirect)
extern DELPHI_PACKAGE System::ResourceString _SReadOnlyNotSupportedWithMTS;
#define Oraconsts_SReadOnlyNotSupportedWithMTS System::LoadResourceString(&Oraconsts::_SReadOnlyNotSupportedWithMTS)
extern DELPHI_PACKAGE System::ResourceString _SChangeNotifyOperationsCannotBeEmpty;
#define Oraconsts_SChangeNotifyOperationsCannotBeEmpty System::LoadResourceString(&Oraconsts::_SChangeNotifyOperationsCannotBeEmpty)
extern DELPHI_PACKAGE System::ResourceString _SChangeNotifyNotSupportedWithDirect;
#define Oraconsts_SChangeNotifyNotSupportedWithDirect System::LoadResourceString(&Oraconsts::_SChangeNotifyNotSupportedWithDirect)
extern DELPHI_PACKAGE System::ResourceString _SChangeNotifyOCIVersion;
#define Oraconsts_SChangeNotifyOCIVersion System::LoadResourceString(&Oraconsts::_SChangeNotifyOCIVersion)
extern DELPHI_PACKAGE System::ResourceString _SChangeNotifyServerVersion;
#define Oraconsts_SChangeNotifyServerVersion System::LoadResourceString(&Oraconsts::_SChangeNotifyServerVersion)
extern DELPHI_PACKAGE System::ResourceString _SQueueNotFound;
#define Oraconsts_SQueueNotFound System::LoadResourceString(&Oraconsts::_SQueueNotFound)
extern DELPHI_PACKAGE System::ResourceString _SQueueTableNotFound;
#define Oraconsts_SQueueTableNotFound System::LoadResourceString(&Oraconsts::_SQueueTableNotFound)
extern DELPHI_PACKAGE System::ResourceString _SUnknownPayloadType;
#define Oraconsts_SUnknownPayloadType System::LoadResourceString(&Oraconsts::_SUnknownPayloadType)
extern DELPHI_PACKAGE System::ResourceString _SUnknownSortOrder;
#define Oraconsts_SUnknownSortOrder System::LoadResourceString(&Oraconsts::_SUnknownSortOrder)
extern DELPHI_PACKAGE System::ResourceString _SUnknownRecipientsType;
#define Oraconsts_SUnknownRecipientsType System::LoadResourceString(&Oraconsts::_SUnknownRecipientsType)
extern DELPHI_PACKAGE System::ResourceString _SUnknownMessageGrouping;
#define Oraconsts_SUnknownMessageGrouping System::LoadResourceString(&Oraconsts::_SUnknownMessageGrouping)
extern DELPHI_PACKAGE System::ResourceString _SUnknownCompatibility;
#define Oraconsts_SUnknownCompatibility System::LoadResourceString(&Oraconsts::_SUnknownCompatibility)
extern DELPHI_PACKAGE System::ResourceString _SUnknownSecurityValue;
#define Oraconsts_SUnknownSecurityValue System::LoadResourceString(&Oraconsts::_SUnknownSecurityValue)
extern DELPHI_PACKAGE System::ResourceString _SUnknownQueueType;
#define Oraconsts_SUnknownQueueType System::LoadResourceString(&Oraconsts::_SUnknownQueueType)
extern DELPHI_PACKAGE System::ResourceString _SSubscriptionNotSupportedWithDirect;
#define Oraconsts_SSubscriptionNotSupportedWithDirect System::LoadResourceString(&Oraconsts::_SSubscriptionNotSupportedWithDirect)
extern DELPHI_PACKAGE System::ResourceString _SSubscriptionOracleVersion;
#define Oraconsts_SSubscriptionOracleVersion System::LoadResourceString(&Oraconsts::_SSubscriptionOracleVersion)
extern DELPHI_PACKAGE System::ResourceString _SPurgeNotSupported;
#define Oraconsts_SPurgeNotSupported System::LoadResourceString(&Oraconsts::_SPurgeNotSupported)
extern DELPHI_PACKAGE System::ResourceString _SEnqueueArrayNotSupported;
#define Oraconsts_SEnqueueArrayNotSupported System::LoadResourceString(&Oraconsts::_SEnqueueArrayNotSupported)
extern DELPHI_PACKAGE System::ResourceString _SPayloadArrayTypeNotFound;
#define Oraconsts_SPayloadArrayTypeNotFound System::LoadResourceString(&Oraconsts::_SPayloadArrayTypeNotFound)
extern DELPHI_PACKAGE System::ResourceString _SMessagesNotEnqueued;
#define Oraconsts_SMessagesNotEnqueued System::LoadResourceString(&Oraconsts::_SMessagesNotEnqueued)
extern DELPHI_PACKAGE System::ResourceString _SQueueTableNameNotDefined;
#define Oraconsts_SQueueTableNameNotDefined System::LoadResourceString(&Oraconsts::_SQueueTableNameNotDefined)
extern DELPHI_PACKAGE System::ResourceString _SQueueNameNotDefined;
#define Oraconsts_SQueueNameNotDefined System::LoadResourceString(&Oraconsts::_SQueueNameNotDefined)
extern DELPHI_PACKAGE System::ResourceString _SPlSqlTraceNotSet;
#define Oraconsts_SPlSqlTraceNotSet System::LoadResourceString(&Oraconsts::_SPlSqlTraceNotSet)
extern DELPHI_PACKAGE System::ResourceString _SBadCallParameterPrepare;
#define Oraconsts_SBadCallParameterPrepare System::LoadResourceString(&Oraconsts::_SBadCallParameterPrepare)
extern DELPHI_PACKAGE System::ResourceString _SIncorrectConnectionType;
#define Oraconsts_SIncorrectConnectionType System::LoadResourceString(&Oraconsts::_SIncorrectConnectionType)
}	/* namespace Oraconsts */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_ORACONSTS)
using namespace Oraconsts;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// OraconstsHPP
