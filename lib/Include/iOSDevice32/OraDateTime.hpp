﻿// CodeGear C++Builder
// Copyright (c) 1995, 2018 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'OraDateTime.pas' rev: 33.00 (iOS)

#ifndef OradatetimeHPP
#define OradatetimeHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.SysUtils.hpp>
#include <CRTypes.hpp>
#include <OraCall.hpp>

//-- user supplied -----------------------------------------------------------

namespace Oradatetime
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TOCIDateTime;
//-- type declarations -------------------------------------------------------
#pragma pack(push,4)
class PASCALIMPLEMENTATION TOCIDateTime : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	System::TArray__1<System::Byte> FValue;
	System::Byte FFracPrecision;
	unsigned FDescriptorType;
	int FServerTZHour;
	int FServerTZMunute;
	void __fastcall GetLocalTimeZone(System::Int8 &tzhour, System::Int8 &tzminute);
	void __fastcall GetActualTimeZoneOffset(System::Int8 &hour, System::Int8 &minute);
	void __fastcall InternalSetDate(short year, System::Byte month, System::Byte day)/* overload */;
	__classmethod void __fastcall InternalSetDate(System::TArray__1<System::Byte> &Value, short year, System::Byte month, System::Byte day)/* overload */;
	void __fastcall InternalGetDate(/* out */ short &year, /* out */ System::Byte &month, /* out */ System::Byte &day)/* overload */;
	__classmethod void __fastcall InternalGetDate(const System::TArray__1<System::Byte> Value, /* out */ short &year, /* out */ System::Byte &month, /* out */ System::Byte &day)/* overload */;
	void __fastcall InternalSetTime(System::Byte hour, System::Byte minute, System::Byte sec, unsigned fsec)/* overload */;
	__classmethod void __fastcall InternalSetTime(System::TArray__1<System::Byte> &Value, System::Byte hour, System::Byte minute, System::Byte sec, unsigned fsec)/* overload */;
	void __fastcall InternalGetTime(/* out */ System::Byte &hour, /* out */ System::Byte &minute, /* out */ System::Byte &sec, /* out */ unsigned &fsec)/* overload */;
	__classmethod void __fastcall InternalGetTime(const System::TArray__1<System::Byte> Value, /* out */ System::Byte &hour, /* out */ System::Byte &minute, /* out */ System::Byte &sec, /* out */ unsigned &fsec)/* overload */;
	
protected:
	virtual void __fastcall Alloc(int Len);
	void __fastcall CheckAlloc(bool fSeconds, bool TimeZone);
	__classmethod int __fastcall GetTrimmedLen(System::TArray__1<System::Byte> &Value, unsigned DescriptorType);
	__classmethod void __fastcall TransformToLocalTime(short &year, System::Byte &month, System::Byte &day, System::Byte &hour, System::Byte &minute, int TZHourMinute, bool ToLocal);
	
public:
	__fastcall TOCIDateTime(unsigned DescriptorType);
	bool __fastcall GetIsNull();
	void __fastcall Assign(TOCIDateTime* src);
	int __fastcall Compare(TOCIDateTime* const date1);
	unsigned __fastcall Check();
	void __fastcall Parse(System::UnicodeString date_str, System::UnicodeString fmt);
	HIDESBASE System::UnicodeString __fastcall ToString(System::UnicodeString fmt, System::Byte fsprec);
	void __fastcall Construct(short year, System::Byte month, System::Byte day, System::Byte hour, System::Byte min, System::Byte sec, unsigned fsec, System::UnicodeString timezone);
	void __fastcall GetDate(short &year, System::Byte &month, System::Byte &day);
	void __fastcall SetDate(short year, System::Byte month, System::Byte day);
	void __fastcall GetTime(System::Byte &hour, System::Byte &minute, System::Byte &sec, unsigned &fsec);
	void __fastcall SetTime(System::Byte hour, System::Byte minute, System::Byte sec, unsigned fsec);
	void __fastcall GetTimeZoneOffset(/* out */ System::Int8 &tzHour, /* out */ System::Int8 &tzMinute);
	void __fastcall SetTimeZoneOffset(System::Int8 tzHour, System::Int8 tzMinute);
	System::UnicodeString __fastcall GetTimeZoneName();
	__property unsigned DescriptorType = {read=FDescriptorType, nodefault};
	__property bool IsNull = {read=GetIsNull, nodefault};
	__property System::TArray__1<System::Byte> Value = {read=FValue, write=FValue};
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TOCIDateTime() { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
#define SNotMatchLength u"ORA-01862 the numeric value does not match the length of t"\
	u"he format item"
#define SFormatNotRecognized u"ORA-01821 date format not recognized"
#define SNonNnumericCharacter u"ORA-01858 a non-numeric character was found where a numeri"\
	u"c was expected"
#define SNotLongEnough u"ORA-01840 input value not long enough for date format"
#define SWrongTZH u"ORA-01874 time zone hour must be between -12 and 13"
#define SWrongTZM u"ORA-01875 time zone minute must be between -59 and 59"
#define SWrongMinute u"ORA-01851 minutes must be between 0 and 59"
#define SWrongSecond u"ORA-01852 seconds must be between 0 and 59"
#define SWrongHour12 u"ORA-01850 hour must be between 0 and 23"
#define SWrongHour24 u"ORA-01849 hour must be between 1 and 12"
#define SWrongYear u"ORA-01841 (full) year must be between -4713 and +9999, and"\
	u" not be 0"
#define SWrongDayOfYear u"ORA-01848 day of year must be between 1 and 365 (366 for l"\
	u"eap year)"
#define SWrongDayOfMonth u"ORA-01847 day of month must be between 1 and last day of m"\
	u"onth"
#define SNotValidMonth u"ORA-01843 not a valid month"
#define SNotValidDayOfWeek u"ORA-01846 not a valid day of the week"
#define SWrongAMPM u"ORA-01855 AM/A.M. or PM/P.M. required"
#define SWrongBCAD u"ORA-01856 BC/B.C. or AD/A.D. required"
#define SWrongLiteral u"ORA-01861 literal does not match format string"
#define SAppearsTwice u"ORA-01810 format code appears twice"
#define SWrongFSec u"ORA-01880 the fractional seconds must be between 0 and 999"\
	u"999999"
#define SConflictMonthJulian u"ORA-01833 month conflicts with Julian date"
#define SConflictDayJulian u"ORA-01834 day of month conflicts with Julian date"
#define SConflictWeekDayJulian u"ORA-01835 day of week conflicts with Julian date"
#define SPrecludesUseMeridian u"ORA-01818 'HH24' precludes use of meridian indicator"
#define SPrecludesUseBCAD u"ORA-01819 signed year precludes use of BC/AD"
#define SInvalidDay u"ORA-01839 date not valid for month specified"
#define SFieldNotFound u"ORA-01878 specified field not found in datetime or interva"\
	u"l"
#define SDatetimesIncomparable u"ORA-01870 the intervals or datetimes are not mutually comp"\
	u"arable"
extern DELPHI_PACKAGE int __fastcall GetServerLocalTimeZoneOffset(TOCIDateTime* Value);
extern DELPHI_PACKAGE int __fastcall GetVectorTimeZoneOffset(const System::TArray__1<System::Byte> Vector);
extern DELPHI_PACKAGE void __fastcall ConvertToLocalTime(TOCIDateTime* Value)/* overload */;
extern DELPHI_PACKAGE void __fastcall ConvertToLocalTime(TOCIDateTime* Value, int Offset)/* overload */;
extern DELPHI_PACKAGE void __fastcall ConvertToLocalTime(System::TArray__1<System::Byte> &Value)/* overload */;
extern DELPHI_PACKAGE void __fastcall ConvertToLocalTime(System::TArray__1<System::Byte> &Value, int Offset)/* overload */;
extern DELPHI_PACKAGE void __fastcall ConvertToServerTZ(System::TArray__1<System::Byte> &Value, bool UseLocalTZ);
extern DELPHI_PACKAGE void __fastcall ConvertFromServerTZ(System::TArray__1<System::Byte> &Value);
}	/* namespace Oradatetime */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_ORADATETIME)
using namespace Oradatetime;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// OradatetimeHPP
