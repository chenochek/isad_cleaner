﻿// CodeGear C++Builder
// Copyright (c) 1995, 2018 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Ora.pas' rev: 33.00 (iOS)

#ifndef OraHPP
#define OraHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.SysUtils.hpp>
#include <System.Classes.hpp>
#include <Data.DB.hpp>
#include <System.Types.hpp>
#include <System.Variants.hpp>
#include <Data.SqlTimSt.hpp>
#include <System.Generics.Collections.hpp>
#include <System.Generics.Defaults.hpp>
#include <CLRClasses.hpp>
#include <CRXml.hpp>
#include <CRTypes.hpp>
#include <CRParser.hpp>
#include <CRAccess.hpp>
#include <CRConnectionPool.hpp>
#include <CRDataTypeMap.hpp>
#include <CRVio.hpp>
#include <CRConnectionString.hpp>
#include <CREncryption.hpp>
#include <MemUtils.hpp>
#include <MemData.hpp>
#include <DBAccess.hpp>
#include <MemDS.hpp>
#include <DAConsts.hpp>
#include <OraConsts.hpp>
#include <DASQLGenerator.hpp>
#include <OraSQLGenerator.hpp>
#include <OraCall.hpp>
#include <OraClasses.hpp>
#include <OraError.hpp>
#include <OraObjects.hpp>
#include <OraConnectionPool.hpp>
#include <OraServices.hpp>

//-- user supplied -----------------------------------------------------------
#ifdef GetObject
#undef GetObject
#endif

namespace Ora
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TOraSessionOptions;
class DELPHICLASS TOraPoolingOptions;
class DELPHICLASS TOraTrace;
class DELPHICLASS TOraEncryptor;
class DELPHICLASS TOraSession;
class DELPHICLASS TSessionList;
class DELPHICLASS TOraParamValue;
class DELPHICLASS TOraParam;
class DELPHICLASS TOraParams;
class DELPHICLASS TCursorField;
class DELPHICLASS TBFileField;
class DELPHICLASS TOraDataSetField;
class DELPHICLASS TOraReferenceField;
class DELPHICLASS TLabelField;
class DELPHICLASS TOraTimeStampField;
class DELPHICLASS TOraIntervalField;
class DELPHICLASS TOraNumberField;
class DELPHICLASS TOraXMLField;
class DELPHICLASS TOraAnyDataField;
class DELPHICLASS TOraEncryption;
class DELPHICLASS TCustomOraComponent;
class DELPHICLASS TOraDataSetOptionsDS;
class DELPHICLASS TOraDataSetOptions;
class DELPHICLASS TOraSQLGenerator;
class DELPHICLASS TOraDataSetUpdater;
class DELPHICLASS TOraDataSetService;
class DELPHICLASS TOraDataSet;
class DELPHICLASS TBFileStream;
class DELPHICLASS TOraNestedTable;
class DELPHICLASS TCustomOraQuery;
class DELPHICLASS TOraQuery;
class DELPHICLASS TOraStoredProc;
class DELPHICLASS TOraSQL;
class DELPHICLASS TOraUpdateSQL;
class DELPHICLASS TOraMetaData;
class DELPHICLASS TOraDataSource;
class DELPHICLASS TOraFieldTypeInfos;
class DELPHICLASS TOraFieldTypeMap;
class DELPHICLASS TOraUtils;
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TSubFieldType : unsigned char { stNone, stOraBlob, stOraClob };

_DECLARE_ARITH_TYPE_ALIAS(System::Int8, TCharLength);

typedef void __fastcall (__closure *TConnectChangeEvent)(System::TObject* Sender, bool Connected);

typedef void __fastcall (__closure *TInfoMessageEvent)(System::TObject* Sender, Oraerror::EOraError* Error);

enum DECLSPEC_DENUM TFailoverState : unsigned char { fsEnd, fsAbort, fsReauth, fsBegin, fsError };

enum DECLSPEC_DENUM TFailoverType : unsigned char { ftNone, ftSession, ftSelect, ftTransaction };

typedef void __fastcall (__closure *TFailoverEvent)(System::TObject* Sender, TFailoverState FailoverState, TFailoverType FailoverType, bool &Retry);

enum DECLSPEC_DENUM TOraIsolationLevel : unsigned char { ilReadCommitted, ilSerializable, ilReadOnly };

enum DECLSPEC_DENUM TSequenceMode : unsigned char { smInsert, smPost };

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraSessionOptions : public Dbaccess::TDAConnectionOptions
{
	typedef Dbaccess::TDAConnectionOptions inherited;
	
private:
	bool FUseOCI7;
	bool FDirect;
	Crvio::TIPVersion FIPVersion;
	bool FEnableIntegers;
	bool FIsDateLanguageStored;
	bool FIsDateFormatStored;
	bool FEnableNumbers;
	bool FEnableLargeint;
	bool FEnableOraTimestamp;
	TCharLength FCharLength;
	System::UnicodeString FCharset;
	bool FUseUnicode;
	System::UnicodeString FDateFormat;
	System::UnicodeString FDateLanguage;
	Oraclasses::TOptimizerMode FOptimizerMode;
	int FConnectionTimeout;
	bool FStatementCache;
	int FStatementCacheSize;
	System::UnicodeString FClientIdentifier;
	bool FUnicodeEnvironment;
	int FSubscriptionPort;
	TCharLength __fastcall GetCharLength();
	void __fastcall SetCharLength(TCharLength Value);
	void __fastcall SetCharset(const System::UnicodeString Value);
	void __fastcall SetUseUnicode(bool Value);
	System::UnicodeString __fastcall GetDateLanguage();
	void __fastcall SetDateLanguage(const System::UnicodeString Value);
	System::UnicodeString __fastcall GetDateFormat();
	void __fastcall SetDateFormat(const System::UnicodeString Value);
	void __fastcall SetEnableIntegers(bool Value);
	void __fastcall SetEnableLargeint(bool Value);
	void __fastcall SetEnableNumbers(bool Value);
	void __fastcall SetEnableOraTimestamp(bool Value);
	bool __fastcall GetConvertEOL();
	void __fastcall SetConvertEOL(bool Value);
	void __fastcall SetNeverConnect(bool Value);
	bool __fastcall GetNeverConnect();
	void __fastcall SetUseOCI7(bool Value);
	bool __fastcall GetNet();
	void __fastcall SetNet(bool Value);
	void __fastcall SetDirect(bool Value);
	void __fastcall SetIPVersion(Crvio::TIPVersion Value);
	void __fastcall SetOptimizerMode(Oraclasses::TOptimizerMode Value);
	void __fastcall SetConnectionTimeout(int Value);
	void __fastcall SetStatementCache(bool Value);
	void __fastcall SetStatementCacheSize(int Value);
	void __fastcall SetClientIdentifier(const System::UnicodeString Value);
	void __fastcall SetUnicodeEnvironment(bool Value);
	void __fastcall SetSubscriptionPort(int Value);
	
protected:
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	
public:
	__fastcall TOraSessionOptions(Dbaccess::TCustomDAConnection* Owner);
	
__published:
	__property TCharLength CharLength = {read=GetCharLength, write=SetCharLength, default=0};
	__property System::UnicodeString Charset = {read=FCharset, write=SetCharset};
	__property bool UseUnicode = {read=FUseUnicode, write=SetUseUnicode, default=0};
	__property bool ConvertEOL = {read=GetConvertEOL, write=SetConvertEOL, default=0};
	__property System::UnicodeString DateFormat = {read=GetDateFormat, write=SetDateFormat, stored=FIsDateFormatStored};
	__property System::UnicodeString DateLanguage = {read=GetDateLanguage, write=SetDateLanguage, stored=FIsDateLanguageStored};
	__property bool EnableIntegers = {read=FEnableIntegers, write=SetEnableIntegers, default=1};
	__property bool EnableLargeint = {read=FEnableLargeint, write=SetEnableLargeint, default=0};
	__property bool EnableNumbers = {read=FEnableNumbers, write=SetEnableNumbers, default=0};
	__property bool EnableOraTimestamp = {read=FEnableOraTimestamp, write=SetEnableOraTimestamp, default=1};
	__property bool NeverConnect = {read=GetNeverConnect, write=SetNeverConnect, stored=false, default=0};
	__property bool UseOCI7 = {read=FUseOCI7, write=SetUseOCI7, default=0};
	__property Oraclasses::TOptimizerMode OptimizerMode = {read=FOptimizerMode, write=SetOptimizerMode, default=0};
	__property int ConnectionTimeout = {read=FConnectionTimeout, write=SetConnectionTimeout, default=0};
	__property bool StatementCache = {read=FStatementCache, write=SetStatementCache, default=0};
	__property int StatementCacheSize = {read=FStatementCacheSize, write=SetStatementCacheSize, default=20};
	__property System::UnicodeString ClientIdentifier = {read=FClientIdentifier, write=SetClientIdentifier};
	__property bool UnicodeEnvironment = {read=FUnicodeEnvironment, write=SetUnicodeEnvironment, default=0};
	__property int SubscriptionPort = {read=FSubscriptionPort, write=SetSubscriptionPort, default=0};
	__property bool Net = {read=GetNet, write=SetNet, stored=false, default=0};
	__property bool Direct = {read=FDirect, write=SetDirect, default=0};
	__property Crvio::TIPVersion IPVersion = {read=FIPVersion, write=SetIPVersion, default=0};
	__property DisconnectedMode = {default=0};
	__property KeepDesignConnected = {default=1};
	__property LocalFailover = {default=0};
	__property DefaultSortType = {default=2};
	__property EnableBCD = {default=0};
	__property EnableFMTBCD = {default=0};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TOraSessionOptions() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraPoolingOptions : public Dbaccess::TPoolingOptions
{
	typedef Dbaccess::TPoolingOptions inherited;
	
private:
	Oraconnectionpool::TOraPoolingType FPoolingType;
	System::UnicodeString FProxyUsername;
	System::UnicodeString FProxyPassword;
	void __fastcall SetPoolingType(Oraconnectionpool::TOraPoolingType Value);
	void __fastcall SetProxyUsername(System::UnicodeString Value);
	void __fastcall SetProxyPassword(System::UnicodeString Value);
	
public:
	__fastcall virtual TOraPoolingOptions(Dbaccess::TCustomDAConnection* Owner);
	
__published:
	__property Oraconnectionpool::TOraPoolingType PoolType = {read=FPoolingType, write=SetPoolingType, default=0};
	__property System::UnicodeString ProxyUsername = {read=FProxyUsername, write=SetProxyUsername};
	__property System::UnicodeString ProxyPassword = {read=FProxyPassword, write=SetProxyPassword};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TOraPoolingOptions() { }
	
};

#pragma pack(pop)

enum DECLSPEC_DENUM Ora__3 : unsigned char { smBasicStatistics, smTypicalStatistics, smAllStatistics, smBindVariables, smWaitEvents, smTimedStatistics };

typedef System::Set<Ora__3, Ora__3::smBasicStatistics, Ora__3::smTimedStatistics> TSqlTraceMode;

enum DECLSPEC_DENUM Ora__4 : unsigned char { pmAllCalls, pmEnabledCalls, pmAllExceptions, pmEnabledExceptions, pmAllSql, pmEnabledSql, pmAllLines, pmEnabledLines };

typedef System::Set<Ora__4, Ora__4::pmAllCalls, Ora__4::pmEnabledLines> TPlSqlTraceMode;

enum DECLSPEC_DENUM Ora__5 : unsigned char { tsSqlTrace, tsPlSqlTrace };

typedef System::Set<Ora__5, Ora__5::tsSqlTrace, Ora__5::tsPlSqlTrace> TTraceState;

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraTrace : public System::Classes::TComponent
{
	typedef System::Classes::TComponent inherited;
	
private:
	__weak TOraSession* FSession;
	bool FEnabled;
	TPlSqlTraceMode FPlSqlTraceMode;
	TSqlTraceMode FSqlTraceMode;
	bool FPlSqlTraceActive;
	bool FSqlTraceActive;
	System::UnicodeString FTraceFileIdentifier;
	int FMaxTraceFileSize;
	TTraceState __fastcall GetState();
	void __fastcall SetSession(TOraSession* Value);
	void __fastcall SetEnabled(bool Value);
	void __fastcall SetTraceFileIdentifier(const System::UnicodeString Value);
	void __fastcall SetMaxTraceFileSize(int Value);
	void __fastcall SetSqlTraceMode(TSqlTraceMode Value);
	
protected:
	bool FDesignCreate;
	virtual void __fastcall Loaded();
	void __fastcall InternalSetTraceFileIdentifier();
	void __fastcall InternalSetMaxTraceFileSize();
	void __fastcall InternalSetSqlTraceMode();
	void __fastcall StartTrace();
	void __fastcall StopTrace();
	
public:
	__fastcall virtual TOraTrace(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TOraTrace();
	void __fastcall PlSqlTraceStart();
	void __fastcall PlSqlTraceStop();
	void __fastcall PlSqlTracePause();
	void __fastcall PlSqlTraceResume();
	void __fastcall PlSqlTraceComment(const System::UnicodeString Comment);
	void __fastcall PlSqlTraceLimit(int Limit = 0x2000);
	int __fastcall PlSqlTraceRunNumber();
	void __fastcall SqlTraceStart();
	void __fastcall SqlTraceStop();
	System::UnicodeString __fastcall GetTraceFileName();
	int __fastcall GetSessionPID();
	__property TTraceState State = {read=GetState, nodefault};
	
__published:
	__property TOraSession* Session = {read=FSession, write=SetSession};
	__property bool Enabled = {read=FEnabled, write=SetEnabled, default=1};
	__property TSqlTraceMode SqlTraceMode = {read=FSqlTraceMode, write=SetSqlTraceMode, default=34};
	__property TPlSqlTraceMode PlSqlTraceMode = {read=FPlSqlTraceMode, write=FPlSqlTraceMode, default=0};
	__property System::UnicodeString TraceFileIdentifier = {read=FTraceFileIdentifier, write=SetTraceFileIdentifier};
	__property int MaxTraceFileSize = {read=FMaxTraceFileSize, write=SetMaxTraceFileSize, default=0};
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraEncryptor : public Crencryption::TCREncryptor
{
	typedef Crencryption::TCREncryptor inherited;
	
public:
	/* TCREncryptor.Create */ inline __fastcall virtual TOraEncryptor(System::Classes::TComponent* AOwner) : Crencryption::TCREncryptor(AOwner) { }
	/* TCREncryptor.Destroy */ inline __fastcall virtual ~TOraEncryptor() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraSession : public Dbaccess::TCustomDAConnection
{
	typedef Dbaccess::TCustomDAConnection inherited;
	
private:
	TOraSessionOptions* FOptions;
	TOraPoolingOptions* FPoolingOptions;
	System::UnicodeString FHomeName;
	bool FThreadSafety;
	Oraclasses::TConnectMode FConnectMode;
	Oracall::TOCICallStyle FOCICallStyle;
	TConnectChangeEvent FOnConnectChange;
	System::UnicodeString FInternalName;
	TFailoverEvent FFailoverEvent;
	System::UnicodeString FSchema;
	TOraSession* FProxySession;
	TOraTrace* FTrace;
	TInfoMessageEvent FOnInfoMessage;
	void __fastcall SetThreadSafety(bool Value);
	System::UnicodeString __fastcall GetOracleVersion();
	TOraSQL* __fastcall GetSQL();
	Oracall::PCDA __fastcall GetLDA();
	Oracall::TOCISvcCtx* __fastcall GetOCISvcCtx();
	void __fastcall SetOCISvcCtx(Oracall::TOCISvcCtx* Value);
	Oracall::TOCICallStyle __fastcall GetOCICallStyle();
	void __fastcall SetOCICallStyle(Oracall::TOCICallStyle Value);
	HIDESBASE void __fastcall SetOptions(TOraSessionOptions* Value);
	HIDESBASE void __fastcall SetPoolingOptions(TOraPoolingOptions* Value);
	int __fastcall GetLastError();
	void __fastcall SetLastError(int Value);
	void __fastcall SetHomeName(System::UnicodeString Value);
	Oracall::TOracleHome* __fastcall GetHome();
	void __fastcall SetHome(Oracall::TOracleHome* Value);
	bool __fastcall GetConnectPrompt();
	void __fastcall SetConnectPrompt(bool Value);
	void __fastcall SetConnectMode(Oraclasses::TConnectMode Value);
	System::UnicodeString __fastcall GetSchema();
	void __fastcall SetSchema(System::UnicodeString Value);
	bool __fastcall IsSchemaStored();
	TOraSession* __fastcall GetProxySession();
	void __fastcall SetProxySession(TOraSession* Value);
	void __fastcall SetInternalName(const System::UnicodeString AValue);
	void __fastcall GetObjectList(System::Classes::TStrings* List, System::UnicodeString SQL);
	void __fastcall SetTrace(TOraTrace* Value);
	
protected:
	Oraclasses::TOCIConnection* FIConnection;
	virtual Craccess::TCRConnectionClass __fastcall GetIConnectionClass();
	virtual Craccess::TCRCommandClass __fastcall GetICommandClass();
	virtual Craccess::TCRRecordSetClass __fastcall GetIRecordSetClass();
	virtual Craccess::TCRMetaDataClass __fastcall GetIMetaDataClass();
	virtual Craccess::TCRTransactionClass __fastcall GetITransactionClass();
	virtual void __fastcall SetConnectionParameters(Crconnectionpool::TCRConnectionParameters* ConnectionParameters);
	virtual void __fastcall SetBaseConnectionProps(Craccess::TCRConnection* Connection);
	virtual void __fastcall SetConnectionProps(Craccess::TCRConnection* Connection);
	virtual Crconnectionpool::TCRConnectionParametersClass __fastcall GetConnectionParametersClass();
	virtual Crconnectionpool::TCRConnectionPoolManagerClass __fastcall GetConnectionPoolingManagerClass();
	virtual void __fastcall SetIConnection(Craccess::TCRConnection* Value);
	virtual void __fastcall DoConnect();
	virtual void __fastcall DoDisconnect();
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	System::UnicodeString __fastcall GetCompilationError(System::UnicodeString SQL);
	virtual System::TClass __fastcall SQLMonitorClass();
	virtual Dbaccess::TConnectDialogClass __fastcall ConnectDialogClass();
	virtual void __fastcall CheckCommand();
	virtual Dbaccess::TDAConnectionOptions* __fastcall CreateOptions();
	virtual Crconnectionstring::TCRConnectionStringBuilder* __fastcall CreateConnectionStringBuilder();
	virtual Dbaccess::TPoolingOptions* __fastcall CreatePoolingOptions();
	virtual void __fastcall SetConnected(bool Value);
	virtual void __fastcall SetConnectionString(const System::UnicodeString Value);
	void __fastcall DoOnFailover(unsigned FailoverState, unsigned FailoverType, bool &Retry);
	void __fastcall DoOnInfoMessage(Oraerror::EOraError* Error);
	System::UnicodeString __fastcall GetCachedSchema();
	virtual System::UnicodeString __fastcall DefaultTableSchema();
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	virtual void __fastcall AssignConnectOptions(Dbaccess::TCustomDAConnection* Source);
	virtual System::Variant __fastcall GetConnectionStringParam(int ParamCode);
	virtual void __fastcall SetConnectionStringParam(int ParamCode, const System::Variant &ParamValue);
	__property TOraTrace* Trace = {read=FTrace, write=SetTrace};
	
public:
	__fastcall virtual TOraSession(System::Classes::TComponent* Owner)/* overload */;
	__fastcall virtual ~TOraSession();
	virtual void __fastcall StartTransaction()/* overload */;
	HIDESBASE void __fastcall StartTransaction(TOraIsolationLevel IsolationLevel, const System::UnicodeString RollbackSegment = System::UnicodeString(), const System::UnicodeString Name = System::UnicodeString())/* overload */;
	void __fastcall Savepoint(const System::UnicodeString Name);
	void __fastcall RollbackToSavepoint(const System::UnicodeString Name);
	HIDESBASE TOraParam* __fastcall ParamByName(System::UnicodeString Name);
	void __fastcall ClearStatementCache();
	HIDESBASE void __fastcall AssignConnect(TOraSession* Source)/* overload */;
	void __fastcall AssignSvcCtx _DEPRECATED_ATTRIBUTE0 (void * hOCISvcCtx)/* overload */;
	void __fastcall AssignSvcCtx(void * hOCISvcCtx, void * hOCIEnv)/* overload */;
	void __fastcall AssignLDA(Oracall::PCDA LDA);
	virtual Dbaccess::TCustomDADataSet* __fastcall CreateDataSet(System::Classes::TComponent* AOwner = (System::Classes::TComponent*)(0x0));
	virtual Dbaccess::TCustomDASQL* __fastcall CreateSQL();
	virtual Dbaccess::TDATransaction* __fastcall CreateTransaction();
	virtual Dbaccess::TDAMetaData* __fastcall CreateMetaData();
	HIDESBASE void __fastcall EncryptTable(const System::UnicodeString TableName, TOraEncryptor* Encryptor, const System::UnicodeString Fields);
	void __fastcall ChangePassword(System::UnicodeString NewPassword);
	void __fastcall GetSequenceNames(System::Classes::TStrings* List, bool AllSequences = false);
	void __fastcall GetPackageNames(System::Classes::TStrings* List, bool AllPackages = false);
	__property Oracall::PCDA LDA = {read=GetLDA};
	__property Oracall::TOCISvcCtx* OCISvcCtx = {read=GetOCISvcCtx, write=SetOCISvcCtx};
	__property Oracall::TOCICallStyle OCICallStyle = {read=GetOCICallStyle, write=SetOCICallStyle, nodefault};
	__property int LastError = {read=GetLastError, write=SetLastError, nodefault};
	__property System::UnicodeString OracleVersion = {read=GetOracleVersion};
	__property TOraSQL* SQL = {read=GetSQL};
	__property TOraSQL* OraSQL = {read=GetSQL};
	__property System::UnicodeString InternalName = {read=FInternalName, write=SetInternalName};
	__property TOraSession* ProxySession = {read=GetProxySession, write=SetProxySession};
	
__published:
	__property bool ThreadSafety = {read=FThreadSafety, write=SetThreadSafety, default=1};
	__property bool ConnectPrompt = {read=GetConnectPrompt, write=SetConnectPrompt, stored=false, default=1};
	__property Oraclasses::TConnectMode ConnectMode = {read=FConnectMode, write=SetConnectMode, default=0};
	__property TOraSessionOptions* Options = {read=FOptions, write=SetOptions};
	__property TOraPoolingOptions* PoolingOptions = {read=FPoolingOptions, write=SetPoolingOptions};
	__property Pooling = {default=0};
	__property DataTypeMap;
	__property Debug = {default=0};
	__property Username = {default=0};
	__property Password = {default=0};
	__property Server = {default=0};
	__property ConnectString = {default=0};
	__property AutoCommit = {default=1};
	__property Connected = {stored=IsConnectedStored, default=0};
	__property ConnectDialog;
	__property LoginPrompt = {default=1};
	__property AfterConnect;
	__property BeforeConnect;
	__property AfterDisconnect;
	__property BeforeDisconnect;
	__property OnLogin;
	__property OnError;
	__property OnConnectionLost;
	__property TConnectChangeEvent OnConnectChange = {read=FOnConnectChange, write=FOnConnectChange};
	__property Oracall::TOracleHome* Home = {read=GetHome, write=SetHome, stored=false};
	__property System::UnicodeString HomeName = {read=FHomeName, write=SetHomeName};
	__property System::UnicodeString Schema = {read=GetSchema, write=SetSchema, stored=IsSchemaStored};
	__property TFailoverEvent OnFailover = {read=FFailoverEvent, write=FFailoverEvent};
	__property TInfoMessageEvent OnInfoMessage = {read=FOnInfoMessage, write=FOnInfoMessage};
public:
	/* TCustomDAConnection.Create */ inline __fastcall TOraSession(System::Classes::TComponent* Owner, const System::UnicodeString ConnectString)/* overload */ : Dbaccess::TCustomDAConnection(Owner, ConnectString) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TSessionList : public System::Classes::TThreadList
{
	typedef System::Classes::TThreadList inherited;
	
public:
	TOraSession* operator[](int i) { return this->Items[i]; }
	
private:
	int __fastcall GetCount();
	TOraSession* __fastcall GetSession(int i);
	
public:
	void __fastcall DisconnectAll();
	__property int Count = {read=GetCount, nodefault};
	__property TOraSession* Items[int i] = {read=GetSession/*, default*/};
public:
	/* TThreadList.Create */ inline __fastcall TSessionList() : System::Classes::TThreadList() { }
	/* TThreadList.Destroy */ inline __fastcall virtual ~TSessionList() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraParamValue : public Dbaccess::TDAParamValue
{
	typedef Dbaccess::TDAParamValue inherited;
	
protected:
	virtual System::TDateTime __fastcall GetAsDateTime();
	virtual void __fastcall SetAsDateTime(const System::TDateTime Value);
	virtual Data::Sqltimst::TSQLTimeStamp __fastcall GetAsSQLTimeStamp();
	virtual void __fastcall SetAsSQLTimeStamp(const Data::Sqltimst::TSQLTimeStamp &Value);
	Oraclasses::TOraTimeStamp* __fastcall GetAsTimeStamp();
	void __fastcall SetAsTimeStamp(Oraclasses::TOraTimeStamp* Value);
	virtual Memdata::TSharedObject* __fastcall GetValueObject();
	virtual void __fastcall SetValueObject(Memdata::TSharedObject* Value);
	
public:
	__property Oraclasses::TOraTimeStamp* AsTimeStamp = {read=GetAsTimeStamp, write=SetAsTimeStamp};
	__property Data::Sqltimst::TSQLTimeStamp AsSQLTimeStamp = {read=GetAsSQLTimeStamp, write=SetAsSQLTimeStamp};
public:
	/* TDAParamValue.Create */ inline __fastcall TOraParamValue(Dbaccess::TDAParam* Param) : Dbaccess::TDAParamValue(Param) { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TOraParamValue() { }
	
};

#pragma pack(pop)

class PASCALIMPLEMENTATION TOraParam : public Dbaccess::TDAParam
{
	typedef Dbaccess::TDAParam inherited;
	
private:
	bool FTable;
	bool FHasDefault;
	bool FIsResult;
	__weak Oraclasses::TOraParamDesc* FParamDescRef;
	System::PVariant __fastcall GetVarArrayPtr();
	void __fastcall CheckItemIndex(int Index);
	bool __fastcall GetTable();
	void __fastcall SetTable(bool Value);
	int __fastcall GetTableLength();
	void __fastcall SetTableLength(int Value);
	void __fastcall ValidateObject(Oraobjects::TOraObject* Obj);
	HIDESBASE Oraclasses::TOraCursor* __fastcall GetAsCursor();
	HIDESBASE void __fastcall SetAsCursor(Oraclasses::TOraCursor* Value);
	Oraclasses::TOraLob* __fastcall GetAsOraBlob();
	void __fastcall SetAsOraBlob(Oraclasses::TOraLob* Value);
	Oraclasses::TOraLob* __fastcall GetAsOraClob();
	void __fastcall SetAsOraClob(Oraclasses::TOraLob* Value);
	Oraclasses::TOraFile* __fastcall GetAsBFile();
	void __fastcall SetAsBFile(Oraclasses::TOraFile* Value);
	HIDESBASE Oraobjects::TOraObject* __fastcall GetAsObject();
	HIDESBASE void __fastcall SetAsObject(Oraobjects::TOraObject* Value);
	Oraobjects::TOraXML* __fastcall GetAsXML();
	void __fastcall SetAsXML(Oraobjects::TOraXML* Value);
	Oraobjects::TOraAnyData* __fastcall GetAsAnyData();
	void __fastcall SetAsAnyData(Oraobjects::TOraAnyData* Value);
	Oraobjects::TOraRef* __fastcall GetAsRef();
	void __fastcall SetAsRef(Oraobjects::TOraRef* Value);
	Oraobjects::TOraArray* __fastcall GetAsArray();
	void __fastcall SetAsArray(Oraobjects::TOraArray* Value);
	Oraobjects::TOraNestTable* __fastcall GetAsTable();
	void __fastcall SetAsTable(Oraobjects::TOraNestTable* Value);
	Oraclasses::TOraTimeStamp* __fastcall GetAsTimeStamp();
	void __fastcall SetAsTimeStamp(Oraclasses::TOraTimeStamp* Value);
	Oraclasses::TOraInterval* __fastcall GetAsInterval();
	void __fastcall SetAsInterval(Oraclasses::TOraInterval* Value);
	Oraclasses::TOraNumber* __fastcall GetAsNumber();
	void __fastcall SetAsNumber(Oraclasses::TOraNumber* Value);
	System::TDateTime __fastcall GetItemAsDateTime(int Index);
	void __fastcall SetItemAsDateTime(int Index, System::TDateTime Value);
	double __fastcall GetItemAsFloat(int Index);
	void __fastcall SetItemAsFloat(int Index, double Value);
	int __fastcall GetItemAsInteger(int Index);
	void __fastcall SetItemAsInteger(int Index, int Value);
	__int64 __fastcall GetItemAsLargeInt(int Index);
	void __fastcall SetItemAsLargeInt(int Index, __int64 Value);
	System::UnicodeString __fastcall GetItemAsString(int Index);
	void __fastcall SetItemAsString(int Index, System::UnicodeString Value);
	Oraclasses::TOraTimeStamp* __fastcall GetItemAsTimeStamp(int Index);
	void __fastcall SetItemAsTimeStamp(int Index, Oraclasses::TOraTimeStamp* Value);
	Oraclasses::TOraInterval* __fastcall GetItemAsInterval(int Index);
	void __fastcall SetItemAsInterval(int Index, Oraclasses::TOraInterval* Value);
	Oraclasses::TOraNumber* __fastcall GetItemAsNumber(int Index);
	void __fastcall SetItemAsNumber(int Index, Oraclasses::TOraNumber* Value);
	Data::Sqltimst::TSQLTimeStamp __fastcall GetItemAsSQLTimeStamp(int Index);
	void __fastcall SetItemAsSQLTimeStamp(int Index, const Data::Sqltimst::TSQLTimeStamp &Value);
	System::Variant __fastcall GetItemAsVariant(int Index);
	void __fastcall SetItemAsVariant(int Index, const System::Variant &Value);
	Memdata::TSharedObject* __fastcall GetItemAsObject(int Index);
	void __fastcall SetItemAsObject(int Index, Memdata::TSharedObject* Value);
	bool __fastcall GetItemIsNull(int Index);
	void __fastcall SetItemText(int Index, System::UnicodeString Value);
	void __fastcall ReadHasDefault(System::Classes::TReader* Reader);
	void __fastcall WriteHasDefault(System::Classes::TWriter* Writer);
	void __fastcall ReadIsResult(System::Classes::TReader* Reader);
	void __fastcall WriteIsResult(System::Classes::TWriter* Writer);
	TOraSession* __fastcall GetSession();
	Oracall::TOCISvcCtx* __fastcall GetOCISvcCtx();
	
protected:
	virtual void __fastcall DefineProperties(System::Classes::TFiler* Filer);
	virtual bool __fastcall IsSharedObjectDataType(Data::Db::TFieldType DataType)/* overload */;
	virtual bool __fastcall IsBlobDataType(Data::Db::TFieldType DataType);
	bool __fastcall IsObjectDataType(Data::Db::TFieldType DataType);
	bool __fastcall IsObject();
	virtual bool __fastcall IsArray();
	virtual void __fastcall SetDataType(Data::Db::TFieldType Value);
	virtual int __fastcall GetSize();
	virtual System::UnicodeString __fastcall GetAsString();
	virtual void __fastcall SetAsString(const System::UnicodeString Value);
	virtual int __fastcall GetAsInteger();
	virtual void __fastcall SetAsInteger(int Value);
	virtual void __fastcall SetAsSmallInt(int Value);
	virtual void __fastcall SetAsWord(int Value);
	virtual double __fastcall GetAsFloat();
	virtual void __fastcall SetAsFloat(double Value);
	virtual __int64 __fastcall GetAsLargeInt();
	virtual void __fastcall SetAsLargeInt(const __int64 Value);
	virtual void __fastcall SetAsShortInt(int Value);
	virtual void __fastcall SetAsByte(int Value);
	virtual System::Variant __fastcall GetAsVariant();
	virtual void __fastcall SetAsVariant(const System::Variant &Value);
	virtual Data::Sqltimst::TSQLTimeStamp __fastcall GetAsSQLTimeStamp();
	virtual void __fastcall SetAsSQLTimeStamp(const Data::Sqltimst::TSQLTimeStamp &Value);
	virtual bool __fastcall GetIsNull();
	virtual void __fastcall SetNational(bool Value);
	Oraclasses::TOraLob* __fastcall CreateLob();
	virtual bool __fastcall NeedBlobUnicode();
	virtual Memdata::TSharedObject* __fastcall CreateObject();
	virtual void __fastcall FreeValues();
	virtual int __fastcall GetValueCount();
	virtual void __fastcall SetValueCount(int Value);
	virtual Dbaccess::TDAParamValueClass __fastcall GetParamValueClass();
	
public:
	__fastcall virtual TOraParam(System::Classes::TCollection* Collection)/* overload */;
	virtual void __fastcall Clear();
	void __fastcall ItemClear(int Index);
	virtual void __fastcall Assign(System::Classes::TPersistent* Source);
	virtual void __fastcall AssignFieldValue(Data::Db::TField* Field, const System::Variant &Value);
	HIDESBASE void __fastcall SetBlobData(void * Buffer, int Size);
	__property Oraclasses::TOraCursor* AsCursor = {read=GetAsCursor, write=SetAsCursor};
	__property Oraclasses::TOraLob* AsOraBlob = {read=GetAsOraBlob, write=SetAsOraBlob};
	__property Oraclasses::TOraLob* AsOraClob = {read=GetAsOraClob, write=SetAsOraClob};
	__property Oraclasses::TOraFile* AsBFile = {read=GetAsBFile, write=SetAsBFile};
	__property Oraobjects::TOraObject* AsObject = {read=GetAsObject, write=SetAsObject};
	__property Oraobjects::TOraXML* AsXML = {read=GetAsXML, write=SetAsXML};
	__property Oraobjects::TOraAnyData* AsAnyData = {read=GetAsAnyData, write=SetAsAnyData};
	__property Oraobjects::TOraRef* AsRef = {read=GetAsRef, write=SetAsRef};
	__property Oraobjects::TOraArray* AsArray = {read=GetAsArray, write=SetAsArray};
	__property Oraobjects::TOraNestTable* AsTable = {read=GetAsTable, write=SetAsTable};
	__property Oraclasses::TOraTimeStamp* AsTimeStamp = {read=GetAsTimeStamp, write=SetAsTimeStamp};
	__property Oraclasses::TOraInterval* AsInterval = {read=GetAsInterval, write=SetAsInterval};
	__property Oraclasses::TOraNumber* AsNumber = {read=GetAsNumber, write=SetAsNumber};
	__property System::TDateTime ItemAsDateTime[int Index] = {read=GetItemAsDateTime, write=SetItemAsDateTime};
	__property double ItemAsFloat[int Index] = {read=GetItemAsFloat, write=SetItemAsFloat};
	__property int ItemAsInteger[int Index] = {read=GetItemAsInteger, write=SetItemAsInteger};
	__property __int64 ItemAsLargeInt[int Index] = {read=GetItemAsLargeInt, write=SetItemAsLargeInt};
	__property System::UnicodeString ItemAsString[int Index] = {read=GetItemAsString, write=SetItemAsString};
	__property System::Variant ItemValue[int Index] = {read=GetItemAsVariant, write=SetItemAsVariant};
	__property bool ItemIsNull[int Index] = {read=GetItemIsNull};
	__property System::UnicodeString ItemText[int Index] = {read=GetItemAsString, write=SetItemText};
	__property Oraclasses::TOraTimeStamp* ItemAsTimeStamp[int Index] = {read=GetItemAsTimeStamp, write=SetItemAsTimeStamp};
	__property Oraclasses::TOraInterval* ItemAsInterval[int Index] = {read=GetItemAsInterval, write=SetItemAsInterval};
	__property Oraclasses::TOraNumber* ItemAsNumber[int Index] = {read=GetItemAsNumber, write=SetItemAsNumber};
	__property Data::Sqltimst::TSQLTimeStamp ItemAsSQLTimeStamp[int Index] = {read=GetItemAsSQLTimeStamp, write=SetItemAsSQLTimeStamp};
	__property Oraclasses::TOraLob* AsBLOBLocator = {read=GetAsOraBlob, write=SetAsOraBlob};
	__property Oraclasses::TOraLob* AsCLOBLocator = {read=GetAsOraClob, write=SetAsOraClob};
	
__published:
	__property National = {default=0};
	__property bool Table = {read=GetTable, write=SetTable, default=0};
	__property int Length = {read=GetTableLength, write=SetTableLength, default=1};
public:
	/* TDAParam.Destroy */ inline __fastcall virtual ~TOraParam() { }
	
public:
	/* TParam.Create */ inline __fastcall TOraParam(Data::Db::TParams* AParams, Data::Db::TParamType AParamType)/* overload */ : Dbaccess::TDAParam(AParams, AParamType) { }
	
};


#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraParams : public Dbaccess::TDAParams
{
	typedef Dbaccess::TDAParams inherited;
	
public:
	TOraParam* operator[](int Index) { return this->Items[Index]; }
	
private:
	HIDESBASE void __fastcall ReadBinaryData(System::Classes::TStream* Stream);
	HIDESBASE TOraParam* __fastcall GetItem(int Index);
	HIDESBASE void __fastcall SetItem(int Index, TOraParam* Value);
	
protected:
	virtual Dbaccess::TCustomDAConnection* __fastcall GetConnection();
	virtual void __fastcall DefineProperties(System::Classes::TFiler* Filer);
	
public:
	__fastcall TOraParams(System::Classes::TPersistent* Owner);
	HIDESBASE TOraParam* __fastcall ParamByName(const System::UnicodeString Value);
	HIDESBASE TOraParam* __fastcall FindParam(const System::UnicodeString Value);
	__property TOraParam* Items[int Index] = {read=GetItem, write=SetItem/*, default*/};
public:
	/* TCollection.Destroy */ inline __fastcall virtual ~TOraParams() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TCursorField : public Dbaccess::TDACursorField
{
	typedef Dbaccess::TDACursorField inherited;
	
private:
	HIDESBASE Oraclasses::TOraCursor* __fastcall GetAsCursor();
	
public:
	__property Oraclasses::TOraCursor* AsCursor = {read=GetAsCursor};
public:
	/* TDACursorField.Create */ inline __fastcall virtual TCursorField(System::Classes::TComponent* Owner) : Dbaccess::TDACursorField(Owner) { }
	
public:
	/* TField.Destroy */ inline __fastcall virtual ~TCursorField() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TBFileField : public Data::Db::TBlobField
{
	typedef Data::Db::TBlobField inherited;
	
private:
	bool FAutoRefresh;
	Oraclasses::TOraFile* __fastcall GetAsFile();
	HIDESBASE Data::Db::TBlobType __fastcall GetBlobType();
	HIDESBASE void __fastcall SetBlobType(Data::Db::TBlobType Value);
	System::UnicodeString __fastcall GetFileDir();
	void __fastcall SetFileDir(const System::UnicodeString Value);
	System::UnicodeString __fastcall GetFileName();
	void __fastcall SetFileName(const System::UnicodeString Value);
	bool __fastcall GetExists();
	
protected:
	virtual bool __fastcall GetCanModify();
	virtual System::UnicodeString __fastcall GetClassDesc();
	bool __fastcall GetValue(Oraclasses::TOraFile* &Value);
	virtual bool __fastcall GetIsNull();
	
public:
	__fastcall virtual TBFileField(System::Classes::TComponent* Owner);
	void __fastcall Refresh();
	__property System::UnicodeString FileDir = {read=GetFileDir, write=SetFileDir};
	__property System::UnicodeString FileName = {read=GetFileName, write=SetFileName};
	__property bool Exists = {read=GetExists, nodefault};
	__property Oraclasses::TOraFile* AsFile = {read=GetAsFile};
	
__published:
	__property Data::Db::TBlobType BlobType = {read=GetBlobType, write=SetBlobType, nodefault};
	__property bool AutoRefresh = {read=FAutoRefresh, write=FAutoRefresh, default=1};
public:
	/* TField.Destroy */ inline __fastcall virtual ~TBFileField() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraDataSetField : public Data::Db::TDataSetField
{
	typedef Data::Db::TDataSetField inherited;
	
private:
	bool FModified;
	
protected:
	virtual void __fastcall FreeBuffers();
	virtual System::UnicodeString __fastcall GetAsString();
	
public:
	__property bool Modified = {read=FModified, write=FModified, nodefault};
public:
	/* TDataSetField.Create */ inline __fastcall virtual TOraDataSetField(System::Classes::TComponent* AOwner) : Data::Db::TDataSetField(AOwner) { }
	/* TDataSetField.Destroy */ inline __fastcall virtual ~TOraDataSetField() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraReferenceField : public Data::Db::TReferenceField
{
	typedef Data::Db::TReferenceField inherited;
	
private:
	bool FModified;
	
protected:
	virtual void __fastcall FreeBuffers();
	virtual System::UnicodeString __fastcall GetAsString();
	
public:
	__property bool Modified = {read=FModified, write=FModified, nodefault};
public:
	/* TReferenceField.Create */ inline __fastcall virtual TOraReferenceField(System::Classes::TComponent* AOwner) : Data::Db::TReferenceField(AOwner) { }
	
public:
	/* TDataSetField.Destroy */ inline __fastcall virtual ~TOraReferenceField() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TLabelField : public Data::Db::TField
{
	typedef Data::Db::TField inherited;
	
protected:
	virtual System::Variant __fastcall GetAsVariant();
	
public:
	__fastcall virtual TLabelField(System::Classes::TComponent* Owner);
public:
	/* TField.Destroy */ inline __fastcall virtual ~TLabelField() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraTimeStampField : public Data::Db::TField
{
	typedef Data::Db::TField inherited;
	
private:
	System::UnicodeString FFormat;
	
protected:
	virtual System::TDateTime __fastcall GetAsDateTime();
	virtual void __fastcall SetAsDateTime(System::TDateTime Value);
	virtual System::UnicodeString __fastcall GetAsString();
	virtual void __fastcall SetAsString(const System::UnicodeString Value);
	virtual System::Variant __fastcall GetAsVariant();
	virtual void __fastcall SetVarValue(const System::Variant &Value);
	virtual Data::Sqltimst::TSQLTimeStamp __fastcall GetAsSQLTimeStamp();
	virtual void __fastcall SetAsSQLTimeStamp(const Data::Sqltimst::TSQLTimeStamp &Value);
	Oraclasses::TOraTimeStamp* __fastcall GetAsTimeStamp();
	virtual void __fastcall DefineProperties(System::Classes::TFiler* Filer);
	void __fastcall ReadDataType(System::Classes::TReader* Reader);
	void __fastcall WriteDataType(System::Classes::TWriter* Writer);
	virtual bool __fastcall GetIsNull();
	
public:
	__fastcall virtual TOraTimeStampField(System::Classes::TComponent* Owner);
	__property Oraclasses::TOraTimeStamp* AsTimeStamp = {read=GetAsTimeStamp};
	virtual void __fastcall SetFieldType(Data::Db::TFieldType Value);
	
__published:
	__property System::UnicodeString Format = {read=FFormat, write=FFormat};
public:
	/* TField.Destroy */ inline __fastcall virtual ~TOraTimeStampField() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraIntervalField : public Data::Db::TField
{
	typedef Data::Db::TField inherited;
	
private:
	int FLeadPrecision;
	int FFracPrecision;
	
protected:
	virtual void __fastcall DefineProperties(System::Classes::TFiler* Filer);
	void __fastcall ReadDataType(System::Classes::TReader* Reader);
	void __fastcall WriteDataType(System::Classes::TWriter* Writer);
	virtual void __fastcall SetSize(int Value);
	virtual System::UnicodeString __fastcall GetAsString();
	virtual void __fastcall SetAsString(const System::UnicodeString Value);
	virtual System::Variant __fastcall GetAsVariant();
	virtual void __fastcall SetVarValue(const System::Variant &Value);
	Oraclasses::TOraInterval* __fastcall GetAsInterval();
	virtual bool __fastcall GetIsNull();
	
public:
	__fastcall virtual TOraIntervalField(System::Classes::TComponent* Owner);
	__classmethod virtual void __fastcall CheckTypeSize(int Value);
	virtual void __fastcall SetFieldType(Data::Db::TFieldType Value);
	__property Oraclasses::TOraInterval* AsInterval = {read=GetAsInterval};
	
__published:
	__property int LeadPrecision = {read=FLeadPrecision, write=FLeadPrecision, default=2};
	__property int FracPrecision = {read=FFracPrecision, write=FFracPrecision, default=6};
public:
	/* TField.Destroy */ inline __fastcall virtual ~TOraIntervalField() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraNumberField : public Data::Db::TNumericField
{
	typedef Data::Db::TNumericField inherited;
	
private:
	Oraclasses::TOraNumber* __fastcall GetAsNumber();
	
protected:
	virtual System::Variant __fastcall GetAsVariant();
	virtual void __fastcall SetVarValue(const System::Variant &Value);
	virtual System::UnicodeString __fastcall GetAsString();
	virtual void __fastcall SetAsString(const System::UnicodeString Value);
	virtual int __fastcall GetAsInteger();
	virtual void __fastcall SetAsInteger(int Value);
	virtual __int64 __fastcall GetAsLargeInt();
	virtual void __fastcall SetAsLargeInt(__int64 Value);
	virtual double __fastcall GetAsFloat();
	virtual void __fastcall SetAsFloat(double Value);
	virtual void __fastcall DefineProperties(System::Classes::TFiler* Filer);
	void __fastcall ReadDataType(System::Classes::TReader* Reader);
	void __fastcall WriteDataType(System::Classes::TWriter* Writer);
	virtual bool __fastcall GetIsNull();
	
public:
	__fastcall virtual TOraNumberField(System::Classes::TComponent* Owner);
	virtual bool __fastcall IsValidChar(System::WideChar InputChar);
	__property Oraclasses::TOraNumber* AsNumber = {read=GetAsNumber};
	__property __int64 AsLargeInt = {read=GetAsLargeInt, write=SetAsLargeInt};
public:
	/* TField.Destroy */ inline __fastcall virtual ~TOraNumberField() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraXMLField : public Data::Db::TField
{
	typedef Data::Db::TField inherited;
	
private:
	Oraobjects::TOraXML* __fastcall GetAsXML();
	
protected:
	virtual System::UnicodeString __fastcall GetAsString();
	virtual void __fastcall SetAsString(const System::UnicodeString Value);
	virtual System::UnicodeString __fastcall GetAsWideString();
	virtual void __fastcall SetAsWideString(const System::UnicodeString Value);
	virtual System::Variant __fastcall GetAsVariant();
	virtual void __fastcall SetVarValue(const System::Variant &Value);
	virtual void __fastcall GetText(System::UnicodeString &Text, bool DisplayText);
	virtual void __fastcall SetText(const System::UnicodeString Value);
	virtual void __fastcall DefineProperties(System::Classes::TFiler* Filer);
	void __fastcall ReadDataType(System::Classes::TReader* Reader);
	void __fastcall WriteDataType(System::Classes::TWriter* Writer);
	virtual bool __fastcall GetIsNull();
	virtual System::UnicodeString __fastcall GetClassDesc();
	
public:
	__fastcall virtual TOraXMLField(System::Classes::TComponent* Owner);
	__classmethod virtual bool __fastcall IsBlob();
	virtual void __fastcall Clear();
	virtual void __fastcall SetFieldType(Data::Db::TFieldType Value);
	__property Oraobjects::TOraXML* AsXML = {read=GetAsXML};
public:
	/* TField.Destroy */ inline __fastcall virtual ~TOraXMLField() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraAnyDataField : public Data::Db::TField
{
	typedef Data::Db::TField inherited;
	
protected:
	virtual System::UnicodeString __fastcall GetAsString();
	virtual void __fastcall SetAsString(const System::UnicodeString Value);
	virtual System::UnicodeString __fastcall GetAsWideString();
	virtual void __fastcall SetAsWideString(const System::UnicodeString Value);
	virtual System::Variant __fastcall GetAsVariant();
	virtual void __fastcall SetVarValue(const System::Variant &Value);
	Oraobjects::TOraAnyData* __fastcall GetAsAnyData();
	virtual void __fastcall GetText(System::UnicodeString &Text, bool DisplayText);
	virtual void __fastcall SetText(const System::UnicodeString Value);
	virtual void __fastcall DefineProperties(System::Classes::TFiler* Filer);
	void __fastcall ReadDataType(System::Classes::TReader* Reader);
	void __fastcall WriteDataType(System::Classes::TWriter* Writer);
	virtual bool __fastcall GetIsNull();
	virtual System::UnicodeString __fastcall GetClassDesc();
	
public:
	__fastcall virtual TOraAnyDataField(System::Classes::TComponent* Owner);
	virtual void __fastcall Clear();
	__classmethod virtual bool __fastcall IsBlob();
	virtual void __fastcall SetFieldType(Data::Db::TFieldType Value);
	__property Oraobjects::TOraAnyData* AsAnyData = {read=GetAsAnyData};
public:
	/* TField.Destroy */ inline __fastcall virtual ~TOraAnyDataField() { }
	
};

#pragma pack(pop)

enum DECLSPEC_DENUM TLockMode : unsigned char { lmNone, lmLockImmediate, lmLockDelayed };

enum DECLSPEC_DENUM TRefreshMode : unsigned char { rmNone, rmAfterInsert, rmAfterUpdate, rmAlways };

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraEncryption : public Dbaccess::TDAEncryption
{
	typedef Dbaccess::TDAEncryption inherited;
	
private:
	TOraEncryptor* __fastcall GetEncryptor();
	HIDESBASE void __fastcall SetEncryptor(TOraEncryptor* Value);
	
__published:
	__property TOraEncryptor* Encryptor = {read=GetEncryptor, write=SetEncryptor};
public:
	/* TDAEncryption.Create */ inline __fastcall TOraEncryption(Dbaccess::TCustomDADataSet* Owner) : Dbaccess::TDAEncryption(Owner) { }
	
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TOraEncryption() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TCustomOraComponent : public System::Classes::TComponent
{
	typedef System::Classes::TComponent inherited;
	
protected:
	TOraSession* FSession;
public:
	/* TComponent.Create */ inline __fastcall virtual TCustomOraComponent(System::Classes::TComponent* AOwner) : System::Classes::TComponent(AOwner) { }
	/* TComponent.Destroy */ inline __fastcall virtual ~TCustomOraComponent() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraDataSetOptionsDS : public Dbaccess::TDADataSetOptions
{
	typedef Dbaccess::TDADataSetOptions inherited;
	
private:
	bool FAutoClose;
	bool FFieldsAsString;
	bool FDeferredLobRead;
	bool FCacheLobs;
	bool FScrollableCursor;
	bool FRawAsString;
	bool FHideRowId;
	bool __fastcall GetKeepPrepared();
	void __fastcall SetKeepPrepared(bool Value);
	void __fastcall SetAutoClose(bool Value);
	void __fastcall SetFieldsAsString(bool Value);
	void __fastcall SetDeferredLobRead(bool Value);
	void __fastcall SetCacheLobs(bool Value);
	void __fastcall SetScrollableCursor(bool Value);
	void __fastcall SetRawAsString(bool Value);
	void __fastcall SetHideRowId(bool Value);
	
protected:
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	
public:
	__fastcall TOraDataSetOptionsDS(Dbaccess::TCustomDADataSet* Owner);
	
__published:
	__property bool KeepPrepared = {read=GetKeepPrepared, write=SetKeepPrepared, stored=false, nodefault};
	__property bool AutoClose = {read=FAutoClose, write=SetAutoClose, stored=false, default=0};
	__property bool FieldsAsString = {read=FFieldsAsString, write=SetFieldsAsString, stored=false, default=0};
	__property bool DeferredLobRead = {read=FDeferredLobRead, write=SetDeferredLobRead, stored=false, default=0};
	__property bool CacheLobs = {read=FCacheLobs, write=SetCacheLobs, stored=false, default=1};
	__property bool ScrollableCursor = {read=FScrollableCursor, write=SetScrollableCursor, stored=false, default=0};
	__property bool RawAsString = {read=FRawAsString, write=SetRawAsString, stored=false, default=0};
	__property bool HideRowId = {read=FHideRowId, write=SetHideRowId, default=1};
	__property FieldOrigins = {stored=false, default=0};
	__property DefaultValues = {stored=false, default=0};
	__property RequiredFields = {stored=false, default=1};
	__property StrictUpdate = {stored=false, default=1};
	__property NumberRange = {stored=false, default=0};
	__property QueryRecCount = {stored=false, default=0};
	__property AutoPrepare = {stored=false, default=0};
	__property ReturnParams = {stored=false, default=0};
	__property TrimFixedChar = {stored=false, default=1};
	__property LongStrings = {stored=false, default=1};
	__property RemoveOnRefresh = {stored=false, default=1};
	__property FlatBuffers = {stored=false, default=0};
	__property DetailDelay = {stored=false, default=0};
	__property MasterFieldsNullable = {default=0};
	__property InsertAllSetFields = {default=0};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TOraDataSetOptionsDS() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraDataSetOptions : public TOraDataSetOptionsDS
{
	typedef TOraDataSetOptionsDS inherited;
	
private:
	int FPrefetchLobSize;
	bool FTemporaryLobUpdate;
	bool FReflectChangeNotify;
	bool FStatementCache;
	int FPrefetchRows;
	bool FProcNamedParams;
	void __fastcall SetPrefetchLobSize(int value);
	void __fastcall SetTemporaryLobUpdate(bool Value);
	void __fastcall SetStatementCache(bool Value);
	void __fastcall SetPrefetchRows(int Value);
	void __fastcall SetProcNamedParams(bool Value);
	
protected:
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	
public:
	__fastcall TOraDataSetOptions(Dbaccess::TCustomDADataSet* Owner);
	
__published:
	__property AutoClose = {stored=true, default=0};
	__property FieldOrigins = {stored=true, default=0};
	__property DefaultValues = {stored=true, default=0};
	__property FieldsAsString = {stored=true, default=0};
	__property DeferredLobRead = {stored=true, default=0};
	__property CacheLobs = {stored=true, default=1};
	__property ScrollableCursor = {stored=true, default=0};
	__property RawAsString = {stored=true, default=0};
	__property RequiredFields = {stored=true, default=1};
	__property StrictUpdate = {stored=true, default=1};
	__property NumberRange = {stored=true, default=0};
	__property QueryRecCount = {stored=true, default=0};
	__property AutoPrepare = {stored=true, default=0};
	__property ReturnParams = {stored=true, default=0};
	__property TrimFixedChar = {stored=true, default=1};
	__property LongStrings = {stored=true, default=1};
	__property RemoveOnRefresh = {stored=true, default=1};
	__property FlatBuffers = {stored=true, default=0};
	__property DetailDelay = {stored=true, default=0};
	__property int PrefetchLobSize = {read=FPrefetchLobSize, write=SetPrefetchLobSize, default=0};
	__property bool TemporaryLobUpdate = {read=FTemporaryLobUpdate, write=SetTemporaryLobUpdate, default=0};
	__property bool ReflectChangeNotify = {read=FReflectChangeNotify, write=FReflectChangeNotify, default=0};
	__property bool StatementCache = {read=FStatementCache, write=SetStatementCache, default=0};
	__property int PrefetchRows = {read=FPrefetchRows, write=SetPrefetchRows, default=1};
	__property bool ProcNamedParams = {read=FProcNamedParams, write=SetProcNamedParams, default=0};
	__property SetFieldsReadOnly = {default=0};
	__property LocalMasterDetail = {default=0};
	__property CacheCalcFields = {default=0};
	__property FullRefresh = {default=0};
	__property QuoteNames = {default=0};
	__property CompressBlobMode = {default=0};
	__property UpdateBatchSize = {default=1};
	__property UpdateAllFields = {default=0};
	__property PrepareUpdateSQL = {default=0};
	__property ExtendedFieldsInfo = {default=0};
	__property EnableBCD = {default=0};
	__property EnableFMTBCD = {default=0};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TOraDataSetOptions() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraSQLGenerator : public Oraservices::TCustomDBOraSQLGenerator
{
	typedef Oraservices::TCustomDBOraSQLGenerator inherited;
	
protected:
	virtual bool __fastcall FieldModified(Craccess::TCRFieldDesc* FieldDesc)/* overload */;
public:
	/* TDASQLGenerator.Create */ inline __fastcall virtual TOraSQLGenerator(Dasqlgenerator::TSQLGeneratorServiceClass ServiceClass) : Oraservices::TCustomDBOraSQLGenerator(ServiceClass) { }
	/* TDASQLGenerator.Destroy */ inline __fastcall virtual ~TOraSQLGenerator() { }
	
	/* Hoisted overloads: */
	
protected:
	inline bool __fastcall  FieldModified(Craccess::TCRFieldDesc* FieldDesc, Memdata::TData* Data, void * OldRecBuf, void * NewRecBuf){ return Dasqlgenerator::TDASQLGenerator::FieldModified(FieldDesc, Data, OldRecBuf, NewRecBuf); }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraDataSetUpdater : public Oraservices::TCustomOraDataSetUpdater
{
	typedef Oraservices::TCustomOraDataSetUpdater inherited;
	
protected:
	virtual void __fastcall SetUpdateQueryOptions(const Dbaccess::TStatementType StatementType);
	virtual bool __fastcall PerformRefreshRecord();
	virtual void __fastcall SetSavepoint(System::UnicodeString SavepointName, bool CachedUpdates);
	virtual void __fastcall RollbackToSavepoint(System::UnicodeString SavepointName, bool CachedUpdates);
public:
	/* TDADataSetUpdater.Create */ inline __fastcall virtual TOraDataSetUpdater(Memds::TDataSetService* AOwner) : Oraservices::TCustomOraDataSetUpdater(AOwner) { }
	/* TDADataSetUpdater.Destroy */ inline __fastcall virtual ~TOraDataSetUpdater() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraDataSetService : public Oraservices::TCustomOraDataSetService
{
	typedef Oraservices::TCustomOraDataSetService inherited;
	
protected:
	virtual void __fastcall CreateDataSetUpdater();
	virtual void __fastcall CreateSQLGenerator();
	virtual bool __fastcall CompatibilityMode();
	virtual Data::Db::TFieldClass __fastcall GetFieldClass(Data::Db::TFieldType FieldType, System::Word DataType);
public:
	/* TDADataSetService.Create */ inline __fastcall virtual TOraDataSetService(Memds::TMemDataSet* AOwner) : Oraservices::TCustomOraDataSetService(AOwner) { }
	/* TDADataSetService.Destroy */ inline __fastcall virtual ~TOraDataSetService() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraDataSet : public Dbaccess::TCustomDADataSet
{
	typedef Dbaccess::TCustomDADataSet inherited;
	
private:
	int FCommandTimeout;
	TSequenceMode FSequenceMode;
	System::UnicodeString FKeySequence;
	TOraEncryption* __fastcall GetEncryption();
	HIDESBASE void __fastcall SetEncryption(TOraEncryption* Value);
	void __fastcall SetCommandTimeout(int Value);
	TOraSession* __fastcall GetSession();
	void __fastcall SetSession(TOraSession* Value);
	HIDESBASE TOraParams* __fastcall GetParams();
	HIDESBASE void __fastcall SetParams(TOraParams* Value);
	int __fastcall GetRowsProcessed();
	bool __fastcall GetIsPLSQL();
	int __fastcall GetSQLType();
	HIDESBASE Oraclasses::TOraCursor* __fastcall GetCursor();
	TOraDataSetOptions* __fastcall GetOptions();
	HIDESBASE void __fastcall SetOptions(TOraDataSetOptions* Value);
	void __fastcall SetCursor(Oraclasses::TOraCursor* Value);
	void __fastcall SetKeySequence(const System::UnicodeString Value);
	void __fastcall SetSequenceMode(TSequenceMode Value);
	TLockMode __fastcall GetLockMode();
	void __fastcall SetLockMode(TLockMode Value);
	bool __fastcall GetStrictUpdate();
	void __fastcall SetStrictUpdate(bool Value);
	bool __fastcall GetReturnParams();
	void __fastcall SetReturnParams(bool Value);
	TRefreshMode __fastcall GetRefreshMode();
	void __fastcall SetRefreshMode(TRefreshMode Value);
	TOraDataSetOptionsDS* __fastcall GetOptionsDS();
	void __fastcall SetOptionsDS(TOraDataSetOptionsDS* Value);
	
protected:
	virtual Data::Db::TIndexDef* __fastcall PSGetDefaultOrder();
	Oraclasses::TOCIRecordSet* FIRecordSet;
	Oraclasses::TOCICommand* FICommand;
	TOraDataSetService* FDataSetService;
	virtual void __fastcall CreateIRecordSet();
	virtual void __fastcall SetIRecordSet(Memdata::TData* Value);
	virtual Memds::TDataSetServiceClass __fastcall GetDataSetServiceClass();
	virtual void __fastcall SetDataSetService(Memds::TDataSetService* Value);
	virtual void __fastcall CreateCommand();
	virtual Dbaccess::TDADataSetOptions* __fastcall CreateOptions();
	virtual Dbaccess::TDAEncryption* __fastcall CreateEncryption();
	virtual Dbaccess::TCustomDAConnection* __fastcall UsedConnection();
	virtual void __fastcall CheckInactive();
	virtual void __fastcall OpenCursor(bool InfoQuery);
	virtual void __fastcall CloseCursor();
	virtual void __fastcall InternalExecute(int Iters, int Offset);
	virtual void __fastcall CheckFieldCompatibility(Data::Db::TField* Field, Data::Db::TFieldDef* FieldDef);
	virtual void __fastcall DoAfterOpen();
	virtual void __fastcall DoAfterScroll();
	virtual bool __fastcall NeedComplexUpdateFieldDefList();
	virtual Data::Db::TFieldType __fastcall GetFieldType(Memdata::TFieldDesc* FieldDesc, /* out */ int &FieldSize, /* out */ int &FieldLength, /* out */ int &FieldScale)/* overload */;
	virtual int __fastcall GetMaxFieldCount();
	virtual void __fastcall InitRecord(System::PByte Buffer)/* overload */;
	virtual void __fastcall AssignFieldValue(Dbaccess::TDAParam* Param, Data::Db::TField* Field, bool Old)/* overload */;
	virtual void __fastcall AssignFieldValue(Dbaccess::TDAParam* Param, Memdata::TFieldDesc* FieldDesc, bool Old)/* overload */;
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	virtual bool __fastcall NeedDetailRefresh(Dbaccess::TDAParam* Param, Memdata::TSharedObject* FieldValue);
	virtual void __fastcall CopyFieldValue(const System::Variant &Value, /* out */ void * &ValuePtr, /* out */ System::Word &ValueType, System::Word FieldType, bool UseFieldType = true);
	virtual void __fastcall InternalOpen();
	virtual Crparser::TSQLParserClass __fastcall GetParserClass();
	HIDESBASE void __fastcall SetModified(bool Value);
	virtual void __fastcall SetNonBlocking(bool Value);
	HIDESBASE bool __fastcall GetActiveRecBuf(System::PByte &RecBuf);
	Memdata::TData* __fastcall GetData();
	TOraUpdateSQL* __fastcall GetUpdateObject();
	HIDESBASE void __fastcall SetUpdateObject(TOraUpdateSQL* Value);
	
public:
	__fastcall virtual TOraDataSet(System::Classes::TComponent* Owner);
	__fastcall virtual ~TOraDataSet();
	HIDESBASE void __fastcall ExecSQL();
	bool __fastcall OpenNext();
	int __fastcall ErrorOffset();
	void __fastcall GetErrorPos(int &Row, int &Col);
	void __fastcall CreateProcCall(System::UnicodeString Name, int Overload = 0x0);
	System::UnicodeString __fastcall GetKeyList(System::UnicodeString TableName, System::Classes::TStrings* List);
	HIDESBASE TOraParam* __fastcall FindParam(const System::UnicodeString Value);
	HIDESBASE TOraParam* __fastcall ParamByName(const System::UnicodeString Value);
	virtual System::Classes::TStream* __fastcall CreateBlobStream(Data::Db::TField* Field, Data::Db::TBlobStreamMode Mode);
	Oraclasses::TOraLob* __fastcall GetLob(const System::UnicodeString FieldName);
	Oraclasses::TOraFile* __fastcall GetFile(const System::UnicodeString FieldName);
	Oraobjects::TOraObject* __fastcall GetObject(const System::UnicodeString FieldName);
	Oraobjects::TOraRef* __fastcall GetRef(const System::UnicodeString FieldName);
	Oraobjects::TOraArray* __fastcall GetArray(const System::UnicodeString FieldName);
	Oraobjects::TOraNestTable* __fastcall GetTable(const System::UnicodeString FieldName);
	Oraclasses::TOraTimeStamp* __fastcall GetTimeStamp(const System::UnicodeString FieldName);
	Oraclasses::TOraInterval* __fastcall GetInterval(const System::UnicodeString FieldName);
	Oraclasses::TOraNumber* __fastcall GetNumber(const System::UnicodeString FieldName);
	Oraclasses::TOraLob* __fastcall GetLobLocator(const System::UnicodeString FieldName);
	__property TOraEncryption* Encryption = {read=GetEncryption, write=SetEncryption};
	__property SmartFetch;
	__property int CommandTimeout = {read=FCommandTimeout, write=SetCommandTimeout, default=0};
	__property TOraSession* Session = {read=GetSession, write=SetSession};
	__property TOraParams* Params = {read=GetParams, write=SetParams, stored=false};
	__property int RowsProcessed = {read=GetRowsProcessed, nodefault};
	__property bool IsQuery = {read=GetIsQuery, nodefault};
	__property bool IsPLSQL = {read=GetIsPLSQL, nodefault};
	__property int SQLType = {read=GetSQLType, nodefault};
	__property Oraclasses::TOraCursor* Cursor = {read=GetCursor, write=SetCursor};
	__property TLockMode LockMode = {read=GetLockMode, write=SetLockMode, nodefault};
	__property CheckMode = {default=0};
	__property TOraDataSetOptions* Options = {read=GetOptions, write=SetOptions};
	__property System::UnicodeString KeySequence = {read=FKeySequence, write=SetKeySequence};
	__property TSequenceMode SequenceMode = {read=FSequenceMode, write=SetSequenceMode, default=1};
	__property TRefreshMode RefreshMode = {read=GetRefreshMode, write=SetRefreshMode, stored=false, nodefault};
	__property bool StrictUpdate = {read=GetStrictUpdate, write=SetStrictUpdate, stored=false, nodefault};
	__property LocalConstraints = {stored=false, default=1};
	__property bool ReturnParams = {read=GetReturnParams, write=SetReturnParams, stored=false, nodefault};
	__property TOraDataSetOptionsDS* OptionsDS = {read=GetOptionsDS, write=SetOptionsDS, stored=false};
	__property TOraUpdateSQL* UpdateObject = {read=GetUpdateObject, write=SetUpdateObject};
	__property AutoCommit = {default=1};
	__property DMLRefresh = {default=0};
	__property FetchAll = {default=0};
	__property KeyFields = {default=0};
	__property NonBlocking = {default=0};
	/* Hoisted overloads: */
	
protected:
	inline Data::Db::TFieldType __fastcall  GetFieldType(System::Word DataType){ return Memds::TMemDataSet::GetFieldType(DataType); }
	inline void __fastcall  InitRecord(NativeInt Buffer){ Memds::TMemDataSet::InitRecord(Buffer); }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TBFileStream : public Memds::TBlobStream
{
	typedef Memds::TBlobStream inherited;
	
public:
	__fastcall TBFileStream(Data::Db::TBlobField* Field, Data::Db::TBlobStreamMode Mode);
	__fastcall virtual ~TBFileStream();
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraNestedTable : public Memds::TMemDataSet
{
	typedef Memds::TMemDataSet inherited;
	
private:
	Oraobjects::TOraNestTable* __fastcall GetTable();
	void __fastcall SetTable(Oraobjects::TOraNestTable* Value);
	Oraobjects::TOraRef* __fastcall GetRef();
	void __fastcall SetRef(Oraobjects::TOraRef* Value);
	
protected:
	virtual void __fastcall CreateIRecordSet();
	virtual void __fastcall OpenCursor(bool InfoQuery);
	virtual void __fastcall CloseCursor();
	virtual void __fastcall CreateFieldDefs();
	virtual void __fastcall DoAfterPost();
	virtual void __fastcall DoBeforeInsert();
	virtual void __fastcall DoBeforeDelete();
	virtual bool __fastcall GetCanModify();
	virtual void __fastcall SetDataSetField(Data::Db::TDataSetField* const Value);
	virtual void __fastcall DataEvent(Data::Db::TDataEvent Event, NativeInt Info);
	virtual Memds::TFieldTypeMapClass __fastcall GetFieldTypeMapClass();
	virtual Data::Db::TFieldClass __fastcall GetFieldClass(Data::Db::TFieldType FieldType, System::Word DataType)/* overload */;
	
public:
	__fastcall virtual TOraNestedTable(System::Classes::TComponent* Owner);
	Oraobjects::TOraObject* __fastcall GetObject(const System::UnicodeString FieldName);
	__property Oraobjects::TOraNestTable* Table = {read=GetTable, write=SetTable};
	__property Oraobjects::TOraRef* Ref = {read=GetRef, write=SetRef};
	
__published:
	__property DataSetField;
	__property Active = {default=0};
	__property AutoCalcFields = {default=1};
	__property Constraints = {stored=IsConstraintsStored};
	__property Filtered = {default=0};
	__property Filter = {default=0};
	__property FilterOptions = {default=0};
	__property IndexFieldNames = {default=0};
	__property ObjectView = {default=1};
	__property BeforeOpen;
	__property AfterOpen;
	__property BeforeClose;
	__property AfterClose;
	__property BeforeInsert;
	__property AfterInsert;
	__property BeforeEdit;
	__property AfterEdit;
	__property BeforePost;
	__property AfterPost;
	__property BeforeCancel;
	__property AfterCancel;
	__property BeforeDelete;
	__property AfterDelete;
	__property BeforeScroll;
	__property AfterScroll;
	__property OnCalcFields;
	__property OnDeleteError;
	__property OnEditError;
	__property OnFilterRecord;
	__property OnNewRecord;
	__property OnPostError;
	__property AfterRefresh;
	__property BeforeRefresh;
public:
	/* TMemDataSet.Destroy */ inline __fastcall virtual ~TOraNestedTable() { }
	
	/* Hoisted overloads: */
	
protected:
	inline Data::Db::TFieldClass __fastcall  GetFieldClass(Data::Db::TFieldType FieldType){ return Memds::TMemDataSet::GetFieldClass(FieldType); }
	inline Data::Db::TFieldClass __fastcall  GetFieldClass(Data::Db::TFieldDef* FieldDef){ return Memds::TMemDataSet::GetFieldClass(FieldDef); }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TCustomOraQuery : public TOraDataSet
{
	typedef TOraDataSet inherited;
	
public:
	/* TOraDataSet.Create */ inline __fastcall virtual TCustomOraQuery(System::Classes::TComponent* Owner) : TOraDataSet(Owner) { }
	/* TOraDataSet.Destroy */ inline __fastcall virtual ~TCustomOraQuery() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraQuery : public TCustomOraQuery
{
	typedef TCustomOraQuery inherited;
	
protected:
	virtual void __fastcall DoBeforeInsert();
	virtual void __fastcall DoBeforeDelete();
	virtual void __fastcall DoBeforeEdit();
	
__published:
	__property UpdatingTable = {default=0};
	__property KeyFields = {default=0};
	__property KeySequence = {default=0};
	__property SequenceMode = {default=1};
	__property SQLInsert;
	__property SQLDelete;
	__property SQLUpdate;
	__property SQLLock;
	__property SQLRefresh;
	__property SQLRecCount;
	__property LocalUpdate = {default=0};
	__property CommandTimeout = {default=0};
	__property DataTypeMap;
	__property Encryption;
	__property SmartFetch;
	__property Session;
	__property ParamCheck = {default=1};
	__property SQL;
	__property MasterSource;
	__property MasterFields = {default=0};
	__property DetailFields = {default=0};
	__property Debug = {default=0};
	__property Macros;
	__property Params;
	__property FetchRows = {default=25};
	__property FetchAll = {default=0};
	__property NonBlocking = {default=0};
	__property ReadOnly = {default=0};
	__property UniDirectional = {default=0};
	__property CachedUpdates = {default=0};
	__property AutoCommit = {default=1};
	__property FilterSQL = {default=0};
	__property LockMode = {default=0};
	__property CheckMode = {default=0};
	__property DMLRefresh = {default=0};
	__property RefreshOptions = {default=0};
	__property Options;
	__property ReturnParams;
	__property LocalConstraints = {default=1};
	__property RefreshMode;
	__property OptionsDS;
	__property BeforeExecute;
	__property AfterExecute;
	__property BeforeUpdateExecute;
	__property AfterUpdateExecute;
	__property OnUpdateError;
	__property OnUpdateRecord;
	__property BeforeFetch;
	__property AfterFetch;
	__property UpdateObject;
	__property Active = {default=0};
	__property AutoCalcFields = {default=1};
	__property Constraints = {stored=IsConstraintsStored};
	__property Filtered = {default=0};
	__property Filter = {default=0};
	__property FilterOptions = {default=0};
	__property IndexFieldNames = {default=0};
	__property ObjectView = {default=0};
	__property BeforeOpen;
	__property AfterOpen;
	__property BeforeClose;
	__property AfterClose;
	__property BeforeInsert;
	__property AfterInsert;
	__property BeforeEdit;
	__property AfterEdit;
	__property BeforePost;
	__property AfterPost;
	__property BeforeCancel;
	__property AfterCancel;
	__property BeforeDelete;
	__property AfterDelete;
	__property BeforeScroll;
	__property AfterScroll;
	__property OnCalcFields;
	__property OnDeleteError;
	__property OnEditError;
	__property OnFilterRecord;
	__property OnNewRecord;
	__property OnPostError;
	__property AfterRefresh;
	__property BeforeRefresh;
	__property Fields;
public:
	/* TOraDataSet.Create */ inline __fastcall virtual TOraQuery(System::Classes::TComponent* Owner) : TCustomOraQuery(Owner) { }
	/* TOraDataSet.Destroy */ inline __fastcall virtual ~TOraQuery() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraStoredProc : public TCustomOraQuery
{
	typedef TCustomOraQuery inherited;
	
private:
	System::UnicodeString FStoredProcName;
	int FOverload;
	void __fastcall SetStoredProcName(System::UnicodeString Value);
	void __fastcall SetOverload(int Value);
	
protected:
	virtual void __fastcall PSSetCommandText(const System::UnicodeString CommandText);
	virtual void __fastcall DoBeforeInsert();
	virtual void __fastcall DoBeforeDelete();
	virtual void __fastcall DoBeforeEdit();
	virtual void __fastcall DoBeforeExecute();
	virtual bool __fastcall SQLAutoGenerated();
	virtual void __fastcall BeforeOpenCursor(bool InfoQuery);
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	
public:
	virtual void __fastcall Prepare();
	void __fastcall PrepareSQL();
	void __fastcall ExecProc();
	
__published:
	__property System::UnicodeString StoredProcName = {read=FStoredProcName, write=SetStoredProcName};
	__property int Overload = {read=FOverload, write=SetOverload, default=0};
	__property SQLInsert;
	__property SQLDelete;
	__property SQLUpdate;
	__property SQLLock;
	__property SQLRefresh;
	__property SQLRecCount;
	__property LocalUpdate = {default=0};
	__property CommandTimeout = {default=0};
	__property DataTypeMap;
	__property Encryption;
	__property SmartFetch;
	__property Session;
	__property ParamCheck = {stored=false, default=1};
	__property SQL;
	__property Debug = {default=0};
	__property Params;
	__property FetchRows = {default=25};
	__property FetchAll = {default=0};
	__property NonBlocking = {default=0};
	__property ReadOnly = {default=0};
	__property UniDirectional = {default=0};
	__property CachedUpdates = {default=0};
	__property AutoCommit = {default=1};
	__property LockMode = {default=0};
	__property RefreshOptions = {default=0};
	__property Options;
	__property ReturnParams;
	__property LocalConstraints = {default=1};
	__property RefreshMode;
	__property OptionsDS;
	__property BeforeExecute;
	__property AfterExecute;
	__property BeforeUpdateExecute;
	__property AfterUpdateExecute;
	__property OnUpdateError;
	__property OnUpdateRecord;
	__property UpdateObject;
	__property Active = {default=0};
	__property AutoCalcFields = {default=1};
	__property Constraints = {stored=IsConstraintsStored};
	__property Filtered = {default=0};
	__property Filter = {default=0};
	__property FilterOptions = {default=0};
	__property IndexFieldNames = {default=0};
	__property ObjectView = {default=0};
	__property BeforeOpen;
	__property AfterOpen;
	__property BeforeClose;
	__property AfterClose;
	__property BeforeInsert;
	__property AfterInsert;
	__property BeforeEdit;
	__property AfterEdit;
	__property BeforePost;
	__property AfterPost;
	__property BeforeCancel;
	__property AfterCancel;
	__property BeforeDelete;
	__property AfterDelete;
	__property BeforeScroll;
	__property AfterScroll;
	__property OnCalcFields;
	__property OnDeleteError;
	__property OnEditError;
	__property OnFilterRecord;
	__property OnNewRecord;
	__property OnPostError;
	__property AfterRefresh;
	__property BeforeRefresh;
	__property Fields;
public:
	/* TOraDataSet.Create */ inline __fastcall virtual TOraStoredProc(System::Classes::TComponent* Owner) : TCustomOraQuery(Owner) { }
	/* TOraDataSet.Destroy */ inline __fastcall virtual ~TOraStoredProc() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraSQL : public Dbaccess::TCustomDASQL
{
	typedef Dbaccess::TCustomDASQL inherited;
	
private:
	int FCommandTimeout;
	bool FStatementCache;
	bool FTemporaryLobUpdate;
	void __fastcall SetCommandTimeout(int Value);
	TOraSession* __fastcall GetSession();
	void __fastcall SetSession(TOraSession* Value);
	int __fastcall GetRowsProcessed();
	System::UnicodeString __fastcall GetText();
	void __fastcall SetText(System::UnicodeString Value);
	int __fastcall GetSQLType();
	void __fastcall SetArrayLength(int Value);
	void __fastcall SetStatementCache(bool Value);
	void __fastcall SetTemporaryLobUpdate(bool Value);
	TOraParams* __fastcall GetParams();
	HIDESBASE void __fastcall SetParams(TOraParams* Value);
	
protected:
	Oraclasses::TOCICommand* FICommand;
	virtual void __fastcall CreateICommand();
	virtual void __fastcall SetICommand(Craccess::TCRCommand* Value);
	virtual Dbaccess::TDAParams* __fastcall CreateParamsObject();
	virtual Dbaccess::TDAFieldTypeMapClass __fastcall GetFieldTypeMapClass();
	virtual void __fastcall SetNonBlocking(bool Value);
	virtual Dbaccess::TCustomDAConnection* __fastcall UsedConnection();
	virtual void __fastcall AssignParam(Craccess::TParamDesc* ParamDesc, Dbaccess::TDAParam* Param);
	virtual void __fastcall AssignParamValue(Craccess::TParamDesc* ParamDesc, Dbaccess::TDAParam* Param);
	virtual void __fastcall AssignParamDesc(Dbaccess::TDAParam* Param, Craccess::TParamDesc* ParamDesc);
	virtual Dbaccess::TDAParam* __fastcall FindResultParam();
	virtual bool __fastcall NeedRecreateProcCall();
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	virtual void __fastcall InternalExecute(int Iters, int Offset);
	
public:
	__fastcall virtual TOraSQL(System::Classes::TComponent* Owner);
	int __fastcall ErrorOffset();
	HIDESBASE TOraParam* __fastcall FindParam(const System::UnicodeString Value);
	HIDESBASE TOraParam* __fastcall ParamByName(const System::UnicodeString Value);
	void __fastcall CreateProcCall(System::UnicodeString Name, int Overload = 0x0);
	__property int RowsProcessed = {read=GetRowsProcessed, nodefault};
	__property int SQLType = {read=GetSQLType, nodefault};
	__property int ArrayLength = {write=SetArrayLength, nodefault};
	__property System::UnicodeString Text = {read=GetText, write=SetText};
	
__published:
	__property int CommandTimeout = {read=FCommandTimeout, write=SetCommandTimeout, default=0};
	__property TOraParams* Params = {read=GetParams, write=SetParams, stored=false};
	__property TOraSession* Session = {read=GetSession, write=SetSession};
	__property bool StatementCache = {read=FStatementCache, write=SetStatementCache, default=0};
	__property bool TemporaryLobUpdate = {read=FTemporaryLobUpdate, write=SetTemporaryLobUpdate, default=0};
	__property AutoCommit = {default=0};
	__property Debug = {default=0};
	__property Macros;
	__property NonBlocking = {default=0};
	__property ParamCheck = {default=1};
	__property SQL;
	__property BeforeExecute;
	__property AfterExecute;
public:
	/* TCustomDASQL.Destroy */ inline __fastcall virtual ~TOraSQL() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraUpdateSQL : public Dbaccess::TCustomDAUpdateSQL
{
	typedef Dbaccess::TCustomDAUpdateSQL inherited;
	
protected:
	virtual Dbaccess::TCustomDADataSetClass __fastcall DataSetClass();
	virtual Dbaccess::TCustomDASQLClass __fastcall SQLClass();
public:
	/* TCustomDAUpdateSQL.Create */ inline __fastcall virtual TOraUpdateSQL(System::Classes::TComponent* Owner) : Dbaccess::TCustomDAUpdateSQL(Owner) { }
	/* TCustomDAUpdateSQL.Destroy */ inline __fastcall virtual ~TOraUpdateSQL() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraMetaData : public Dbaccess::TDAMetaData
{
	typedef Dbaccess::TDAMetaData inherited;
	
private:
	TOraSession* __fastcall GetSession();
	void __fastcall SetSession(TOraSession* Value);
	
__published:
	__property Active = {default=0};
	__property Filtered = {default=0};
	__property Filter = {default=0};
	__property FilterOptions = {default=0};
	__property IndexFieldNames = {default=0};
	__property BeforeOpen;
	__property AfterOpen;
	__property BeforeClose;
	__property AfterClose;
	__property BeforeScroll;
	__property AfterScroll;
	__property OnFilterRecord;
	__property MetaDataKind = {default=0};
	__property Restrictions;
	__property TOraSession* Session = {read=GetSession, write=SetSession};
public:
	/* TDAMetaData.Create */ inline __fastcall virtual TOraMetaData(System::Classes::TComponent* AOwner) : Dbaccess::TDAMetaData(AOwner) { }
	/* TDAMetaData.Destroy */ inline __fastcall virtual ~TOraMetaData() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraDataSource : public Dbaccess::TCRDataSource
{
	typedef Dbaccess::TCRDataSource inherited;
	
public:
	/* TCRDataSource.Create */ inline __fastcall virtual TOraDataSource(System::Classes::TComponent* Owner) : Dbaccess::TCRDataSource(Owner) { }
	
public:
	/* TDataSource.Destroy */ inline __fastcall virtual ~TOraDataSource() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraFieldTypeInfos : public Dbaccess::TFieldTypeInfos
{
	typedef Dbaccess::TFieldTypeInfos inherited;
	
public:
	__fastcall TOraFieldTypeInfos();
public:
	/* TFieldTypeInfos.Destroy */ inline __fastcall virtual ~TOraFieldTypeInfos() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraFieldTypeMap : public Oraservices::TCustomOraFieldTypeMap
{
	typedef Oraservices::TCustomOraFieldTypeMap inherited;
	
public:
	__classmethod virtual Dbaccess::TFieldTypeInfos* __fastcall GetFieldTypeInfos();
	__classmethod virtual Data::Db::TFieldType __fastcall GetFieldType(System::Word DataType);
	__classmethod virtual int __fastcall GetDataType(Data::Db::TFieldType FieldType, System::Word SubDataType = (System::Word)(0x0));
public:
	/* TObject.Create */ inline __fastcall TOraFieldTypeMap() : Oraservices::TCustomOraFieldTypeMap() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TOraFieldTypeMap() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraUtils : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	__classmethod Oraclasses::TOCIConnection* __fastcall GetIConnection(TOraSession* Obj);
	__classmethod void __fastcall SetIConnection(TOraSession* Obj, Oraclasses::TOCIConnection* Value);
	__classmethod TOraSession* __fastcall UsedConnection(System::Classes::TComponent* Obj);
	__classmethod void __fastcall GetObjectList(TOraSession* Obj, System::Classes::TStrings* List, System::UnicodeString SQL);
	__classmethod System::UnicodeString __fastcall GetCachedSchema(TOraSession* Obj);
	__classmethod System::UnicodeString __fastcall FSchema(TOraSession* Obj);
	__classmethod TOraSession* __fastcall FOwner(TOraSessionOptions* Obj);
	__classmethod Oraclasses::TOCICommand* __fastcall GetICommand(TOraDataSet* Obj);
	__classmethod void __fastcall FreeICommand(TOraSQL* Obj);
	__classmethod System::UnicodeString __fastcall GetRowId(System::Classes::TComponent* Obj);
	__classmethod void __fastcall SetDesignCreate(TOraTrace* Obj, bool Value)/* overload */;
	__classmethod bool __fastcall GetDesignCreate(TOraTrace* Obj)/* overload */;
	__classmethod bool __fastcall ParseSPName(System::UnicodeString FullName, System::UnicodeString &Name, int &Overload);
	__classmethod System::UnicodeString __fastcall GetFinalSPName(System::UnicodeString Name, int Overload);
public:
	/* TObject.Create */ inline __fastcall TOraUtils() : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TOraUtils() { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
#define DACVersion u"11.1.3"
#define ODACVersion u"11.1.3"
static const Data::Db::TFieldType ftObject = (Data::Db::TFieldType)(26);
static const Data::Db::TFieldType ftTable = (Data::Db::TFieldType)(29);
static constexpr System::Int8 ftBFile = System::Int8(0x64);
static constexpr System::Int8 ftLabel = System::Int8(0x65);
static constexpr System::Int8 ftIntervalYM = System::Int8(0x67);
static constexpr System::Int8 ftIntervalDS = System::Int8(0x68);
static constexpr System::Int8 ftOraTimeStampTZ = System::Int8(0x69);
static constexpr System::Int8 ftOraTimeStampLTZ = System::Int8(0x6a);
static constexpr System::Int8 ftNumber = System::Int8(0x6b);
static constexpr System::Int8 ftXML = System::Int8(0x6c);
static constexpr System::Int8 ftAnyData = System::Int8(0x6d);
static constexpr System::Int8 DEFAULT_TRACE_FILE_SIZE = System::Int8(0x0);
static constexpr System::Int8 UNLIMITED_TRACE_FILE_SIZE = System::Int8(-1);
extern DELPHI_PACKAGE __weak TOraSession* DefSession;
extern DELPHI_PACKAGE bool UseDefSession;
extern DELPHI_PACKAGE TSessionList* Sessions;
extern DELPHI_PACKAGE System::TClass __fastcall (*DefConnectDialogClassProc)(void);
extern DELPHI_PACKAGE bool ShowRefId;
extern DELPHI_PACKAGE bool OraQueryCompatibilityMode;
extern DELPHI_PACKAGE bool UseLockInTransaction;
#define STrialMsg u"You are using ODAC Trial edition!\nThe maximum number of c"\
	u"olumns in a dataset is limited to 6 fields.\nYou may regis"\
	u"ter ODAC at http://www.devart.com"
extern DELPHI_PACKAGE int TrialShowCounter;
extern DELPHI_PACKAGE TOraSession* __fastcall DefaultSession(void);
extern DELPHI_PACKAGE TOraSession* __fastcall SessionByName(System::UnicodeString Name);
extern DELPHI_PACKAGE System::UnicodeString __fastcall AddWhere(System::UnicodeString SQL, System::UnicodeString Condition);
extern DELPHI_PACKAGE System::UnicodeString __fastcall SetWhere(System::UnicodeString SQL, System::UnicodeString Condition);
extern DELPHI_PACKAGE System::UnicodeString __fastcall DeleteWhere(System::UnicodeString SQL);
extern DELPHI_PACKAGE System::UnicodeString __fastcall SetOrderBy(System::UnicodeString SQL, System::UnicodeString Fields);
extern DELPHI_PACKAGE System::UnicodeString __fastcall GetOrderBy(System::UnicodeString SQL);
extern DELPHI_PACKAGE System::UnicodeString __fastcall SetGroupBy(System::UnicodeString SQL, System::UnicodeString Fields);
extern DELPHI_PACKAGE System::UnicodeString __fastcall SetFieldList(System::UnicodeString SQL, System::UnicodeString Fields);
extern DELPHI_PACKAGE System::UnicodeString __fastcall SetTableList(System::UnicodeString SQL, System::UnicodeString Tables);
extern DELPHI_PACKAGE System::UnicodeString __fastcall GetTableList(System::UnicodeString SQL);
extern DELPHI_PACKAGE void __fastcall RegisterFieldClasses(void);
}	/* namespace Ora */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_ORA)
using namespace Ora;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// OraHPP
