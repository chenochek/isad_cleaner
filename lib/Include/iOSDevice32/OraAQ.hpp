﻿// CodeGear C++Builder
// Copyright (c) 1995, 2018 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'OraAQ.pas' rev: 33.00 (iOS)

#ifndef OraaqHPP
#define OraaqHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.Classes.hpp>
#include <System.SysUtils.hpp>
#include <System.Types.hpp>
#include <Data.DB.hpp>
#include <System.Variants.hpp>
#include <CLRClasses.hpp>
#include <CRTypes.hpp>
#include <Ora.hpp>
#include <OraObjects.hpp>
#include <OraCall.hpp>

//-- user supplied -----------------------------------------------------------

namespace Oraaq
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TQueueAgent;
class DELPHICLASS TQueueAgents;
class DELPHICLASS TQueueMessageProperties;
class DELPHICLASS TQueueMessage;
class DELPHICLASS TEnqueueOptions;
class DELPHICLASS TDequeueOptions;
class DELPHICLASS TOraQueue;
class DELPHICLASS TOraQueueTable;
class DELPHICLASS TOraQueueAdmin;
class DELPHICLASS TOraAQUtils;
class DELPHICLASS TQueueTableList;
//-- type declarations -------------------------------------------------------
enum DECLSPEC_DENUM TQueueMessageState : unsigned char { qmsReady, qmsWaiting, qmsProcessed, qmsExpired };

enum DECLSPEC_DENUM TQueueDeliveryMode : unsigned char { qdmPersistent, qdmBuffered, qdmPersistentOrBuffered };

enum DECLSPEC_DENUM TQueueVisibility : unsigned char { qvOnCommit, qvImmediate };

enum DECLSPEC_DENUM TQueueSequenceDeviation : unsigned char { qsdNone, qsdBefore, qsdTop };

enum DECLSPEC_DENUM TDequeueMode : unsigned char { dqmBrowse, dqmLocked, dqmRemove, dqmRemoveNoData };

enum DECLSPEC_DENUM TQueueNavigation : unsigned char { qnNextMessage, qnNextTransaction, qnFirstMessage, qnFirstMessageMultiGroup, qnNextMessageMultiGroup };

typedef System::UnicodeString TMessageId;

typedef System::DynamicArray<System::UnicodeString> TMessageIds;

#pragma pack(push,4)
class PASCALIMPLEMENTATION TQueueAgent : public System::Classes::TCollectionItem
{
	typedef System::Classes::TCollectionItem inherited;
	
private:
	System::UnicodeString FName;
	System::UnicodeString FAddress;
	int FProtocol;
	
protected:
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	
public:
	__fastcall TQueueAgent()/* overload */;
	
__published:
	__property System::UnicodeString Name = {read=FName, write=FName};
	__property System::UnicodeString Address = {read=FAddress, write=FAddress};
	__property int Protocol = {read=FProtocol, write=FProtocol, default=0};
public:
	/* TCollectionItem.Destroy */ inline __fastcall virtual ~TQueueAgent() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TQueueAgents : public System::Classes::TOwnedCollection
{
	typedef System::Classes::TOwnedCollection inherited;
	
public:
	TQueueAgent* operator[](int Index) { return this->Items[Index]; }
	
private:
	HIDESBASE TQueueAgent* __fastcall GetItem(int Index);
	HIDESBASE void __fastcall SetItem(int Index, TQueueAgent* Value);
	
public:
	__fastcall TQueueAgents()/* overload */;
	__fastcall TQueueAgents(System::Classes::TPersistent* AOwner)/* overload */;
	HIDESBASE TQueueAgent* __fastcall Add();
	HIDESBASE TQueueAgent* __fastcall Insert(int Index);
	__property TQueueAgent* Items[int Index] = {read=GetItem, write=SetItem/*, default*/};
public:
	/* TCollection.Destroy */ inline __fastcall virtual ~TQueueAgents() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TQueueMessageProperties : public System::Classes::TPersistent
{
	typedef System::Classes::TPersistent inherited;
	
private:
	System::Classes::TPersistent* FOwner;
	TQueueAgents* FRecipientList;
	int FAttempts;
	System::TDateTime FEnqueueTime;
	TQueueMessageState FState;
	int FPriority;
	int FDelay;
	int FExpiration;
	System::UnicodeString FCorrelation;
	System::UnicodeString FExceptionQueue;
	TQueueAgent* FSenderId;
	System::UnicodeString FOriginalMessageId;
	System::UnicodeString FTransactionGroup;
	TQueueDeliveryMode FDeliveryMode;
	void __fastcall SetRecipientList(TQueueAgents* Value);
	bool __fastcall IsRecipientListStored();
	void __fastcall SetSenderId(TQueueAgent* Value);
	
protected:
	DYNAMIC System::Classes::TPersistent* __fastcall GetOwner();
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	
public:
	__fastcall TQueueMessageProperties()/* overload */;
	__fastcall TQueueMessageProperties(System::Classes::TPersistent* AOwner)/* overload */;
	__fastcall virtual ~TQueueMessageProperties();
	__property int Attempts = {read=FAttempts, nodefault};
	__property System::TDateTime EnqueueTime = {read=FEnqueueTime};
	__property TQueueMessageState State = {read=FState, nodefault};
	__property System::UnicodeString TransactionGroup = {read=FTransactionGroup};
	__property System::UnicodeString Correlation = {read=FCorrelation, write=FCorrelation};
	__property System::UnicodeString OriginalMessageId = {read=FOriginalMessageId, write=FOriginalMessageId};
	
__published:
	__property TQueueAgents* RecipientList = {read=FRecipientList, write=SetRecipientList, stored=IsRecipientListStored};
	__property int Priority = {read=FPriority, write=FPriority, default=1};
	__property int Delay = {read=FDelay, write=FDelay, default=0};
	__property int Expiration = {read=FExpiration, write=FExpiration, default=-1};
	__property System::UnicodeString ExceptionQueue = {read=FExceptionQueue, write=FExceptionQueue};
	__property TQueueAgent* SenderId = {read=FSenderId, write=SetSenderId};
	__property TQueueDeliveryMode DeliveryMode = {read=FDeliveryMode, write=FDeliveryMode, default=0};
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TQueueMessage : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	System::TArray__1<System::Byte> FRawPayload;
	Oraobjects::TOraObject* FObjectPayload;
	TQueueMessageProperties* FMessageProperties;
	System::UnicodeString FMessageId;
	System::TArray__1<System::Byte> __fastcall GetRawPayload();
	void __fastcall SetRawPayload(const System::TArray__1<System::Byte> Value);
	System::UnicodeString __fastcall GetStringPayload();
	void __fastcall SetStringPayload(const System::UnicodeString Value);
	Oraobjects::TOraObject* __fastcall GetObjectPayload();
	void __fastcall SetObjectPayload(Oraobjects::TOraObject* Value);
	void __fastcall SetMessageProperties(TQueueMessageProperties* Value);
	
public:
	__fastcall TQueueMessage()/* overload */;
	__fastcall TQueueMessage(const System::UnicodeString Payload, TQueueMessageProperties* MessageProperties)/* overload */;
	__fastcall TQueueMessage(const System::TArray__1<System::Byte> Payload, TQueueMessageProperties* MessageProperties)/* overload */;
	__fastcall TQueueMessage(Oraobjects::TOraObject* Payload, TQueueMessageProperties* MessageProperties)/* overload */;
	__fastcall virtual ~TQueueMessage();
	__property System::UnicodeString MessageId = {read=FMessageId};
	__property System::TArray__1<System::Byte> RawPayload = {read=GetRawPayload, write=SetRawPayload};
	__property System::UnicodeString StringPayload = {read=GetStringPayload, write=SetStringPayload};
	__property Oraobjects::TOraObject* ObjectPayload = {read=GetObjectPayload, write=SetObjectPayload};
	__property TQueueMessageProperties* MessageProperties = {read=FMessageProperties, write=SetMessageProperties};
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TEnqueueOptions : public System::Classes::TPersistent
{
	typedef System::Classes::TPersistent inherited;
	
private:
	TQueueVisibility FVisibility;
	System::UnicodeString FRelativeMessageId;
	TQueueSequenceDeviation FSequenceDeviation;
	System::UnicodeString FTransformation;
	TQueueDeliveryMode FDeliveryMode;
	
protected:
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	
public:
	__property System::UnicodeString RelativeMessageId = {read=FRelativeMessageId, write=FRelativeMessageId};
	
__published:
	__property TQueueVisibility Visibility = {read=FVisibility, write=FVisibility, default=0};
	__property TQueueSequenceDeviation SequenceDeviation = {read=FSequenceDeviation, write=FSequenceDeviation, default=0};
	__property System::UnicodeString Transformation = {read=FTransformation, write=FTransformation};
	__property TQueueDeliveryMode DeliveryMode = {read=FDeliveryMode, write=FDeliveryMode, default=0};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TEnqueueOptions() { }
	
public:
	/* TObject.Create */ inline __fastcall TEnqueueOptions() : System::Classes::TPersistent() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TDequeueOptions : public System::Classes::TPersistent
{
	typedef System::Classes::TPersistent inherited;
	
private:
	System::UnicodeString FConsumerName;
	TDequeueMode FDequeueMode;
	TQueueNavigation FNavigation;
	TQueueVisibility FVisibility;
	int FWaitTimeout;
	System::UnicodeString FMessageId;
	System::UnicodeString FCorrelation;
	System::UnicodeString FDequeueCondition;
	System::UnicodeString FTransformation;
	TQueueDeliveryMode FDeliveryMode;
	
protected:
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	
public:
	__fastcall TDequeueOptions();
	__property System::UnicodeString MessageId = {read=FMessageId, write=FMessageId};
	
__published:
	__property System::UnicodeString ConsumerName = {read=FConsumerName, write=FConsumerName};
	__property TDequeueMode DequeueMode = {read=FDequeueMode, write=FDequeueMode, default=2};
	__property TQueueNavigation Navigation = {read=FNavigation, write=FNavigation, default=0};
	__property TQueueVisibility Visibility = {read=FVisibility, write=FVisibility, default=0};
	__property int WaitTimeout = {read=FWaitTimeout, write=FWaitTimeout, default=-1};
	__property System::UnicodeString Correlation = {read=FCorrelation, write=FCorrelation};
	__property System::UnicodeString DequeueCondition = {read=FDequeueCondition, write=FDequeueCondition};
	__property System::UnicodeString Transformation = {read=FTransformation, write=FTransformation};
	__property TQueueDeliveryMode DeliveryMode = {read=FDeliveryMode, write=FDeliveryMode, default=0};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TDequeueOptions() { }
	
};

#pragma pack(pop)

typedef void __fastcall (__closure *TQueueMessageEvent)(TOraQueue* Sender, const System::UnicodeString MessageId, TQueueMessageProperties* const MessageProperties);

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraQueue : public System::Classes::TComponent
{
	typedef System::Classes::TComponent inherited;
	
private:
	Ora::TOraSession* FSession;
	Ora::TOraSQL* FSQL;
	System::UnicodeString FQueueName;
	System::UnicodeString FPayloadTypeName;
	System::UnicodeString FPayloadArrayTypeName;
	Oraobjects::TOraType* FPayloadOraType;
	TQueueMessageProperties* FEnqueueMessageProperties;
	TEnqueueOptions* FEnqueueOptions;
	TDequeueOptions* FDequeueOptions;
	void *FGCHandle;
	System::Word FServerVersion;
	void __fastcall SetSession(Ora::TOraSession* Value);
	System::UnicodeString __fastcall GetPayloadTypeName();
	void __fastcall SetQueueName(const System::UnicodeString Value);
	void __fastcall SetEnqueueMessageProperties(TQueueMessageProperties* Value);
	void __fastcall SetEnqueueOptions(TEnqueueOptions* Value);
	void __fastcall SetDequeueOptions(TDequeueOptions* Value);
	void * __fastcall GetGCHandle();
	Oracall::TOCISvcCtx* __fastcall GetOCISvcCtx();
	Oracall::TOCI8API* __fastcall GetOCI8();
	
protected:
	bool FDesignCreate;
	void __fastcall Init();
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	virtual void __fastcall Loaded();
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	Ora::TOraSession* __fastcall UsedConnection();
	void __fastcall BeginConnection();
	void __fastcall EndConnection();
	void __fastcall CheckOracleVersion();
	void __fastcall CheckPayloadType();
	void __fastcall CheckPayloadArrayType();
	__property void * GCHandle = {read=GetGCHandle};
	__property Oracall::TOCISvcCtx* OCISvcCtx = {read=GetOCISvcCtx};
	__property Oracall::TOCI8API* OCI8 = {read=GetOCI8};
	
public:
	__fastcall virtual TOraQueue(System::Classes::TComponent* AOwner)/* overload */;
	__fastcall TOraQueue(System::Classes::TComponent* AOwner, Ora::TOraSession* Session, const System::UnicodeString QueueName)/* overload */;
	__fastcall virtual ~TOraQueue();
	System::UnicodeString __fastcall Enqueue(const System::UnicodeString Payload, TQueueMessageProperties* MessageProperties = (TQueueMessageProperties*)(0x0), TEnqueueOptions* EnqueueOptions = (TEnqueueOptions*)(0x0))/* overload */;
	System::UnicodeString __fastcall Enqueue(const System::TArray__1<System::Byte> Payload, TQueueMessageProperties* MessageProperties = (TQueueMessageProperties*)(0x0), TEnqueueOptions* EnqueueOptions = (TEnqueueOptions*)(0x0))/* overload */;
	System::UnicodeString __fastcall Enqueue(Oraobjects::TOraObject* Payload, TQueueMessageProperties* MessageProperties = (TQueueMessageProperties*)(0x0), TEnqueueOptions* EnqueueOptions = (TEnqueueOptions*)(0x0))/* overload */;
	System::UnicodeString __fastcall Enqueue(TQueueMessage* Message, TEnqueueOptions* EnqueueOptions = (TEnqueueOptions*)(0x0))/* overload */;
	System::UnicodeString __fastcall Dequeue(/* out */ System::UnicodeString &Payload, TQueueMessageProperties* MessageProperties = (TQueueMessageProperties*)(0x0), TDequeueOptions* DequeueOptions = (TDequeueOptions*)(0x0))/* overload */;
	System::UnicodeString __fastcall Dequeue(/* out */ System::TArray__1<System::Byte> &Payload, TQueueMessageProperties* MessageProperties = (TQueueMessageProperties*)(0x0), TDequeueOptions* DequeueOptions = (TDequeueOptions*)(0x0))/* overload */;
	System::UnicodeString __fastcall Dequeue(Oraobjects::TOraObject* Payload, TQueueMessageProperties* MessageProperties = (TQueueMessageProperties*)(0x0), TDequeueOptions* DequeueOptions = (TDequeueOptions*)(0x0))/* overload */;
	System::UnicodeString __fastcall Dequeue(TQueueMessage* Message, TDequeueOptions* DequeueOptions = (TDequeueOptions*)(0x0))/* overload */;
	TMessageIds __fastcall EnqueueArray(TQueueMessage* const *MessageArray, const int MessageArray_High, TEnqueueOptions* EnqueueOptions = (TEnqueueOptions*)(0x0));
	TMessageIds __fastcall DequeueArray(TQueueMessage* const *MessageArray, const int MessageArray_High, /* out */ int &DequeuedSize, TDequeueOptions* DequeueOptions = (TDequeueOptions*)(0x0));
	void __fastcall Listen(TQueueAgents* Agents, TQueueAgent* Agent, int WaitTimeout = 0xffffffff)/* overload */;
	void __fastcall Listen(TQueueAgents* Agents, TQueueDeliveryMode ListDeliveryMode, TQueueAgent* Agent, TQueueDeliveryMode &MessageDeliveryMode, int WaitTimeout = 0xffffffff)/* overload */;
	__property System::UnicodeString PayloadTypeName = {read=GetPayloadTypeName, write=FPayloadTypeName};
	__property System::UnicodeString PayloadArrayTypeName = {read=FPayloadArrayTypeName, write=FPayloadArrayTypeName};
	
__published:
	__property Ora::TOraSession* Session = {read=FSession, write=SetSession};
	__property System::UnicodeString QueueName = {read=FQueueName, write=SetQueueName};
	__property TQueueMessageProperties* EnqueueMessageProperties = {read=FEnqueueMessageProperties, write=SetEnqueueMessageProperties};
	__property TEnqueueOptions* EnqueueOptions = {read=FEnqueueOptions, write=SetEnqueueOptions};
	__property TDequeueOptions* DequeueOptions = {read=FDequeueOptions, write=SetDequeueOptions};
};

#pragma pack(pop)

enum DECLSPEC_DENUM TQueueSortList : unsigned char { qslDefault, qslPriority, qslEnqueueTime, qslPriorityEnqueueTime, qslEnqueueTimePriority };

enum DECLSPEC_DENUM TQueueMessageGrouping : unsigned char { qmgNone, qmgTransactional };

enum DECLSPEC_DENUM TQueueCompatible : unsigned char { qcDefault, qc80, qc81, qc100 };

enum DECLSPEC_DENUM TQueuePrivilege : unsigned char { qpEnqueue, qpDequeue, qpAll };

enum DECLSPEC_DENUM TQueueSystemPrivilege : unsigned char { qspEnqueueAny, qspDequeueAny, qspManageAny };

enum DECLSPEC_DENUM TQueueType : unsigned char { qtNormalQueue, qtExceptionQueue };

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraQueueTable : public System::Classes::TComponent
{
	typedef System::Classes::TComponent inherited;
	
private:
	Ora::TOraSession* FSession;
	Ora::TOraSQL* FSQL;
	System::UnicodeString FQueueTableName;
	System::UnicodeString FStorageClause;
	System::UnicodeString FPayloadTypeName;
	TQueueSortList FSortList;
	bool FMultipleConsumers;
	TQueueMessageGrouping FMessageGrouping;
	System::UnicodeString FComment;
	int FPrimaryInstance;
	int FSecondaryInstance;
	TQueueCompatible FCompatible;
	bool FSecure;
	System::Word FServerVersion;
	void __fastcall SetSession(Ora::TOraSession* Value);
	bool __fastcall IsPayloadTypeNameStored();
	
protected:
	bool FDesignCreate;
	void __fastcall Init();
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	virtual void __fastcall Loaded();
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	Ora::TOraSession* __fastcall UsedConnection();
	void __fastcall BeginConnection();
	void __fastcall EndConnection();
	void __fastcall CheckOracleVersion();
	
public:
	__fastcall virtual TOraQueueTable(System::Classes::TComponent* AOwner)/* overload */;
	__fastcall TOraQueueTable(System::Classes::TComponent* AOwner, Ora::TOraSession* Session, const System::UnicodeString QueueTableName)/* overload */;
	__fastcall virtual ~TOraQueueTable();
	void __fastcall CreateQueueTable();
	void __fastcall ReadQueueTableProperties();
	void __fastcall AlterQueueTable(const System::UnicodeString Comment, int PrimaryInstance = 0xffffffff, int SecondaryInstance = 0xffffffff);
	void __fastcall AlterComment(const System::UnicodeString Comment);
	void __fastcall AlterPrimaryInstance(int PrimaryInstance);
	void __fastcall AlterSecondaryInstance(int SecondaryInstance);
	void __fastcall DropQueueTable(bool Force = false);
	void __fastcall PurgeQueueTable(const System::UnicodeString PurgeCondition, bool Block = false, TQueueDeliveryMode DeliveryMode = (TQueueDeliveryMode)(0x0));
	void __fastcall MigrateQueueTable(TQueueCompatible Compatible);
	void __fastcall GrantSystemPrivilege(TQueueSystemPrivilege Privilege, const System::UnicodeString Grantee, bool AdminOption = false);
	void __fastcall RevokeSystemPrivilege(TQueueSystemPrivilege Privilege, const System::UnicodeString Grantee);
	
__published:
	__property Ora::TOraSession* Session = {read=FSession, write=SetSession};
	__property System::UnicodeString QueueTableName = {read=FQueueTableName, write=FQueueTableName};
	__property System::UnicodeString StorageClause = {read=FStorageClause, write=FStorageClause};
	__property System::UnicodeString PayloadTypeName = {read=FPayloadTypeName, write=FPayloadTypeName, stored=IsPayloadTypeNameStored};
	__property TQueueSortList SortList = {read=FSortList, write=FSortList, default=0};
	__property bool MultipleConsumers = {read=FMultipleConsumers, write=FMultipleConsumers, default=0};
	__property TQueueMessageGrouping MessageGrouping = {read=FMessageGrouping, write=FMessageGrouping, default=0};
	__property System::UnicodeString Comment = {read=FComment, write=FComment};
	__property int PrimaryInstance = {read=FPrimaryInstance, write=FPrimaryInstance, default=0};
	__property int SecondaryInstance = {read=FSecondaryInstance, write=FSecondaryInstance, default=0};
	__property TQueueCompatible Compatible = {read=FCompatible, write=FCompatible, default=0};
	__property bool Secure = {read=FSecure, write=FSecure, default=0};
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraQueueAdmin : public System::Classes::TComponent
{
	typedef System::Classes::TComponent inherited;
	
private:
	Ora::TOraSession* FSession;
	Ora::TOraSQL* FSQL;
	System::UnicodeString FQueueName;
	TQueueType FQueueType;
	int FMaxRetries;
	int FRetryDelay;
	int FRetentionTime;
	System::UnicodeString FComment;
	System::UnicodeString FQueueTableName;
	bool FMultipleConsumers;
	System::Word FServerVersion;
	void __fastcall SetSession(Ora::TOraSession* Value);
	
protected:
	bool FDesignCreate;
	void __fastcall Init();
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	virtual void __fastcall Loaded();
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	Ora::TOraSession* __fastcall UsedConnection();
	void __fastcall BeginConnection();
	void __fastcall EndConnection();
	void __fastcall CheckOracleVersion();
	void __fastcall CreateNonPersistentQueue();
	System::UnicodeString __fastcall GetNameWithSchema(const System::UnicodeString Name);
	
public:
	__fastcall virtual TOraQueueAdmin(System::Classes::TComponent* AOwner)/* overload */;
	__fastcall TOraQueueAdmin(System::Classes::TComponent* AOwner, Ora::TOraSession* Session, const System::UnicodeString QueueName)/* overload */;
	void __fastcall CreateQueue(bool NonPersistent = false);
	void __fastcall ReadQueueProperties();
	void __fastcall DropQueue();
	void __fastcall AlterQueue(int MaxRetries, int RetryDelay = 0xffffffff, int RetentionTime = 0xffffffff, const System::UnicodeString Comment = System::UnicodeString());
	void __fastcall AlterComment(const System::UnicodeString Comment);
	void __fastcall AlterMaxRetries(int MaxRetries);
	void __fastcall AlterRetryDelay(int RetryDelay);
	void __fastcall AlterRetentionTime(int RetentionTime);
	void __fastcall StartEnqueue();
	void __fastcall StartDequeue();
	void __fastcall StartQueue(bool Enqueue = true, bool Dequeue = true);
	void __fastcall StopEnqueue(bool Wait = true);
	void __fastcall StopDequeue(bool Wait = true);
	void __fastcall StopQueue(bool Enqueue = true, bool Dequeue = true, bool Wait = true);
	void __fastcall GetSubscribers(TQueueAgents* Subscribers);
	void __fastcall AddSubscriber(TQueueAgent* Subscriber, const System::UnicodeString Rule = System::UnicodeString(), const System::UnicodeString Transformation = System::UnicodeString(), bool QueueToQueue = false, TQueueDeliveryMode DeliveryMode = (TQueueDeliveryMode)(0x0));
	void __fastcall RemoveSubscriber(TQueueAgent* Subscriber);
	void __fastcall AlterSubscriber(TQueueAgent* Subscriber, const System::UnicodeString Rule, const System::UnicodeString Transformation = System::UnicodeString());
	void __fastcall GrantQueuePrivilege(TQueuePrivilege Privilege, const System::UnicodeString Grantee, bool GrantOption = false);
	void __fastcall RevokeQueuePrivilege(TQueuePrivilege Privilege, const System::UnicodeString Grantee);
	void __fastcall SchedulePropagation(const System::UnicodeString Destination, System::TDateTime StartTime, int Duration, const System::UnicodeString NextTime, int Latency, const System::UnicodeString DestinationQueue = System::UnicodeString());
	void __fastcall UnschedulePropagation(const System::UnicodeString Destination, const System::UnicodeString DestinationQueue = System::UnicodeString());
	void __fastcall AlterPropagationSchedule(const System::UnicodeString Destination, int Duration, const System::UnicodeString NextTime, int Latency, const System::UnicodeString DestinationQueue = System::UnicodeString());
	void __fastcall EnablePropagationSchedule(const System::UnicodeString Destination, const System::UnicodeString DestinationQueue = System::UnicodeString());
	void __fastcall DisablePropagationSchedule(const System::UnicodeString Destination, const System::UnicodeString DestinationQueue = System::UnicodeString());
	bool __fastcall VerifyQueueTypes(const System::UnicodeString DestQueueName, const System::UnicodeString Destination);
	
__published:
	__property Ora::TOraSession* Session = {read=FSession, write=SetSession};
	__property System::UnicodeString QueueName = {read=FQueueName, write=FQueueName};
	__property TQueueType QueueType = {read=FQueueType, write=FQueueType, default=0};
	__property int MaxRetries = {read=FMaxRetries, write=FMaxRetries, default=-1};
	__property int RetryDelay = {read=FRetryDelay, write=FRetryDelay, default=0};
	__property int RetentionTime = {read=FRetentionTime, write=FRetentionTime, default=0};
	__property System::UnicodeString Comment = {read=FComment, write=FComment};
	__property System::UnicodeString QueueTableName = {read=FQueueTableName, write=FQueueTableName};
	__property bool MultipleConsumers = {read=FMultipleConsumers, write=FMultipleConsumers, default=0};
public:
	/* TComponent.Destroy */ inline __fastcall virtual ~TOraQueueAdmin() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraAQUtils : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	__classmethod void __fastcall SetDesignCreate(TOraQueue* Obj, bool Value)/* overload */;
	__classmethod bool __fastcall GetDesignCreate(TOraQueue* Obj)/* overload */;
	__classmethod void __fastcall SetDesignCreate(TOraQueueTable* Obj, bool Value)/* overload */;
	__classmethod bool __fastcall GetDesignCreate(TOraQueueTable* Obj)/* overload */;
	__classmethod void __fastcall SetDesignCreate(TOraQueueAdmin* Obj, bool Value)/* overload */;
	__classmethod bool __fastcall GetDesignCreate(TOraQueueAdmin* Obj)/* overload */;
public:
	/* TObject.Create */ inline __fastcall TOraAQUtils() : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TOraAQUtils() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TQueueTableList : public System::Classes::TThreadList
{
	typedef System::Classes::TThreadList inherited;
	
public:
	TOraQueueTable* operator[](int Index) { return this->Items[Index]; }
	
private:
	int __fastcall GetCount();
	TOraQueueTable* __fastcall GetTable(int Index);
	
public:
	__property int Count = {read=GetCount, nodefault};
	__property TOraQueueTable* Items[int Index] = {read=GetTable/*, default*/};
public:
	/* TThreadList.Create */ inline __fastcall TQueueTableList() : System::Classes::TThreadList() { }
	/* TThreadList.Destroy */ inline __fastcall virtual ~TQueueTableList() { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
static constexpr System::Int8 AQ_NO_DELAY = System::Int8(0x0);
static constexpr System::Int8 AQ_NEVER = System::Int8(-1);
static constexpr System::Int8 AQ_FOREVER = System::Int8(-1);
static constexpr System::Int8 AQ_NO_WAIT = System::Int8(0x0);
static constexpr System::Int8 AQ_PERSISTENT = System::Int8(0x1);
static constexpr System::Int8 AQ_BUFFERED = System::Int8(0x2);
static constexpr System::Int8 AQ_PERSISTENT_OR_BUFFERED = System::Int8(0x3);
static constexpr System::Int8 AQ_IMMEDIATE = System::Int8(0x1);
static constexpr System::Int8 AQ_ON_COMMIT = System::Int8(0x2);
static constexpr System::Int8 AQ_BEFORE = System::Int8(0x2);
static constexpr System::Int8 AQ_TOP = System::Int8(0x3);
static constexpr System::Int8 AQ_BROWSE = System::Int8(0x1);
static constexpr System::Int8 AQ_LOCKED = System::Int8(0x2);
static constexpr System::Int8 AQ_REMOVE = System::Int8(0x3);
static constexpr System::Int8 AQ_REMOVE_NODATA = System::Int8(0x4);
static constexpr System::Int8 AQ_FIRST_MESSAGE = System::Int8(0x1);
static constexpr System::Int8 AQ_NEXT_TRANSACTION = System::Int8(0x2);
static constexpr System::Int8 AQ_NEXT_MESSAGE = System::Int8(0x3);
static constexpr System::Int8 AQ_FIRST_MESSAGE_MULTI_GROUP = System::Int8(0x4);
static constexpr System::Int8 AQ_NEXT_MESSAGE_MULTI_GROUP = System::Int8(0x5);
static constexpr System::Int8 AQ_READY = System::Int8(0x0);
static constexpr System::Int8 AQ_WAITING = System::Int8(0x1);
static constexpr System::Int8 AQ_PROCESSED = System::Int8(0x2);
static constexpr System::Int8 AQ_EXPIRED = System::Int8(0x3);
static constexpr System::Int8 AQ_NONE = System::Int8(0x0);
static constexpr System::Int8 AQ_TRANSACTIONAL = System::Int8(0x1);
static constexpr System::Int8 AQ_NORMAL_QUEUE = System::Int8(0x0);
static constexpr System::Int8 AQ_EXCEPTION_QUEUE = System::Int8(0x1);
static constexpr System::Int8 AQ_INFINITE = System::Int8(-1);
static constexpr System::Int8 AQ_NOT_DEFINED = System::Int8(-1);
extern DELPHI_PACKAGE TQueueTableList* QueueTableList;
}	/* namespace Oraaq */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_ORAAQ)
using namespace Oraaq;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// OraaqHPP
