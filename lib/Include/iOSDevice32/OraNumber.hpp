﻿// CodeGear C++Builder
// Copyright (c) 1995, 2018 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'OraNumber.pas' rev: 33.00 (iOS)

#ifndef OranumberHPP
#define OranumberHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.SysUtils.hpp>
#include <CRTypes.hpp>
#include <OraCall.hpp>

//-- user supplied -----------------------------------------------------------

namespace Oranumber
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TOCINumber;
//-- type declarations -------------------------------------------------------
typedef System::StaticArray<System::Byte, 23> TOCINumberValue;

typedef System::StaticArray<System::Byte, 35> TOCIBCDValue;

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOCINumber : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	__classmethod int __fastcall Compare(const System::TArray__1<System::Byte> number1, const System::TArray__1<System::Byte> number2);
	__classmethod System::TArray__1<System::Byte> __fastcall FromInt(__int64 Value, unsigned IntLength, unsigned SignFlag);
	__classmethod __int64 __fastcall ToInt(const System::TArray__1<System::Byte> Value, unsigned IntLength, unsigned SignFlag);
	__classmethod System::TArray__1<System::Byte> __fastcall FromReal(double Value, unsigned RealLength);
	__classmethod double __fastcall ToReal(const System::TArray__1<System::Byte> Value, unsigned RealLength);
	__classmethod System::TArray__1<System::Byte> __fastcall FromBCD(const System::TArray__1<System::Byte> Value, unsigned RealLength);
	__classmethod void __fastcall ToBCD(const TOCINumberValue &number, unsigned numberLength, TOCIBCDValue &Result, unsigned ResultLength);
	__classmethod int __fastcall Parse(const System::UnicodeString str, const System::UnicodeString fmt, System::TArray__1<System::Byte> &number);
	__classmethod System::UnicodeString __fastcall ToString(const System::TArray__1<System::Byte> number, const System::UnicodeString fmt);
public:
	/* TObject.Create */ inline __fastcall TOCINumber() : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TOCINumber() { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
}	/* namespace Oranumber */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_ORANUMBER)
using namespace Oranumber;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// OranumberHPP
