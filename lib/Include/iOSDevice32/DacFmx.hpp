﻿// CodeGear C++Builder
// Copyright (c) 1995, 2018 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'DacFmx.pas' rev: 33.00 (iOS)

#ifndef DacfmxHPP
#define DacfmxHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.Classes.hpp>
#include <System.SysUtils.hpp>
#include <Data.DB.hpp>
#include <System.TypInfo.hpp>
#include <System.UITypes.hpp>
#include <FMX.Types.hpp>
#include <FMX.Platform.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Memo.hpp>
#include <FMX.StdCtrls.hpp>
#include <MemData.hpp>
#include <DBAccess.hpp>
#include <DASQLMonitor.hpp>

//-- user supplied -----------------------------------------------------------

namespace Dacfmx
{
//-- forward type declarations -----------------------------------------------
//-- type declarations -------------------------------------------------------
_DECLARE_METACLASS(System::TMetaClass, TFormClass);

//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE System::Uitypes::TCursor __fastcall GetScreenCursor(void);
extern DELPHI_PACKAGE void __fastcall SetScreenCursor(System::Uitypes::TCursor Value);
extern DELPHI_PACKAGE void __fastcall SetCursor(int Value);
extern DELPHI_PACKAGE void __fastcall ShowDebugForm(Dasqlmonitor::TDASQLMonitorClass DASQLMonitorClass, System::Classes::TComponent* Component, System::UnicodeString SQL, Dbaccess::TDAParams* Params, System::UnicodeString Caption);
extern DELPHI_PACKAGE bool __fastcall ShowConnectForm(Dbaccess::TCustomConnectDialog* ConnectDialog);
extern DELPHI_PACKAGE void __fastcall StartWait(void);
extern DELPHI_PACKAGE void __fastcall StopWait(void);
extern DELPHI_PACKAGE System::UnicodeString __fastcall ApplicationTitle(void);
}	/* namespace Dacfmx */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_DACFMX)
using namespace Dacfmx;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// DacfmxHPP
