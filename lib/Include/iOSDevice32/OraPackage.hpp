﻿// CodeGear C++Builder
// Copyright (c) 1995, 2018 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'OraPackage.pas' rev: 33.00 (iOS)

#ifndef OrapackageHPP
#define OrapackageHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.Classes.hpp>
#include <System.Types.hpp>
#include <System.Variants.hpp>
#include <Data.DB.hpp>
#include <CRTypes.hpp>
#include <MemUtils.hpp>
#include <MemData.hpp>
#include <DBAccess.hpp>
#include <OraCall.hpp>
#include <Ora.hpp>
#include <System.Generics.Collections.hpp>

//-- user supplied -----------------------------------------------------------

namespace Orapackage
{
//-- forward type declarations -----------------------------------------------
struct TPlSqlRecordParameter;
class DELPHICLASS TVariable;
class DELPHICLASS TCustomPlSqlRecord;
class DELPHICLASS TCustomOraPackage;
class DELPHICLASS TOraPackage;
class DELPHICLASS TOraPackageUtils;
//-- type declarations -------------------------------------------------------
struct DECLSPEC_DRECORD TPlSqlRecordParameter
{
public:
	System::UnicodeString Name;
	Data::Db::TParamType ParamType;
};


typedef System::DynamicArray<TPlSqlRecordParameter> TPlSqlRecordParameters;

#pragma pack(push,4)
class PASCALIMPLEMENTATION TVariable : public System::TObject
{
	typedef System::TObject inherited;
	
private:
	System::UnicodeString FName;
	TCustomOraPackage* FPackage;
	void __fastcall SetName(const System::UnicodeString Value);
	void __fastcall CheckPackage();
	System::TDateTime __fastcall GetAsDateTime();
	double __fastcall GetAsFloat();
	int __fastcall GetAsInteger();
	System::UnicodeString __fastcall GetAsString();
	void __fastcall SetAsDateTime(const System::TDateTime Value);
	void __fastcall SetAsFloat(const double Value);
	void __fastcall SetAsInteger(const int Value);
	void __fastcall SetAsString(const System::UnicodeString Value);
	
public:
	__fastcall TVariable(TCustomOraPackage* APackage);
	__property System::UnicodeString Name = {read=FName, write=SetName};
	__property TCustomOraPackage* Package = {read=FPackage};
	__property System::TDateTime AsDateTime = {read=GetAsDateTime, write=SetAsDateTime};
	__property double AsFloat = {read=GetAsFloat, write=SetAsFloat};
	__property int AsInteger = {read=GetAsInteger, write=SetAsInteger, nodefault};
	__property System::UnicodeString AsString = {read=GetAsString, write=SetAsString};
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TVariable() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TCustomPlSqlRecord : public Memdata::TSharedObject
{
	typedef Memdata::TSharedObject inherited;
	
private:
	Oracall::TOCISvcCtx* FSvcCtx;
	System::Classes::TList* FParentRecordList;
	void __fastcall SetParentRecordList(System::Classes::TList* const Value);
	
protected:
	virtual void __fastcall SetOCISvcCtx(Oracall::TOCISvcCtx* const Value);
	void __fastcall KeepReferences(Memdata::TSharedObject* const OldValue, Memdata::TSharedObject* const NewValue);
	
public:
	__fastcall virtual ~TCustomPlSqlRecord();
	virtual void __fastcall Assign(TCustomPlSqlRecord* Source);
	__property System::Classes::TList* ParentRecordList = {read=FParentRecordList, write=SetParentRecordList};
	__property Oracall::TOCISvcCtx* OCISvcCtx = {read=FSvcCtx, write=SetOCISvcCtx};
public:
	/* TSharedObject.Create */ inline __fastcall TCustomPlSqlRecord() : Memdata::TSharedObject() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TCustomOraPackage : public Ora::TCustomOraComponent
{
	typedef Ora::TCustomOraComponent inherited;
	
private:
	System::UnicodeString FPackageName;
	System::Generics::Collections::TObjectList__1<System::TObject*> * FVariables;
	Ora::TOraParams* FParams;
	bool FDebug;
	bool FRestoreOld;
	bool FOldDebug;
	bool FOldParamCheck;
	TPlSqlRecordParameters FPlSqlRecordStack;
	System::UnicodeString FPlSqlDeclSt;
	System::UnicodeString FPlSqlInSt;
	System::UnicodeString FPlSqlRetSt;
	System::UnicodeString FPlSqlParamSt;
	System::UnicodeString FPlSqlOutSt;
	System::Classes::TList* FPlSqlRecordList;
	void __fastcall SetPackageName(const System::UnicodeString Value);
	void __fastcall SetSession(Ora::TOraSession* const Value);
	Ora::TOraSession* __fastcall UsedConnection();
	void __fastcall BeginConnection();
	void __fastcall EndConnection();
	void __fastcall CheckPackage();
	void __fastcall SetDebug(const bool Value);
	
protected:
	bool FDesignCreate;
	virtual void __fastcall Loaded();
	virtual void __fastcall Notification(System::Classes::TComponent* AComponent, System::Classes::TOperation Operation);
	Data::Db::TParam* __fastcall GetVariable(System::UnicodeString Name, Data::Db::TFieldType DataType);
	void __fastcall SetVariable(System::UnicodeString Name, Data::Db::TFieldType DataType, const System::Variant &Value);
	void __fastcall ClearPlSqlRecordList();
	void __fastcall BeginExecPlSql();
	void __fastcall ExecProc(const System::UnicodeString Name)/* overload */;
	void __fastcall EndExecPlSql();
	Ora::TOraParam* __fastcall AddParam(const System::UnicodeString Name, const Data::Db::TFieldType DataType, const Data::Db::TParamType ParamType = (Data::Db::TParamType)(0x0));
	void __fastcall BeginRecordParam(const System::UnicodeString Name, const System::UnicodeString DataTypeStr, const Data::Db::TParamType ParamType = (Data::Db::TParamType)(0x0));
	void __fastcall EndRecordParam();
	Oracall::TOCISvcCtx* __fastcall OCISvcCtx();
	__property System::Classes::TList* PlSqlRecordList = {read=FPlSqlRecordList};
	
public:
	__fastcall virtual TCustomOraPackage(System::Classes::TComponent* AOwner);
	__fastcall virtual ~TCustomOraPackage();
	TVariable* __fastcall VariableByName(System::UnicodeString Name);
	System::Variant __fastcall ExecProc(System::UnicodeString Name, const System::Variant *Params, const int Params_High)/* overload */;
	System::Variant __fastcall ExecProcEx(System::UnicodeString Name, const System::Variant *Params, const int Params_High);
	Ora::TOraParam* __fastcall FindParam(const System::UnicodeString Value);
	Ora::TOraParam* __fastcall ParamByName(const System::UnicodeString Value);
	__property Ora::TOraParams* Params = {read=FParams};
	__property System::UnicodeString PackageName = {read=FPackageName, write=SetPackageName};
	
__published:
	__property bool Debug = {read=FDebug, write=SetDebug, default=0};
	__property Ora::TOraSession* Session = {read=FSession, write=SetSession};
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraPackage : public TCustomOraPackage
{
	typedef TCustomOraPackage inherited;
	
__published:
	__property PackageName = {default=0};
public:
	/* TCustomOraPackage.Create */ inline __fastcall virtual TOraPackage(System::Classes::TComponent* AOwner) : TCustomOraPackage(AOwner) { }
	/* TCustomOraPackage.Destroy */ inline __fastcall virtual ~TOraPackage() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraPackageUtils : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	__classmethod void __fastcall SetDesignCreate(TCustomOraPackage* Obj, bool Value);
	__classmethod bool __fastcall GetDesignCreate(TCustomOraPackage* Obj);
	__classmethod Ora::TOraSession* __fastcall UsedConnection(TCustomOraPackage* Obj);
public:
	/* TObject.Create */ inline __fastcall TOraPackageUtils() : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TOraPackageUtils() { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
}	/* namespace Orapackage */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_ORAPACKAGE)
using namespace Orapackage;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// OrapackageHPP
