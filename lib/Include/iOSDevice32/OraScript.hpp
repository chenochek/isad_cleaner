﻿// CodeGear C++Builder
// Copyright (c) 1995, 2018 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'OraScript.pas' rev: 33.00 (iOS)

#ifndef OrascriptHPP
#define OrascriptHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.Variants.hpp>
#include <System.Classes.hpp>
#include <System.SysUtils.hpp>
#include <CRTypes.hpp>
#include <CRParser.hpp>
#include <DBAccess.hpp>
#include <DAScript.hpp>
#include <OraParser.hpp>
#include <OraClasses.hpp>
#include <OraScriptProcessor.hpp>
#include <Ora.hpp>

//-- user supplied -----------------------------------------------------------

namespace Orascript
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TOraStatement;
class DELPHICLASS TOraStatements;
class DELPHICLASS TOraScript;
class DELPHICLASS TOraScriptProcessor;
//-- type declarations -------------------------------------------------------
#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraStatement : public Dascript::TDAStatement
{
	typedef Dascript::TDAStatement inherited;
	
private:
	HIDESBASE Ora::TOraParams* __fastcall GetParams();
	
public:
	__property Ora::TOraParams* Params = {read=GetParams};
public:
	/* TDAStatement.Destroy */ inline __fastcall virtual ~TOraStatement() { }
	
public:
	/* TCollectionItem.Create */ inline __fastcall virtual TOraStatement(System::Classes::TCollection* Collection) : Dascript::TDAStatement(Collection) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraStatements : public Dascript::TDAStatements
{
	typedef Dascript::TDAStatements inherited;
	
public:
	TOraStatement* operator[](int Index) { return this->Items[Index]; }
	
private:
	HIDESBASE TOraStatement* __fastcall GetItem(int Index);
	
public:
	__property TOraStatement* Items[int Index] = {read=GetItem/*, default*/};
public:
	/* TDAStatements.Create */ inline __fastcall TOraStatements(System::Classes::TCollectionItemClass ItemClass, Dascript::TDAScript* Script) : Dascript::TDAStatements(ItemClass, Script) { }
	
public:
	/* TCollection.Destroy */ inline __fastcall virtual ~TOraStatements() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraScript : public Dascript::TDAScript
{
	typedef Dascript::TDAScript inherited;
	
protected:
	Ora::TOraSession* __fastcall GetSession();
	void __fastcall SetSession(Ora::TOraSession* Value);
	HIDESBASE Ora::TOraDataSet* __fastcall GetDataSet();
	HIDESBASE void __fastcall SetDataSet(Ora::TOraDataSet* Value);
	HIDESBASE Ora::TOraParams* __fastcall GetParams();
	HIDESBASE TOraStatements* __fastcall GetStatements();
	virtual Dascript::TDAScriptProcessorClass __fastcall GetProcessorClass();
	virtual Dascript::TDAStatements* __fastcall CreateStatementsObject();
	virtual Dbaccess::TCustomDASQL* __fastcall CreateCommand();
	virtual void __fastcall CalculateErrorOffset(System::Sysutils::Exception* E);
	
public:
	__fastcall virtual TOraScript(System::Classes::TComponent* Owner);
	__property TOraStatements* Statements = {read=GetStatements};
	
__published:
	__property ScanParams = {default=1};
	__property Ora::TOraSession* Session = {read=GetSession, write=SetSession};
	__property Ora::TOraDataSet* DataSet = {read=GetDataSet, write=SetDataSet};
public:
	/* TDAScript.Destroy */ inline __fastcall virtual ~TOraScript() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraScriptProcessor : public Orascriptprocessor::TCustomOraScriptProcessor
{
	typedef Orascriptprocessor::TCustomOraScriptProcessor inherited;
	
protected:
	virtual void __fastcall SetConnectMode(Dbaccess::TCustomDAConnection* Connection, Oraclasses::TConnectMode ConnectMode);
public:
	/* TCustomOraScriptProcessor.Create */ inline __fastcall virtual TOraScriptProcessor(Dascript::TDAScript* Owner) : Orascriptprocessor::TCustomOraScriptProcessor(Owner) { }
	
public:
	/* TDAScriptProcessor.Destroy */ inline __fastcall virtual ~TOraScriptProcessor() { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
}	/* namespace Orascript */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_ORASCRIPT)
using namespace Orascript;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// OrascriptHPP
