﻿// CodeGear C++Builder
// Copyright (c) 1995, 2018 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'OraError.pas' rev: 33.00 (iOS)

#ifndef OraerrorHPP
#define OraerrorHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.SysUtils.hpp>
#include <System.Classes.hpp>
#include <Data.DB.hpp>
#include <DBAccess.hpp>
#include <CRTypes.hpp>
#include <CRAccess.hpp>
#include <OraCall.hpp>

//-- user supplied -----------------------------------------------------------

namespace Oraerror
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS EOraError;
class DELPHICLASS TOraError;
//-- type declarations -------------------------------------------------------
#pragma pack(push,4)
class PASCALIMPLEMENTATION EOraError : public Dbaccess::EDAError
{
	typedef Dbaccess::EDAError inherited;
	
private:
	System::Classes::TComponent* FSender;
	
public:
	__fastcall EOraError(int ErrorCode, const System::UnicodeString Msg, System::TObject* Component);
	__fastcall virtual ~EOraError();
	virtual bool __fastcall IsFatalError();
	virtual bool __fastcall IsKeyViolation();
	__property System::Classes::TComponent* Sender = {read=FSender, write=FSender};
public:
	/* Exception.CreateFmt */ inline __fastcall EOraError(const System::UnicodeString Msg, const System::TVarRec *Args, const int Args_High) : Dbaccess::EDAError(Msg, Args, Args_High) { }
	/* Exception.CreateRes */ inline __fastcall EOraError(System::PResStringRec ResStringRec) : Dbaccess::EDAError(ResStringRec) { }
	/* Exception.CreateResFmt */ inline __fastcall EOraError(System::PResStringRec ResStringRec, const System::TVarRec *Args, const int Args_High) : Dbaccess::EDAError(ResStringRec, Args, Args_High) { }
	/* Exception.CreateHelp */ inline __fastcall EOraError(const System::UnicodeString Msg, int AHelpContext) : Dbaccess::EDAError(Msg, AHelpContext) { }
	/* Exception.CreateFmtHelp */ inline __fastcall EOraError(const System::UnicodeString Msg, const System::TVarRec *Args, const int Args_High, int AHelpContext) : Dbaccess::EDAError(Msg, Args, Args_High, AHelpContext) { }
	/* Exception.CreateResHelp */ inline __fastcall EOraError(System::PResStringRec ResStringRec, int AHelpContext) : Dbaccess::EDAError(ResStringRec, AHelpContext) { }
	/* Exception.CreateResFmtHelp */ inline __fastcall EOraError(System::PResStringRec ResStringRec, const System::TVarRec *Args, const int Args_High, int AHelpContext) : Dbaccess::EDAError(ResStringRec, Args, Args_High, AHelpContext) { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraError : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	__classmethod void __fastcall Check(Oracall::_OCIErrorGet OCIErrorGet, int Status, bool UnicodeEnv, void * hOCIError);
	__classmethod void __fastcall DoOraError(Oracall::_OCIErrorGet OCIErrorGet, int ErrorCode, bool UnicodeEnv, void * hOCIError);
	__classmethod int __fastcall GetOraError(Oracall::_OCIErrorGet OCIErrorGet, int ErrorCode, bool UnicodeEnv, void * hOCIError, System::UnicodeString &ErrorMsg);
public:
	/* TObject.Create */ inline __fastcall TOraError() : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TOraError() { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
static constexpr System::Int8 erKeyViol = System::Int8(0x0);
static constexpr System::Int8 erRequiredFieldMissing = System::Int8(0x1);
static constexpr System::Int8 erCheck = System::Int8(0x2);
static constexpr System::Int8 erLockRecord = System::Int8(-54);
static constexpr short erParentKeyNotFound = short(-2291);
static constexpr short erChildRecordCount = short(-2292);
extern DELPHI_PACKAGE void __fastcall RaiseError(System::UnicodeString Msg);
extern DELPHI_PACKAGE void __fastcall RaiseOraError(int ErrorCode, System::UnicodeString Msg);
}	/* namespace Oraerror */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_ORAERROR)
using namespace Oraerror;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// OraerrorHPP
