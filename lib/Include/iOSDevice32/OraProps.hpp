﻿// CodeGear C++Builder
// Copyright (c) 1995, 2018 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'OraProps.pas' rev: 33.00 (iOS)

#ifndef OrapropsHPP
#define OrapropsHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>

//-- user supplied -----------------------------------------------------------

namespace Oraprops
{
//-- forward type declarations -----------------------------------------------
//-- type declarations -------------------------------------------------------
//-- var, const, procedure ---------------------------------------------------
static constexpr System::Word prOCIBase = System::Word(0x3e8);
static constexpr System::Word prThreadSafety = System::Word(0x3ea);
static constexpr System::Word prAutoClose = System::Word(0x3eb);
static constexpr System::Word prErrorOffset = System::Word(0x3ec);
static constexpr System::Word prDateFormat = System::Word(0x3ee);
static constexpr System::Word prDeferredLobRead = System::Word(0x3ef);
static constexpr System::Word prConnectMode = System::Word(0x3f0);
static constexpr System::Word prCharLength = System::Word(0x3f1);
static constexpr System::Word prCacheLobs = System::Word(0x3f2);
static constexpr System::Word prEnableIntegers = System::Word(0x3f3);
static constexpr System::Word prInternalName = System::Word(0x3f4);
static constexpr System::Word prScrollableCursor = System::Word(0x3f5);
static constexpr System::Word prStoreRowId = System::Word(0x3f6);
static constexpr System::Word prCharset = System::Word(0x3f7);
static constexpr System::Word prDateLanguage = System::Word(0x3f8);
static constexpr System::Word prTimeStampFormat = System::Word(0x3f9);
static constexpr System::Word prTimeStampTZFormat = System::Word(0x3fa);
static constexpr System::Word prRawAsString = System::Word(0x3fb);
static constexpr System::Word prNumberAsString = System::Word(0x3fc);
static constexpr System::Word prNumericCharacters = System::Word(0x3fd);
static constexpr System::Word prEnableNumbers = System::Word(0x3fe);
static constexpr System::Word prIntegerPrecision = System::Word(0x400);
static constexpr System::Word prFloatPrecision = System::Word(0x401);
static constexpr System::Word prTemporaryLobUpdate = System::Word(0x402);
static constexpr System::Word prDisconnectMode = System::Word(0x403);
static constexpr System::Word prInactiveTimeout = System::Word(0x404);
static constexpr System::Word prResumeTimeout = System::Word(0x405);
static constexpr System::Word prTransactionName = System::Word(0x406);
static constexpr System::Word prHasObjectFields = System::Word(0x408);
static constexpr System::Word prStatementCache = System::Word(0x409);
static constexpr System::Word prStatementCacheSize = System::Word(0x40a);
static constexpr System::Word prEnabled = System::Word(0x40b);
static constexpr System::Word prTimeout = System::Word(0x40c);
static constexpr System::Word prPersistent = System::Word(0x40d);
static constexpr System::Word prOperations = System::Word(0x40e);
static constexpr System::Word prPrefetchRows = System::Word(0x40f);
static constexpr System::Word prTransactionResume = System::Word(0x410);
static constexpr System::Word prRollbackSegment = System::Word(0x411);
static constexpr System::Word prHomeName = System::Word(0x412);
static constexpr System::Word prDirect = System::Word(0x413);
static constexpr System::Word prClientIdentifier = System::Word(0x414);
static constexpr System::Word prOptimizerMode = System::Word(0x415);
static constexpr System::Word prUseOCI7 = System::Word(0x416);
static constexpr System::Word prSchema = System::Word(0x417);
static constexpr System::Word prEnableSQLTimeStamp = System::Word(0x418);
static constexpr System::Word prIntervalAsString = System::Word(0x419);
static constexpr System::Word prSmallintPrecision = System::Word(0x41a);
static constexpr System::Word prLargeintPrecision = System::Word(0x41b);
static constexpr System::Word prBCDPrecision = System::Word(0x41c);
static constexpr System::Word prFmtBCDPrecision = System::Word(0x41d);
static constexpr System::Word prCheckParamHasDefault = System::Word(0x41e);
static constexpr System::Word prUseDefaultDataTypes = System::Word(0x420);
static constexpr System::Word prEnableLargeint = System::Word(0x421);
static constexpr System::Word prUnicodeEnvironment = System::Word(0x422);
static constexpr System::Word prDirectPath = System::Word(0x423);
static constexpr System::Word prEnableWideOraClob = System::Word(0x424);
static constexpr System::Word prAlerterTimeout = System::Word(0x425);
static constexpr System::Word prAlerterInterval = System::Word(0x426);
static constexpr System::Word prAlerterEventType = System::Word(0x427);
static constexpr System::Word prAlerterSelfEvents = System::Word(0x428);
static constexpr System::Word prUnicodeAsNational = System::Word(0x429);
static constexpr System::Word prQueryResultOnly = System::Word(0x42a);
static constexpr System::Word prSubscriptionPort = System::Word(0x42b);
static constexpr System::Word prProcNamedParams = System::Word(0x42c);
static constexpr System::Word prHideRowId = System::Word(0x42d);
static constexpr System::Word prPrefetchLobSize = System::Word(0x42e);
}	/* namespace Oraprops */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_ORAPROPS)
using namespace Oraprops;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// OrapropsHPP
