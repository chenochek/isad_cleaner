﻿// CodeGear C++Builder
// Copyright (c) 1995, 2018 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'OraServerEnumerator.pas' rev: 33.00 (iOS)

#ifndef OraserverenumeratorHPP
#define OraserverenumeratorHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.Classes.hpp>
#include <System.SysUtils.hpp>
#include <CRServerEnumerator.hpp>
#include <OraCall.hpp>

//-- user supplied -----------------------------------------------------------

namespace Oraserverenumerator
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TOraServerEnumerator;
//-- type declarations -------------------------------------------------------
#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraServerEnumerator : public Crserverenumerator::TCRServerEnumerator
{
	typedef Crserverenumerator::TCRServerEnumerator inherited;
	
private:
	bool FDirect;
	System::UnicodeString FHomeName;
	
public:
	virtual bool __fastcall SetProp(int Prop, const System::Variant &Value);
	virtual bool __fastcall GetProp(int Prop, System::Variant &Value);
	System::UnicodeString __fastcall GetTNSFileName(Oracall::TOracleHome* Home);
	virtual void __fastcall GetServerList(System::Classes::TStrings* List);
public:
	/* TCRServerEnumerator.Create */ inline __fastcall virtual TOraServerEnumerator() : Crserverenumerator::TCRServerEnumerator() { }
	
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TOraServerEnumerator() { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
}	/* namespace Oraserverenumerator */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_ORASERVERENUMERATOR)
using namespace Oraserverenumerator;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// OraserverenumeratorHPP
