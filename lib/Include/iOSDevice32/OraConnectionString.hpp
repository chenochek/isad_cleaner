﻿// CodeGear C++Builder
// Copyright (c) 1995, 2018 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'OraConnectionString.pas' rev: 33.00 (iOS)

#ifndef OraconnectionstringHPP
#define OraconnectionstringHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.SysUtils.hpp>
#include <System.Classes.hpp>
#include <System.Variants.hpp>
#include <CRParser.hpp>
#include <CRAccess.hpp>
#include <CRConnectionString.hpp>
#include <DAConsts.hpp>
#include <OraConsts.hpp>
#include <OraCall.hpp>
#include <OraClasses.hpp>

//-- user supplied -----------------------------------------------------------

namespace Oraconnectionstring
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TOraConnectionStringBuilder;
//-- type declarations -------------------------------------------------------
#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraConnectionStringBuilder : public Crconnectionstring::TCRConnectionStringBuilder
{
	typedef Crconnectionstring::TCRConnectionStringBuilder inherited;
	
private:
	Oracall::TDirectServerInfo* FServerInfo;
	
protected:
	virtual void __fastcall InitParams();
	virtual System::Variant __fastcall GetParamValue(Crconnectionstring::TConnectionStringParam* Param);
	virtual void __fastcall SetParamValue(Crconnectionstring::TConnectionStringParam* Param, const System::Variant &Value);
	
public:
	__fastcall virtual TOraConnectionStringBuilder(Crconnectionstring::TGetConnectionStringParamMethod GetPropMethod, Crconnectionstring::TSetConnectionStringParamMethod SetPropMethod);
	__fastcall virtual ~TOraConnectionStringBuilder();
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
static constexpr System::Int8 cpHost = System::Int8(-101);
static constexpr System::Int8 cpPort = System::Int8(-102);
static constexpr System::Int8 cpSID = System::Int8(-103);
static constexpr System::Int8 cpServiceName = System::Int8(-104);
}	/* namespace Oraconnectionstring */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_ORACONNECTIONSTRING)
using namespace Oraconnectionstring;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// OraconnectionstringHPP
