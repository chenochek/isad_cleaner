﻿// CodeGear C++Builder
// Copyright (c) 1995, 2018 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'OraLoader.pas' rev: 33.00 (iOS)

#ifndef OraloaderHPP
#define OraloaderHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.Classes.hpp>
#include <System.SysUtils.hpp>
#include <Data.DB.hpp>
#include <System.Variants.hpp>
#include <CLRClasses.hpp>
#include <CRTypes.hpp>
#include <MemData.hpp>
#include <MemDS.hpp>
#include <CRAccess.hpp>
#include <DBAccess.hpp>
#include <DALoader.hpp>
#include <OraCall.hpp>
#include <OraClasses.hpp>
#include <Ora.hpp>

//-- user supplied -----------------------------------------------------------

namespace Oraloader
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TDPColumn;
class DELPHICLASS TOraLoaderOptions;
class DELPHICLASS TOraLoader;
//-- type declarations -------------------------------------------------------
#pragma pack(push,4)
class PASCALIMPLEMENTATION TDPColumn : public Daloader::TDAColumn
{
	typedef Daloader::TDAColumn inherited;
	
private:
	int FSize;
	int FPrecision;
	int FScale;
	System::UnicodeString FDateFormat;
	int __fastcall GetSize();
	void __fastcall SetSize(int Value);
	
protected:
	virtual Daloader::TDAColumnDataType __fastcall GetDataType();
	virtual void __fastcall SetFieldType(Data::Db::TFieldType Value);
	
public:
	__fastcall virtual TDPColumn(System::Classes::TCollection* Collection);
	
__published:
	__property DataType = {stored=false};
	__property int Size = {read=GetSize, write=SetSize, default=0};
	__property int Precision = {read=FPrecision, write=FPrecision, default=0};
	__property int Scale = {read=FScale, write=FScale, default=0};
	__property System::UnicodeString DateFormat = {read=FDateFormat, write=FDateFormat};
public:
	/* TCollectionItem.Destroy */ inline __fastcall virtual ~TDPColumn() { }
	
};

#pragma pack(pop)

enum DECLSPEC_DENUM TLoadMode : unsigned char { lmDirect, lmDML };

typedef void __fastcall (__closure *TDPPutDataEvent)(TOraLoader* Sender);

typedef void __fastcall (__closure *TDPGetColumnDataEvent)(System::TObject* Sender, TDPColumn* Column, int Row, System::Variant &Value, bool &IsEOF);

enum DECLSPEC_DENUM TDPErrorAction : unsigned char { dpAbort, dpFail, dpIgnore };

typedef void __fastcall (__closure *TDPErrorEvent)(TOraLoader* Sender, System::Sysutils::Exception* E, int Col, int Row, TDPErrorAction &Action);

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraLoaderOptions : public Daloader::TDALoaderOptions
{
	typedef Daloader::TDALoaderOptions inherited;
	
private:
	TLoadMode FLoadMode;
	void __fastcall SetLoadMode(TLoadMode Value);
	
protected:
	virtual void __fastcall AssignTo(System::Classes::TPersistent* Dest);
	
public:
	__fastcall virtual TOraLoaderOptions(Daloader::TDALoader* Owner);
	
__published:
	__property QuoteNames = {default=0};
	__property UseBlankValues = {default=1};
	__property TLoadMode LoadMode = {read=FLoadMode, write=SetLoadMode, default=0};
public:
	/* TPersistent.Destroy */ inline __fastcall virtual ~TOraLoaderOptions() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraLoader : public Daloader::TDALoader
{
	typedef Daloader::TDALoader inherited;
	
private:
	TDPPutDataEvent FOnPutData;
	TDPGetColumnDataEvent FOnGetColumnData;
	TDPErrorEvent FOnError;
	Ora::TOraSession* __fastcall GetSession();
	void __fastcall SetSession(Ora::TOraSession* Value);
	void __fastcall SetOnError(const TDPErrorEvent Value);
	void __fastcall SetOnPutData(const TDPPutDataEvent Value);
	void __fastcall SetOnGetColumnData(const TDPGetColumnDataEvent Value);
	TOraLoaderOptions* __fastcall GetOptions();
	HIDESBASE void __fastcall SetOptions(TOraLoaderOptions* const Value);
	void __fastcall DAPutDataEvent(Daloader::TDALoader* Sender);
	void __fastcall DAGetColumnDataEvent(System::TObject* Sender, Daloader::TDAColumn* Column, int Row, System::Variant &Value, bool &IsEOF);
	void __fastcall DoOnError(System::Sysutils::Exception* E, int Col, int Row, Oraclasses::_TDPErrorAction &Action);
	
protected:
	virtual Craccess::TCRLoaderClass __fastcall GetInternalLoaderClass();
	virtual void __fastcall SetInternalLoader(Craccess::TCRLoader* Value);
	__classmethod virtual Daloader::TDAColumnClass __fastcall GetColumnClass();
	virtual Memds::TFieldTypeMapClass __fastcall GetFieldTypeMapClass();
	virtual Dbaccess::TCustomDAConnection* __fastcall UsedConnection();
	virtual bool __fastcall NeedRecreateColumns();
	virtual void __fastcall ReadColumn(Daloader::TDAColumn* Column, Craccess::TCRLoaderColumn* CRColumn);
	virtual void __fastcall WriteColumn(Daloader::TDAColumn* Column, Craccess::TCRLoaderColumn* CRColumn);
	virtual Daloader::TDALoaderOptions* __fastcall CreateOptions();
	virtual void __fastcall DefineProperties(System::Classes::TFiler* Filer);
	void __fastcall ReadLoadMode(System::Classes::TReader* Reader);
	TLoadMode __fastcall GetLoadMode();
	void __fastcall SetLoadMode(TLoadMode Value);
	
public:
	__property TLoadMode LoadMode = {read=GetLoadMode, write=SetLoadMode, nodefault};
	
__published:
	__property Ora::TOraSession* Session = {read=GetSession, write=SetSession};
	__property TOraLoaderOptions* Options = {read=GetOptions, write=SetOptions};
	__property TableName = {default=0};
	__property Columns;
	__property TDPPutDataEvent OnPutData = {read=FOnPutData, write=SetOnPutData};
	__property TDPGetColumnDataEvent OnGetColumnData = {read=FOnGetColumnData, write=SetOnGetColumnData};
	__property TDPErrorEvent OnError = {read=FOnError, write=SetOnError};
	__property OnProgress;
public:
	/* TDALoader.Create */ inline __fastcall virtual TOraLoader(System::Classes::TComponent* Owner) : Daloader::TDALoader(Owner) { }
	/* TDALoader.Destroy */ inline __fastcall virtual ~TOraLoader() { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
}	/* namespace Oraloader */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_ORALOADER)
using namespace Oraloader;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// OraloaderHPP
