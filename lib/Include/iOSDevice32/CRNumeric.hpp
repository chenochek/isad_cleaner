﻿// CodeGear C++Builder
// Copyright (c) 1995, 2018 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'CRNumeric.pas' rev: 33.00 (iOS)

#ifndef CrnumericHPP
#define CrnumericHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.SysUtils.hpp>
#include <Data.DBConsts.hpp>
#include <Data.FmtBcd.hpp>
#include <DAConsts.hpp>
#include <CRTypes.hpp>
#include <CRFunctions.hpp>
#include <CRBigInteger.hpp>
#include <MemUtils.hpp>

//-- user supplied -----------------------------------------------------------

namespace Crnumeric
{
//-- forward type declarations -----------------------------------------------
struct DB_NUMERIC;
//-- type declarations -------------------------------------------------------
typedef DB_NUMERIC *PDBNumeric;

#pragma pack(push,1)
struct DECLSPEC_DRECORD DB_NUMERIC
{
public:
	System::Byte precision;
	System::Byte scale;
	System::Byte sign;
	
public:
	union
	{
		struct 
		{
			__int64 ValLow;
			__int64 ValHigh;
		};
		struct 
		{
			System::StaticArray<System::Byte, 16> Val;
		};
		
	};
};
#pragma pack(pop)


typedef DB_NUMERIC TDBNumeric;

//-- var, const, procedure ---------------------------------------------------
static constexpr int SizeOf_TDBNumeric = int(0x13);
extern DELPHI_PACKAGE System::UnicodeString __fastcall DBNumericToStr(const DB_NUMERIC &Value);
extern DELPHI_PACKAGE Data::Fmtbcd::TBcd __fastcall DBNumericToBCD(const DB_NUMERIC &Value);
extern DELPHI_PACKAGE double __fastcall DBNumericToDouble(const DB_NUMERIC &Value);
extern DELPHI_PACKAGE DB_NUMERIC __fastcall BcdToDBNumeric(const Data::Fmtbcd::TBcd &Bcd);
extern DELPHI_PACKAGE DB_NUMERIC __fastcall DoubleToDBNumeric(double Value, int Precision, int Scale);
}	/* namespace Crnumeric */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_CRNUMERIC)
using namespace Crnumeric;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// CrnumericHPP
