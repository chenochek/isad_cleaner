﻿// CodeGear C++Builder
// Copyright (c) 1995, 2018 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'CRServerEnumerator.pas' rev: 33.00 (iOS)

#ifndef CrserverenumeratorHPP
#define CrserverenumeratorHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.Classes.hpp>
#include <System.SysUtils.hpp>
#include <System.SyncObjs.hpp>

//-- user supplied -----------------------------------------------------------

namespace Crserverenumerator
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TCRServerEnumerator;
//-- type declarations -------------------------------------------------------
#pragma pack(push,4)
class PASCALIMPLEMENTATION TCRServerEnumerator : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	__fastcall virtual TCRServerEnumerator();
	virtual bool __fastcall SetProp(int Prop, const System::Variant &Value);
	virtual bool __fastcall GetProp(int Prop, System::Variant &Value);
	virtual void __fastcall GetServerList(System::Classes::TStrings* List);
public:
	/* TObject.Destroy */ inline __fastcall virtual ~TCRServerEnumerator() { }
	
};

#pragma pack(pop)

_DECLARE_METACLASS(System::TMetaClass, TCRServerEnumeratorClass);

//-- var, const, procedure ---------------------------------------------------
}	/* namespace Crserverenumerator */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_CRSERVERENUMERATOR)
using namespace Crserverenumerator;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// CrserverenumeratorHPP
