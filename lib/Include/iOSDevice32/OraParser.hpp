﻿// CodeGear C++Builder
// Copyright (c) 1995, 2018 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'OraParser.pas' rev: 33.00 (iOS)

#ifndef OraparserHPP
#define OraparserHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.Classes.hpp>
#include <System.SysUtils.hpp>
#include <CRTypes.hpp>
#include <CRParser.hpp>

//-- user supplied -----------------------------------------------------------

namespace Oraparser
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TOraParser;
class DELPHICLASS TTNSParser;
//-- type declarations -------------------------------------------------------
#pragma pack(push,4)
class PASCALIMPLEMENTATION TOraParser : public Crparser::TSQLParser
{
	typedef Crparser::TSQLParser inherited;
	
private:
	bool FInWrapped;
	
protected:
	virtual void __fastcall InitParser();
	
public:
	virtual int __fastcall GetNext(/* out */ System::UnicodeString &Lexem);
	virtual bool __fastcall IsMacroAllowed(int Code);
	virtual bool __fastcall IsSelectModifier(int Code);
	__classmethod virtual bool __fastcall IsNumericMacroNameAllowed();
	__classmethod virtual bool __fastcall IsFunctionOrConst(const System::UnicodeString UpperedName);
	__classmethod virtual bool __fastcall IsQuasiColumn(const System::UnicodeString UpperedName);
public:
	/* TParser.Create */ inline __fastcall virtual TOraParser(const System::UnicodeString Text)/* overload */ : Crparser::TSQLParser(Text) { }
	/* TParser.Create */ inline __fastcall TOraParser(System::Classes::TStream* const Stream, Clrclasses::Encoding* AEncoding)/* overload */ : Crparser::TSQLParser(Stream, AEncoding) { }
	/* TParser.Create */ inline __fastcall virtual TOraParser(System::Classes::TStream* const Stream, __int64 ASize, Clrclasses::Encoding* AEncoding)/* overload */ : Crparser::TSQLParser(Stream, ASize, AEncoding) { }
	/* TParser.Destroy */ inline __fastcall virtual ~TOraParser() { }
	
};

#pragma pack(pop)

#pragma pack(push,4)
class PASCALIMPLEMENTATION TTNSParser : public Crparser::TParser
{
	typedef Crparser::TParser inherited;
	
protected:
	virtual bool __fastcall IsInlineComment(System::WideChar Ch, int Pos);
	
public:
	System::UnicodeString __fastcall GetText();
public:
	/* TParser.Create */ inline __fastcall virtual TTNSParser(const System::UnicodeString Text)/* overload */ : Crparser::TParser(Text) { }
	/* TParser.Create */ inline __fastcall TTNSParser(System::Classes::TStream* const Stream, Clrclasses::Encoding* AEncoding)/* overload */ : Crparser::TParser(Stream, AEncoding) { }
	/* TParser.Create */ inline __fastcall virtual TTNSParser(System::Classes::TStream* const Stream, __int64 ASize, Clrclasses::Encoding* AEncoding)/* overload */ : Crparser::TParser(Stream, ASize, AEncoding) { }
	/* TParser.Destroy */ inline __fastcall virtual ~TTNSParser() { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
static constexpr System::Word lxOraFirst = System::Word(0x3e8);
static constexpr System::Word lxALTER = System::Word(0x3e8);
static constexpr System::Word lxBODY = System::Word(0x3e9);
static constexpr System::Word lxCONNECT = System::Word(0x3ea);
static constexpr System::Word lxCREATE = System::Word(0x3eb);
static constexpr System::Word lxDEFINE = System::Word(0x3ec);
static constexpr System::Word lxDISCONNECT = System::Word(0x3ed);
static constexpr System::Word lxEDITIONABLE = System::Word(0x3ee);
static constexpr System::Word lxEXEC = System::Word(0x3ef);
static constexpr System::Word lxEXIT = System::Word(0x3f0);
static constexpr System::Word lxEXPLAIN = System::Word(0x3f1);
static constexpr System::Word lxFUNCTION = System::Word(0x3f2);
static constexpr System::Word lxJAVA = System::Word(0x3f3);
static constexpr System::Word lxMERGE = System::Word(0x3f4);
static constexpr System::Word lxNONEDITIONABLE = System::Word(0x3f5);
static constexpr System::Word lxPACKAGE = System::Word(0x3f6);
static constexpr System::Word lxPARTITION = System::Word(0x3f7);
static constexpr System::Word lxPAUSE = System::Word(0x3f8);
static constexpr System::Word lxPROCEDURE = System::Word(0x3f9);
static constexpr System::Word lxPROMPT = System::Word(0x3fa);
static constexpr System::Word lxQUIT = System::Word(0x3fb);
static constexpr System::Word lxREM = System::Word(0x3fc);
static constexpr System::Word lxREMARK = System::Word(0x3fd);
static constexpr System::Word lxREPLACE = System::Word(0x3fe);
static constexpr System::Word lxSTART = System::Word(0x3ff);
static constexpr System::Word lxTRIGGER = System::Word(0x400);
static constexpr System::Word lxTYPE = System::Word(0x401);
static constexpr System::Word lxUNDEFINE = System::Word(0x402);
static constexpr System::Word lxUNIQUE = System::Word(0x403);
static constexpr System::Word lxWRAPPED = System::Word(0x404);
static constexpr System::Word lxACCEPT = System::Word(0x405);
static constexpr System::Word lxGRANT = System::Word(0x406);
static constexpr System::Word lxREVOKE = System::Word(0x407);
}	/* namespace Oraparser */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_ORAPARSER)
using namespace Oraparser;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// OraparserHPP
