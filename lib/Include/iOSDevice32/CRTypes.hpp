﻿// CodeGear C++Builder
// Copyright (c) 1995, 2018 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'CRTypes.pas' rev: 33.00 (iOS)

#ifndef CrtypesHPP
#define CrtypesHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.Types.hpp>
#include <System.Generics.Collections.hpp>
#include <System.Classes.hpp>
#include <System.SysUtils.hpp>
#include <System.SyncObjs.hpp>
#include <System.Generics.Defaults.hpp>

//-- user supplied -----------------------------------------------------------

namespace Crtypes
{
//-- forward type declarations -----------------------------------------------
struct AnsiString;
__interface DELPHIINTERFACE IScAsyncResult;
typedef System::DelphiInterface<IScAsyncResult> _di_IScAsyncResult;
struct TIntValueItem;
class DELPHICLASS TIntValueStringList;
struct TStrValueItem;
class DELPHICLASS TStrValueStringList;
//-- type declarations -------------------------------------------------------
typedef int Int;

typedef short Int16;

typedef int Int32;

typedef System::Word UInt16;

typedef unsigned UInt32;

typedef System::Int8 SByte;

typedef void * IntPtr;

typedef void * MulticastDelegate;

struct DECLSPEC_DRECORD AnsiString
{
	
private:
	class DELPHICLASS TDisposer;
	#pragma pack(push,4)
	class PASCALIMPLEMENTATION TDisposer : public System::TObject
	{
		typedef System::TObject inherited;
		
	private:
		System::StaticArray<void *, 4> FInline;
		System::TArray__1<void *> FOverflow;
		int FCount;
		void __fastcall AddDispose(void * P);
		void __fastcall Flush();
		
	public:
		__fastcall virtual ~TDisposer();
	public:
		/* TObject.Create */ inline __fastcall TDisposer() : System::TObject() { }
		
	};
	
	#pragma pack(pop)
	
	
public:
	char * operator[](int index) { return this->Chars[index]; }
	
private:
	char *FPtr;
	int FLength;
	TDisposer* FDisposer;
	void __fastcall AddDispose(void * P);
	char * __fastcall GetChars(int Index);
	void __fastcall SetChars(int Index, char * Value);
	void __fastcall SetPtr(char * Value);
	
public:
	void __fastcall SetLength(int NewLength);
	int __fastcall Length();
	static bool __fastcall _op_Equality(const AnsiString &Left, const AnsiString &Right);
	bool __fastcall operator==(const AnsiString& __rhs) { return AnsiString::_op_Equality(*this, __rhs); };
	static bool __fastcall _op_Inequality(const AnsiString &Left, const AnsiString &Right);
	bool __fastcall operator!=(const AnsiString& __rhs) { return AnsiString::_op_Inequality(*this, __rhs); };
	__fastcall operator char *();
	static AnsiString __fastcall _op_Explicit(const char * Ptr);
	__fastcall operator System::UnicodeString();
	static AnsiString __fastcall _op_Implicit(const System::UnicodeString Str);
	AnsiString& __fastcall operator=(const System::UnicodeString Str) { *this = AnsiString::_op_Implicit(Str); return *this; };
	__fastcall operator System::Variant();
	static AnsiString __fastcall _op_Explicit(const System::Variant &v);
	__fastcall operator System::TArray__1<System::Byte>();
	static AnsiString __fastcall _op_Explicit(const System::TArray__1<System::Byte> b);
	__property char * Chars[int index] = {read=GetChars, write=SetChars};
	__property char * Ptr = {read=FPtr, write=SetPtr};
};


typedef System::UnicodeString WideString;

typedef System::Byte AnsiChar;

typedef char * PAnsiChar;

typedef char * TValueArr;

typedef char * PAChar;

typedef System::WideChar * PWChar;

typedef System::TArray__1<System::UnicodeString> TStringArray;

typedef System::TArray__1<System::Byte> TByteArr;

typedef System::DynamicArray<int> TIntArr;

typedef System::DynamicArray<System::Word> TWordArr;

typedef System::DynamicArray<unsigned> TLongWordArr;

typedef System::StaticArray<unsigned, 1044> TCardinalConstArray;

typedef TCardinalConstArray *TCardinalArray;

typedef System::StaticArray<unsigned __int64, 1044> TUInt64ConstArray;

typedef TUInt64ConstArray *TUInt64Array;

typedef NativeUInt HWND;

typedef void * LPSTR;

typedef void * LPWSTR;

__interface  INTERFACE_UUID("{0426C822-A1FE-4760-AB89-60B6CB2FE1F3}") IScAsyncResult  : public System::IInterface 
{
	virtual System::TObject* __fastcall get_AsyncState() = 0 ;
	virtual System::Syncobjs::TEvent* __fastcall get_AsyncWaitHandle() = 0 ;
	virtual bool __fastcall get_IsCompleted() = 0 ;
	__property System::TObject* AsyncState = {read=get_AsyncState};
	__property System::Syncobjs::TEvent* AsyncWaitHandle = {read=get_AsyncWaitHandle};
	__property bool IsCompleted = {read=get_IsCompleted};
};

typedef void __fastcall (__closure *AsyncCallback)(_di_IScAsyncResult Result);

typedef System::Generics::Collections::TThreadList__1<System::TObject*> * TCRThreadList;

enum DECLSPEC_DENUM TMachineType : unsigned char { mtUnknown, mtAM33, mtAMD64, mtARM, mtEBC, mtI386, mtIA64, mtM32R, mtMIPS16, mtMIPSFPU, mtMIPSFPU16, mtPOWERPC, mtPOWERPCFP, mtR4000, mtSH3, mtSH3DSP, mtSH4, mtSH5, mtTHUMB, mtWCEMIPSV2 };

struct DECLSPEC_DRECORD TIntValueItem
{
public:
	System::UnicodeString Key;
	int Value;
};


typedef TIntValueItem *PIntValueItem;

#pragma pack(push,4)
class PASCALIMPLEMENTATION TIntValueStringList : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	System::UnicodeString operator[](int Index) { return this->Keys[Index]; }
	
private:
	System::Classes::TList* FCodeStrings;
	bool FSorted;
	System::UnicodeString __fastcall GetKey(int Index);
	int __fastcall GetValue(int Index);
	void __fastcall SetValue(int Index, const int Value);
	int __fastcall GetCount();
	
protected:
	virtual void __fastcall ListChanged();
	bool __fastcall Find(const System::UnicodeString Key, /* out */ int &Index);
	
public:
	__fastcall virtual TIntValueStringList();
	__fastcall virtual ~TIntValueStringList();
	void __fastcall Assign(TIntValueStringList* Source);
	int __fastcall Add(const System::UnicodeString S, int Code);
	void __fastcall Insert(int Index, const System::UnicodeString Key, int Value);
	void __fastcall Delete(int Index);
	void __fastcall Clear();
	int __fastcall IndexOf(const System::UnicodeString Key);
	bool __fastcall TryGetValue(const System::UnicodeString Key, /* out */ int &Value);
	void __fastcall Sort();
	__property System::UnicodeString Keys[int Index] = {read=GetKey/*, default*/};
	__property int Values[int Index] = {read=GetValue, write=SetValue};
	__property int Count = {read=GetCount, nodefault};
};

#pragma pack(pop)

struct DECLSPEC_DRECORD TStrValueItem
{
public:
	System::UnicodeString Key;
	System::UnicodeString Value;
};


typedef TStrValueItem *PStrValueItem;

#pragma pack(push,4)
class PASCALIMPLEMENTATION TStrValueStringList : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	System::UnicodeString operator[](const System::UnicodeString Key) { return this->Names[Key]; }
	
private:
	System::Classes::TList* FStrValueStrings;
	bool FSorted;
	System::UnicodeString __fastcall GetKey(int Index);
	System::UnicodeString __fastcall GetValue(int Index);
	void __fastcall SetValue(int Index, const System::UnicodeString Value);
	System::UnicodeString __fastcall GetName(const System::UnicodeString Key);
	void __fastcall SetName(const System::UnicodeString Key, const System::UnicodeString Value);
	int __fastcall GetCount();
	
protected:
	bool __fastcall Find(const System::UnicodeString Key, /* out */ int &Index);
	virtual void __fastcall CheckNewValue(const System::UnicodeString Key, const System::UnicodeString Value);
	
public:
	__fastcall TStrValueStringList();
	__fastcall virtual ~TStrValueStringList();
	void __fastcall Assign(TStrValueStringList* Source);
	int __fastcall Add(const System::UnicodeString Key, const System::UnicodeString Value);
	void __fastcall Insert(int Index, const System::UnicodeString Key, const System::UnicodeString Value);
	void __fastcall Delete(int Index);
	void __fastcall Clear();
	void __fastcall Remove(const System::UnicodeString Key);
	int __fastcall IndexOf(const System::UnicodeString Key);
	bool __fastcall TryGetValue(const System::UnicodeString Key, /* out */ System::UnicodeString &Value);
	void __fastcall Sort();
	__property System::UnicodeString Keys[int Index] = {read=GetKey};
	__property System::UnicodeString Values[int Index] = {read=GetValue, write=SetValue};
	__property System::UnicodeString Names[const System::UnicodeString Key] = {read=GetName, write=SetName/*, default*/};
	__property int Count = {read=GetCount, nodefault};
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
#define EMPTY_GUID u"{00000000-0000-0000-0000-000000000000}"
static constexpr System::Int8 pidDevartWinPlatforms = System::Int8(0x3);
static constexpr System::Word pidDevartAllPlatforms = System::Word(0x94df);
}	/* namespace Crtypes */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_CRTYPES)
using namespace Crtypes;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// CrtypesHPP
