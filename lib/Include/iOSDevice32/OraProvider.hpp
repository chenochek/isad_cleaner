﻿// CodeGear C++Builder
// Copyright (c) 1995, 2018 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'OraProvider.pas' rev: 33.00 (iOS)

#ifndef OraproviderHPP
#define OraproviderHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.Classes.hpp>
#include <System.SysUtils.hpp>
#include <Datasnap.Provider.hpp>
#include <Data.DB.hpp>
#include <Datasnap.DBClient.hpp>
#include <CRTypes.hpp>
#include <Ora.hpp>
#include <System.Generics.Collections.hpp>
#include <System.Generics.Defaults.hpp>
#include <System.Types.hpp>

//-- user supplied -----------------------------------------------------------

namespace Oraprovider
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TOraProvider;
//-- type declarations -------------------------------------------------------
class PASCALIMPLEMENTATION TOraProvider : public Datasnap::Provider::TDataSetProvider
{
	typedef Datasnap::Provider::TDataSetProvider inherited;
	
__published:
	virtual System::OleVariant __fastcall InternalGetParams(Data::Db::TParamTypes Types = (Data::Db::TParamTypes() << Data::Db::TParamType::ptUnknown << Data::Db::TParamType::ptInput << Data::Db::TParamType::ptOutput << Data::Db::TParamType::ptInputOutput << Data::Db::TParamType::ptResult ));
public:
	/* TDataSetProvider.Create */ inline __fastcall virtual TOraProvider(System::Classes::TComponent* AOwner) : Datasnap::Provider::TDataSetProvider(AOwner) { }
	/* TDataSetProvider.Destroy */ inline __fastcall virtual ~TOraProvider() { }
	
};


//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE void __fastcall UseOraProv(void);
}	/* namespace Oraprovider */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_ORAPROVIDER)
using namespace Oraprovider;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// OraproviderHPP
