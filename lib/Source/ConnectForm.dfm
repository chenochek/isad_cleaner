object ConnectForm: TConnectForm
  Left = 521
  Top = 242
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Connect'
  ClientHeight = 287
  ClientWidth = 291
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  Scaled = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel: TPanel
    Left = 8
    Top = 8
    Width = 276
    Height = 243
    BevelInner = bvRaised
    BevelOuter = bvLowered
    FullRepaint = False
    TabOrder = 0
    object pUserName: TPanel
      Left = 2
      Top = 3
      Width = 272
      Height = 34
      BevelOuter = bvNone
      TabOrder = 0
      object lbUsername: TLabel
        Left = 15
        Top = 10
        Width = 73
        Height = 13
        AutoSize = False
        Caption = 'Username'
        Layout = tlCenter
      end
      object edUsername: TEdit
        Left = 103
        Top = 6
        Width = 164
        Height = 21
        AutoSelect = False
        MaxLength = 32767
        TabOrder = 0
      end
    end
    object pPassword: TPanel
      Left = 2
      Top = 37
      Width = 272
      Height = 34
      BevelOuter = bvNone
      TabOrder = 1
      object lbPassword: TLabel
        Left = 15
        Top = 10
        Width = 73
        Height = 13
        AutoSize = False
        Caption = 'Password'
        Layout = tlCenter
      end
      object edPassword: TEdit
        Left = 103
        Top = 6
        Width = 164
        Height = 21
        AutoSelect = False
        MaxLength = 32767
        PasswordChar = '*'
        TabOrder = 0
      end
    end
    object pServer: TPanel
      Left = 2
      Top = 71
      Width = 272
      Height = 34
      BevelOuter = bvNone
      TabOrder = 2
      object lbServer: TLabel
        Left = 15
        Top = 11
        Width = 73
        Height = 13
        AutoSize = False
        Caption = 'Server'
        Layout = tlCenter
      end
      object edServer: TComboBox
        Left = 103
        Top = 7
        Width = 164
        Height = 21
        DropDownCount = 10
        ItemHeight = 13
        TabOrder = 0
        OnDropDown = edServerDropDown
      end
    end
    object pHome: TPanel
      Left = 2
      Top = 105
      Width = 272
      Height = 34
      BevelOuter = bvNone
      TabOrder = 3
      object lbHome: TLabel
        Left = 15
        Top = 10
        Width = 73
        Height = 13
        AutoSize = False
        Caption = 'Home Name'
        Layout = tlCenter
      end
      object cbHome: TComboBox
        Left = 103
        Top = 6
        Width = 164
        Height = 21
        DropDownCount = 10
        ItemHeight = 13
        TabOrder = 0
        OnChange = cbHomeChange
        OnDropDown = cbHomeDropDown
        Items.Strings = (
          '')
      end
    end
    object pRole: TPanel
      Left = 2
      Top = 139
      Width = 272
      Height = 34
      BevelOuter = bvNone
      TabOrder = 4
      object lbRole: TLabel
        Left = 15
        Top = 10
        Width = 73
        Height = 13
        AutoSize = False
        Caption = 'Connect Mode'
        Layout = tlCenter
      end
      object cbRole: TComboBox
        Left = 103
        Top = 6
        Width = 164
        Height = 21
        DropDownCount = 10
        ItemHeight = 13
        TabOrder = 0
      end
    end
    object pDirect: TPanel
      Left = 2
      Top = 206
      Width = 272
      Height = 34
      BevelOuter = bvNone
      TabOrder = 5
      object lbDirect: TLabel
        Left = 15
        Top = 10
        Width = 26
        Height = 13
        Caption = 'Mode'
      end
      object cbDirect: TCheckBox
        Left = 105
        Top = 8
        Width = 97
        Height = 17
        Caption = 'Direct'
        TabOrder = 0
        OnClick = cbDirectClick
      end
    end
    object pSchema: TPanel
      Left = 2
      Top = 173
      Width = 272
      Height = 34
      BevelOuter = bvNone
      TabOrder = 6
      object lbSchema: TLabel
        Left = 15
        Top = 10
        Width = 73
        Height = 13
        AutoSize = False
        Caption = 'Schema'
        Layout = tlCenter
      end
      object edSchema: TEdit
        Left = 103
        Top = 6
        Width = 164
        Height = 21
        AutoSelect = False
        MaxLength = 32767
        TabOrder = 0
      end
    end
  end
  object pButtons: TPanel
    Left = 8
    Top = 254
    Width = 276
    Height = 34
    BevelOuter = bvNone
    TabOrder = 1
    object btCancel: TButton
      Left = 142
      Top = 4
      Width = 89
      Height = 25
      Cancel = True
      Caption = 'Cancel'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ModalResult = 2
      ParentFont = False
      TabOrder = 1
    end
    object btConnect: TButton
      Left = 46
      Top = 4
      Width = 89
      Height = 25
      Caption = 'Connect'
      Default = True
      TabOrder = 0
      OnClick = btConnectClick
    end
  end
end
