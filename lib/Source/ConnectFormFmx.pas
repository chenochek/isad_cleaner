
//////////////////////////////////////////////////
//  Oracle Data Access Components
//  Copyright � 1998-2018 Devart. All right reserved.
//  Connect Form
//////////////////////////////////////////////////

{$DEFINE FMX}
{$I IdeVer.inc}

unit ConnectFormFmx;

interface

uses
 Classes, SysUtils,
{$IFDEF MSWINDOWS}
  Winapi.Windows, System.Win.Registry,
{$ENDIF}
  System.UITypes, FMX.Types, FMX.Platform,
  FMX.Forms, FMX.Controls, FMX.Edit, FMX.ListBox, FMX.Memo,
{$IFDEF VER18P}
  FMX.StdCtrls, 
{$IFDEF VER21P}
  FMX.ComboEdit,
{$ENDIF}
{$ENDIF}
  CRtypes, DBAccess;

type
  TConnectForm = class(TForm)
    pMain: TPanel;
    pButtons: TPanel;
    btConnect: TButton;
    btCancel: TButton;
    pUserName: TPanel;
    pPassword: TPanel;
    pServer: TPanel;
    pHome: TPanel;
    pRole: TPanel;
    pSchema: TPanel;
    pDirect: TPanel;
    lbUsername: TLabel;
    edUsername: TEdit;
    lbPassword: TLabel;
    edPassword: TEdit;
    lbServer: TLabel;
    edServer: TComboEdit;
    lbHome: TLabel;
    cbHome: TComboEdit;
    lbRole: TLabel;
    cbRole: TComboEdit;
    lbSchema: TLabel;
    edSchema: TEdit;
    lbDirect: TLabel;
    cbDirect: TCheckBox;
    procedure btConnectClick(Sender: TObject);
    procedure edServerPopup(Sender: TObject);
    procedure cbHomeChange(Sender: TObject);
    procedure cbHomePopup(Sender: TObject);
    procedure cbDirectChange(Sender: TObject);

  private
    FConnectDialog: TCustomConnectDialog;
    FRetries: integer;
    FRetry: boolean;
    FInDoInit: Boolean;

    procedure SetConnectDialog(Value: TCustomConnectDialog);
    procedure AssignHomeName(const Value: string);
    procedure AssignDirect(const Value: boolean);
    function GetHomeName: string;
    procedure FillServerList;
    procedure FillHomeName;
  protected
    procedure DoInit; virtual;
    procedure DoConnect; virtual;

  public

  published
    property ConnectDialog: TCustomConnectDialog read FConnectDialog write SetConnectDialog;
  end;

implementation

{$R *.fmx}

uses
  CRFunctions, OraClasses, TypInfo, Ora, OraCall;
  
procedure TConnectForm.DoInit;
var
  CurTop, i: integer;
  ConnectDialogOptions: TConnectDialogOptionArray;
  CurPanel: TPanel;
  CurControl: TStyledControl;
  NeedSetActiveControl: Boolean;
begin
  FInDoInit := true;
  FRetry := False;
  FRetries := FConnectDialog.Retries;
  Caption := FConnectDialog.Caption;

  FillHomeName;
  FillServerList;

  for i := Integer(Low(TConnectMode)) to Integer(High(TConnectMode)) do
    cbRole.Items.Add(GetEnumName(TypeInfo(TConnectMode), i));

  FConnectDialog.GetOptions(ConnectDialogOptions);
  CurTop := 0;
  NeedSetActiveControl := True;
  for i := Low(ConnectDialogOptions) to High(ConnectDialogOptions) do begin
    CurPanel := nil;
    CurControl := nil;
    case ConnectDialogOptions[i].Kind of
      okServer: begin
        pServer.Visible := ConnectDialogOptions[i].Visible;
        CurPanel := pServer;
        CurControl := edServer;
        lbServer.Text := ConnectDialogOptions[i].Caption;
      {$IFDEF VER17P}
        edServer.OnPopup := edServerPopup;
      {$ENDIF}
      end;
      okUserName: begin
        pUserName.Visible := ConnectDialogOptions[i].Visible;
        CurPanel := pUserName;
        CurControl := edUsername;
        lbUsername.Text := ConnectDialogOptions[i].Caption;
      end;
      okPassword: begin
        pPassword.Visible := ConnectDialogOptions[i].Visible;
        CurPanel := pPassword;
        CurControl := edPassword;
        lbPassword.Text := ConnectDialogOptions[i].Caption;
      end;
      okHome: begin
        pHome.Visible := ConnectDialogOptions[i].Visible;
        CurPanel := pHome;
        CurControl := cbHome;
        lbHome.Text := ConnectDialogOptions[i].Caption;
      {$IFDEF VER17P}
        cbHome.OnPopup := cbHomePopup;
      {$ENDIF}
      end;
      okSchema: begin
        pSchema.Visible := ConnectDialogOptions[i].Visible;
        CurPanel := pSchema;
        CurControl := edSchema;
        lbSchema.Text := ConnectDialogOptions[i].Caption;
      end;
      okDirect: begin
        pDirect.Visible := ConnectDialogOptions[i].Visible;
        CurPanel := pDirect;
        CurControl := cbDirect;
        lbDirect.Text := ConnectDialogOptions[i].Caption;
      end;
      okRole: begin
        pRole.Visible := ConnectDialogOptions[i].Visible;
        CurPanel := pRole;
        CurControl := cbRole;
        lbRole.Text := ConnectDialogOptions[i].Caption;
      end;
    else
      Assert(False);
    end;
    CurPanel.TabOrder := i;
    if CurPanel.Visible then begin
      CurPanel.Position.Y := CurTop;
      CurTop := CurTop + Trunc(CurPanel.Height) + 2;
    end;
    if NeedSetActiveControl and CurPanel.Visible then begin
      ActiveControl := CurControl;
      NeedSetActiveControl := False;
    end;
  end;
  pMain.Height := CurTop - 2;
  pButtons.Position.Y := pMain.Position.Y + pMain.Height + 8;
  ClientHeight := Trunc(pButtons.Position.Y + pButtons.Height) + 8;

  btConnect.Text := FConnectDialog.ConnectButton;
  btCancel.Text := FConnectDialog.CancelButton;

  edUsername.Text := FConnectDialog.Connection.Username;
  edPassword.Text := FConnectDialog.Connection.Password;
  edServer.Text := FConnectDialog.Connection.Server;

  if FConnectDialog.Connection is TOraSession then begin
    cbRole.ItemIndex := Integer(TOraSession(FConnectDialog.Connection).ConnectMode);
    edSchema.Text := TOraSession(FConnectDialog.Connection).Schema;
    if TOraSession(FConnectDialog.Connection).HomeName <> '' then
    cbHome.Text := TOraSession(FConnectDialog.Connection).HomeName;
    cbDirect.IsChecked := TOraSession(FConnectDialog.Connection).Options.Direct;
    cbHome.Enabled := not TOraSession(FConnectDialog.Connection).Options.Direct
  end;

  if (edUsername.Text <> '') and (edPassword.Text = '') and pPassword.Visible then
    ActiveControl := edPassword;

  FInDoInit := False;
end;

procedure TConnectForm.DoConnect;
begin
  FConnectDialog.Connection.Disconnect;

  if pServer.Visible then
    FConnectDialog.Connection.Server := edServer.Text;
  if pUserName.Visible then
    FConnectDialog.Connection.UserName := edUsername.Text;
  if pPassword.Visible then
    FConnectDialog.Connection.Password := edPassword.Text;
  if pDirect.Visible then
    TOraSession(FConnectDialog.Connection).Options.Direct := cbDirect.IsChecked;
  if pHome.Visible then
    TOraSession(FConnectDialog.Connection).HomeName := cbHome.Text;
  if pRole.Visible then
    TOraSession(FConnectDialog.Connection).ConnectMode := TConnectMode(cbRole.ItemIndex);
  if pSchema.Visible then
    TOraSession(FConnectDialog.Connection).Schema := edSchema.Text;

  try
    FConnectDialog.Connection.PerformConnect(FRetry);
    ModalResult := mrOk;
  except
    on E: EDAError do begin
      Dec(FRetries);
      FRetry := True;
      if FRetries = 0 then
        ModalResult:= mrCancel;

      case E.ErrorCode of
        1005: ActiveControl := edPassword;
        1017: if ActiveControl <> edUsername then ActiveControl:= edPassword;
        12203,12154: ActiveControl := edServer;
      end;
      raise;
    end
    else
      raise;
  end;
end;

procedure TConnectForm.SetConnectDialog(Value: TCustomConnectDialog);
begin
  FConnectDialog := Value;
  DoInit;
end;

procedure TConnectForm.btConnectClick(Sender: TObject);
begin
  DoConnect;
end;

procedure TConnectForm.AssignHomeName(const Value: string);
begin
  if TOraSession(FConnectDialog.Connection).HomeName <> Value then begin
    TOraSession(FConnectDialog.Connection).Disconnect;
    TOraSession(FConnectDialog.Connection).HomeName := Value;
  end;
end;

function TConnectForm.GetHomeName: string;
begin
  if cbHome.Text = cbHome.Items[0] then
    Result := ''
  else
    Result := cbHome.Text;
end;

procedure TConnectForm.cbDirectChange(Sender: TObject);
begin
  if FInDoInit then
    Exit;
  AssignDirect(cbDirect.IsChecked);
  lbHome.Enabled := not cbDirect.IsChecked;
  cbHome.Enabled := not cbDirect.IsChecked;
end;

procedure TConnectForm.cbHomeChange(Sender: TObject);
begin
  if FInDoInit then
    Exit;
  AssignHomeName(GetHomeName);
  FConnectDialog.UseServerHistory := False;
  FillServerList;
end;

procedure TConnectForm.FillServerList;
var
  List: TStringList;
begin
  List := TStringList.Create;
  try
    FConnectDialog.GetServerList(List);
    AssignStrings(List, edServer.Items);
    if edServer.Items.Count >= 0 then
      edServer.ItemIndex := 0;
  finally
    List.Free;
  end;
end;

procedure TConnectForm.AssignDirect(const Value: boolean);
begin
  if TOraSession(FConnectDialog.Connection).Options.Direct <> Value then begin
    TOraSession(FConnectDialog.Connection).Disconnect;
    TOraSession(FConnectDialog.Connection).Options.Direct := Value;
  end;
end;

procedure TConnectForm.edServerPopUp(Sender: TObject);
begin
  FillServerList;
end;

procedure TConnectForm.FillHomeName;
var
  i: integer;
begin
  cbHome.Items.Clear;
  cbHome.Items.Add('Default');
  if (OracleHomes.Default <> nil) and (OracleHomes.Default.Name <> '') then
    cbHome.Items[0] := cbHome.Items[0] + ' [' + OracleHomes.Default.Name + ']';
  for i := 0 to OracleHomes.Count - 1 do
    cbHome.Items.Add(OracleHomes[i].Name);
  if cbHome.Items.Count > 0 then
    cbHome.ItemIndex := 0;
end;

procedure TConnectForm.cbHomePopUp(Sender: TObject);
begin
  if FInDoInit then
    Exit;
  FillHomeName;
end;

initialization
//  if GetClass('TConnectForm') = nil then
//    Classes.RegisterClass(TConnectForm);

end.

