
//////////////////////////////////////////////////
//  Oracle Data Access Components
//  Copyright � 1998-2018 Devart. All right reserved.
//  Connect Form
//////////////////////////////////////////////////

unit ConnectForm;

interface

{$IFDEF WIN32}
  {$DEFINE MSWINDOWS}
{$ENDIF}
{$IFDEF WIN64}
  {$DEFINE MSWINDOWS}
{$ENDIF}

uses
{$IFDEF MSWINDOWS}
  Windows, Messages,
{$ENDIF}
  SysUtils, Classes, Graphics, Controls, Forms, Dialogs, StdCtrls, ExtCtrls,
{$IFDEF FPC}
  LResources,
{$ENDIF}
  CRtypes, DBAccess;

type
  TConnectForm = class(TForm)
    Panel: TPanel;
    pUserName: TPanel;
    lbUsername: TLabel;
    edUsername: TEdit;
    pPassword: TPanel;
    lbPassword: TLabel;
    edPassword: TEdit;
    pServer: TPanel;
    lbServer: TLabel;
    edServer: TComboBox;
    pHome: TPanel;
    lbHome: TLabel;
    pRole: TPanel;
    lbRole: TLabel;
    cbRole: TComboBox;
    pButtons: TPanel;
    btCancel: TButton;
    btConnect: TButton;
    pDirect: TPanel;
    lbDirect: TLabel;
    cbDirect: TCheckBox;
    cbHome: TComboBox;
    pSchema: TPanel;
    lbSchema: TLabel;
    edSchema: TEdit;
    procedure btConnectClick(Sender: TObject);
    procedure cbHomeChange(Sender: TObject);
    procedure cbDirectClick(Sender: TObject);
    procedure edServerDropDown(Sender: TObject);
    procedure cbHomeDropDown(Sender: TObject);

  private
    FConnectDialog: TCustomConnectDialog;
    FRetries: integer;
    FRetry: boolean;
    FInDoInit: Boolean;

    procedure SetConnectDialog(Value: TCustomConnectDialog);
    procedure AssignHomeName(const Value: string);
    procedure AssignDirect(const Value: boolean);
    function GetHomeName: string;
    procedure FillServerList;
    procedure FillHomeName;
  protected
    procedure DoInit; virtual;
    procedure DoConnect; virtual;

  public

  published
    property ConnectDialog: TCustomConnectDialog read FConnectDialog write SetConnectDialog;
  end;

implementation

{$IFNDEF FPC}
{$IFDEF IDE}
{$R *.dfm}
{$ELSE}
{$R ConnectForm.dfm}
{$ENDIF}
{$ELSE}
{$R *.lfm}
{$ENDIF}

uses
  CRFunctions, OraClasses, TypInfo, Ora, OraCall;
  
procedure TConnectForm.DoInit;
var
  CurTop, i: integer;
  ConnectDialogOptions: TConnectDialogOptionArray;
  CurPanel: TPanel;
  CurControl: TWinControl;
  NeedSetActiveControl: Boolean;
begin
  FInDoInit := true;
  FRetry := False;
  FRetries := FConnectDialog.Retries;
  Caption := FConnectDialog.Caption;

  FillHomeName;
  FillServerList;

  for i := Integer(Low(TConnectMode)) to Integer(High(TConnectMode)) do
    cbRole.Items.Add(GetEnumName(TypeInfo(TConnectMode), i));

  FConnectDialog.GetOptions(ConnectDialogOptions);
  CurTop := 0;
  NeedSetActiveControl := True;
  for i := Low(ConnectDialogOptions) to High(ConnectDialogOptions) do begin
    CurPanel := nil;
    CurControl := nil;
    case ConnectDialogOptions[i].Kind of
      okServer: begin
        pServer.Visible := ConnectDialogOptions[i].Visible;
        CurPanel := pServer;
        CurControl := edServer;
        lbServer.Caption := ConnectDialogOptions[i].Caption;
      end;
      okUserName: begin
        pUserName.Visible := ConnectDialogOptions[i].Visible;
        CurPanel := pUserName;
        CurControl := edUsername;
        lbUsername.Caption := ConnectDialogOptions[i].Caption;
      end;
      okPassword: begin
        pPassword.Visible := ConnectDialogOptions[i].Visible;
        CurPanel := pPassword;
        CurControl := edPassword;
        lbPassword.Caption := ConnectDialogOptions[i].Caption;
      end;
      okHome: begin
        pHome.Visible := ConnectDialogOptions[i].Visible;
        CurPanel := pHome;
        CurControl := cbHome;
        lbHome.Caption := ConnectDialogOptions[i].Caption;
      end;
      okSchema: begin
        pSchema.Visible := ConnectDialogOptions[i].Visible;
        CurPanel := pSchema;
        CurControl := edSchema;
        lbSchema.Caption := ConnectDialogOptions[i].Caption;
      end;
      okDirect: begin
        pDirect.Visible := ConnectDialogOptions[i].Visible;
        CurPanel := pDirect;
        CurControl := cbDirect;
        lbDirect.Caption := ConnectDialogOptions[i].Caption;
      end;
      okRole: begin
        pRole.Visible := ConnectDialogOptions[i].Visible;
        CurPanel := pRole;
        CurControl := cbRole;
        lbRole.Caption := ConnectDialogOptions[i].Caption;
      end;
    else
      Assert(False);
    end;
    CurPanel.TabOrder := i;
    if CurPanel.Visible then begin
      if i = 0 then
        CurPanel.Top := CurTop + 2
      else
        CurPanel.Top := CurTop;
      CurTop := CurTop + CurPanel.Height;
    end;
    if NeedSetActiveControl and CurPanel.Visible then begin
      ActiveControl := CurControl;
      NeedSetActiveControl := False;
    end;
  end;
  Panel.Height := CurTop + 2;
  pButtons.Top := Panel.Height + 8;
  ClientHeight := pButtons.Top + pButtons.Height + 2;

  btConnect.Caption := FConnectDialog.ConnectButton;
  btCancel.Caption := FConnectDialog.CancelButton;

  edUsername.Text := FConnectDialog.Connection.Username;
  edPassword.Text := FConnectDialog.Connection.Password;
  edServer.Text := FConnectDialog.Connection.Server;

  if (edUsername.Text <> '') and (edPassword.Text = '') and pPassword.Visible then
    ActiveControl := edPassword;

  if FConnectDialog.Connection is TOraSession then begin
    cbRole.ItemIndex := Integer(TOraSession(FConnectDialog.Connection).ConnectMode);
    edSchema.Text := TOraSession(FConnectDialog.Connection).Schema;
    if TOraSession(FConnectDialog.Connection).HomeName <> '' then
    cbHome.Text := TOraSession(FConnectDialog.Connection).HomeName;
    cbDirect.Checked := TOraSession(FConnectDialog.Connection).Options.Direct;
    cbHome.Enabled := not TOraSession(FConnectDialog.Connection).Options.Direct
  end;
  FInDoInit := False;
end;

procedure TConnectForm.DoConnect;
begin
  FConnectDialog.Connection.Disconnect;

  if pServer.Visible then
    FConnectDialog.Connection.Server := edServer.Text;
  if pUserName.Visible then
    FConnectDialog.Connection.UserName := edUsername.Text;
  if pPassword.Visible then
    FConnectDialog.Connection.Password := edPassword.Text;
  if pDirect.Visible then
    TOraSession(FConnectDialog.Connection).Options.Direct := cbDirect.Checked;
  if pHome.Visible then begin
    if cbDirect.Checked then
      TOraSession(FConnectDialog.Connection).HomeName := ''
    else if OracleHomes.Count = 0 then
      TOraSession(FConnectDialog.Connection).HomeName := cbHome.Text
    else if OracleHomes.Default <> nil then
      TOraSession(FConnectDialog.Connection).HomeName := OracleHomes.Default.Name
    else if cbHome.Items.Count > OracleHomes.Count then // if default home is present in list
      TOraSession(FConnectDialog.Connection).HomeName := OracleHomes.Homes[cbHome.ItemIndex - 1].Name
    else
      TOraSession(FConnectDialog.Connection).HomeName := OracleHomes.Homes[cbHome.ItemIndex].Name
  end;
  if pRole.Visible then
    TOraSession(FConnectDialog.Connection).ConnectMode := TConnectMode(cbRole.ItemIndex);
  if pSchema.Visible then
    TOraSession(FConnectDialog.Connection).Schema := edSchema.Text;

  try
    FConnectDialog.Connection.PerformConnect(FRetry);
    ModalResult := mrOk;
  except
    on E: EDAError do begin
      Dec(FRetries);
      FRetry := True;
      if FRetries = 0 then
        ModalResult:= mrCancel;

      case E.ErrorCode of
        1005: ActiveControl := edPassword;
        1017: if ActiveControl <> edUsername then ActiveControl:= edPassword;
        12203,12154: ActiveControl := edServer;
      end;
      raise;
    end
    else
      raise;
  end;
end;

procedure TConnectForm.SetConnectDialog(Value: TCustomConnectDialog);
begin
  FConnectDialog := Value;
  DoInit;
end;

procedure TConnectForm.btConnectClick(Sender: TObject);
begin
  DoConnect;
end;

procedure TConnectForm.AssignHomeName(const Value: string);
begin
  if TOraSession(FConnectDialog.Connection).HomeName <> Value then begin
    TOraSession(FConnectDialog.Connection).Disconnect;
    TOraSession(FConnectDialog.Connection).HomeName := Value;
  end;
end;

function TConnectForm.GetHomeName: string;
begin
  if cbHome.Text = cbHome.Items[0] then
    Result := ''
  else
    Result := cbHome.Text;
end;

procedure TConnectForm.cbHomeChange(Sender: TObject);
begin
  if FInDoInit then
    Exit;
  AssignHomeName(GetHomeName);
  FConnectDialog.UseServerHistory := False;
  FillServerList;
end;

procedure TConnectForm.FillServerList;
var
  List: TStringList;
begin
  List := TStringList.Create;
  try
    FConnectDialog.GetServerList(List);
    AssignStrings(List, edServer.Items);
    if edServer.Items.Count >= 0 then
      edServer.ItemIndex := 0;
  finally
    List.Free;
  end;
end;

procedure TConnectForm.cbDirectClick(Sender: TObject);
begin
  if FInDoInit then
    Exit;
  AssignDirect(cbDirect.Checked);
  lbHome.Enabled := not cbDirect.Checked;
  cbHome.Enabled := not cbDirect.Checked;
end;

procedure TConnectForm.AssignDirect(const Value: boolean);
begin
  if TOraSession(FConnectDialog.Connection).Options.Direct <> Value then begin
    TOraSession(FConnectDialog.Connection).Disconnect;
    TOraSession(FConnectDialog.Connection).Options.Direct := Value;
  end;
end;

procedure TConnectForm.edServerDropDown(Sender: TObject);
begin
  FillServerList;
end;

procedure TConnectForm.FillHomeName;
var
  i: integer;
begin
  cbHome.Items.Clear;
  if OracleHomes.Count = 0 then
    cbHome.Items.Add('< Home not found >')
  else begin
    if OracleHomes.Default <> nil  then
      cbHome.Items.Add('Default [' + OracleHomes.Default.Name + ']');
    for i := 0 to OracleHomes.Count - 1 do
      cbHome.Items.Add(OracleHomes[i].Name);
  end;
  cbHome.ItemIndex := 0;
end;

procedure TConnectForm.cbHomeDropDown(Sender: TObject);
begin
  if FInDoInit then
    Exit;
  FillHomeName;
end;

initialization
//  if GetClass('TConnectForm') = nil then
//    Classes.RegisterClass(TConnectForm);

end.
