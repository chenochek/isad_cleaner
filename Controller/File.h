// ---------------------------------------------------------------------------

#ifndef FileH
#define FileH

#include<Library.h>

#endif

using namespace std;

// ******************************************************************************
///<summary>
///   ����� ��� ����������� ����������
///</summary>
class TMyLog {
private:
	// ������ ������� ����
	TStringList* FListLog;
	// ��� �����
	String filename;

public:
	///<summary>
	///����������� ����, ��� ����������
	///</summary>
	TMyLog();
	///<summary>
	///����������� ����, �� ���� �������� ��� ����� ����
	///</summary>
	TMyLog(String filename);
	///<summary>
	///���������� ����
	///</summary>
	~TMyLog();
	///<summary>
	///�����, ����������� ������ �� ���������� FListLOg � ���� � ������ filename
	///</summary>
	void flush();
	///<summary>
	///�����, ����������� ������ � FListLOg
	///</summary>
	void addstr(String str);
};

// ******************************************************************************
///<summary>
///   �����, ���������� � �������� �������� ��� �������� ������
///</summary>
class TFile {
private:
	// ���, ���������� � �������� ����� ���� �����. � �������� ��������  - ���
	map<String, String>masFileNames;
	// ������ ���������� ������, ������� ���������� ������� �� ����
	list<String>masRemovedFileId;
	// �����, � ������� ���� ������� �����
	String nameDirectory;
	// ��������� ������ ��� �����������
	TMyLog * logger;
	// ��������� �� ������
	String errorMessage;
	// �������� ���������
	String successMessage;

	///<summary>
	///����� ���������� ������ � ���
	///</summary>
	void writeLogs(String str);
	///<summary>
	///����� ��������� ���� � �����
	///</summary>
	void saveLogs();

public:
	///<summary>
	///�����������, ����������� ���� � ���� � ������ �����, ������������ ����������, �� ������� �������
	///</summary>
	TFile(map<String, String>, String nameDirectory);
	///<summary>
	///���������� ������
	///</summary>
	~TFile();
	///<summary>
	///����� ��� �������� ������ �� ����������
	///</summary>
	list<String>deleteFiles();
};
