// ---------------------------------------------------------------------------

#pragma hdrstop

#include "SettingsReader.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)

/*******************************************************************************
 * � ������������ �� ����� � ���-����� � �����������  �������� � ���������������� ��� ���������
 *
 *
 */
SettingsReader::SettingsReader() {
	(*this).iniFileLocation = ExtractFileDir(Application->ExeName) +
		"\\settings.ini";
	(*this).SettingsFile = new TIniFile(this->iniFileLocation);

	(*this).oraclehome = SettingsFile->ReadString("db", "oraclehome", "");
	(*this).oraclehomename = SettingsFile->ReadString("db",
		"oraclehomename", "");
	(*this).server = SettingsFile->ReadString("db", "server", "");
	(*this).username = SettingsFile->ReadString("db", "login", "");
	(*this).password = SettingsFile->ReadString("db", "password", "");
	(*this).port = SettingsFile->ReadString("db", "port", "");
	(*this).nameprocget = SettingsFile->ReadString("db", "procedure_get", "");
	(*this).nameprocaccept = SettingsFile->ReadString("db",
		"procedure_confirm_delete", "");
	(*this).sid = SettingsFile->ReadString("db", "sid", "");
	(*this).filedirectory = SettingsFile->ReadString("folder", "path", "");
	AnsiString serv_ansi((*this).server);
	char * servChar = new char[serv_ansi.Length() + 1];
	strcpy(servChar, serv_ansi.c_str());
	cout<< "Server: " << servChar << endl;
	AnsiString user_ansi((*this).username);
	char * userChar = new char[user_ansi.Length() + 1];
	strcpy(userChar, user_ansi.c_str());
	cout<< "Username: " << userChar << endl;
	AnsiString pass_ansi((*this).password);
	char * passChar = new char[pass_ansi.Length() + 1];
	strcpy(passChar, pass_ansi.c_str());
	cout<< "Password: " << passChar << endl;
	cout<<"Read settings" << endl;

}

/*******************************************************************************
 *
 *  ���������� ������
 *
 */
SettingsReader::~SettingsReader() {
	delete this->SettingsFile;
}

/*******************************************************************************
 *
 *   ��������� ����� ������� ���� ������
 *
 */
String SettingsReader::getServer() {
	return this->server;
}

/*******************************************************************************
 *
 *   ��������� ������������ ��
 *
 */
String SettingsReader::getUsername() {
	return this->username;
}

/*******************************************************************************
 *
 *   ��������� ������ �� ��
 *
 */
String SettingsReader::getPassword() {
	return this->password;
}

/*******************************************************************************
 *
 * ��������� ����� ���������, ������� ���������� ����� ������ ��� ��������
 *
 */
String SettingsReader::getNameprocget() {
	return this->nameprocget;
}

/*******************************************************************************
 *
 *     ��������� ����� ���������, ������� ���������� �� ���� ������� ��������� ������
 *
 */
String SettingsReader::getNameprocaccept() {
	return this->nameprocaccept;
}

/*******************************************************************************
 *
 *      ��������� ����� ��� ����������� � ��
 *
 */
String SettingsReader::getPort() {
	return this->port;
}

/*******************************************************************************
 *
 *   ��������� ����������, � ������� ���������� ���������� �������
 *
 */
String SettingsReader::getFiledirectory() {
	return this->filedirectory;
}

/*******************************************************************************
 *
 *       ��������� ��� ��
 *
 */
String SettingsReader::getSid() {
	return this->sid;
}

/*******************************************************************************
 *
 *   ��������� ������������ ����� ������
 *
 */
String SettingsReader::getOraclehome() {
	return this->oraclehome;
}

/*******************************************************************************
 *
 *   ��������� ����� ����� �������
 *
 */
String SettingsReader::getOraclehomename() {
	return this->oraclehomename;
}
