// ---------------------------------------------------------------------------

#ifndef DBH
#define DBH

#include<Library.h>
// ---------------------------------------------------------------------------
#endif

///<summary>
///����� ��� �������������� � ��
///</summary>
class TDBConnector {
private:
	// ������������ �����-�������
	String oraclehome;
	// ��� �����-�������
	String oraclehomename;
	// ��� ������� ��
	String server;
	// ����� ������������ ��
	String username;
	// ������ ������������ ��
	String password;
	// ��� ������� �� ��� ��������� ������ ��� ��������
	String nameprocget;
	// ��� ��������� �� ��� ������������� ��������� ������
	String nameprocaccept;
	// ���� ��
	String port;
	// ��������� ������ ��� ������ � ��
	TOraSession * tOraSession;
	// ��������� ���������
	TOraStoredProc *tOraStoredProc;
	// ��� ������� ���������� ���������
	String currentProc;

	///<summary>
	///����� ������ ������� ��������� ��� ������ � �����
	///</summary>
	void setCurrentProc(String nameProc);

public:
	///<summary>
	///�������� ��������� �� ��� ���������� ���������� �� �������� ������
	///</summary>
	void updateInfoAboutRemovedFiles(list<String>removedIds);
	///<summary>
	///�������� ��������� �� ��� ��������� ���� ������ ��� ��������
	///</summary>
	map<String, String>getNamesForRemove();
	///<summary>
	///����������� ������, ���� ���������� �������� ������ ���������
	///</summary>
	TDBConnector(String server, String username, String password,
		String nameprocget, String nameprocaccept, String port,
		String oraclehome, String oraclehomename);
	///<summary>
	///���������� ������
	///</summary>
	~TDBConnector();

};
// ---------------------------------------------------------------------------
