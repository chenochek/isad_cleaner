#pragma hdrstop

#include "File.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)

/*******************************************************************************
 *
 *  ����������� ������. �� ���� ���������� ���� � �������� ����� � ������,
 *  �����, � ������� �������. ���������������� ���������
 *
 */

TFile::TFile(map<String, String>masFileNames, String nameDirectory)
	: masFileNames(masFileNames), nameDirectory(nameDirectory) {
	(*this).logger = new TMyLog();
	errorMessage = "Error with removing. See details.";
	successMessage = "File has deleted successful";
}

/*******************************************************************************
 *
 *  ���������� ������
 *
 */

TFile::~TFile() {
	delete(*this).logger;
}

/*******************************************************************************
 *
 *  ����� ��� �������� ������. �������� �� ����, ������ ��� ����� ���������������
 *  �� ���� UnicodeString � char *. ������� ����, ���������� , � �����������
 *  �� ���������� ��������� �� ������ ��� ������. �������� ��� � ���.
 *
 */

list<String>TFile::deleteFiles() {
	list<String>listIds;
	for (auto file : masFileNames) {
		String nameFile = (*this).nameDirectory + file.second; ;
		String id = file.first;
		AnsiString str(nameFile);
		char * name = new char[str.Length() + 1];
		strcpy(name, str.c_str());
		int res = remove(name);
		cout << "Removed file" << name << endl;
		if (res == 0) {
			(*this).writeLogs(this->successMessage + nameFile);
			listIds.push_back(id);
		}
		else {
			(*this).writeLogs((*this).errorMessage + nameFile);
			(*this).writeLogs(std::strerror(errno));
		}
	}
	this->saveLogs();
	return listIds;

}

/*******************************************************************************
 *
 *  ����� ��� ���������� ������ � ������.
 *
 */

void TFile::writeLogs(String str) {
	(*this).logger->addstr(str);
}

/*******************************************************************************
 *
 *  ����� ��� ������ ���� � ����
 *
 */

void TFile::saveLogs() {
	(*this).logger->flush();
	cout << "Flushed logs" << endl;
}

/*******************************************************************************
 *
 *  ����������� ��� ���������� ������ �������.
 *
 */

TMyLog::TMyLog() {
	(*this).FListLog = new TStringList;
	(*this).filename = FormatDateTime("dd-mm-yyyy_hh_mm_ss", IncHour(Now(), 0))
		+ "daily_log.txt";
}

/*******************************************************************************
 *
 *  ����������� ������ �������, ��� �� ���� ���������� ��� ����������, � �������
 *  �������
 *
 */

TMyLog::TMyLog(String filename) : filename(filename) {
	FListLog = new TStringList;
}

/*******************************************************************************
 *
 *  ���������� ������ �������
 *
 */

TMyLog::~TMyLog() {
	delete(*this).FListLog;
}

/*******************************************************************************
 *
 *  �����, ����������� ��� � ����
 *
 */

void TMyLog::flush() {
	(*this).FListLog->SaveToFile((*this).filename.c_str());
	cout << "Saved logs" << endl;
}

/*******************************************************************************
 *
 *  �����, ����������� ������ � ���
 *
 */

void TMyLog::addstr(String str) {
	(*this).FListLog->Add(str.c_str());
}
