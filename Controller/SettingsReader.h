// ---------------------------------------------------------------------------

#ifndef SettingsReaderH
#define SettingsReaderH
#include<Library.h>
// ---------------------------------------------------------------------------
#endif

///<summary>
///  ����� ������ ���   ������ �������� �� ini-�����
///</summary>
class SettingsReader {
private:
	// ���� ������ ��� ��������  ������������ � ����� �����
	String iniFileLocation;
	// ���� ������ ��� �������� ������������ �����
	TIniFile* SettingsFile;
	// ���� ������ ��� �������� ������������ ����� ������
	String oraclehome;
	// ���� ������ ��� �������� �������� �����-������
	String oraclehomename;
	// ���� ������ ��� �������� ����� ������� ���� ������
	String server;
	// ���� ������ ��� �������� ������ ������������ ����
	String username;
	// ���� ������ ���  �������� ������ �� ����
	String password;
	// ���� ������ ���  �������� �������� ��������� ��� ��������� ������ ��� ��������
	String nameprocget;
	// ���� ������ ��� �������� �������� ��������� ��� ������������� ��������
	String nameprocaccept;
	// ���� ������ ��� �������� ����� ��� ����������� � ��
	String port;
	// ���� ������ ��� �������� ��� ���� ������
	String sid;
	// ���� ������ ��� �������� ������������ ��������
	String filedirectory;

public:
	///<summary>
	///����������� ��� ��������
	///</summary>
	SettingsReader();
	///<summary>
	///���������� ��� ��������
	///</summary>
	~SettingsReader();
	///<summary>
	///���������� ��� �������
	///</summary>
	String getServer();
	///<summary>
	/// ���������� ����� ������������
	///</summary>
	String getUsername();
	///<summary>
	///���������� ������
	///</summary>
	String getPassword();
	///<summary>
	/// ���������� ��� ��������� ��� ��������� ������ ��� ��������
	///</summary>
	String getNameprocget();
	///<summary>
	/// ���������� ��� ��������� ��� ������������� ��������
	///</summary>
	String getNameprocaccept();
	///<summary>
	///���������� ����
	///</summary>
	String getPort();
	///<summary>
	///���������� ������������ ��������
	///</summary>
	String getFiledirectory();
	///<summary>
	///���������� ��� ����
	///</summary>
	String getSid();
	///<summary>
	///���������� ������������ �����-�������
	///</summary>
	String getOraclehome();
	///<summary>
	///���������� ��� �����-�������
	///</summary>
	String getOraclehomename();
};
