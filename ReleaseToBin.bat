@Echo OFF
REM Written path to file which you want copy here...
del /S bin\ISAD_Cleaner.exe
cd tools
upx.exe ..\Win32\Debug\ISAD_Cleaner.exe
upx.exe ..\Win64\Debug\ISAD_Cleaner.exe
upx.exe ..\Win32\Release\ISAD_Cleaner.exe
upx.exe ..\Win64\Release\ISAD_Cleaner.exe
cd ../
REM copy /v /y Win32\Debug\ISAD_Cleaner.exe /b /v /y bin\ISAD_Cleaner.exe /b
copy /v /y Win32\Release\ISAD_Cleaner.exe /b /v /y bin\ISAD_Cleaner.exe /b
REM copy /v /y Win64\Debug\ISAD_Cleaner.exe /b /v /y bin\ISAD_Cleaner.exe /b
copy /v /y Win64\Release\ISAD_Cleaner.exe /b /v /y bin\ISAD_Cleaner.exe /b
pause
